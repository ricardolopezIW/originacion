	<script src="${pageContext.request.contextPath}/resources/js/hiperion/duplicidad/duplicidadReglas.js"  type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resources/js/hiperion/duplicidad/reglasForm.js"  type="text/javascript"></script>
	            	
     <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Configuraci�n Duplicidad</h1>
                    <input id="hiddenOpenMenu" type="hidden" value="#{orDuplicidadController.bean.openMenu}"/>
                    <input id="urlRestBackOffice" type="hidden" value="#{orGeneracionAceController.bean.urlRest}"/>                    
                </section>


				<section class="content">
						<div class="panel panel-default">
							<div id="headPanel" class="panel-heading">
								<label class="control-label">Reglas Existentes </label>
							</div>
							<div class="panel-body">
							    
								<div class="row col-md-12 form-horizontal">
									<form class="form-horizontal" role="form" id="formBuscar" data-parsley-validate="">
									
									<div class="form-group">
						                    <label  class="col-sm-1 control-label" for="inputEmail3">Producto:</label>
						                    <div class="col-sm-8">
						                        <select class="form-control" id="productoSelMod" required=""></select>
						                    </div>
					                </div>
				                    <div class="form-group">
						                    <label class="col-sm-1 control-label" for="inputPassword3" >Flujo:</label>
						                    <div class="col-sm-8">
						                        <select class="form-control" id="flujoSelMod" required=""></select>
						                    </div>
				                    </div>
				                    <div class="form-group">
						                  	<label class="col-sm-1 control-label" >Estatus: </label>
											<div class="col-sm-8">
									        	<select id="cmbEstatus" class="form-control" required=""> 
									           		<option value="">Seleccione</option>
									           		<option value="0">Inactiva</option>
									           		<option value="1">Activa</option>
									           	</select>
											</div>
								    </div>
								    </form>				                
								</div>
									
								<div class="form-group">				                  		
					                    <div class="col-sm-offset-2 col-sm-4">
					                      <button type="button" class="btn btn-primary" id="btnBuscar">Buscar</button>	
					                    </div>	
					                    <div class="col-sm-offset-2 col-sm-3">
					                      <button type="button" class="btn btn-primary" id="btnClonar">Clonar</button>	
					                    </div>
								</div>
								
								<div class="row">
									<div class="col-lg-12" id="divTablaReglas"></div>
								</div>
							</div>
						</div>
				</section>
				
				
				<section class="content">
					<div class="panel panel-default">
							<div id="headPanel" class="panel-heading">
								<label class="control-label">Formulario de la regla</label>
							</div>
							<div class="panel-body">

								<div id="alertaDiv" class="bs-callout bs-callout-warning hidden">
									<h4>Advertencia!</h4>
									<p>Verificar los registros del formulario.</p>
								</div>

								<form role="form" data-parsley-validate="" id="formaEditar">
									<input id="idRegla" type="hidden" />
									
									<div class="form-group col-xs-1">
										 <label for="nombreRegla">Nombre Regla</label>
									</div>			
															
									<div class="form-group col-xs-4">									  
									  <input type="text" class="form-control" id="nombreRegla" placeholder="Ingresa el nombre de la regla." name="nombreRegla" required=""/>
									</div>	
									
									<div class="form-group col-xs-1">
									  <label for="accionSel">Acci�n</label>
									</div>
									
									<div class="form-group col-xs-4">									  
									  <select class="form-control" id="accionSel" required="">
									    <option value="">Seleccione</option>
										<option value="1">Seguir Flujo</option>
										<option value="2">Rechazar solicitud</option>
										<option value="3">Mandar a fila de revisi�n</option>				
									  </select>
									</div>										
									
									<div class="clearfix"></div>
									<br/><br/>
                					<div class="form-group col-xs-1">
									  <label for="nombreRegla">Producto</label>
									</div>			
															
									<div class="form-group col-xs-4">									  
									  <select class="form-control" id="productoSel" required=""> 	</select>																					  
									</div>	
									
									<div class="form-group col-xs-1">
									  <label for="nombreRegla">Flujo</label>
									</div>			
															
									<div class="form-group col-xs-4">									  
									  <select class="form-control" id="flujoSel" required=""> 	</select>	
									</div>

									<div class="clearfix"></div>
									
									<br/><br/>                					
									<div class="form-group col-xs-1" id="divLbActiva" style="display:none">
									  <label for="nombreRegla">Estado</label>
									</div>
									
									<div class="form-group col-xs-4"  id="divActiva" style="display:none">									  
									  <select class="form-control" id="estado">
									  	<option value="1">Activa</option>
									  	<option value="0">Inactiva</option>
									  </select>	
									</div>
									
								    <br/><br/><br/>
					   				<div class="form-group col-xs-6">
									  	   <label>Crear Nueva Regla</label>
								    </div>	
					    			
					    			<br/>
					    		    <div class="form-group col-xs-4 col-md-offset-8">				  
										  <button type="button" class="btn btn-primary" id="addParam">Agregar Par�metro</button>
									</div>					
					      
									<div class="form-inline col-xs-10 " >
										  <br/>
										  <table class="table table-bordered table-condensed " id="reglasTable">
												<thead>
												  <tr>
													<th></th>
													<th>Par�metro</th>
													<th>Operador</th>
												  </tr>
												</thead>
												<tbody/>			  		  			
										  </table>
								    </div>
					   
								    <br/><br/><br/>
								    <div class="clearfix"></div>
								   	<div class="form-group col-xs-2">
										  <label for="email">Mensaje Coincidencia</label>
									</div>								
									<div class="form-group col-xs-7">				  
										 <input type="text" class="form-control" id="mensaje" placeholder="Ingresa el mensaje de coincidencia." required=""/>
									</div>						
									
									<br/><br/><br/>
									<div class="clearfix"></div>
									<div class="form-group col-xs-2">
										  <label for="email">Orden</label>
									</div>	
									<div class="form-group col-xs-4">				  
										 <input type="text" class="form-control" id="orden" placeholder="Ingresa la prioridad de la Regla." required="" data-parsley-type="number"/>
									</div>
																							
						   	  </form>
						   	  <div class="form-group col-xs-4 col-md-offset-9">				  
								  <button type="button" class="btn btn-primary" id="guardar">Guardar</button>
							  </div>
						   </div>
					 </div>
				</section>



				<!-- Modales -->
				<!-- Modal clonar -->
				<div id="modalClonar" class="modal fade" role="dialog">
				  <div class="modal-dialog">
			
					<!-- Modal content-->
					<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Clonar Reglas</h4>
						  </div>
						  												  						  																				 
						  <div class="modal-body">
						  	 
						  	 <form class="form-horizontal" role="form" id="formClonar" data-parsley-validate="">
						     	  <br/>
				                  <div class="form-group">
				                    <label  class="col-sm-2 control-label" for="inputEmail3">Producto:</label>
				                    <div class="col-sm-8">
				                        <select class="form-control" id="productoSelClo" required="">	</select>
				                    </div>
				                  </div>
				                  <div class="form-group">
				                    <label class="col-sm-2 control-label" for="inputPassword3" >Flujo:</label>
				                    <div class="col-sm-8">
				                        <select class="form-control" id="flujoSelClo" required=""> 	</select>
				                    </div>
				                  </div>				                  				                  
				             </form>	           						  	 						  	  						  	 						  	 						  	
						  </div>
						  
						  <div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btClonarReglas">Clonar</button>
						  </div>
					</div>
				  </div>
				</div>
				
				<!-- fIN MODALES -->
        
        </div><!-- ./wrapper -->