		<script src="${pageContext.request.contextPath}/resources/js/hiperion/restriccionCobertura/orCoberturaVenta.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Restricción de Cobertura de Venta</h1>		
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- div para seleccionar el tipo de cobertura -->
				<div class="row" style="margin-top: 20px">
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<label class="control-label">Elige un tipo de Cobertura: </label>
					</div>
					<div class="col-lg-2">
						<select id="dropdownCobertura" class="form-control"
							onChange="consultarCampoCobertura(this);">
							<option value="0" selected="selected">Categorías</option>
							<option value="ESTADOS">Estados</option>
							<option value="MUNICIPIOS">Municipios</option>
							<option value="CP">Código Postal</option>
						</select>
					</div>
				</div>
				<div class="row" style="margin-top: 30px">
				
				
				<!-- Div de la cobertura por Estado -->
				<div id="divEstadosCobertura">
						
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
						
							<div class="form-group col-xs-4">
								<label id="lblTemplate">Restricción de Cobertura:</label>
							</div>
							
							<div class="form-group col-xs-2">
								<input type='checkbox' class='form-control' name='checkEdoSi'>Si</input>
								<input type='checkbox' class='form-control' name='checkEdoNo'>No</input>
							</div>
							
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnBuscarEstado" onclick="buscarEstado();">Buscar</button>
					
							</div>
						
						</div>
						
					</div>
						
					<div class="row" style="margin-top:10px">
						<div class="col-lg-1"></div>
						<div class="col-lg-8">
						<!-- Tabla de la cobertura por Estado -->
							<div class="form-group" id="divTablaEstado">
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:0px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnGuardarEstado" onclick="insertaEstado();">Guardar</button>
							</div>
						</div>
					</div>
						
				</div>

				<!-- Div de la cobertura por Municipio -->
				<div id="divMunicipiosCobertura">
				
					<DIV class="row" style="margin-top: 20px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
							<div class="form-group col-xs-2">
								<label>Estado:</label>
							</div>
								
							<div class="form-group col-xs-4">									  
								<select class="form-control" id="comboEstados" onChange="obtenerMunicipio();">
									<option value="0">Seleccione</option>				
								</select>
							</div>
						
						</div>
					</DIV>
						
					<div class="row" style="margin-top: 0px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">				
							<div class="form-group col-xs-4">
								<label id="lblTemplate">Restricción de Cobertura:</label>
							</div>
										
							<div class="form-group col-xs-2">
								<input type='checkbox' class='form-control' name='checkMunSi'>Si</input>
								<input type='checkbox' class='form-control' name='checkMunNo'>No</input>
							</div>
										
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnBuscarMunicipio" onclick="obtenerMunicipio();">Buscar</button>
							</div>
							
						</div>
					</div>
				
					<div class="row" style="margin-top:10px">
						<div class="col-lg-1"></div>
						<div class="col-lg-10">
							<!-- Tabla Municipio -->
							<div class="form-group" id="divTablaMunicipio"></div>
						</div>
					</div>
						
					<div class="row" style="margin-top:0px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnGuardarMunicipio" onclick="insertaMunicipio();">Guardar</button>
							</div>
						</div>
					</div>		
				</div>
							
				<!-- Div de la cobertura por Codigo Postal -->
	 			<div id="divCpCobertura">
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
							<div class="form-group col-xs-2">
								<label for="producto">Estado:</label>
							</div>
									
							<div class="form-group col-xs-4">									  
								<select class="form-control" id="comboEstadosCP" onChange="obtenerMunicipioCP();">
									<option value="0">Seleccione</option>				
								</select>
							</div>
						</div>
					</div>	
					
					<div class="row" style="margin-top: 10px">
						<div class="col-xs-1"></div>	
						<div class="col-lg-7">	
							<div class="form-group col-xs-2">
								<label id="lblTemplate">Municipio:</label>
							</div>
										
							<div class="form-group col-xs-4">									  
								<select class="form-control" id="comboMunicipios" onchange="consultarCP();">
									<option value="0">Seleccione</option>				
								</select>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top: 10px">
						<div class="col-xs-1"></div>	
						<div class="col-lg-7">		
							<div class="form-group col-xs-4">
								<label id="lblTemplate">Restricción de Cobertura:</label>
							</div>		
							<div class="form-group col-xs-2">
								<input type='checkbox' class='form-control' name='checkCPSi'>Si</input>
								<input type='checkbox' class='form-control' name='checkCPNo'>No</input>
							</div>
								
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnBuscarCP" onclick="consultarCP();">Buscar</button>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top: 10px">
						<div class="col-lg-1"></div>
						<div class="col-lg-8">
						<!-- Tabla de la cobertura por Codigo Postal -->
							<div class="form-group" id="divTablaCP">
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:0px">
						<div class="col-xs-1"></div>
						<div class="col-lg-7">
							<div class="form-group col-xs-2">
								<button type="button" class="btn btn-primary" id="btnGuardarCP" onclick="insertaCP();">Guardar</button>
							</div>
						</div>
					</div>
	
				</div>
			</div>

			<div class="modal fade" id="loadingModalAccesos" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<div class="form-group" style="text-align: center;">
								<label style="font-size: 18px;">Cargando ...</label>
								<div class="bar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>