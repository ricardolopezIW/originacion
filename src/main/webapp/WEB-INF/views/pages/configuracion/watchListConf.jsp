		<script src="${pageContext.request.contextPath}/resources/js/hiperion/watchlist/watchListReglas.js"  type="text/javascript"></script>
		<script>
		    var requestContextPath = '${pageContext.request.contextPath}';
		</script>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">

			<section class="content-header">
				<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Configuraci�n Reglas WatchList</h1>
			</section>

			<!-- Main content -->
			<section class="content">

				<div class="panel panel-default">
					<div id="headPanel" class="panel-heading">
						<label class="control-label">Reglas Existentes </label>
					</div>
					
					<div class="panel-body">
						<div class="row col-md-12">
							<div class="col-lg-2">
								<label class="control-label">Estatus: </label>
							</div>
							<div class="col-lg-4">
								<select id="cmbEstatus" class="form-control"
									onchange="verReglas()">
									<option value="-1">Seleccione</option>
									<option value="0">Inactiva</option>
									<option value="1">Activa</option>
								</select>
							</div>
						</div>
					
						<br>
						
						<div class="row">
							<div class="col-lg-12" id="divTablaReglas"></div>
						</div>
					</div>
				</div>
			</section>

			<section class="content">

				<div class="panel panel-default">
					<div id="headPanel" class="panel-heading">
						<label class="control-label">Formulario de la regla</label>
					</div>
					<div class="panel-body">



						<form class="form-inline col-xs-12" id="forma">

							<input id="idRegla" type="hidden" />

							<div class="div-inline col-xs-12">


								<div class="classActiva hide  div-group col-xs-1">
									<label for="nombreRegla">Activa</label>
								</div>

								<div id="idCheck" class="classActiva hide  col-xs-1"></div>

								<div class="form-group col-xs-1">
									<label for="nombreRegla">Nombre Regla</label>
								</div>

								<div class="form-group col-xs-4">
									<input type="text" class="form-control" id="nombreRegla"
										placeholder="Ingresa el nombre de la regla."
										name="nombreRegla" style="width: 300px;" />
								</div>

								<div class="form-group col-xs-1">
									<label for="accionSel">Acci�n</label>
								</div>

								<div class="form-group col-xs-4">
									<select class="form-control" id="accionSel">
										<option value="1">Seguir Flujo</option>
										<option value="2">Rechazar solicitud</option>
										<option value="3">Mandar a fila de revisi�n</option>
									</select>
								</div>
							</div>


							<div class="div-inline col-xs-12">

								<div class="form-group col-xs-3">
									<br /> <br /> <br />
									<button type="button" class="btn btn-primary" id="addParam">Agregar
										Par�metro</button>
								</div>


								<div class="div-inline col-xs-9 ">
									<br/><br/><br/>
									<table class="table table-bordered table-condensed"
										id="reglasTable">
										<thead>
											<tr>
												<th></th>
												<th>Par�metro</th>
												<th>Operador</th>
											</tr>
										</thead>
										<tbody />
									</table>

								</div>
							</div>


							<div class="div-inline col-xs-12">
								<br /> <br /> <br />
								<div class="div-inline col-xs-6">
									<label for="email">Mensaje Coincidencia</label> <input
										type="text" class="form-control" id="mensaje"
										placeholder="Ingresa el mensaje de coincidencia."
										style="width: 450px;" />
								</div>
								<div class="div-inline col-xs-3">
									<label for="email">Orden</label> <input type="text"
										class="form-control" id="orden"
										placeholder="Ingresa la prioridad de la Regla."
										style="width: 220px;" />
								</div>

								<div class="div-inline col-xs-1"></div>
							</div>
							
							<div class="div-inline col-xs-2">
								<br />
								<button type="button" class="btn btn-block btn-primary" id="guardar">Guardar</button>
							</div>

							<div class="div-inline col-xs-2">
								<br />
								<button type="button" class="btn btn-block btn-primary" id="limpiar">Limpiar</button>
							</div>
						</form>
					</div>
				</div>
			</section>
			<!-- /.content -->


			<!--modales-->
			<div id="modalReglasOLD" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Reglas Actuales</h4>
						</div>

						<div class="modal-body">

							<div class="row">
								<div class="col-lg-2">
									<label class="control-label">Estatus: </label>
								</div>
								<div class="col-lg-4">
									<select id="cmbEstatus" class="form-control" onchange="verReglas()">
										<option value="-1">Seleccione</option>
										<option value="0">Inactiva</option>
										<option value="1">Activa</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12" id="divTablaReglas"></div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btGuardarReglas">Guardar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		