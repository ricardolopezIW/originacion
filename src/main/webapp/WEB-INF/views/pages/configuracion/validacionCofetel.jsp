		<script src="${pageContext.request.contextPath}/resources/js/hiperion/cofetel/orValidacionCofetel.js" 	type="text/javascript"></script>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 style="padding-left: 10px; font-size: 25px;">Validaci�n Cofetel</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                	
                	<table id="dataTableReglas" class="table table-striped" style="text-align:center">
					  <thead>
					    <tr>
					      <th style="text-align:center">Tipo de Tel�fono			</th>
					      <th style="text-align:center">Validacion Tel�fono			</th>
					      <th style="text-align:center">Valida Tipo Red				</th>
					      <th style="text-align:center">Muestra Alerta de Captura	</th>
					      <th style="text-align:center">Valida Estado				</th>
					      <th style="text-align:center">Valida Municipio			</th>
					      <th style="text-align:center">Modificar Valores			</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr id="1">
					      <th scope="row">Casa titular</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"    onclick="guardarValidaCofetel($(this), 1);">Guardar</button></td>
					    </tr>
					    <tr id="2">
					      <th scope="row">Celular titular</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 2);">Guardar</button></td>
					    </tr>
					    <tr id="3">
					      <th scope="row">Contacto de Venta</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"    				   class="button"	 onclick="guardarValidaCofetel($(this), 3);">Guardar</button></td>
					    </tr>
					    <tr id="4">
					      <th scope="row">Trabajo</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 4);">Guardar</button></td>
					    </tr>
					    <tr id="5">
					      <th scope="row">Referencia Particular 1</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 5);">Guardar</button></td>
					    </tr>
					    <tr id="6">
					      <th scope="row">Referencia Particular 2</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 6);">Guardar</button></td>
					    </tr>
					    <tr id="7">
					      <th scope="row">Referencia Celular 1</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 7);">Guardar</button></td>
					    </tr>
					    <tr id="8">
					      <th scope="row">Referencia Celular 2</th>
					      <td><input  id="inputEstatusValidaTelefono"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusDetieneCaptura"  checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusMuestraAlerta"   checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaEstado"    checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><input  id="inputEstatusValidaMunicipio" checked="checked" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="primary" type="checkbox" onchange="swSwitch($(this));" valor=""/></td>
					      <td><button type="button"     			   class="button"	 onclick="guardarValidaCofetel($(this), 8);">Guardar</button></td>
					    </tr>
					  </tbody>
					</table>
                </section><!-- /.content -->
                
             
                
                
        </div><!-- ./wrapper -->