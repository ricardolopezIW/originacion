		<script src="${pageContext.request.contextPath}/resources/js/hiperion/producto/confProducto.js"  type="text/javascript"></script>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Administración Productos</h1>
                </section>
					
                <!-- Main content -->
                <section class="content">
                		
                		  <div class="container">			
							  <form class="form-inline col-xs-8" >
									<div class="form-group col-xs-4">
									  <label for="numIntento">Cargar Productos APM</label>
									</div>
									<div class="form-group col-xs-4">
									  <button type="button" class="btn btn-primary" onclick="cargarApmProductos()">Cargar </button>									  
									</div>																		 
							  </form>	 
						  </div>
													
						  <br/><br/>
					      <br/><br/>
						
						   <div class="container">		  
							  <form class="form-inline" >
									<div class="form-group">
										<label>Edición Productos</label>			
									</div>							
							  </form>	
							   
							  <br/>
							    
							  <form class="form-inline col-xs-12" >								
									<div class="form-group col-xs-4">
									  <label for="tokenCreado">Producto a Editar:</label>
									  <select id="productos" class="form-control">
									  	<option value="">Seleccionar</option>
									  </select>
									  <button type="button" class="btn btn-primary" id="btSeleccionar" onclick="cargarSeleccion()">Seleccionar</button>
									</div>	
							  </form>	 							  							  							  
						  </div>
						
					      <br/>
					      <br/>
																				
						  <div class="container" id="formaEdicion">
						    <input id="idProducto" type="hidden"/>
							<div class="row">
							  <form role="form" data-parsley-validate="" id="formaProducto">
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="exampleInputEmail1">Nombre Producto</label>
									  <input type="text" class="form-control input-sm" id="nombre" name="nombre" readOnly="true"/>
						        </div>
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="exampleInputEmail1">Nombre Comercial</label>
								    <input type="text" class="form-control input-sm" id="nombreComercial" placeholder="Ingresa nombre Comercial." name="nombreComercial" required=""/>
						        </div>
						        
						        <div class="clearfix"></div>
						        
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="exampleInputPassword1">TPC</label>
									<input type="text" class="form-control input-sm" id="tpc" placeholder="Ingresa TPC."  name="tpc" required=""/>
						        </div>
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="exampleInputPassword1">CPC</label>
									<input type="text" class="form-control input-sm" id="cpc" placeholder="Ingresa CPC."  name="cpc" required=""/>
						        </div>
						        
						        <div class="clearfix"></div>
						        
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="exampleInputPassword1">Leyenda Carátula</label>
									<textarea rows="2" cols="35" id="leyCaratula" style="resize:none" required=""></textarea>
						        </div>
						        
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						        	<p>La captura de la leyenda, debe especificar a través de un "." la sentencia superior e inferior.</p>
						        	<p>p.ej. "Solicítalo a Línea CIBanco. 4000 4040 o 01 55 4000 4040"</p>						      
						        </div>
						 						 						         
						        <div class="clearfix"></div>
						        
						        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
						            <label for="tasaAnual">Anualidad</label>
									<input type="text" class="form-control input-sm" id="tasaAnual" placeholder="0%"   name="tasaAnual" required=""/>
						        </div>
						        	
						        
						        
						        <form >
						    		<div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
									  <label class="mr-sm-2" for="emisor">Emisor</label>
									  <select  id="emisor" class="form-control col-md-4" style="resize:auto" required="">
									  		<option value="">Seleccionar</option>
									  </select>
									</div>	
						    	</form>
						        
						        <div class="clearfix"></div> 
						        <div class="col-xs-10">
									<button type="button" class="btn btn-primary" id="guardar" onclick="validarGuardar()">Guardar</button>
						        </div>
						        
						    </form>
						    <div class="clearfix"></div>

						    <br /><br />
							</div>
						 </div>												                		                 		 
                </section><!-- /.content -->
                
        </div><!-- ./wrapper -->
