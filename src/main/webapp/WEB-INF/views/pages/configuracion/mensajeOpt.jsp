        <script src="${pageContext.request.contextPath}/resources/js/hiperion/opt/opt.js"  type="text/javascript"></script>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Configuración Mensaje Opt</h1>                                       
                </section>
					
                <!-- Main content -->
                <section class="content">
                		
                		<div class="container">			
                		                			
							  <form class="form-inline col-xs-5" >
									<div class="form-group col-xs-4 col-sm-10">
									  <label for="numIntento">Número Intentos</label>
									  <br/>
									  <input type="text" class="form-control input-sm" id="numIntento" placeholder="Ingresa el numero intentos." name="numIntento"/>
									</div>											
							 
							 		<div class="form-group col-xs-4 col-sm-10">
							 		  <label for="horas">Horas Vigencia</label>
									  <br/>
									  <input type="text" class="form-control input-sm" id="horas" placeholder="Ingresa las Horas Vigencia." name="horas" />
									</div>							
							  
							  		<div class="form-group col-xs-4 col-sm-10">
							 		  <label for="token">Token Master</label>
									   <br/>
									  <input type="text" class="form-control input-sm" id="token" placeholder="Ingresa el Token Master." name="token" />
									</div>							
							  </form>	 
						  </div>
							  
							  
						  <br/>
					      <br/>
						
						  <div class="container">		  
							  <form class="form-inline" >
									<div class="form-group">
										<label>Mensajes</label>			
									</div>							
							  </form>	
							  
							  <form class="form-row" >								
									<div class="form-group col-md-4">
									  <label for="tokenCreado">Token Creado</label>
									  <input type="text" class="form-control input-sm" id="tokenCreado" placeholder="Ingresa el mensaje cuando el token se crea." 
									      name="tokenCreado" />
									</div>	
									<div class="form-group col-md-4">
									  <label for="tokenNoCreado">Token No Creado</label>
									  <input type="text" class="form-control input-sm" id="tokenNoCreado" placeholder="Ingresa el mensaje cuando el token no se crea." 
									      name="tokenNoCreado" />
									</div>										
							  </form>	 
							  
							  <div class="clearfix"></div>
							  
							  <form class="form-row" >
								    <br/>
								    <br/>
									<div class="form-group col-md-4">
									  <label for="folioNoExiste">Folio No Existe</label>
									  <input type="text" class="form-control input-sm" id="folioNoExiste" placeholder="Ingresa el mensaje." 
									  	   name="folioNoExiste" />
									</div>	
									
									<div class="form-group col-md-4">
									  <label for="vigenciaError">Vigencia Error</label>
									  <input type="text" class="form-control input-sm" id="vigenciaError" placeholder="Ingresa el mensaje." 
									       name="vigenciaError" />
									</div>																						
							  </form>	 

							  <div class="clearfix"></div>
							  
							  <form class="form-row" >
							        <br/>
								    <br/>
									<div class="form-group col-md-4">
									  <label for="tokenCorrecto">Token Correcto</label>
									  <input type="text" class="form-control input-sm " id="tokenCorrecto" placeholder="Ingresa el mensaje." 
									  	    name="tokenCorrecto" />
									</div>	
									
									<div class="form-group col-md-4">
									  <label for="tokenIncorrecto">Token Incorrecto</label>
									  <input type="text" class="form-control input-sm" id="tokenIncorrecto" placeholder="Ingresa el mensaje." 
									  		name="tokenIncorrecto" />
									</div>				
							  </form>	 
							  
							  <div class="clearfix"></div>
							  
							  <form class="form-row" >
							        <br/>
								    <br/>
									<div class="form-group col-md-4">
									  <label for="errorValidacion">Error Validación</label>
									  <input type="text" class="form-control input-sm" id="errorValidacion" placeholder="Ingresa el mensaje." 
									  		name="errorValidacion" />
									</div>			
									
									<div class="form-group col-md-4">
									  <label for="tokenIncorrecto">Mensaje SMS</label>
									  <input type="text" class="form-control input-sm" id="mensajeSms" placeholder="Ingresa el mensaje." 
									  		name="tokenIncorrecto" />
									</div>		
							  </form>	 
							  		  							    		  
						</div>
						<br/>
						<div class="container">
							<form class="form-inline col-xs-6" >					  														
							</form>	
							<form class="form-inline" >					  
									<div class="form-group">				  
										<button type="button" class="btn btn-primary" id="guardar">Guardar</button>
									</div>					
							  </form>								
						</div>
                	
                </section><!-- /.content -->
                
        </div><!-- ./wrapper -->
    