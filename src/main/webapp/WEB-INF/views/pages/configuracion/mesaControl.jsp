		<script src="${pageContext.request.contextPath}/resources/js/hiperion/mesaControl/mesaControl.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
        	<section class="content-header">
            	<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Configuraci�n Mesa de Control</h1>
			</section>
                
            <section class="content">
            
            	<div id="no-more-tables" class="form-group">
					<table id="tableMesaCtrl" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
						<tbody id="tbodyMesaCtrl"></tbody>
					</table>
					<button type="button" class="btn btn-primary" id="btnNuevo">Nuevo</button>
				</div>

				
				<div id="divDetalleMesaCtrl" class="panel panel-default form-group col-xs-10">
				
					<div class="panel-body">
 		   		
 				   		<div class="row" >
							
							<div class="form-group col-xs-2">
								<label>Nombre:</label>
							</div>
							<div class="form-group col-xs-5">									  
								<input type="text" class="form-control" id="nombreMesaCtrl"/>
							</div>
							
							<div class="form-group col-xs-2"/>
							
							<div class="form-group col-xs-3">									  
								<button type="button" class="btn" id="btnLugarVenta">Ver Lugares Venta</button>
							</div>
							<br/><br/><br/>
						</div>
						
						<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
				
						<form class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-3">
								<label>Check List:</label>
							</div>
							<div class="form-group col-xs-3">									  
								<select class="form-control" id="listaChecklist">
									<option value="0">Seleccione</option>		
								</select>
							</div>
			
							<div class="form-group col-xs-4">
								<label>Forma de asignaci�n de n�mero de cuenta:</label>
							</div>
							<div class="checkbox, form-group col-xs-2">
		  						<select class="form-control" id="listaFormaAsignacion">
									<option value="0">Seleccione</option>				
								</select>
							</div>
						</form>
				
				
						<form id ="prueba" class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-4">
								<label>�Permite editar la solicitud?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkEditSol" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
							
							<div class="form-group col-xs-4">
								<label>Ver bot�n asignar cuenta:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkAsigCuenta" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
						</form>
				
						<form class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-4">
								<label>Ver bot�n actualizar en TSYS:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkActCuentaTsys" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
		
							<div class="form-group col-xs-4">
								<label>Ver bot�n Asignaci�n/Activaci�n:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkAsignaAct" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
						</form>

						<form class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-4">
								<label>Ver bot�n Rechazar Solicitud :</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkRechazar" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
		
							<div class="form-group col-xs-4">
								<label>Ver bot�n Enviar Revisi�n:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkRevision" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
						</form>
												
						<form class="form-inline col-xs-12" >
							<br/>					
							<div class="form-group col-xs-4">
								<label>�Permite ver notas?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkVerNotas" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
							
							<div class="form-group col-xs-4">
								<label>�Enviar email de asignaci�n de n�mero de cuenta a solicitud?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkEnviaMail" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
						</form>
						
						<form class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-4">
								<label>�Enviar alerta de entrega inmediata de tarjeta de cr�dito?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkEnviaAlerta" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
							
							<div class="form-group col-xs-4">
								<label>�Ver Bur� de Cr�dito?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkVerBuro" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
						</form>
						
						<form class="form-inline col-xs-12" >
							<br/>					
							<div class="form-group col-xs-4">
								<label>�Ver nivel de riesgo?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkVerRiesgo" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
		
							<div class="form-group col-xs-4">
								<label>�Ver l�mite de cr�dito?:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkLimCredito" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
							
						</form>
						
						<form class="form-inline col-xs-12" >
							<br/>
							<div class="form-group col-xs-4">
								<label id="lblActivacion">Muestra Activaci�n:</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkActivacion" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>
											
							<div class="form-group col-xs-4">
								<label id="lblActivo">Activo:</label>
							</div>
							<div id="divCheckActivo" class="form-group col-xs-2">
								<input id="checkActivo" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small" onchange="swSwitch($(this));"/>
							</div>							
						</form>
					</div>
				</div>
				
				<div id="divBotones">
			        <form class="form-inline col-xs-4" >
						<div class="form-group col-xs-4">				  
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar</button>
						</div>
												
						<div class="form-group col-xs-4">				  
							<button type="button" class="btn btn-primary" id="btnCancelar">Cancelar</button>
						</div>					
					</form>
				</div>					    
			</section>
			
			<!--modales-->
			
			<div id="modalLugarVenta" class="modal fade" role="dialog">	
				<div class="modal-dialog">
			
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Lugares de Venta</h4>
						</div>
						
						<div class="modal-body">
							<div id="no-more-tables" class="form-group">
								<table id="tableLugarVenta" class="table table-striped table-bordered nowrap" cellspacing="0" width="60%">
									<tbody id="tbodyMesaCtrl"></tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			
        </div>