		<script src="${pageContext.request.contextPath}/resources/js/hiperion/ace/configuracionAce.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		     
        <div class="wrapper row-offcanvas row-offcanvas-left">
        	<section class="content-header">
            	<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Configuraci�n Ace</h1>
			</section>
                
            <section class="content">
               
	        	<form class="form-inline col-xs-12" >
					<div class="form-group col-xs-1">
						<label for="producto">Producto:</label>
					</div>
					
					<div class="form-group col-xs-4">									  
						<select class="form-control" id="listaProductos">
							<option value="0">Seleccione</option>				
						</select>
					</div>																
				</form>
				
				<form class="form-inline col-xs-4" >
					<br/><br/>
					<div class="form-group col-xs-4">				  
						<button type="button" class="btn btn-primary" id="btnBuscar">Buscar</button>
					</div>
									
					<div class="form-group col-xs-4">				  
						<button type="button" class="btn btn-primary" id="btnNuevo">Nuevo</button>
					</div>					
				</form>
				
 				<form class="form-inline col-xs-10 " >
					<br/><br/>
					<table class="table table-bordered table-condensed" id="configuracionTable">
						<thead  class="fixedHeader">
							<tr>
								<th style="text-align:center;">Nombre Configuraci�n</th>
								<th style="text-align:center;">Acciones</th>
							</tr>
						</thead>
						<tbody/>			  		  			
					</table>
				</form>
				
				<form id="formConfig" class="form-inline col-xs-12" data-parsley-validate="">
					<div class="form-group col-xs-3">
						<label id="lblNombreConfig">Nombre Configuraci�n:</label>
					</div>
					<div class="form-group col-xs-3">
						<input type="text" class="form-control" id="nombreConfiguracion" name="nombreConfiguracion" />
					</div>
															
				</form>
	
				<form id="formTemplate" class="form-inline col-xs-12" >
				<br/>
					<div class="form-group col-xs-1">
						<label id="lblTemplate">Template:</label>
					</div>
					
					<div class="form-group col-xs-5">									  
						<select class="form-control" id="listaTemplates">
							<option value="0">Seleccione</option>				
						</select>
					</div>
					
					<div class="form-group col-xs-4">				  
						<button type="button" class="btn btn-primary" id="btnAgregarTemplate">Agregar Template</button>
					</div>																
				</form>
				
				<form class="form-inline col-xs-10 " >
					<br/><br/>
					<table class="table table-bordered table-condensed" id="templatesTable">
						<thead>
							<tr>
								<th style="text-align:center;">Nombre Template</th>
								<th style="text-align:center;">Acciones</th>
							</tr>
						</thead>
						<tbody/>			  		  			
					</table>
				</form>
				
				<form class="form-inline col-xs-12" >
					<br/>					
					<div class="form-group col-xs-4">				  
						<button type="button" class="btn btn-primary" id="btnGuardarConfig">Guardar Configuraci�n</button>
					</div>																
				</form>
				
			</section>
			
			<!-- Modal de Parametros -->
			
			<div id="modalTemplate" class="modal fade" role="dialog">	
				<div class="modal-dialog">
			
					<!-- Modal content-->
					<div class="modal-content" style="width:800px;align:center;">
						<div class="modal-header">
	 						<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Par�metros</h4>
						</div>
						
						<form id="formParametros" data-parsley-validate="">

							<div class="modal-body">
						     	<br/>				                  
								<div class="form-group">
										  	
									<table id="ParametrosTable" class="table table-bordered table-condensed scrollTable">				  	 								
										<thead  class="fixedHeader">
											<tr>
												<th style="text-align:center; ">Campo</th>
												<th style="text-align:center;">Posici�n Inicial</th>
												<th style="text-align:center;">Posici�n Final</th>
												<th style="text-align:center;">Longitud</th>
												<th style="text-align:center;">Valor</th>											 										
											</tr>
										</thead>					  					  
										<tbody class="scrollContent"/>					 									
									</table>
								</div>
							</div>
						
						<div class="modal-footer">
						 <button id="btnGuardarTemplate" type="button" class="btn btn-primary">Guardar</button>

						</div>
						
						</form>
					</div>
				</div>
			</div>
			
			<!-- Modal Configuración clonada -->
			
			<div id="modalConfiguracionClon" class="modal fade" role="dialog">
				<div class="modal-dialog">
			
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
	 						<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Configuraci�n Ace</h4>
						</div>
						
						<div class="modal-body" style="height:120px">
							<form>
								<div class="form-group col-xs-4">
									<label>Nombre Configuraci�n:</label>
								</div>
								
								<div class="form-group col-xs-6">									  
									<input type="text" class="form-control" id="nombreConfiguracionClon" />
								</div>																
							</form>
							
	 						<form>
								<br/><br/>
								<div class="form-group col-xs-4">				  
									<label>Producto:</label>
								</div>
	
								<div class="form-group col-xs-6">				  
									<select class="form-control" id="listaProductosClon">
										<option value="0">Seleccione</option>			
									</select>
								</div>					
							</form>
						</div>
					
						<div class="modal-footer">
							<button id="btnClonar" type="button" class="btn btn-primary">Clonar</button>
						</div>
					</div>
				</div>
			</div>
        </div>