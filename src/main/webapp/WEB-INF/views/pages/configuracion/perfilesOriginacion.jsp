		<script src="${pageContext.request.contextPath}/resources/js/hiperion/perfiles/orPerfilesOriginacion.js"  type="text/javascript"></script>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Perfiles Originaci�n</h1>
			</section>

			<section id="formPerfiles" class="content">

				<div class="row" style="margin-top: 20px">
					<div class="col-md-6">

						<table id="tablaPerfiles" class="table" cellspacing="0"
							width="90%"></table>

					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div id="headPanel" class="panel-heading"></div>
							<div class="panel-body">
								<div class="row col-md-12">

									<input type="hidden" id="idPerfil" />

									<div class="col-md-10">
										<label class="control-label">Mesa de control: </label> <select
											id="selectMesa" class="form-control">
											<option value="" selected="selected">Seleccionar</option>
											<option value="4">Mesa1</option>
										</select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/add.png" onClick="agregarMesa();" />
									</div>
								</div>
								<div class="row col-md-12" style="margin-top: 20px">
									<div class="col-md-10">
										<select id="activeMesa" multiple="multiple" size="20"
											style="width: 100%; font-size: 18px; color: #840925; height: 80px;"></select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/icon_remove.png" onClick="quitarMesa();" />
									</div>
								</div>


								<div class="row col-md-12" style="margin-top: 20px">
								<div class="col-md-10">
										<label class="control-label">Lugares de venta disponibles: </label> <select
											id="activeLv" multiple="multiple" size="20"
											style="width: 100%; font-size: 18px; color: #840925; height: 120px;"></select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/add.png" onClick="agregarLugares();" />
									</div>
								</div>
								
									<div class="row col-md-12" style="margin-top: 20px">
									<div class="col-md-10">
										<label class="control-label">Lugares de venta asignados: </label> <select
											id="activeLvAsig" multiple="multiple" size="20"
											style="width: 100%; font-size: 18px; color: #840925; height: 100px;"></select>
									</div>
										<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/icon_remove.png" onClick="quitarLV();" />
									</div>
								</div>
								
								
							</div>
						</div>
					</div>


				</div>



				<div class="container">
					<form class="form-inline col-xs-6"></form>
					<form class="form-inline">


						<div class="form-group">
							<button type="button" class="btn btn-primary" id="btnGuardarN">Guardar</button>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary" id="btnCancelarN">Cancelar</button>
						</div>
					</form>
				</div>
				<div class="modal fade" id="loadingModalAccesos" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				
				<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Notificaci�n</h3>
							</div>
							<div id="msgModalAlert" class="modal-body">
								<h4>Aqu� va el mensaje</h4>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Aceptar</button>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>