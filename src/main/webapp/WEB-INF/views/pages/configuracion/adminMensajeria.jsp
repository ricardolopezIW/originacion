		<script src="${pageContext.request.contextPath}/resources/js/hiperion/inventario/adminMensajeria/orAdminMensajeria.js"  type="text/javascript"></script>

		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Administracion de mensajeria</h1>
			</section>

			<section id="sectionTable" class="content">

				<div class="col-lg-2">
					<input type="button" class="btn btn-primary" value="Nueva Mensajeria"
						onclick="nuevaMensajeria();" style="padding: 15px" />
				</div>
				<div class="col-lg-10">

					<table id="tablaMensajeria" class="table table-striped"
						cellspacing="0" width="90%"></table>
				</div>

			</section>

			<section id="formMensajeria" class="content hide">

				<input type="hidden" id="idMensajeria" />

				<div class="row" style="margin-top: 20px">
					<div class="form-group col-xs-4">
						<label for="nombreMensajeria">Nombre</label> 
						<input type="text" class="form-control input-sm" id="nombreMensajeria" />
					</div>
				</div>
				
				<div class="row" style="margin-top: 20px">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<font size="2">Cobertura de estados</font>
								</h4>
							</div>
							<div class="panel-body">
								<div class="row col-md-12">
									<div class="col-md-10">
										<label class="control-label">Estado: </label> 
										<select id="selectStates" class="form-control">
											<option value="-1" selected="selected">Seleccionar</option>
										</select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/add.png" onClick="agregarEstado();" />
									</div>
								</div>
								<div class="row col-md-12" style="margin-top: 20px">
									<div class="col-md-10">
										<select id="activeStates" multiple="multiple" size="20"
											style="width: 100%; font-size: 23px; color: #840925; height: 300px;"></select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/icon_remove.png" onClick="quitarEstado();" />
									</div>
								</div>
							</div>
						</div>
				</div>
				
									<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<font size="2">Cobertura de municipios</font>
								</h4>
							</div>
							<div class="panel-body">
								<div class="row col-md-12">
									<div class="col-md-10">
										<label class="control-label">Estado: </label>
										<select id="selectStatesMun" class="form-control">
											<option value="-1" selected="selected">Seleccionar</option>
										</select>
									</div>
								</div>
								<div class="row col-md-12" style="margin-top: 20px">
									<div class="col-md-10">
										<label class="control-label">Municipio: </label>
										 <select id="selectMun" class="form-control">
											<option value="-1" selected="selected">Seleccionar</option>
										</select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/add.png" onClick="validAgregarMunicipio();" />
									</div>
								</div>
								<div class="row col-md-12" style="margin-top: 20px">
									<div class="col-md-10">
										<select id="activeMun" multiple="multiple" size="20"
											style="width: 100%; font-size: 23px; color: #840925; height: 300px;"></select>
									</div>
									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/icon_remove.png" onClick="quitarMunicipio();" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row" style="margin-top: 20px">
					<div class="col-md-6"></div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<font size="2">Cobertura de codigos postales</font>
								</h4>
							</div>
							<div class="panel-body">
								<div class="row col-md-12">
									<label class="control-label">Rango de codigos postales:</label>
								</div>
								<div class="row col-md-12">
									<div class="form-group col-md-5">
										<input type="text" class="form-control input-sm" id="initCP" />
									</div>
									<div class="form-group col-md-5">
										<input type="text" class="form-control input-sm" id="endCP" />
									</div>
									<div class=" col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/add.png" onClick="buscarCP();" />
									</div>
								</div>

								<div class="row col-md-12" style="margin-top: 20px">

									<div class="col-md-10">
										<select id="activeCp" multiple="multiple" size="20"
											style="width: 100%; font-size: 23px; color: #840925; height: 300px;"></select>
									</div>

									<div class="col-md-2">
										<img class="btnDelete" width="30px" height="30px"
											src="${pageContext.request.contextPath}/resources/img/icon_remove.png" onClick="quitarCP();" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-inline" style="margin-top: 20px" id="divBotones">
					<div class="col-md-6"></div>
					<div class="col-md-6">
						<div class="form-group">
							<button type="button" class="btn btn-primary" id="btnGuardarCamb">Guardar cambios</button>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar</button>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary" id="btnCancelar">Cancelar</button>
						</div>
					</div>
				</div>
				
				<!-- modales -->
				<div class="modal fade" id="modalMunEdo" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">

							<div class="modal-header">

								<h3 class="modal-title">Notificacion</h3>
							</div>
							<div class="modal-body">
								<h4>Seleccionaste un municipio de un estado ya agregado, el
									estado correspondiente se eliminara y se tomara en cuenta solo
									el municipio</h4>
							</div>

							<div class="modal-footer">

								<button type="button" class="deleteState btn btn-primary"
									data-dismiss="modal">Aceptar</button>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Cancelar</button>

							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modalCpMun" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Notificacion</h3>
							</div>
							<div class="modal-body">
								<h4>Seleccionaste un CP de un municipio ya agregado,el
									municipio correspondiente se eliminara y se tomara en cuenta
									solo el CP</h4>
							</div>
								
							<div class="modal-footer">
								<button type="button" class="deleteStateCp btn btn-primary"
									data-dismiss="modal">Aceptar</button>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Cancelar</button>

							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modalCpEdo" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Notificación</h3>
							</div>
							<div class="modal-body">
								<h4>Seleccionaste un CP de un estado ya agregado, el estado
									correspondiente se eliminara y se tomara en cuenta solo el CP</h4>
							</div>

							<div class="modal-footer">

								<button type="button" class="deleteStateCp btn btn-primary"
									data-dismiss="modal">Aceptar</button>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Cancelar</button>
								
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Notificación</h3>
							</div>
							<div id="msgModalAlert" class="modal-body">
								<h4>Aqui va el mensaje</h4>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Aceptar</button>
							</div>
						</div>
					</div>
				</div>
			
				<!-- fin modales -->

			</section>

		</div>
	