        <script src="${pageContext.request.contextPath}/resources/js/hiperion/buroCredito/claveBuroReglas.js"  type="text/javascript"></script>

        <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Administrar Claves de Usuario</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                		                			 								  			  			  						  		                	
                		<form class="form-inline col-xs-12" id="formInicial" >
																	
									<div class="form-group col-xs-1">
									  <label for="accionSel">Usuario:</label>
									</div>
									
									<div class="form-group col-xs-3" id="cargaUsuarios">									  
									  
									</div>
									<div class="form-group col-xs-1">
									<label for="accionSel">Usuario Default:</label>									  
									</div>
									<div class="form-group col-xs-1" id="checkDefault">									  
									  
									</div>
									<div class="form-group col-xs-2" >
										  <button type="button" class="btn btn-primary" id="agregarUsuario">Agregar Usuario</button>
									</div>	
									<div class="form-group col-xs-1" >
										  <button type="button" class="btn btn-primary" id="editUser">Editar Usuario</button>
									</div>
																			
						</form>	
						
						<form class="form-inline col-xs-12" id="Usuario Actual" >
								<div class="form-group col-xs-1">
									  
									</div>
									
									<div class="form-group col-xs-3" id="printUser">									  
									  
									</div>
						</form> 

						
							<div class="form-group col-xs-12" id= "duallistboxLugarVenta">
			                	
							</div>
							
							<div class="form-group col-sm-2 " style="line-height: 150px;">
										  <button type="button" class="btn btn-primary" id="guardarLV">Guardar</button>
									</div>
											    																		 
                </section><!-- /.content -->
              
              
              	<!--modales-->
				<div class='modal fade' id='loginModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
   <div class='modal-dialog'>
	 <div class='modal-content'>
	   <div class='modal-header'>
         <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>X</button>
         <h4 class='modal-title text-center'>Agregar Usuario</h4>
	   </div>
   	   <div class='modal-body'>
          <form id='loginForm' class='form-horizontal'     >
            <div class='form-group'>
			  <label class='col-md-3 control-label'>Nombre de Usuario</label>
			  <div class='col-md-7'>
			    <input type='text' class='form-control' name='usuario' id="usr" required="required"  />
			  </div>
		    </div>
			<div class='form-group'>
		 	  <label class='col-md-3 control-label'>Password</label>
			  <div class='col-md-7'>
			    <input type='text' class='form-control' name='password' id="pass"   />
			  </div>
			</div>
			<div class='form-group'>
		 	  <label class='col-md-3 control-label'>Default</label>
			  <div class='col-md-7' id="divCheckDefault">
<!-- 			    <input type='checkbox' class='form-control' name='checkbox' id="uDefault" /> -->
			  </div>
			</div>
			
			<div class='form-group'>
		      <div class='col-md-5 col-md-offset-3'>
		      	<button type='button' class="btn btn-primary " data-dismiss="modal">Cerrar</button>
		      	<button type="button" class="btn btn-primary "  id="btnGuardarUsr"  >Guardar</button>
			  </div>
			</div>
			
			<div id="dialog-confirm"></div>
			
	      </form>
	    </div>
	  </div>
   </div>
 </div>
        </div><!-- ./wrapper -->
        
        
        <div class="modal fade" id="loadingModalAccesos" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
    