	    <script src="${pageContext.request.contextPath}/resources/js/hiperion/lugarVenta/orLugarVenta.js"  type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
	
		<div class="wrapper row-offcanvas row-offcanvas-left">

			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Lugar de Venta</h1>
			</section>

			<section id="sectionNew" class="content">
				<div class="col-lg-12">
					<input type="button" id="btnNuevo" class="btn btn-primary" value="Nuevo Lugar de Venta" onclick="nuevoLugar();" style="padding: 15px" /> 
					<input type="button" id="btnLv"  class="btn btn-primary" value="Actualizar Lugar de Venta" onclick="actualizarLugar();" style="padding: 15px" /> <br /> <br />
				</div>

			</section>

			<!-- Main content -->
			<section class="content" id="content">

				<div class="col-lg-12">
					<table id="tablaLugarVenta" class="table table-striped">
					</table>
				</div>


				<div class="modal fade" id="modalMatch" data-keyboard="false" data-backdrop="static">
					<div id="modalRemove" class="modal-dialog modal-lg">

						<div class="modal-content">
							<div class="modal-header">
								<h4>Detalle de comparacion</h4>
							</div>
							<div id="bodyModal" class="modal-body"
								style="height: 400px; overflow-y: auto;">



								<div class="col-md-6">
									<p style="padding-left: 10px; font-size: 15px;">Lugares de
										venta que no existen en SIDIT y si estan en FICO</p>
									<select id="lugaresApm" multiple="multiple" size="20"
										style="width: 90%; font-size: 17px; color: #840925; height: 300px;"></select>
								</div>


								<div class="col-md-6">
									<p style="padding-left: 10px; font-size: 15px;">Lugares de
										venta con estatus diferente al de FICO</p>
									<select id="lugaresDifEstatus" multiple="multiple" size="20"
										style="width: 90%; font-size: 17px; color: #840925; height: 200px;"></select>
									<p></p>
									<div class=" col-md-6">
										<button type="button" class="btn btn-primary" id="btnAplicar">Sincronizar</button>
									</div>

									<div class="col-md-6">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>








				<div class="modal fade" id="modalDetalleLugar" data-keyboard="false"
					data-backdrop="static">
					<div id="modalRemove" class="modal-dialog modal-lg">
						<form id="formDetalle" data-parsley-validate="">
							<div class="modal-content">
								<div class="modal-header">
									<h4>Detalle Lugar de Venta</h4>
								</div>
								<div id="bodyModal" class="modal-body"
									style="height: 400px; overflow-y: auto;">
									<div class="row" id="row1">
										<div class="col-lg-2">
											<label class="control-label">Nombre: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="inDescripcion"
												name="descripcion" data-parsley-required="true"
												data-parsley-maxlength="50" data-parsley-minlength="3" />
										</div>
										<div class="col-lg-2">
											<label class="control-label">Id APM: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm" id="inAPM"
												name="APM" data-parsley-required="true"
												data-parsley-maxlength="20" />
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">C�digo Canal: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inCodCanal" name="codCanal" data-parsley-maxlength="20" />
										</div>
										<div class="col-lg-2">
											<label class="control-label">Canal: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm" id="inCanal"
												name="canal" data-parsley-maxlength="200" />
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">Branch: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inBranch" name="branch" data-parsley-maxlength="6" />
										</div>
										<div class="col-lg-2">
											<label class="control-label">Mesa Control: </label>
										</div>
										<div class="col-lg-4">
											<select name="lstMesaControl" id="lstMesaControl"
												class="form-control"></select>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">Riesgo: </label>
										</div>
										<div class="col-lg-4">
											<select name="lstRiesgo" id="lstRiesgo" class="form-control"
												data-parsley-required="true"></select>
										</div>
										<div class="col-lg-2">
											<label class="control-label">Estatus: </label>
										</div>
										<div class="col-lg-4">
											<select name="lstEstatus" id="lstEstatus"
												class="form-control"></select>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">Custom Data 10: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inCustom10" name="branch" data-parsley-maxlength="20" />
										</div>
										<div class="col-lg-2">
											<label class="control-label">Custom Data 11: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inCustom11" name="branch" data-parsley-maxlength="20" />
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">Custom Data 80: </label>
										</div>
										<div class="col-lg-4">
											<select id="lstCustom80" class="form-control"
												data-parsley-required="true" ></select>
										</div>
										<div class="col-lg-2">
											<label class="control-label">Status Code: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inStatusCode" name="branch" data-parsley-maxlength="20" />
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-lg-2">
											<label class="control-label">Reason Code: </label>
										</div>
										<div class="col-lg-4">
											<input type="text" class="form-control input-sm"
												id="inReasonCode" name="branch" data-parsley-maxlength="20" />
										</div>
										<div class="col-lg-2">
											<label class="control-label">�Genera NIP?: </label>
										</div>
										<div class="col-lg-4" id="divRadioGeneraNip"></div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label class="control-label">Tipo de Venta: </label>
										</div>
										<div class="col-lg-4">
											<select id="lstTipoVenta" class="form-control"
												data-parsley-required="true" ></select>
										</div>
										<div class="col-lg-2">
											<label class="control-label">Descripci�n: </label>
										</div>
										<div class="col-lg-4">
											<textarea id="txaDescripcion" class="form-control" rows="3"></textarea>
										</div>
									</div>

									<br/>
									<div class="row">
										<div class="form-group col-xs-12" id="duallistboxMensajeria"></div>
									</div>

								</div>
								<div class="modal-footer">
									<button type="button" class="button" id="btnCerrar"
										data-dismiss="modal">Cerrar</button>
									<button type="button" class="btn btn-primary" id="btnEditar">Editar</button>
									<button type="button" class="btn btn-primary" id="btnActualizar">Guardar
										Cambios</button>
								</div>

							</div>
						</form>
					</div>
				</div>

				<div class="modal fade" id="modalConfirmacion" tabindex="1000"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Confirmaci�n</h3>
							</div>
							<div class="modal-body">
								<h4>�Est� seguro de realizar esta acci�n?</h4>
							</div>

							<div class="modal-footer">

								<button type="button" class="btn btn-primary"
									data-dismiss="modal" id="btnCancelarEdit">Cancelar</button>

								<button type="button" class="btn btn-primary"
									data-dismiss="modal" id="btnAceptarEdit">Aceptar</button>
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modalLoadingLugar" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body" id="bodyModal">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="modal fade" id="modalResultado" tabindex="1000"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Resultado</h3>
							</div>
							<div class="modal-body">
								<div id="resultadoModal"></div>
							</div>

							<div class="modal-footer">

								<button type="button" class="btn btn-primary"
									data-dismiss="modal" id="btnCancelarEdit">Cerrar</button>

							</div>
						</div>
					</div>
				</div>

			</section>
			<!-- /.content -->
		</div>