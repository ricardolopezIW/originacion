<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/solicitudesMultiva/orSolicitudesMultiva.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
		
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Mesa de Control</h1>
			</section>
			
			<section id="sectionNew" class="content">
				<div class="col-lg-12">
					<input type="button" id="btnActualizar" class="btn btn-primary" value="Actualizar" style="padding: 15px" />
					<br/><br/>	
				</div>
				
			</section>
			
			<!-- Main content -->
			<section class="content" id="content">
				
				<div class="col-lg-12">
					<table id="tablaSolicitudes" class="table table-striped" >
					</table>
				</div>
				
				<div class="modal fade" id="modalLoadingSolicitud" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body" id="bodyModal">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</section>
			<!-- /.content -->
		</div>