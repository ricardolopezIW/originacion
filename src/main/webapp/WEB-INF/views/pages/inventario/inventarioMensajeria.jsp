		<script src="${pageContext.request.contextPath}/resources/js/hiperion/inventario/inventarioMensajeria/invMensajeriaService.js"  type="text/javascript"></script>
		
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.iframe-transport.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.fileupload.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.ui.widget.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-process.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-validate.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/dataTables-1.10.16/Buttons-1.5.1/js/dataTables.buttons.min.js"  type="text/javascript"></script>
		
		<link href="${pageContext.request.contextPath}/resources/fileUpload/css/jquery.fileupload.css"  rel="stylesheet"/>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Asignaci&oacute;n de Inventario a Mensajer&iacute;a</h1>
			</section>

			<!-- Main content -->
			<section class="content">

				<div class="container col-md-11">
					<div class="clearfix"></div>

					<div class="panel-group" id="accordion" role="tablist"
						aria-multiselectable="true">


						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse"
										data-parent="#accordion" href="#collapseTs"
										aria-expanded="false" aria-controls="collapseTs"
										class="collapsed"> Cargar Cuentas TS2 </a>
								</h4>
							</div>
							<div id="collapseTs" class="panel-collapse collapse"
								role="tabpanel" aria-labelledby="headingOne"
								aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<div class="row">
												<div class="col-lg-8 vcenter">
													<blockquote>
														<p class=".text-justify">1.-Para iniciar el proceso de
															Carga de Cuentas de TS2, dar click sobre la imagen de la
															derecha.</p>
														<p class=".text-justify">2.-Se abrir&aacute; una ventana para
															seleccionar un archivo con la extensi&oacute;n ".txt", del cu&aacute;l
															se obtendr&aacute;n los datos a procesar.</p>
														<p class=".text-justify">3.-Una vez cargadas las
															cuentas se mostrar&aacute; el resultado de la carga</p>
													</blockquote>
												</div>

												<div class="col-lg-3">

													<p class=".text-justify">&iquest;El archivo incluye tarjeras
														adicionales?</p>

													<form id="myForm">
														<label><input type="radio" id="cbox1"
															name="adicional" value="true"></input>Si</label> <label><input
															type="radio" name="adicional" value="false" id="cbox1" checked="checked" ></input>No</label>
													</form>



													<div class="form-group">
														<br /> <label for="productoSel">Producto</label>
														  <select class="form-control" id="productoSel"> </select>
													</div>

												</div>






												<div class="col-lg-2">
													<span class="btn btn-success fileinput-button" id="spload">
														<i class="glyphicon glyphicon-plus"></i> <span>Cargar
															Cuentas...</span> <input id="fileuploadTs" type="file"
														name="files[]" />
													</span>
												</div>





												<!-- fin raw -->
											</div>
											<div id="resultado" class="hide row">
												<div class="col-lg-8 vcenter">
													<blockquote>
														<h2 style="padding-left: 10px; font-size: 25px;" class="panel-title">Resultados:</h2>
														<p class=".text-justify">
															- Total de registros: <span id ="total"
																class="badge badge-lg  badge-success center"></span>
														</p>
														<p class=".text-justify">
															- Cuentas importadas con &eacute;xito: <span id ="cuentasValidas"
																class="badge badge-lg  badge-success center"></span>
														</p>
														<p class=".text-justify">
															- Producto no v&aacute;lido: <span id ="cuentasProductoInvalido"
																class="badge badge-lg  badge-success center"></span>
														</p>
														<p class=".text-justify">
															- Cuentas duplicadas en el archivo: <span id ="cuentasDuplicadas"
																class="badge badge-lg  badge-success center"></span>
														</p>

														<p class=".text-justify">
															- Cuentas duplicadas en el sistema: <span id ="cuentasDuplicadasSistema"
																class="badge badge-lg  badge-success center"></span>
														</p>


														<p class=".text-justify">
															- Cuentas No v&aacute;lidas: <span id ="cuentasInvalidas"
																class="badge badge-lg  badge-success center"></span>
														</p>
													</blockquote>
												</div>
											</div>
											<!-- Mensajes de error para cargar cuentas con adicionales -->
                                            <div id="resultadoErrorCargarArchivo" class="hide row">
												<div class="col-lg-8 vcenter">
													<blockquote>
														<h2 style="padding-left: 10px; font-size: 25px;" class="panel-title">
														Errores:</h2>
														<p class=".text-justify">
														   - <span id="errorArchivo" style="color:#8c1900"></span>
														</p>
													</blockquote>
												</div>
											</div>
										</div>
									</div>

								</div>
								<!-- fin panel body carga -->
							</div>
							<!-- fin tab panel -->



						</div>





						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse"
										data-parent="#accordion" href="#collapseOne"
										aria-expanded="false" aria-controls="collapseOne"
										class="collapsed"> Cargar Cuentas Inventario Mensajer&iacute;a </a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse"
								role="tabpanel" aria-labelledby="headingOne"
								aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-lg-11 vcenter">
													<blockquote>
														<p class=".text-justify">1.-Para iniciar el proceso de
															Carga de Cuentas de Inventario, dar click sobre la imagen
															de la derecha.</p>
														<p class=".text-justify">2.-Se abrir&aacute; una ventana para
															seleccionar un archivo con la extensi&oacute;n ".txt", del cu&aacute;l
															se obtendr&aacute;n los datos a procesar.</p>
														<p class=".text-justify">3.-Una vez cargadas las
															cuentas se mostrar&aacute;n las cuentas.</p>
													</blockquote>
												</div>

												<div class="col-lg-1">
													<span class="btn btn-success fileinput-button" id="spload">
														<i class="glyphicon glyphicon-plus"></i> <span>Cargar
															Cuentas...</span> <input id="fileupload" type="file"
														name="files[]" />
													</span>
												</div>

												<div class="col-lg-1"></div>
											</div>
											<!-- fin raw -->
										</div>
									</div>
									<!-- fin raw -->

									<br />

									<div class="row col-md-11" id="modalAsignar">

										<blockquote>
											<p class=".text-justify">Asignar Mensajer&iacute;a</p>
										</blockquote>

										<div>
											<form class="form-inline col-md-11">

												<div class="form-group col-xs-4 col-sm-10">
													<br /> <label for="productoSel">Producto</label>
													 <select class="form-control" id="productos"></select>
												</div>

												<div class="form-group col-xs-4 col-sm-10">
													<br /> <label for="mensajeriaSel">Mensajer&iacute;a</label> <select
														class="form-control" id="mensajeriaSel"></select>
												</div>

												<div class="form-group col-xs-4 col-sm-8">
													<br /> <label for="agente">Agente</label> <input
														type="text" class="form-control" id="agente" name="agente" />
													<br /> <br /> <br />

												</div>
											</form>
										</div>



										<div class="row col-md-11" style="display: block;">
											<blockquote>
												<p class=".text-justify">Cuentas Cargadas</p>
												<p class=".text-justify">
													Cuentas con <span class="glyphicon glyphicon-envelope"></span>
													est&aacute;n asignadas a una mensajer&iacute;a.
												</p>
												<p class=".text-justify">
													Cuentas con <span class="glyphicon glyphicon-ok"></span> est&aacute;n
													asociadas a una venta.
												</p>
												<p class=".text-justify">
													Cuentas con <span class="glyphicon glyphicon-remove"></span> no
													existen en el inventario.
												</p>
											</blockquote>

											<div class="col-sm-11 col-md-offset-1" id="containerTable">
											</div>

										</div>

										<form class="form-inline col-xs-12">
											<br /> <br />
											<div class="form-group col-xs-8"></div>
											<div class="form-group col-xs-2">
												<button type="button" class="btn btn-primary" id="guardar">Asignar</button>
											</div>
											<div class="form-group col-xs-2">
												<button type="button" class="btn btn-primary" id="limpiar">Cancelar</button>
											</div>
										</form>

									</div>
									<!-- fin raw -->
								</div>
								<!-- fin panel body carga -->
							</div>
							<!-- fin tab panel -->
						</div>
						<!-- fin panel carga -->








						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse"
										data-parent="#accordion" href="#collapseTwo"
										aria-expanded="true" aria-controls="collapseTwo"> Buscar
										Mensajer&iacute;as </a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse"
								role="tabpanel" aria-labelledby="headingTwo"
								aria-expanded="false" style="height: 0px;">
								<div id="contenedorTablaMensajeriaInventario" class="panel-body">
									<table id="tablaMensajeriaInventario"
										class="table table-striped" cellspacing="0" width="90%">
									</table>
								</div>
							</div>
						</div>


					</div>
				</div>

				<div class="modal fade" id="modalAccounts" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Cuentas por producto</h3>
							</div>
							<div class="modal-body">
								<table id="tablaCuentas" class="table table-striped"
									cellspacing="0" width="50%">
									<thead>
										<tr>
											<th>Numero de cuenta</th>
										</tr>
									</thead>
									<tbody id="tbodyR">
									</tbody>

								</table>
							</div>

							<div class="modal-footer">
								<button type="button" class="tableModal btn btn-primary"
									data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>


			</section>
			<!-- /.content -->
		</div>