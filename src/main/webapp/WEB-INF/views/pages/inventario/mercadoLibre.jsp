<script src="${pageContext.request.contextPath}/resources/js/hiperion/inventario/mercadoLibre/mercadoLibre.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>

<div class="wrapper row-offcanvas row-offcanvas-left">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Mercado
			Libre</h1>
	</section>
	<section class="content">
		<div class="container">
			<form class="form-horizontalcol-xs-5" id="formGral">
				<div class="form-group col-xs-4 col-sm-10">
					<label for="folioAnt" class="col-sm-4">Folio anterior</label>
					<input type="text" class="form-control" id="folioAnt" maxlength="12" pattern="^[0-9]+" /> 
				    <label for="folioNuevo" class="col-sm-4">Folio Nuevo</label> 
					<input type="text" class="form-control" id="folioNuevo" maxlength="12" pattern="^[0-9]+"/>
				</div>
				<div class="form-group col-xs-4 col-sm-10">
				<label><input type="radio" id="checkAccept" name="checkAcc" value="1"/>Aceptar</label><br/>
				<label><input type="radio" id="checkRech" name="checkAcc" value="2"/>Rechazar</label>
				</div>
				 <div class="form-group col-xs-4">
					<button type="button" class="btn btn-primary" id="consultar" onclick="consultaFolios();">Consultar Folios</button>
				</div>
			    <div>
				<table id="tablaFolios" class="table table-striped">
				 <thead>
				    <tr>
				     <td><h4>Folio Anterior</h4></td>
				     <td><h4>Folio Nuevo</h4></td>
				     </tr>
				  </thead>
				  <tbody>
				   <tr>
				    <td><label id="tdFolioAnt" class="col-sm-4"/></td>
				    <td><label id="tdFolioNuevo" class="col-sm-4"/></td>
				   </tr>
				   <tr>
				    <td><label id="tdNombreAnt" class="col-sm-4"/></td>
				    <td><label id="tdNombreNuevo" class="col-sm-4"/></td>
				   </tr>
				   <tr>
				    <td><label id="tdApePatAnt" class="col-sm-4"/></td>
				    <td><label id="tdApePatNuevo" class="col-sm-4"/></td>
				   </tr>
				   <tr>
				    <td><label id="tdApeMatAnt" class="col-sm-4"/></td>
				    <td><label id="tdApeMatNuevo" class="col-sm-4"/></td>
				   </tr>
				   <tr>
				    <td><label id="tdRfcAnt" class="col-sm-4"/></td>
				    <td><label id="tdRfcNuevo" class="col-sm-4"/></td>
				   </tr>
				  </tbody>
				</table>
				</div>
				<div class="form-group col-xs-4">
					<button type="reset" class="btn btn-primary" id="limpiarDatos" onclick="limpiar();">Limpiar</button>
				</div>
				<div class="form-group col-xs-4">
					<button type="button" class="btn btn-primary" id="validar" onclick="validaFolios();" disabled="disabled">Validar</button>
				</div>
			</form>
		</div>
	</section>

</div>