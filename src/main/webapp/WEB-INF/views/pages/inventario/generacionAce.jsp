		<script src="${pageContext.request.contextPath}/resources/js/hiperion/inventario/generacionAce/generacionAce.js"  type="text/javascript"></script>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Generación Ace</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                		                			 								  			  			  						  		                	
						<div class="container">			
                		                			
							  <form class="form-horizontal col-xs-5" >
									<div class="form-group col-xs-4 col-sm-10">
									  <label for="numTarjeta" class="col-sm-4">Número de Tarjetas</label>
									  <input type="text" class="form-control" id="numTarjeta" name="numTarjeta"/>
									</div>											
							 
							 		<div class="form-group col-xs-4 col-sm-10">
							 		  <br/>
									  <label for="productoSel">Producto</label>
									  <select class="form-control" id="productoSel"></select>			
									</div>							
							  
							  		<div class="form-group col-xs-4 col-sm-10">
							  		  <br/>
									  <label for="lugarSel">Lugar de Venta</label>
									  <select class="form-control" id="lugarSel"></select>												
									</div>
									
									<div class="form-group col-xs-4 col-sm-10">
							  		  <br/>
									  <label for="configSel">Configuración</label>
									  <select class="form-control" id="configSel"></select>												
									</div>
									
									<div class="form-group col-xs-4 col-sm-10">
									  <br/>
									  <label for="fecha">Fecha Transmisión</label>
									  <input type="text" class="form-control" id="fecha" name="fecha"/>
									</div>
									
									<div class="form-group col-xs-4 col-sm-10">
									   <br/>
									  <label for="transmission">Transmission ID</label>
									  <input type="text" class="form-control" id="transmission" name="transmission"/>
									</div>
									
									<div class="form-group col-xs-4 col-sm-10">
									  <br/>
									  <label><input type="checkbox" id="adicionales" value="" /> Tarjetas Adicionales</label>
									</div>							
							  </form>	 
						  </div>
							  
						<form class="form-inline col-xs-8" >
								<br/>
								<div class="form-group col-xs-4">				  
								    <button type="button" class="btn btn-primary" id="limpiar">Limpiar</button>
								</div>
									
								<div class="form-group col-xs-4">				  
								  <button type="button" class="btn btn-primary" id="guardar">Generar</button>
								</div>					
						</form>									                  		
                </section><!-- /.content -->           
        </div><!-- ./wrapper -->