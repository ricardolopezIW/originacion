		<script src="${pageContext.request.contextPath}/resources/js/hiperion/impresionDocs/impresion.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.iframe-transport.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.fileupload.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.ui.widget.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-process.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-validate.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/dataTables-1.10.16/Buttons-1.5.1/js/dataTables.buttons.min.js"  type="text/javascript"></script>
		

		<style type="">
			.custom-file-upload {
			  border: 1px solid #ccc;
			  display: inline-block;
			  padding: 6px 12px;
			  cursor: pointer;
			}
		</style>
		<div class="wrapper row-offcanvas row-offcanvas-left">
		
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 style="padding-left: 10px; font-size: 25px;">Impresión Documentos</h1>
			</section>
			<br></br>
			
			<div class="container">
				<div class="row">
					  <form role="form">
				        
				        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
				            <label for="exampleInputEmail1">Folios de Impresión</label>
				            <br/>
				            <input type="text" id="folio"/>  
				            <span data-toggle="tooltip" title="Agregar"  class="glyphicon glyphicon-plus"    onclick="agregarFolio()"></span>   
				            <span data-toggle="tooltip" title="Eliminar" class="glyphicon glyphicon-remove"  onclick="eliminarFolio()"></span>   
				            <span data-toggle="tooltip" title="Limpiar"  class="glyphicon glyphicon-refresh" onclick="limpiar()"></span>
				        </div>
				        
				        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
				            <label>Documentos disponibles para Impresión</label>
				            <br/>
				            <label>
				                <input type="checkbox" id="cbSolicitud"/> Solicitud   <input type="checkbox" id="cbCaratula"/> Carátula   <input type="checkbox" id="cbChecklist"/> Checklist
				            </label>				            				            
				        </div>
				        
				        <div class="clearfix"></div>
				        
				        <div class="form-group col-xs-10 col-sm-4 col-md-4 col-lg-4">
				            <select multiple="true" class="form-control" id="folios" size="10">
							</select>				            
				        </div>
				        
				        <div class="clearfix"></div>
				        <div class="form-group col-xs-10 col-sm-10 col-md-4 col-lg-4">
				            
				            <label for="fileupload" class="btn btn-success custom-file-upload">
							    <i class="fa fa-cloud-upload"></i> Subir Archivo
							</label>
							<input id="fileupload" type="file" name="files[]" style="display:none;" />
				        </div>
				        				   
				        <div class="clearfix"></div>
				        <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
				            <button type="button" class="btn btn-primary" onclick="imprimir()">Imprimir</button>
				        </div>
				    </form>
			    <div class="clearfix"></div>
			
			    <br /><br />
				</div>
		   </div>
			
		   
		   <div class="modal fade" id="loadingModalAccesos" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Procesando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>