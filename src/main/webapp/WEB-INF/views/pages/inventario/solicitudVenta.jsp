		<script src="${pageContext.request.contextPath}/resources/js/hiperion/solicitudVenta/solicitudVenta.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/solicitudVenta/solicitudForm.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/solicitudesMultiva/asignacionActivacion.js"  type="text/javascript"></script>

    	<style type="">
			.modal-content {
				  width: 900px;
				  margin-left: -150px;
			}
			.form-inline select.form-control {
			    width: 146px;
			}
			.form-inline select.form-control[id='preguntaActivacion'] {
			    width: 300px;
			}
			input[type=text]:disabled {
			    background: #FFFFEF;
			}
			input[type="text"][disabled]{
			    color: #969292;
			    cursor: default;
			}
		</style>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
        	<section class="content-header">
            	<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Solicitud Venta</h1>
             </section>
                
            <section class="content">
            
            	<div class="modal fade" id="loadingModalAccesos" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Procesando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
            <form id='formGeneral'  >			
               <div class='panel-group' id='accordion'>
               	                 
                 		<!-- seccion datos de venta -->
                 		<div class='panel panel-default' id='datosVenta'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse1'>Datos de Venta</a>
								</h4>
							</div>
							<div id='collapse1' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form id='formDatosVenta' role='form' class='form-inline'>					  			
							  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Folio</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='folioApm' style="width:350px;"  
												 /></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Lugar Venta</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='lugarVenta' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Producto</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='producto' style="width:350px;" /></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Nombre Tarjeta</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='nombreTarjeta' style="width:350px;" data-parsley-required="true"
												data-parsley-maxlength="25" data-parsley-minlength="0"/></div>
											  </div>
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de venta -->
                 
                 <!-- seccion datos de tarjeta adicional -->
                 <div class='panel panel-default hide' id='datosTarjetaAddicional'>
                     <div class='panel-heading'>
                         <h4 class='panel-title'>
                             <a data-toggle='collapse' href='#collapse10'>Tarjeta Adicional</a>
                         </h4>
                     </div>
                     <div id='collapse10' class='panel-collapse collapse in'>
                       <div class='panel-body'>
                             <div>
                                 <form role='form' class='form-inline'>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Apellido Paterno</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='apellidoPaternoAdicional' style="width:350px;"/></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Apellido Materno</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='apellidoMaternoAdicional' style="width:350px;"/></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Nombre</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='nombreAdicional' style="width:350px;" /></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Fecha de nacimiento</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='fechaNacimientoAdicional' style="width:350px;"/></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Parentesco</label></div>
                                           <div class='form-group col-sm-8'><select class="form-control input-sm" id='parentescoAdicional'></select></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Tel�fono celular</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='celularAdicional' style="width:350px;"/></div>
                                       </div>
                                       <div class="row">
                                          <div class='form-group col-sm-4'><label>Correo electr�nico</label></div>
                                          <div class='form-group col-sm-8'><input type='text' class='texto' id='emailAdicional' style="width:350px;"/></div>
                                       </div>
                                       <div class="row">
                                          <div class='form-group col-sm-4'><label>Porcentajes de Otorgamiento</label></div>
                                          <div class='form-group col-sm-8'><input type='text' class='texto' id='porcentajeAdicional' style="width:350px;" disabled /></div>
                                       </div>
                                       <div class="row">
                                           <div class='form-group col-sm-4'><label>Nombre en la tarjeta adicional</label></div>
                                           <div class='form-group col-sm-8'><input type='text' class='texto' id='nombreTarjetaAdicional' style="width:350px;"/></div>
                                       </div>
                                 </form>
                             </div>
                        </div>
                     </div>
                 </div>
                 <!-- seccion datos de tarjeta adicional -->

                 		<!-- seccion datos de sistema -->
                 		<div class='panel panel-default' id='datosSistema'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse2'>Datos Sistema</a>
								</h4>
							</div>
							<div id='collapse2' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form'  class='form-inline'>						  			
							  				  <div class="row">
												  <div class='form-group col-sm-4'><label>L�mite Cr�dito Venta</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='limiteCredito' style="width:350px;"  /></div>
											  </div>											  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de sistema -->

                 		
                 		<!-- seccion datos de personas -->
                 		<div class='panel panel-default' id='datosPersonales'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse3'>Datos Personales</a>
								</h4>
							</div>
							<div id='collapse3' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' data-parsley-validate="" class='form-inline'>					 			  				  
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Apellido Paterno</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='apellidoPaterno' style="width:350px;" 
												  data-parsley-maxlength="50"  /></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Apellido Materno</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='apellidoMaterno' style="width:350px;"  
												  data-parsley-maxlength="24"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Nombre</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='nombre'  style="width:350px;" 
												  data-parsley-maxlength="50"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Sexo</label></div>
												  <div class='form-group col-sm-8' id="divSexo">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Fecha Nacimiento (titular)</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='fechaNacimiento' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Curp</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='curp' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Nacionalidad</label></div>
												  <div class='form-group col-sm-8' id='divNacionalidad'>
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Entidad Federativa</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='entidadFederativa' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>T�lefono Celular</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='celular' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Correo Electr�nico</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='email' style="width:350px;" 
												  data-parsley-type="email"  data-parsley-maxlength="80"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Estado Civil</label></div>
												  <div class='form-group col-sm-8' id="divEstadoCivil">
												  </div>
											  </div>											  
											  <div class="row">
												  <div class='form-group col-sm-4'><label>N�mero de Dependientes Ec�nomicos</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='dependientesEconomicos' style="width:350px;"/></div>
											  </div>			
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Grado M�ximo de Estudios</label></div>
												  <div class='form-group col-sm-8' id="divEstudios">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>�Cuenta con cr�dito Automotriz?</label></div>
												  <div class='form-group col-sm-8' id="divTieneCreditoAutomotriz">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>�Cuenta con cr�dito Hipotecario?</label></div>
												  <div class='form-group col-sm-8' id="divTieneCreditoHipotecario">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>�Es usted titular de alguna tarjeta de cr�dito?</label></div>
												  <div class='form-group col-sm-8' id="divTieneTDC">
												  </div>
											  </div>											  		
											  <div class="row">
												  <div class='form-group col-sm-4'><label>RFC</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='rfc' style="width:350px;" 
												  data-parsley-maxlength="20"/></div>
											  </div>									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de personas -->
                 	    
                 	    				  
                 	    <!-- seccion datos de domiciliaria -->
                 		<div class='panel panel-default' id='datosDomicilio'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse4'>Datos Domiciliaria</a>
								</h4>
							</div>
							<div id='collapse4' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Calle</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='calle' style="width:350px;"
												  data-parsley-maxlength="20"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>N�mero Exterior</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='numeroExterior' style="width:350px;"
												  data-parsley-maxlength="10"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>N�mero Interior</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='numeroInterior' style="width:350px;"
												  data-parsley-maxlength="10"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>C�digo Postal</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='codigoPostal' style="width:350px;"
												  data-parsley-maxlength="13"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Delegaci�n/Municipio</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='municipio' style="width:350px;" 
												  data-parsley-maxlength="19"/></div>
											  </div>											  
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Colonia/Fraccionamiento</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='colonia' style="width:350px;"
												  data-parsley-maxlength="40"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>T�lefono Casa Titular</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='telefonoCasa' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Tipo de Vivienda</label></div>
												  <div class='form-group col-sm-8' id="divTipoVivienda">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Antig�edad En Residencia</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='antigResidencia' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>�Autentica con otro domicilio?</label></div>
												  <div class='form-group col-sm-8' id="divAutenticaOtroDomicilio">
												  </div>
											  </div>											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de personas -->
                 	    
                 	                 
                 	    <!-- seccion datos de adicionales -->
                 		<div class='panel panel-default' id='datosAdicionales'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse5'>Datos Adicionales</a>
								</h4>
							</div>
							<div id='collapse5' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Autorizaci�n Mercadeo</label></div>
												  <div class='form-group col-sm-8' id="divAutorizacionMercadeo">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Pregunta de Activaci�n</label></div>
												  <div class='form-group col-sm-8' id="divPreguntaActivacion">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Respuesta de Activaci�n</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='respuestaActivacion' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Mensajer�a</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='mensajeria' style="width:350px;"/></div>
											  </div>											  											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de adicionales -->
				
				
						<!-- seccion datos de laborales -->
                 		<div class='panel panel-default' id='datosLaborales'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse6'>Datos Laborales</a>
								</h4>
							</div>
							<div id='collapse6' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Nombre de la Empresa</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='nombreEmpleo' style="width:350px;"
												  data-parsley-maxlength="36"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>T�lefono</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='telefonoEmpleo' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Extensi�n</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='extension' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Correo Eletr�nico Laboral</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='emailEmpleo' style="width:350px;"/></div>
											  </div>
											  
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Calle</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='calleEmpleo' style="width:350px;"
												  data-parsley-maxlength="20"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>N�mero Exterior</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='numeroExteriorEmpleo' style="width:350px;"
												  data-parsley-maxlength="10"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>N�mero Interior</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='numeroInteriorEmpleo' style="width:350px;"
												  data-parsley-maxlength="10"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>C�digo Postal</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='codigoPostalEmpleo' style="width:350px;"
												  data-parsley-maxlength="13"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Delegaci�n/Municipio</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='municipioEmpleo' style="width:350px;"
												  data-parsley-maxlength="20"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Colonia O Fraccionamiento</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='coloniaEmpleo' style="width:350px;"
												  data-parsley-maxlength="40"/></div>
											  </div>											  
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Anitg�edad Empleo Actual</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='antigEmpleoActual' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Anitg�edad Empleo Anterior</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='antigEmpleoAnterior' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Actividad o Giro Empresarial</label></div>
												  <div class='form-group col-sm-8' id="divActividadEmpresarial">
												  </div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Tipo de Actividad Laboral</label></div>
												  <div class='form-group col-sm-8' id="divActividadLaboralEm">
												  </div>
											  </div>									  											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de laborales -->
                 	    
                 	    
                 	    
                 	    <!-- seccion datos de ingresos -->
                 		<div class='panel panel-default' id='datosIngresos'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse7'>Ingresos</a>
								</h4>
							</div>
							<div id='collapse7' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Ingreso Fijo Mensual Comprobable</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='ingresoFijoMensual' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Otros Ingresos Comprobables</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='otrosIngresosComprobables' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Fuente de Otros Ingresos</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='fuenteOtrosIngresos' style="width:350px;"/></div>
											  </div>											  											  									  											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de ingresos -->
                 	    
                 	    
                 	    <!-- seccion datos de referencias -->
                 		<div class='panel panel-default' id='datosReferencias'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse8'>Referencias Personales</a>
								</h4>
							</div>
							<div id='collapse8' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Referencia Familiar</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='nombreRefFamiliar' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>T�lefono</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='telefonoReferencia' style="width:350px;"/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>T�lefono Celular</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='telefonoCelReferencia' style="width:350px;"/></div>
											  </div>											  											  									  											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de referencia -->         
    
    
    
    					<!-- seccion datos de buro -->
                 		<div class='panel panel-default' id='datosBuro'>
							<div class='panel-heading'>
								<h4 class='panel-title'>
									<a data-toggle='collapse' href='#collapse9'>Consulta Bur� Cr�dito</a>
								</h4>
							</div>
							<div id='collapse9' class='panel-collapse collapse in'>
							  <div class='panel-body'>							  
							  		<div>
							  			<form role='form' class='form-inline'>	
				 			  				  <div class="row">
								  				  <div class='form-group col-sm-4'><label>Registro BC</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='registro'/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Fecha</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='fecha'/></div>
											  </div>
											  <div class="row">
												  <div class='form-group col-sm-4'><label>Hora</label></div>
												  <div class='form-group col-sm-8'><input type='text' class='texto' id='hora'/></div>
											  </div>											  											  									  											  											  									  					  				
							  			</form>
							  		</div>							  									  									  
							   </div>
							</div>
					    </div>
                 	    <!-- seccion datos de referencia -->         
    
                 </div>
                 <div class="invalid-form-error-message"></div>
                 </form>
                 
                 
                 <!-- modal buto -->
                 <div id="modalBuro" class="modal fade" role="dialog">
				  <div class="modal-dialog">			
					<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Bur� de Cr�dito</h4>
						  </div>
						  												  						  																				 
						  <div class="modal-body">						  	 
						  		<pre lang="xml" id="stringBuro"></pre>						  	 						  	 						  	 						  	
						  </div>
						  
						  <div class="modal-footer">
							<button type="button" class="btn " data-dismiss="modal" >Cerrar</button>
						  </div>
					</div>
				  </div>
				</div>  
                <!--fin modal buto -->
			</section>
        </div>