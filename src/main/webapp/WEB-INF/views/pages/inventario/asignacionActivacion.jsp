<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<script src="/Originacion/resources/js/hiperion/solicitudesMultiva/asignacionActivacion.js"  type="text/javascript"></script>
<script src="/Originacion/resources/js/hiperion/util/util.js"  type="text/javascript"></script>


<div class="wrapper row-offcanvas row-offcanvas-left">
<section class="content-header">
			<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Activacion
				de Cuenta</h1>
</section>

<!-- Main content -->
<section class="content" id="content">
			<div class="panel panel-default">
				<div id="headPanel" class="panel-heading">
					<label class="control-label">Cuenta Asignada </label>
				</div>
				<div class="panel-body">
					<div class="row col-md-12">
						<div class="col-lg-2">
							<label class="control-label">No. Cuenta: </label>
						</div>
						<div id="cuenta" class="col-lg-6">
							<input id="noCuenta" class="form-control" maxlength="16"
								onkeyup="validateNumber(this);" placeholder="Nmero de Cuenta" />
							<br />
							
						</div>
						<div id="cerrar" class="col-lg-4"></div>
						<button id="irMultiva" type="button" class="btn btn-primary"
								onclick="irListado();">Cerrar</button>
					</div>
					<div class="row">
						<div class="col-lg-12" id="divTablaReglas"></div>
					</div>
				</div>
			</div>

			<div id="divCodAct">
			</div>>
			
<!-- 			<div id="test"  data-keyboard="false" data-backdrop="static">  -->
<!-- 			<img id="imgCode1" src="/Originacion/resources/img/activacion/icon_code_off.png" /> -->
<!-- 			</div> -->






<div class="modal fade" id="chipAndPinModal" data-keyboard="false" 
				data-backdrop="static"  tabindex="-1" role="dialog">
				<div id="modalRemove" class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="text-align: center;">
							<h4 id="ingresarNip" class="modal-title">Ingrese el NIP de
								su Tarjeta de Cr�dito</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<input id="inputNumCuentaModal" value="" type="hidden" />
							
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12" style="text-align: center;">
									<img id="imgCode1" src="/Originacion/resources/img/activacion/icon_code_off.png" />
									<img id="imgCode2" src="/Originacion/resources/img/activacion/icon_code_off.png" />
									<img id="imgCode3" src="/Originacion/resources/img/activacion/icon_code_off.png" />
									<img id="imgCode4" src="/Originacion/resources/img/activacion/icon_code_off.png" />

								</div>
							</div>
							<br /> <br />
							<div class="row" >
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber1"src="/Originacion/resources/img/activacion/icon_code_number1_off.png"
										onclick="onClickNumber(1);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber2"src="/Originacion/resources/img/activacion/icon_code_number2_off.png"
										onclick="onClickNumber(2);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber3"
										src="/Originacion/resources/img/activacion/icon_code_number3_off.png"
										onclick="onClickNumber(3);" />
								</div>
							</div>
							<br />
							<div class="row" >
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber4"
										src="/Originacion/resources/img/activacion/icon_code_number4_off.png"
										onclick="onClickN1umber(4);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber5"
										src="/Originacion/resources/img/activacion/icon_code_number5_off.png"
										onclick="onClickNumber(5);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber6"
										src="/Originacion/resources/img/activacion/icon_code_number6_off.png"
										onclick="onClickNumber(6);" />
								</div>
							</div>
							<br />
							<div class="row" >
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber7"
										src="/Originacion/resources/img/activacion/icon_code_number7_off.png"
										onclick="onClickNumber(7);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber8"
										src="/Originacion/resources/img/activacion/icon_code_number8_off.png"
										onclick="onClickNumber(8);" />
								</div>
								<div class="col-lg-4" style="text-align: center;">
									<img id="imgNumber9"
										src="/Originacion/resources/img/activacion/icon_code_number9_off.png"
										onclick="onClickNumber(9);" />
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-lg-12" style="text-align: center;">
								
									<img id="imgNumber0"
										src="/Originacion/resources/img/activacion/icon_code_number0_off.png"
										onclick="onClickNumber(0);" />
								</div>
								
							</div>
						</div>
						<div class="modal-footer">
						<div class="col-lg-12" style="text-align: center;">
							<button type="button" class="btn" onclick="limpiarPIN();">Limpiar</button>
							<button type="button" class="btn btn-primary"
								onclick="validarPIN();">Aceptar</button>
						
					</div>
				</div>
			</div>





				<div class="modal fade" id="modalLoadingSolicitud" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body" id="bodyModal">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>




			<div class="modal fade" id="loadingModalAccesos" tabindex="-1"
				role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
				data-keyboard="false" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<div class="form-group" style="text-align: center;">
								<label style="font-size: 18px;">Procesando ...</label>
								<div class="bar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			


</section>	

</div>	
		
		
		
<style type="text/css">
.caracs{
    width: 460px !important;
    font-family: 'Poppins', sans-serif;
}
@media (min-width: 768px) { .caracs{
    width: 500px !important;
    font-family: 'Poppins', sans-serif;
} }
@media (min-width: 992px) { .caracs{
    width: 810px !important;
    font-family: 'Poppins', sans-serif;
} }
@media (min-width: 1200px) { .caracs{
    width: 810px !important;
    font-family: 'Poppins', sans-serif;
} }

.modal{
 overflow: scroll;
}

</style>
