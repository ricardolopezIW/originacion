<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">		
		
		<script src="/Originacion/resources/js/hiperion/solicitudesAprobadas/listadoSolicitudesAprobadas.js"  type="text/javascript"></script>
		<script src="/Originacion/resources/js/hiperion/util/util.js"  type="text/javascript"></script>
		

<div class="wrapper row-offcanvas row-offcanvas-left">

	<!-- Content Header (Page header) -->
	<section class="content-header">
	<h1 style="padding-left: 10px; font-size: 25px;">Buscar en el
		Listado de Solicitudes</h1>
	</section>


	<!-- Main content -->
	<section class="content" id="content">



	<form class="form-inline" id="formIngreseNip">
		<div class="row">
			<div class="form-group col-lg-2">
				<!-- Folio -->
				<label for="folio" class="help-block text-muted small-font">Folio</label>
				<input type="text" class="form-control" id="folio" name="folio_apm"
					placeholder="Buscar Folio" size="15px">
			</div>

			<div class="form-group col-md-2">
				<!-- Cuenta -->
				<label for="cuenta" class="help-block text-muted small-font">N&uacute;mero
					de Cuenta</label> <input type="text" class="form-control" id="cuenta"
					name="cuenta_solicitud" placeholder="Buscar cuenta" size="15px">
			</div>

			<div class="form-group col-md-2">
				<!-- RFC -->
				<label for="cuenta" class="help-block text-muted small-font">RFC</label>
				<input type="text" class="form-control" id="rfc"
					name="rfc_solicitud" placeholder="Buscar RFC" size="15px">
			</div>

			<div class="form-group col-md-2">
				<!-- Telefono -->
				<label for="cuenta" class="help-block text-muted small-font">Tel&eacute;fono
					Celular</label> <input type="text" class="form-control" id="telCelular"
					name="telefono_casa" placeholder="Buscar Telefono Celular" size="15px">
			</div>

		</div>

		<div class="row">
			<div class="form-group col-md-4">
				<!-- Nombre -->
				<label for="nombre" class="help-block text-muted small-font">Nombre</label>
				<input type="text" class="form-control" id="nombre" name="nombre"
					placeholder="Buscar nombre" size="46px">
			</div>

			<div class="form-group col-md-4">
				<!-- Apellido Paterno -->
				<label for="aPaterno" class="help-block text-muted small-font">Apellido
					Paterno</label> <input type="text" class="form-control" id="aPaterno"
					name="aPaterno" placeholder="Buscar Apellido Paterno" size="46px">
			</div>

			<div class="form-group col-md-4">
				<!-- Apellido Materno -->
				<label for="aMaterno" class="help-block text-muted small-font">Apellido
					Materno</label> <input type="text" class="form-control" id="aMaterno"
					name="aMaterno" placeholder="Buscar Apellido Materno" size="46px">
			</div>

		</div>
		<br />
		<div class="panel-footer">
			<div class="col-lg-12">
				<input type="button" id="btnBuscar" class="btn btn-primary"
					value="Buscar" style="padding: 15px"  onclick="obtenerSolicitudes()"/> <br />
				<br />
			</div>
		</div>
	</form>
	<hr></hr>


	<!-- Tabla Solicitudes -->
	<div class="col-lg-12">
		<table id="tblSolicitudesAprobadas" class="table table-striped">
		</table>
	</div>
	
	
	<!-- Modal de carga -->

	<div class="modal fade" id="modalLoadingSolicitudGeneral" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
		data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" id="bodyModal">
					<div class="form-group" style="text-align: center;">
						<label style="font-size: 18px;">Cargando ...</label>
						<div class="bar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</section>
	<!-- /.content -->
</div>