		<script src="${pageContext.request.contextPath}/resources/js/hiperion/cargaCofetel/cargaCofetel.js"  type="text/javascript"></script>
        
        <script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.ui.widget.js"  type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.iframe-transport.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.fileupload.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.ui.widget.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-process.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-validate.js"  type="text/javascript"></script>
		<link href="${pageContext.request.contextPath}/resources/fileUpload/css/jquery.fileupload.css"  rel="stylesheet"/>
        
         		
        <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 style="padding-left: 10px; font-size: 25px;">Carga Cofetel</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                	
                	<div class="row">
                		<div class="col-lg-6">
		                	<div class="row">		                		
		                		<div class="col-lg-11 vcenter">
		                			<blockquote>
		                			   <p class=".text-justify">
		                				1.-Para iniciar el proceso de Carga de Cofetel, dar click sobre la imagen de la derecha.
		                			    </p>
		                			   <p class=".text-justify">
		                			   2.-Se abrira una ventana para seleccionar un archivo con la extensi�n ".csv", del cu�l se obtendr�n
		                			   los datos a procesar.
		                			   </p>
		                			   <p class=".text-justify">
		                			    3.-Se iniciar� el proceso de actualizaci�n durante la noche.
		                			   </p>
		                			 </blockquote>
		                		</div>
		                		
		                		<div class="col-lg-1">		                		
			                		<span class="btn btn-success fileinput-button" id="spload">
								        <i class="glyphicon glyphicon-plus"></i>
								        <span>Archivo Cofetel...</span>
								        <!-- The file input field used as target for the file upload widget -->
								        <input id="fileupload" type="file" name="files[]" />
								    </span>								    
		                        </div>
		                       
		                       <div class="col-lg-1">		                		
			                	</div>
		                      	                      
		                    </div>	
		                    
		                    <div class="row">			
                		    	<h3 style="padding-left: 10px; font-size: 25px;">Informaci�n �ltima Actualizaci�n</h3>
                		    </div>
                		    
		                    <div class="row">			
                		                			
							  <form class="form-group col-xs-10" >
									<div class="form-group col-xs-4 col-sm-10">
									  <label for="numIntento">Fecha Actualizaci�n</label>
									  <input type="text" class="form-control input-sm" id="fecha"/>
									</div>											
							 
							 		<div class="form-group col-xs-4 col-sm-10">
							 		  <br/>
									  <label for="horas">Registros Actualizados</label>
									  <input type="text" class="form-control input-sm" id="actualizados"/>
									</div>							
							  
							  		<div class="form-group col-xs-4 col-sm-10">
							  		  <br/>
									  <label for="token">Total Registros</label>
									  <input type="text" class="form-control input-sm" id="total"/>
									</div>							
							  </form>	 
						    </div>	                			                	    
						</div>
					</div>
                	
                </section><!-- /.content -->                                       
        </div><!-- ./wrapper -->