		<script src="${pageContext.request.contextPath}/resources/js/hiperion/watchlist/orDirectorioWatchList.js"  type="text/javascript"></script>

		<style>
			.parsley-errors-list {
				color: red;
			}
		</style>

		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Registros WatchList</h1>
			</section>

			<!-- Main content -->
			<section class="content">

				<div class="panel panel-default">
					<div id="headPanel" class="panel-heading">
						<label class="control-label">Registros Existentes </label>
					</div>
					<div class="panel-body">
						<div class="row col-md-12"></div>
						<div class="row">
							<table width="95%" class="table table-striped" id="divTablaRegistros"></table>
						</div>
					</div>
				</div>
			</section>


			<!--modales-->
			<div id="modalEditarWatchList" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content" style="width: 910px;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Edicion WatchList</h4>
						</div>

						<div class="modal-body">

							<form id="formCargaWatch" class="form-inline col-xs-12"
								data-parsley-validate="">

								<input id="id" type="hidden" />
								
								<div class="container" id="formaCarga">
									<div class="row">
										<div class="col-lg-2">
											<label for="origen">Origen</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="origen"
												data-parsley-required="true" data-parsley-maxlength="50" />
										</div>
										<div class="col-lg-2">
											<label for="lugarVenta">Lugar de Venta</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm"
												id="lugarVenta" data-parsley-required="true" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="caso">Caso</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="caso"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="numero">N�mero Cuenta</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="numero"
												data-parsley-maxlength="16" data-parsley-minlength="16"
												data-parsley-type="number" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="tokenCreado">Nombre</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="nombre"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="calle">Calle</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="calle"
												data-parsley-required="true" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="paterno">Apellido Paterno</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="paterno"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="ext">N�mero Exterior</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="ext"
												data-parsley-required="true" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="materno">Apellido Materno</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="materno" />
										</div>
										<div class="col-lg-2">
											<label for="int">N�mero Interior</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="int" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="curp">Curp</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="curp" />
										</div>
										<div class="col-lg-2">
											<label for="colonia">Colonia</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="colonia"
												data-parsley-required="true" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="rfc">RFC</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="rfc"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="codigoPostal">C�digo Postal</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm"
												id="codigoPostal" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="empresa">Empresa donde labora</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="empresa"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="municipio">Municipio</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm"
												id="municipio" data-parsley-required="true" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="estado">Estado</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="estado"
												data-parsley-required="true" />
										</div>
										<div class="col-lg-2">
											<label for="telCasa">Tel�fono Casa</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="telCasa"
												data-parsley-required="true" data-parsley-maxlength="10"
												data-parsley-type="number" />
										</div>
									</div>
									<div class="row">
										<br />
										<div class="col-lg-2">
											<label for="telOfi">Tel�fono Oficina</label>
										</div>
										<div class="col-lg-2">
											<input type="text" class="form-control input-sm" id="telOfi"
												data-parsley-maxlength="10" data-parsley-type="number" />
										</div>
									</div>

									<div class="row">
										<br />
										<div class="form-group col-xs-2">
											<label for="observaciones">Observaciones</label>
										</div>
										<div class="form-group col-xs-2">
											<textarea class="form-control" rows="5" id="observaciones"></textarea>
										</div>
									</div>



									<br />
									<!-- <div class="form-group col-xs-2"></div> -->
									<!-- 	<div class="form-group">
										<button type="button" class="btn" id="limpiar">Limpiar</button>
									</div>
									<div class="form-group">
										<button type="button" class="btn" id="guardar">Guardar</button>
									</div> -->
								</div>

							</form>



						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-primary " data-dismiss="modal">Cerrar</button>
							<button type="button" class="btn btn-primary " id="editWatchList">Guardar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./wrapper -->