        <script src="${pageContext.request.contextPath}/resources/js/hiperion/cargaFoliosLowes/cargaFoliosLowes.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/js/hiperion/cargaFoliosLowes/simple-excel.js"  type="text/javascript"></script>


		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.iframe-transport.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/jquery.fileupload.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.ui.widget.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-process.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/fileUpload/vendor/jquery.fileupload-validate.js"  type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/resources/dataTables-1.10.16/Buttons-1.5.1/js/dataTables.buttons.min.js"  type="text/javascript"></script>
		
		<link href="${pageContext.request.contextPath}/resources/dataTables-1.10.16/media/css/jquery.dataTables.min.css"  rel="stylesheet"/>
		<link href="${pageContext.request.contextPath}/resources/dataTables-1.10.16/Buttons-1.5.1/css/buttons.dataTables.min.css"  rel="stylesheet"/>
		<link href="${pageContext.request.contextPath}/resources/fileUpload/css/jquery.fileupload.css"  rel="stylesheet"/>
		
		<script src="${pageContext.request.contextPath}/resources/jquery-table2excel/jquery.table2excel.js"></script>
	
		

		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1 id="titlePage" style="padding-left: 10px; font-size: 25px;">Carga Folios Lowe's</h1>
			</section>

			<!-- Main content -->

			<div class="panel panel-default">
				<div class="panel-heading">Cargar Folios</div>
				<div class="panel-body">
					<!-- Carga de Folios Lowes -->
					<div class="col-lg-1">
						<span class="btn btn-success fileinput-button" id="spload">
							<i class="glyphicon glyphicon-plus"></i> <span>Seleccionar
								Archivo...</span> <!-- The file input field used as target for the file upload widget -->
							<input id="input" type="file" name="files[]" />
						</span>

						<div id="result"></div>
						<table id="grid1"></table>

					</div>
				</div>
				<div class="panel-heading">Buscar Folios</div>
				<div class="panel-body">
					<div class="container">
						<div class="row">
							<div class="col-sm-2">
								<label>Fecha de Asignacion</label>
							</div>
							<div class='col-sm-3'>
								<div class="form-group">
									<div class='input-group date' id='datetimepicker1'
										data-provide="datepicker">
										<input type='text' class="form-control" id="fecha" />
										<span class="input-group-addon" id="calenda"> <span
											class="glyphicon glyphicon-calendar" id="cale"></span>
										</span>
									</div>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-sm-2">
								<label>Disponible</label>
							</div>
							<div class="form-group col-xs-2">
								<input id="checkDisponible" data-toggle="toggle" type="checkbox"
									data-onstyle="success" data-size="small"
									onchange="swSwitch($(this));" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" id="buscar">Buscar...</button>
							</div>
						</div>


						<div class="col-lg-10">
							<table id="tablaFolios" class="table table-striped">
							</table>
						</div>

					</div>

				</div>
			</div>

			<section class="content"></section>
			<!-- /.content -->
		</div>
		<!-- ./wrapper -->

		<!-- Modal Folios Erroneos -->
		<div class="modal fade" id="modalFolios"  tabindex="-1" role="dialog"  aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Error en los siguientes folios</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
					</div>
					<div class="modal-body" id="bodyFolios">
						
							<button id="btn-export" data-toggle="modal"
								class="btn btn-primary center-block">
								<img src="${pageContext.request.contextPath}/resources/img/Excel-icon.png" width="9%" height="5%" />Exportar...
							</button>

							<table id="tablaFoliosrError" class="table table-striped">
							</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>


<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false"
	data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="form-group" style="text-align: center;">
					<label style="font-size: 18px;">Cargando ...</label>
					<div class="bar"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>




