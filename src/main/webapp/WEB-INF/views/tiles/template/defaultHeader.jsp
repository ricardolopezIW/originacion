	<a href="#" class="logo"> 
		
	</a>
	
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span> 
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		
		<div class="navbar-right">
				<ul class="nav navbar-nav">
			
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle"> 
							<input id="hiddenIdUsuario" type="hidden" value="${idUsuario}" />
							<input id="hiddenIdPerfil"  type="hidden" value="${idPerfil}" />
							<input id="hiddenUser"      type="hidden" value="${usuario}" />
							<input id="urlRestBackOffice" type="hidden" value="${urlRest}"/> 
							<i class="glyphicon glyphicon-user"></i>
							<span><label class="control-label">${usuario}</label></span>
						</a>
					</li>
					
					<li class="dropdown user user-menu">
					 	<a href="${pageContext.request.contextPath}/logout"	class="dropdown-toggle">
							<img src="${pageContext.request.contextPath}/resources/img/logout.png" align="middle" alt="Salir" />
						</a>
					</li>
				</ul>
		</div>
	</nav>