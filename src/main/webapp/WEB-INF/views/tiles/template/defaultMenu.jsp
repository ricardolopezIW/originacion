	<style>
		.nav>li>a {
		    padding: 10px 25px;
    	}
    	.menuPadre{
    		font-weight: bold;
    	}
    	.menuHijo{
    		font-size: 12px;
    	}
	</style>
	
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left info">
				<p>Bienvenido</p>
			</div>
		</div>
				
		<!--menu-->
 		<div>
			<nav class="vertical-menu">
				<div id="menu">
				</div>
			</nav>
		</div>
	</section>
	<script>
   		var requestContextPath = '${pageContext.request.contextPath}';
   		var menu = '${menu}';
	</script>
	<script src="${pageContext.request.contextPath}/resources/js/hiperion/general/menu.js" type="text/javascript"></script>
		