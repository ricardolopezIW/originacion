<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 
<html>
 
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
	
	    <title>ORIGINACION</title>
		
    	<link href="<c:url value="/resources/theme/AdminLTE.css" />" rel="stylesheet" type="text/css" />
		<link href="<c:url value="/resources/bootstrap3.3.6/dist/css/bootstrap.min.css"/>" rel='stylesheet'/>
    	<link href="<c:url value="/resources/theme/estilos.css" />" rel="stylesheet" type="text/css" />		    
	</head>
  
	<body>

		<div align="middle">
	    	<div class="row">
		    	<div class="col-sm-offset-4 col-sm-10 text-center">
			        <div class="col-md-12 col-md-5 center login-box">
			        <p/>
			        	<fieldset>
							<div class="row">
						        <div class="col-md-12 login-header">
<%-- 						            <img src="${pageContext.request.contextPath}/resources/img/invex_inicio.png" align="middle" class="img-responsive" width="100%"/> --%>
						        </div>
						        <!--/span-->
						    </div><!--/row-->
						    <br/>
						    
						   	<c:url var="loginUrl" value="/login" />
						   	
							<form action="${loginUrl}" method="post" class="form-group">
								<c:if test="${error != null}">
									<div class="alert alert-danger">
										<p>Usuario o contraseña inválida.</p>
									</div>
								</c:if>
								<c:if test="${param.logout != null}">
									<div class="alert alert-success">
										<p>Has salido de la aplicación.</p>
									</div>
								</c:if>
								<div class="input-group input-group-lg">
			                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
									<input type="text" class="form-control" id="username" name="ssoId" placeholder="Usuario" required>
								</div>
								<div class="input-group input-group-lg">
			                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
									<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" required>
								</div>
								<input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
		
								<br><br>
															
								<div class="form-actions">
									<input type="submit" class="btn btn-block btn-primary" value="Ingresar">
								</div>
							</form>
								   	   
							<section id="site-content" >
							     <tiles:insertAttribute name="body" />
							</section>
								
				            <br />
						</fieldset>
			        </div>
			        <!--/span-->
				</div>
		    </div><!--/row-->
		</div><!--/fluid-row-->       
	</body>
</html>