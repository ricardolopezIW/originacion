<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 
<html>
 
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
	
	    <title>ORIGINACION</title>
	    		
	    <!-- Libreria verificadas refactor -->
		
			<!-- jQuery 2.0.2 -->
			<script src="<c:url value="/resources/jquery/jquery.js" />"></script>
			<script src="<c:url value="/resources/jquery/moment.min.js"/>" type="text/javascript"></script>
			
			<script src="<c:url value="/resources/jquery-ui-1.12.1/jquery-ui.min.js"/>"></script>
			
			<!-- Bootstrap 3.3.6 -->
		    <link href="<c:url value="/resources/bootstrap3.3.6/dist/css/bootstrap.min.css"/>" rel='stylesheet'/>
			<script src="<c:url value="/resources/bootstrap3.3.6/dist/js/bootstrap.min.js"/>"></script>
		
			<!-- DataTable -->
			<script src="<c:url value="/resources/dataTables-1.10.16/media/js/jquery.dataTables.min.js"/>"></script>
			<script src="<c:url value="/resources/dataTables-1.10.16/media/js/dataTables.bootstrap.min.js"/>"></script>
			<link href="<c:url value="/resources/dataTables-1.10.16/datatables.min.css"/>"  rel="stylesheet"/>
			<link href="<c:url value="/resources/dataTables-1.10.16/Responsive-2.2.1/css/responsive.dataTables.min.css"/>" rel="stylesheet" />
		
			<!-- JQuery Alerts -->
			<link rel="stylesheet" href="<c:url value="/resources/jquery-confirm/3.3.0/jquery-confirm.min.css"/>" />
			<script src="<c:url value="/resources/jquery-confirm/3.3.0/jquery-confirm.min.js"/>"></script>
		      	   
			<!-- Validator Parsley 2.7 -->
			<link href="<c:url value="/resources/parsley/parsley.css"/>" rel='stylesheet'/>
			<script src="<c:url value="/resources/parsley/parsley.js"/>"></script>
			<script src="<c:url value="/resources/parsley/parsley.min.js"/>"></script>
			<script src="<c:url value="/resources/parsley/es.js"/>"></script>  
	
			<!-- ListBox -->
			<link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap-listbox/src/bootstrap-duallistbox.css"/>" />
			<script src="<c:url value="/resources/bootstrap-listbox/src/jquery.bootstrap-duallistbox.js"/>"></script>

			<!-- bootstrap-toggle -->
			<link href="<c:url value="/resources/bootstrap-toggle/css/bootstrap-toggle.min.css"/>" 	rel="stylesheet" />
			<script src="<c:url value="/resources/bootstrap-toggle/js/bootstrap-toggle.min.js"/>"></script>
	
			<!-- Datepicker -->
			<link id="bsdp-css" rel="stylesheet" href="<c:url value="/resources/datepicker/datepicker.css"/>" />
			<script type="text/javascript" src="<c:url value="/resources/datepicker/bootstrap-datepicker.js"/>"></script>
			<script type="text/javascript" src="<c:url value="/resources/datepicker/bootstrap-datetimepicker.es.js"/>" charset="UTF-8"></script>
	
			<!-- Bootstrap Validator -->
	    	<link href="<c:url value="/resources/bootstrapValidator/bootstrapValidator.min.css"/>" rel='stylesheet'/>
	    	<script src="<c:url value="/resources/bootstrapValidator/bootstrapValidator.min.js"/>"></script>	    
	    
	    	<!-- Theme style -->
			<script src="<c:url value="/resources/theme/app.js"/>" type="text/javascript"></script>
			<link href="<c:url value="/resources/theme/AdminLTE.css" />" rel="stylesheet" type="text/css" />
			<link href="<c:url value="/resources/theme/estilos.css" />" rel="stylesheet" type="text/css" />
	
			<script>
		    	var requestContextPath = '${pageContext.request.contextPath}';
			</script>
		<!-- Fin verificacion -->	
			
		<!-- CheckBox -->
		<script src="<c:url value="/resources/bootstrap-checkbox/bootstrap-checkbox.min.js"/>"></script>
	
	</head>
  
	<body class="skin-blue">
	        <header id="header" class="header">
	            <tiles:insertAttribute name="header" />
	        </header>
	     
	     	<div class="wrapper row-offcanvas row-offcanvas-left">
		        <section id="sidemenu" class="left-side sidebar-offcanvas">
		            <tiles:insertAttribute name="menu" />
		        </section>
		             
		        <section id="site-content" class="right-side">
		            <tiles:insertAttribute name="body" />
		        </section>
		        
		        <!-- modal carga -->
		        <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="form-group" style="text-align: center;">
									<label style="font-size: 18px;">Cargando ...</label>
									<div class="bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
	</body>
</html>