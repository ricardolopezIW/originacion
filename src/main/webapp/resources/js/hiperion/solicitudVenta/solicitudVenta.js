    var contadorSeccion  = 10;
    var idSolicitudVenta = getURLParameter('idSolicitud');
	var restUrl          = $('#urlRestBackOffice').val();
	//var restUrl          = "http://localhost:8081/siditOriginacion/backOffice"; //$('#urlRestBackOffice').val();
	var restBuro         = "Hiperion/ServiciosOrigen/buro/consulta-xml/";
	var restManual       = restUrl + "/solicitud-venta/{id}/cuenta/manual";
	var restAutomatico   = restUrl + "/solicitud-venta/{id}/cuenta/automatica";
	var restActualizar   = restUrl + "/solicitud-venta/cuenta-actualizar/{id}";
	var restActualizarForma = restUrl + "/solicitud-venta/solicitud-actualizar/{id}";
	var restCatalogo     = restUrl + "/catalogo/lista"
	var tipoAsignacion;
	var error;
	var flujoCuenta      = false;
	var estadoSolicitud;
	var activar			 = false; 
	var producto 		 = '';
	var editarSolicitud  = false;
	var catalogoParentesco = [];

	$(document).ready(function(){	
		restBuro       = restUrl.substring(0, restUrl.indexOf("si")) + restBuro;
		restManual     = restManual.replace("{id}", idSolicitudVenta);
		restAutomatico = restAutomatico.replace("{id}", idSolicitudVenta);
		restActualizar = restActualizar.replace("{id}", idSolicitudVenta);
		restActualizarForma = restActualizarForma.replace("{id}", idSolicitudVenta);
		obtenerCatalogos();
	});
	
	function obtenerCatalogos(){
		$.ajax({
			url : restCatalogo,
			type : 'get',
			contentType : "application/json",
			success : function(response) {
				console.log(response)
				$("#divSexo                   ").append(crearCombos(response.sexo, "sexo"));
				$("#divNacionalidad           ").append(crearCombos(response.nacionalidad, "nacionalidad"));
				$("#divEstadoCivil            ").append(crearCombosValor(response.estadoCivil, "estadoCivil"));
				$("#divEstudios               ").append(crearCombosValor(response.estudios, "maximoEstudios"));
				$("#divTieneCreditoAutomotriz ").append(crearCombos(response.respuestaSiNo, "tieneCreditoAutomotriz"));
				$("#divTieneCreditoHipotecario").append(crearCombos(response.respuestaSiNo, "tieneCreditoHipotecario"));
				$("#divTieneTDC               ").append(crearCombos(response.respuestaSiNo, "tieneTDC"));
				$("#divTipoActividadLaboral   ").append(crearCombosValor(response.actividadLaboral, "tipoActividadLaboral"));
				$("#divTipoVivienda           ").append(crearCombosValor(response.tipoVivienda, "tipoVivienda"));
				$("#divAutenticaOtroDomicilio ").append(crearCombos(response.respuestaSiNo, "autenticaOtroDomicilio"));
				$("#divAutorizacionMercadeo   ").append(crearCombos(response.respuestaSiNo, "autorizacionMercadeo"));
				$("#divPreguntaActivacion     ").append(crearCombos(response.preguntaActivacion, "preguntaActivacion"));
				$("#divActividadEmpresarial   ").append(crearCombosValor(response.actividadEmpresarial, "actividadEmpresarial"));
				$("#divActividadLaboralEm     ").append(crearCombosValor(response.actividadLaboral, "tipoActividadLaboralEm"));
				catalogoParentesco = response.parentesco;
				var solicitudVenta = obtenerDatos(idSolicitudVenta);
			},
		    error: function(xhr) {
		        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);			     
		    }
		});
	}
	
	function obtenerDatos(idSolicitudVenta){
		$.ajax({
			url : restUrl + '/solicitud-venta/'+idSolicitudVenta,
			type : 'get',
			contentType : "application/json",
			success : function(response) {
				console.log(response)
				var solicitudVenta = { 
						venta: response.venta, sistema: response.sistema, personales:response.personal,
						domicilio:response.domicilio, adicionales:response.adicional,
						laborales:response.laborales, ingresos:response.ingresos    ,
						referencias:response.referencias, 
						buro:response.buro, 
						buroEncabezadosSegmentos:response.buroEncabezadosSegmentos, buroDatosSegmentos:response.buroDatosSegmentos,
						buroEncabezadosDirecciones:response.buroEncabezadosDirecciones, buroDatosDirecciones:response.buroDatosDirecciones,
						editarDatos:response.editarDatos,
						asignacion:response.asignacion, verActualizarTsys:response.verActualizarTsys, verAsignar:response.verAsignar,
						verAsignarActualizar: response.verAsignarActualizar, verBuro : response.verBuro,
						numeroCuenta : response.numeroCuenta, checklist:response.checklist,
                        verBtnRechazar: response.verBtnRechazar , verBtnRevision : response.verBtnRevision,
                        tarjetaAdicional : response.tarjetaAdicional
						}
				estadoSolicitud = response.estado;
				tipoAsignacion 	= response.asignacion;
				activar 		= response.verBtnActivacion;
				producto 		= response.venta.producto;
				editarSolicitud = response.editarDatos;
				console.log(tipoAsignacion)
				crearFormaSolicitud(solicitudVenta);
				
				if(response.recalculoCat){
					mostrarMensajeAdvertencia("Se ha aplicado el rec\u00E1lculo del CAT.");
				}
			},
		    error: function(xhr) {
		        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);			     
		    }
		});
	}
				
	function verBuro(){
		$.ajax(
		{
			url:   restBuro + $("#folioApm").val(),
		    type:  'get',
		    contentType: "application/json",
		    success:  function (response)
		    {			    	
		    	if(response.codError == 0){		    	
		    	   $("#stringBuro").text(response.xmlRepsonse);
		    	   $('#modalBuro').modal('show');
		    	}
		    	else{
		    		mostrarMensajeAdvertencia(response.msgError);
		    	}		    	
		    }
		});
	}

	function validarNumeoCuenta(numCuenta){
		var numCuentaConfirm = $("#numCuentaconfirm").val();
		var mensaje = "";
		if(numCuenta.length != 16 || numCuentaConfirm.length != 16 && mensaje == ""){
			mensaje = "El numero de cuenta debe ser de 16 digitos.";
		}
		if(isNaN(numCuenta) ||  isNaN(numCuentaConfirm) && mensaje == "" ){
			mensaje = "El numero de cuenta debe ser solo numeros.";
		}
		if(!cuentaVerificada() && mensaje == ""){
			
			mensaje = 'El numero de tarjeta no coincide';
			
		}
		
		$("#alertasConfirmacion").remove();
		var alerta = "<div class='alert alert-danger' id='alertasConfirmacion'>"+
			"<strong>Error! </strong> "+mensaje+
		  "<br>"+
		"</div>";
		$("#accordion").find("div[id='datosAsignacion']").find("div.panel-body").append(alerta);
		$("#alertasConfirmacion").hide();
		
		return mensaje;
	}
	
	function asignar(){
			callAsignar();
		
	}

	function callAsignar(actualizar){
		if(tipoAsignacion == "MANUAL")
			asginarManual(actualizar);
		else
			asignarAutomatico(actualizar);
	}
	
	function asginarManual(actualizar){
		var mensaje = validarNumeoCuenta($("#numCuenta").val());
		
		if(mensaje == ""){
			var request = { "noCuenta": $("#numCuenta").val(),
							"usuario": $("#hiddenUser").val()
						};
			var data    = JSON.stringify(request);
			
			$.ajax({
				data : data,
				url  : restManual,
				type : 'post',
				contentType : "application/json",
				beforeSend : function() {
					$("#loadingModalAccesos").modal('show');
				},
				success : function(response) {
					$("#loadingModalAccesos").modal('hide');
					if(response.estatus == "200"){
						mostrarMensajeAdvertencia(response.mensaje);   
						flujoCuenta = "ACTUALIZAR";
						$("#btAsignar").hide();
						$("#btAsignarAct").hide();
			        	$("#numCuenta").attr("disabled",true);
			        	$("#numCuentaconfirm").attr("disabled",true);
						$("#btActualizar").show();

						if(response.noCuentaAdicional != undefined){
                            $("#divNumCuentaAdicionalResp").removeClass("hide");
                            $("#numCuentaAdicionalResp").val(response.noCuentaAdicional).attr("disabled",true);
                            $("#divNumCuentaConfirmacion").addClass("hide");
                        }

						if(actualizar != null){
							actualizar();
						}
					}
					else{
						error = true;
					}
				},
			    error: function(xhr) {
			    	$("#loadingModalAccesos").modal('hide');
			        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);
			        error = true;
			    }
			});
		}
		else{
			mostrarMensajeAdvertencia(mensaje);
		}
	}
	
	function asignarAutomatico(actualizar){
		var request = { "usuario": $("#hiddenUser").val()
						};
		var data = JSON.stringify(request);
		
		$.ajax({
			data : data,
			url  : restAutomatico,
			type : 'post',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(response) {
				$("#loadingModalAccesos").modal('hide');
				if(response.noCuenta != null && response.noCuenta != ""){
					mostrarMensajeAdvertencia("Se ha asignado la cuenta.");   
					flujoCuenta = "ACTUALIZAR";
					$("#btAsignar")   .hide();
		        	$("#numCuenta").val(response.noCuenta).attr("disabled",true);
					$("#btActualizar").show();
					$("#btAsignarAct").hide();
					$("#divNumCuenta").show();

					if(response.noCuentaAdicional != undefined){
					    $("#divNumCuentaAdicionalResp").removeClass("hide");
					    $("#numCuentaAdicionalResp").val(response.noCuentaAdicional).attr("disabled",true);
					}

					if(actualizar != null){
						actualizar();
					}
				}			      
				else{
					error = true;
				}
			},
		    error: function(xhr) {
		    	$("#loadingModalAccesos").modal('hide');
		        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);
		        error = true;
		    }
		});	
	}
	
	function actualizar(){
		callActualizar();
	}

	function callActualizar(){
		var data = JSON.stringify({
			  "usuario": $("#hiddenUser").val(),
			  "sidit"  : true
			});
	
		$.ajax({
			data : data,
			url  : restActualizar,
			type : 'put',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(response) {
				$("#loadingModalAccesos").modal('hide');
				if(response.estatus == "200"){
					mostrarMensajeAdvertencia(response.mensaje);
				   $("#btActualizar").hide();
				   $("#btActFormulario").hide();
				   $("div.modal-backdrop").hide();
				   mostrarActivacion(activar);
				}
				
			},
		    error: function(xhr) {
		    	$("#loadingModalAccesos").modal('hide');
		    	$("div.modal-backdrop").hide()
		        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);			     
		    }
		});	
	}

	function callActualizarForma(){


		
		var datos = obtenerDatosForma();
		var request = { "solicitud" : datos,
						"usuario": $("#hiddenUser").val()
					};
		var data = JSON.stringify(request);
	
		$.ajax({
			data : data,
			url  : restActualizarForma,
			type : 'put',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(response) {
				$("#loadingModalAccesos").modal('hide');
				if(response.estatus == "200"){
				   mostrarMensajeAdvertencia(response.mensaje);
				   $("div.modal-backdrop").hide();
				   deshabilitarForma(true);
				}			        			   
			},
		    error: function(xhr) {
		    	$("#loadingModalAccesos").modal('hide');
		    	$("div.modal-backdrop").hide()
		        mostrarMensajeAdvertencia("Error comunicar con el Administrador.");			     
		    }
		});	
	}

	
	function callActualizarFormaRechazar(){
		var datos = obtenerDatosForma();
		datos.edoRechazado = true;
		var request = { "solicitud" : datos,
						"usuario": $("#hiddenUser").val()
					};
		var data = JSON.stringify(request);
	
		$.ajax({
			data : data,
			url  : restActualizarForma,
			type : 'put',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(response) {
				$("#loadingModalAccesos").modal('hide');
				if(response.estatus == "200"){
					mostrarMensajeAdvertencia(response.mensaje);
				   $("div.modal-backdrop").hide();
				   deshabilitarForma(true);
				   irListado();
				}			        				
			},
		    error: function(xhr) {
		    	$("#loadingModalAccesos").modal('hide');
		    	$("div.modal-backdrop").hide()
		        mostrarMensajeAdvertencia("Error comunicar con el Administrador.");			     
		    }
		});	
	}

	function asignarActualizar(){
		error          = false;
		var data = obtenerDatosForma();

		if(tipoAsignacion == "MANUAL"){
			var mensaje = validarNumeoCuenta($("#numCuenta").val());
			if(mensaje != ""){
				$("#alertasConfirmacion").show();
				error = true;
			}
		}
		if(!error){
		   callAsignar(actualizar);
		}
	}
	
	function mostrarActivacion(activacion){
		console.log('Permitir activacion: ' + activacion);
		
		if (activacion) {
			$("#datosActivacion").show();
		}
		
		
	}
	
	function cuentaVerificada(){
		console.log('Verificando cuenta');
		var cuenta1 = $("#numCuenta").val();
		var cuenta2 = $("#numCuentaconfirm").val();
		if (cuenta1 == cuenta2){
			return true;
		}else
			return false;
		
		
	}
	
	


	function validarAlGuardar(fn_name) {
	if (editarSolicitud) {
		if ($('#formGeneral').parsley().validate()) {
			fn_name();
		} else {
			mostrarMensajeAdvertencia("Corregir errores en el formulario");
			modificarSolicitud();
		}

	} else {
		fn_name();
	}
}
	
	
	function confirmaAccion(fn_name){

		$.confirm({
		    title: 'Rechazar Solicitud',
		    content: '¿Desea Continuar?',
		    buttons: {
		    	
		        confirm:{ 
		        	text   : "Confirmar",
		        	action : function () {
		        	fn_name();
		        }
		        },
		        cancel:  {
		           text : "Cancelar"
		        }
		    }
		});

		
	}

	
	function getURLParameter(name) {
	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}