	var containsAdicional = false;

	function crearCombos(catalogo, id){
		  var selectList = $('<select class="form-control input-sm" id="'+id+'">');		  		  	 
		  selectList.append($('<option>').attr('value', '-1').text("Selecciona Valor"));
		  for(var i=0; i<catalogo.length; i++){
			    selectList.append($('<option>').attr('value', catalogo[i].id).text(catalogo[i].valor));				
		  }
		  return selectList;
	}

	function crearCombosValor(catalogo, id){
		  var selectList = $('<select class="form-control input-sm" id="'+id+'">');		  		  	 
		  selectList.append($('<option>').attr('value', '-1').text("Selecciona Valor"));
		  for(var i=0; i<catalogo.length; i++){
			    selectList.append($('<option>').attr('value', catalogo[i].valor.trim()).text(catalogo[i].descripcion));				
		  }
		  return selectList;
	}
	
	function crearFormaSolicitud(solicitudVenta){
		if(solicitudVenta.venta != null){
			crearSeccionDatos(solicitudVenta.venta);
			$("#folioApm").attr("disabled","false");
	    	$("#lugarVenta").attr("disabled","false");
	    	$("#producto").attr("disabled","false");	    		    	
		}
	    if(solicitudVenta.sistema != null){
			crearSeccionDatos(solicitudVenta.sistema);
		}
		if(solicitudVenta.personales != null){			
			crearSeccionDatos(solicitudVenta.personales);
		}	
		if(solicitudVenta.domicilio != null){
			crearSeccionDatos(solicitudVenta.domicilio);
		}
		if(solicitudVenta.adicionales != null){
			crearSeccionDatos(solicitudVenta.adicionales);
		}	
		if(solicitudVenta.laborales != null){
			$("#actividadEmpresarial").val(solicitudVenta.laborales.actividadEmpresarial);
			$("#tipoActividadLaboralEm").val(solicitudVenta.laborales.actividadLaboral);			
			crearSeccionDatos(solicitudVenta.laborales);
		}
		if(solicitudVenta.ingresos != null){
			crearSeccionDatos(solicitudVenta.ingresos);
		}	
		if(solicitudVenta.referencias != null){
			crearSeccionDatos(solicitudVenta.referencias);
		}
		if(solicitudVenta.buro != null){
			crearSeccionBuro(solicitudVenta.buro, solicitudVenta.buroEncabezadosSegmentos, solicitudVenta.buroDatosSegmentos,
							 solicitudVenta.buroEncabezadosDirecciones, solicitudVenta.buroDatosDirecciones);
		}		
		if(solicitudVenta.operaciones != null){
			crearSeccionAcciones();
		}
		if(solicitudVenta.checklist != null){
			crearSeccionCheckList(solicitudVenta.checklist);
		}

        if(solicitudVenta.tarjetaAdicional != null){
            crearSeccionDatos(solicitudVenta.tarjetaAdicional);
            $("#datosTarjetaAddicional").removeClass("hide");
            crearComboParentescoAdicional(solicitudVenta.tarjetaAdicional.parentescoAdicional);
            containsAdicional = true;

        } else {
            $("#datosTarjetaAddicional").addClass("hide");
            containsAdicional = false;
        }

		var acciones = { verActualizarTsys:solicitudVenta.verActualizarTsys, 
                         verAsignar:solicitudVenta.verAsignar,
                         verAsignarActualizar: solicitudVenta.verAsignarActualizar,
                         editarDatos:solicitudVenta.editarDatos, verBuro : solicitudVenta.verBuro,
                         numeroCuenta : solicitudVenta.numeroCuenta,
                         verBtnRechazar: solicitudVenta.verBtnRechazar , verBtnRevision : solicitudVenta.verBtnRevision
                         };
        if(containsAdicional){
            acciones.cuentaAdicional = solicitudVenta.tarjetaAdicional.cuentaAdicional;
		}

		crearSeccionAcciones(acciones);
		crearSeccionAsignacion(solicitudVenta.asignacion, acciones);
		crearSeccionActivacion(true);
		crearSeccionBotones();
		deshabilitarForma(true);
	}

	function crearComboParentescoAdicional(parentescoId){
        if(catalogoParentesco.length != 0) {
            for(i=0; i< catalogoParentesco.length; i++) {
                if(parentescoId === catalogoParentesco[i].valor) {
                    $("#parentescoAdicional").
                    append('<option value="'+ catalogoParentesco[i].valor + '" selected="selected"> ' + catalogoParentesco[i].descripcion + '</option>');
                } else {
                    $("#parentescoAdicional")
                    .append('<option value="' + catalogoParentesco[i].valor + '"> '+ catalogoParentesco[i].descripcion + '</option>');
                }
            }
        }
	}

	function crearSeccionDatos(objeto){	
		for (var key in objeto) {
		  if (objeto.hasOwnProperty(key)) {
		 	var val = objeto[key];
			$("#"+key).val(val);
		  }
		}
	}
	
	function crearSeccion(id, titulo){
		var div =  "<div class='panel panel-default' id='"+id+"'>"+
						"<div class='panel-heading'>"+
							"<h4 class='panel-title'>"+
								"<a data-toggle='collapse' data-parent='#accordion' href='#collapse"+contadorSeccion+"'>"+titulo+"</a>"+
							"</h4>"+
						"</div>"+
						"<div id='collapse"+contadorSeccion+"' class='panel-collapse collapse in'>"+
						  "<div class='panel-body'>"+
						  "</div>"+
						"</div>"+
					"</div>";
		contadorSeccion++;
		return div;
	}
	
	function crearTabla(encabezados, datos, id){
	    var tabla = $("<table id='"+id+"'/>").addClass("table-condensed table-bordered");
		var head  = $("<thead/>");
		var fila  = $("<tr/>").attr("bgcolor","#58ACFA");
		
		for(var i=0; i < encabezados.length;i++){
		    fila.append($("<th/>").text(encabezados[i]));
		}
	    head.append(fila);
		
		var fila  = $("<tr/>");
		var body  = $("<tbody/>");

		for(var i=0; i < datos.length;i++){
			var fila  = $("<tr/>");
			for(var j=0; j < datos[i].length;j++){
				fila.append($("<td/>").text(datos[i][j]));
			}
			body.append(fila);			
		}
		tabla.append(head);
		tabla.append(body);
		return tabla;
	}
	
	function crearTablaCheckList(datos, id){
		
	    var tabla = $("<table id='"+id+"'/>").addClass("table-condensed table-bordered");
		var head  = $("<thead/>");
		var fila  = $("<tr/>").attr("bgcolor","#58ACFA");
		
		fila.append($("<th/>").text(""));
		fila.append($("<th/>").text("SI"));
		fila.append($("<th/>").text("NO"));
		
	    head.append(fila);
		
		var fila  = $("<tr/>");
		var body  = $("<tbody/>");

		for(var i=0; i < datos.length;i++){
			fila.append($("<td/>").text(datos[i].nombre));
			if(datos[i].ejecutar == true){
			   fila.append($("<td/>").append("<input type='checkbox' value='' checked='checked'/>"));
			   fila.append($("<td/>").append("<input type='checkbox' value=''/>"));
			}
			else{
			   fila.append($("<td/>").append("<input type='checkbox' value='' />"));
			   fila.append($("<td/>").append("<input type='checkbox' value='' checked='checked'/>"));
			}
			body.append(fila);			
		}
		tabla.append(head);
		tabla.append(body);
		return tabla;
	}
	
	function crearSeccionBuro(datosBuro,buroEncabezadosSegmentos,buroDatosSegmentos,buroEncabezadosDirecciones,buroDatosDirecciones){
			crearSeccionDatos(datosBuro);
			
			var nombres = [];			
			for(var i=0; i<buroDatosSegmentos.length;i++){
				 var dato = [buroDatosSegmentos[i].nombre, buroDatosSegmentos[i].rfc, buroDatosSegmentos[i].fechaNacimiento!=null?buroDatosSegmentos[i].fechaNacimiento:""];
			     nombres.push(dato);	
			}
			
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append("<br/>");
			var etiqueta = $("<div class='row col-md-4 col-md-offset-4'/>").append($("<label/>").text("Segmento de Nombres"));
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append(etiqueta);
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append("<br/>");
			
			var tabla = crearTabla(buroEncabezadosSegmentos, nombres, "buroNombres");
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append(tabla);
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body table[id='buroNombres']");
			$('#buroNombres').DataTable( {
				"paging":   false,
				"ordering": false,
				"searching": false,
				"info":     false
			} );
			
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append("<br/>");
			var etiqueta = $("<div class='row col-md-4 col-md-offset-4'/>").append($("<label/>").text("Segmento de Direcciones"));
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append(etiqueta);
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append("<br/>");

			var direccion = [];			
			for(var i=0; i<buroDatosDirecciones.length;i++){
				 var dato = [buroDatosDirecciones[i].direccion, buroDatosDirecciones[i].domicilio, 
					 		 buroDatosDirecciones[i].telefono!=null?buroDatosDirecciones[i].telefono:"",
					 		 buroDatosDirecciones[i].codigoPostal];
				 direccion.push(dato);	
			}
			var tabla = crearTabla(buroEncabezadosDirecciones, direccion, "buroDirecciones");
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body").append(tabla);
			$("#accordion").find("div[id='datosBuro']").find("div.panel-body table[id='buroDirecciones']");
			$('#buroDirecciones').DataTable( {
				"paging":   false,
				"ordering": false,
				"searching": false,
				"info":     false
			} );
			$("#datosBuro").find("input[type='text']").attr("disabled","false");
	}

	function crearSeccionCheckList(checklistDatos){
		var seccion   = crearSeccion("datosCheckList","Checklist");
		$("#accordion").append(seccion);
		
		var tabla = crearTablaCheckList(checklistDatos, "datosCheckListTb");
		
		$("#accordion").find("div[id='datosCheckList']").find("div.panel-body").append(tabla);
		$("#accordion").find("div[id='datosCheckList']").find("div.panel-body").append("<br/>");
		
		$('#datosCheckListTb').DataTable( {
			"paging":   false,
			"ordering": false,
			"searching": false,
			"info":     false
		} );
	}
	
	function crearSeccionDecision(){
	    var seccion   = crearSeccion('datosDecision','Decision');
		$('#accordion').append(seccion);
				
		var seccion = "<form><div class='radio'>"+
						  "<label><input type='radio' name='optradio'>Continuar</label>"+
						"</div>"+
						"<div class='radio'>"+
						  "<label><input type='radio' name='optradio'>Rechazar Solicitud</label>"+
						"</div>"+
						"<div class='radio disabled'>"+
						  "<label><input type='radio' name='optradio'>En espera de Documentación</label>"+
					  "</div><form/>";
					  
		$('#accordion').find("div[id='datosDecision']").find('div.panel-body').append(seccion);
		$('#accordion').find("div[id='datosDecision']").find('div.panel-body').append('<br/>');
	}
	
	function crearSeccionAcciones(accion){
		var seccion   = crearSeccion("datosOperaciones","Operaciones sobre la Solicitud");
		var botones = "<div class='row'>"+
						"<div class='container col-md-8 col-md-offset-2'>"+
							"<button type='button' id='btModificar'     class='btn btn-primary'>Modificar Solicitud</button>&nbsp;"+
							"<button type='button' id='btActFormulario' class='btn btn-primary' onclick='validarAlGuardar(callActualizarForma)'>Guardar</button>&nbsp;"+
							"<button type='button' id='btHistorial'     class='btn btn-primary'>Historial Solicitud</button>&nbsp;"+
							"<button type='button' id='btReporte'       class='btn btn-primary'>Ver Reporte BNC</button>&nbsp;"+
							"<button type='button' id='btNotas'         class='btn btn-primary'>Ver Notas</button>"+
							"<button type='button' id='btRechazar'      class='btn btn-primary' onclick='confirmaAccion(callActualizarFormaRechazar);'>Rechazar Solicitud</button>"+														
							"<div class='form-group pull-right col-xs-4' id='divRevision'>"+
								"<label>En Revisión:</label>"+
								"<input id='checkRevision' data-toggle='toggle' type='checkbox'"+
									"data-onstyle='success' data-size='small' onchange='swSwitch($(this));' class='newBSswitch'/>"+
							"</div>"+														
						"</div>";
						  "</div>";
		$("#accordion").append(seccion);
		$("#datosOperaciones").find("div.panel-body").append(botones);
		$("#accordion").find("div[id='datosOperaciones']").find("div.panel-body").append("<br/>");
		
		$("#btModificar").hide();
		$("#btHistorial").hide();
		$("#btReporte").hide();
		$("#btNotas").hide();
		$("#btRechazar").hide();
		$("#divRevision").hide();
		$("#btActFormulario").hide();
		
		if(accion.verBuro){
            $("#btReporte").show();
            $( "#btReporte" ).click(function() {
            	verBuro();
			});
        }
		if(accion.editarDatos){
            $("#btModificar").show();
            $("#btActFormulario").show();
            $( "#btModificar" ).click(function() {
            	modificarSolicitud();
			});
		}
		if(accion.verBtnRechazar){
			$("#btRechazar").show();
		}
		if(accion.verBtnRevision){
			$("#divRevision").show();
			$('.newBSswitch').bootstrapToggle();
		}
		
		if(estadoSolicitud != 9){
			$("#checkRevision").bootstrapToggle('off');
			$("#checkRevision").val('off')
		}
		else{
			$("#checkRevision").bootstrapToggle('on');
			$("#checkRevision").val('on')
			$("#formBtnTsys").hide();
		}				
	}
	
	function crearSeccionAsignacion(tipoAsignacion, accion){
		
				var forma =	"<form class='form' id='formBtnTsys'>"+
								"<div  class='form-group' id='divNumCuenta'>"+
									"<div class='col-md-4'> " +
									"<p>N&uacute;mero de tarjeta Titular</p>"+
									"<input type='text' maxlength='16' class='form-control col-md-4' id='numCuenta' placeholder='Ingrese No. de Tarjeta de Credito'>&nbsp; "+
									"</div>"+
									"<div class='col-md-12'/> " +
								"</div>"+
								"<br>"+
								"<br>"+
								"<div ='form-group' id='divNumCuentaAdicionalResp' class='hide'>"+
								    "<div class='col-md-4'> " +
                                        "<p>N&uacute;mero de tarjeta Adicional</p>"+
                                        "<input type='text' class='form-control col-md-4' id='numCuentaAdicionalResp' placeholder=''>&nbsp; "+
                                    "</div>"+
                                    "<div class='col-md-12'/> " +
                                 "</div>"+
								"<br>"+
                                "<br>"+
								"<div class='form-group' id='divNumCuentaConfirmacion'>"+
                                    "<div class=' form-group col-md-4'> " +
                                    "<input type='text'  maxlength='16' class='form-control form-control-sm' id='numCuentaconfirm' placeholder='Confirme No. de Tarjeta de Credito'>&nbsp;"+
                                    "</div>"+
                                    "<div class='col-md-12'/> " +
                                "</div>"+
                                "<br>"+
                                "<br>";
				forma = forma + (containsAdicional == true & (accion.cuentaAdicional != undefined) ? "<div class='form-group' id='divNumCuentaConfirmacionAdicional'>"+
                                    "<div class=' form-group col-md-4'> " +
                                    "<p>N&uacute;mero de tarjeta Adicional</p>"+
                                    "<input type='text'  maxlength='16' class='form-control form-control-sm' id='cuentaAdicional' placeholder='' disabled='disabled' value="+
                                    accion.cuentaAdicional + ">&nbsp;"+
                                    "</div>"+
                                    "<div class='col-md-12'/> " +
                                "</div>"+
                                "<br>"+
                                "<br>" : "")
                
				forma = forma + "<div class='form-group col-lg-10'>"+
								"<button type='button' class='btn btn-primary' id='btAsignar'       onclick='validarAlGuardar(asignar)'>Asignar</button>&nbsp;"+
								"<button type='button' class='btn btn-primary' id='btActualizar'    onclick='validarAlGuardar(actualizar)'>Actualizar</button>&nbsp;"+
								"<button type='button' class='btn btn-primary' id='btAsignarAct'    onclick='validarAlGuardar(asignarActualizar)'>Asignar y Actualizar</button>"+
								"</div>"+
								"<div class='col-md-12'/> " +
								"<br>"+
							"</form>"+
							"<br>"+
							"<div class='alert alert-warning' id='alertas'>"+
							
							  "<br>"+
							"</div>";				
	
			var seccion   = crearSeccion("datosAsignacion","Asignacion de Numero de Cuenta");
			$("#accordion").append(seccion);
			
			$("#accordion").find("div[id='datosAsignacion']").find("div.panel-body").append(forma);
			$("#accordion").find("div[id='datosAsignacion']").find("div.panel-body").append("<br/>");
			$("#alertas").hide();
			
			
			$("#btAsignar   ").hide();
			$("#btActualizar").hide();
			$("#btAsignarAct").hide();
			
			if(tipoAsignacion == "AUTOMATICA"){
				$("#divNumCuenta").hide();	
				$("#divNumCuentaConfirmacion").hide();
			}
			else{
				$("#divNumCuenta").show();
				$("#divNumCuentaConfirmacion").show();
			}
			
			console.log(accion.verAsignar)
	        if(accion.verAsignar && accion.numeroCuenta == ""){
	            $("#btAsignar").show();
	            flujoCuenta = "ASIGNACION";
	        }
	        if(accion.verActualizarTsys && accion.numeroCuenta != ""){
	            $("#btActualizar").show();
	            flujoCuenta = "ACTUALIZAR";
	        }			 
	        if(accion.verAsignarActualizar && accion.numeroCuenta == ""){
	            $("#btAsignarAct").show();
	            flujoCuenta = "ASIG-ACTUALIZAR";
	        }
	        
	        if( accion.numeroCuenta != ""){
	        	$("#numCuenta").val(accion.numeroCuenta).attr("disabled",true);
	        	$("#divNumCuenta").show();
	        	$("#numCuentaconfirm").hide();
	        }
	        
	        if(estadoSolicitud == 9){
				$("#formBtnTsys").hide();
			}
	}
	
	function crearSeccionBotones(){
			var botones = "<br/><div class='row'>"+
							"<div class='container col-md-8 col-md-offset-5'>"+
								"<button type='button' class='btn btn-primary' onclick='irListado()'>Cerrar</button>&nbsp;"+
							"</div>";
							  "</div><br/>";
			$("#accordion").append(botones);			
	}

	function deshabilitarForma(accion){
		$("#nombreTarjeta").attr("disabled",accion);
    	$("#datosSistema").find("input[type='text']").attr("disabled",accion);
		$("#datosPersonales").find("input[type='text']").attr("disabled",accion);
		$("#datosDomicilio").find("input[type='text']").attr("disabled",accion);
		$("#datosLaborales").find("input[type='text']").attr("disabled",accion);
		$("#datosIngresos").find("input[type='text']").attr("disabled",accion);
		$("#datosReferencias").find("input[type='text']").attr("disabled",accion);
		$("select").attr("disabled",accion);
		$("#limiteCredito").attr("disabled","false");
		$("#preguntaActivacion").attr("disabled","false");
		$("#autorizacionMercadeo").attr("disabled","false");
		$("#respuestaActivacion").attr("disabled",accion);
		$("#mensajeria").attr("disabled",accion);
		$("#datosAdicionales").attr("disabled",true);
		/** Seccion de tarjeta adicional*/
		$("#apellidoPaternoAdicional").attr("disabled",accion);
		$("#apellidoMaternoAdicional").attr("disabled",accion);
		$("#nombreAdicional").attr("disabled",accion);
		$("#fechaNacimientoAdicional").attr("disabled",accion);
		$("#parentescoAdicional").attr("disabled",accion);
		$("#celularAdicional").attr("disabled",accion);
		$("#emailAdicional").attr("disabled",accion);
		$("#porcentajeAdicional").attr("disabled",true);
		$("#nombreTarjetaAdicional").attr("disabled",accion);
	}
	
	function modificarSolicitud(){
		$("#loadingModalAccesos").modal('show');
		deshabilitarForma(false);
		$("#loadingModalAccesos").modal('hide');
	}

	function obtenerDatosForma(){
		var jsonData = {  
				nombreTarjeta  : $("#nombreTarjeta").val(),  
	 		    apellidoMaterno: $("#apellidoMaterno").val(), 
	 		    apellidoPaterno: $("#apellidoPaterno").val(), 
	 		    nombre         : $("#nombre").val(),
		  		sexo           : $("#sexo").val(), 
		  		fechaNacTemp	: $("#fechaNacimiento").val(),
		  		curp           : $("#curp").val(),
		  		nacionalidad   : $("#nacionalidad").val(), 
		  		entidadFederativa: $("#entidadFederativa").val(), 
		  		telefonoCelular  : $("#celular").val(),
		  		email            : $("#email").val(), 
		  		estadoCivil      : $("#estadoCivil").val(), 
		  		dependientesEconomicos : $("#dependientesEconomicos").val(),
		  		maximoEstudios         : $("#maximoEstudios").val(), 
		  		tieneCreditoAutomotriz : $("#tieneCreditoAutomotriz").val(),
		  		tieneCreditoHipotecario: $("#tieneCreditoHipotecario").val(), 
		  		tieneTDC               : $("#tieneTDC").val(),
		  		rfc                    : $("#rfc").val(), 
		  		calle                  : $("#calle").val(), 
		  		numeroExterior : $("#numeroExterior").val(),
		  		numeroInterior : $("#numeroInterior").val(),
		  		municipio      : $("#municipio").val(), 
		  		codigoPostal   : $("#codigoPostal").val(),  
		  		colonia        : $("#colonia").val(),
		  		telefonoCasa   : $("#telefonoCasa").val(),
		  		tipoVivienda   : $("#tipoVivienda").val(), 
		  		antigResidencia: $("#antigResidencia").val(),
		  		autenticaOtroDomicilio: $("#autenticaOtroDomicilio").val(),
		  		nombreEmpleo          : $("#nombreEmpleo").val(), 
		  		telefonoEmpleo        : $("#telefonoEmpleo").val(),		  		  
		  		extension             : $("#extension").val(), 
		  		emailEmpleo           : $("#emailEmpleo").val(), 
		  		calleEmpleo           : $("#calleEmpleo").val(), 
		  		numeroExteriorEmpleo  : $("#numeroExteriorEmpleo").val(),
		  		numeroInteriorEmpleo  : $("#numeroInteriorEmpleo").val(), 
		  		codigoPostalEmpleo    : $("#codigoPostalEmpleo").val(), 
		  		municipioEmpleo       : $("#municipioEmpleo").val(),
		  		coloniaEmpleo         : $("#coloniaEmpleo").val(), 
		  		antigEmpleoActual     : $("#antigEmpleoActual").val(), 
		  		antigEmpleoAnterior   : $("#antigEmpleoAnterior").val(),
		  		actividadEmpresarial  : $("#actividadEmpresarial").val(), 
		  		actividadLaboral      : $("#tipoActividadLaboralEm").val(), 
		  		fuenteOtrosIngresos   : $("#fuenteOtrosIngresos").val(),		  		
		  		ingresoFijoMensual    : $("#ingresoFijoMensual").val(),
		  		otrosIngresosComprobables: $("#otrosIngresosComprobables").val(),
		  		nombreRefFamiliar    : $("#nombreRefFamiliar").val(),
		  		telefonoCelReferencia: $("#telefonoCelReferencia").val(), 
		  		telefonoReferencia   : $("#telefonoReferencia").val(),
		  		idSolicitud          : parseInt(idSolicitudVenta),
		  		edoRevision           : $('#checkRevision').val() == "on" ? true : false };			
			
		    if(jsonData.sexo == "-1"){
		    	jsonData.sexo = null;
		    }  
		    if(jsonData.nacionalidad == "-1"){
		    	jsonData.nacionalidad = null;
		    }
		    if(jsonData.estadoCivil == "-1"){
		    	jsonData.estadoCivil = null;
		    }
		    if(jsonData.maximoEstudios == "-1"){
		    	jsonData.maximoEstudios = null;
		    }  
		    if(jsonData.tieneCreditoAutomotriz == "-1"){
		    	jsonData.tieneCreditoAutomotriz = null;
		    }
		    if(jsonData.tieneCreditoHipotecario == "-1"){
		    	jsonData.tieneCreditoHipotecario = null;
		    }
		    if(jsonData.tieneTDC == "-1"){
		    	jsonData.tieneTDC = null;
		    }
		    if(jsonData.tipoVivienda == "-1"){
		    	jsonData.tipoVivienda = null;
		    }
		    if(jsonData.autenticaOtroDomicilio == "-1"){
		    	jsonData.autenticaOtroDomicilio = null;
		    }
		    if(jsonData.autorizacionMercadeo == "-1"){
		    	jsonData.autorizacionMercadeo = null;
		    }
		    if(jsonData.preguntaActivacion == "-1"){
		    	jsonData.preguntaActivacion = null;
		    }
		    if(jsonData.actividadEmpresarial == "-1"){
		    	jsonData.actividadEmpresarial = null;
		    }
		    if(jsonData.actividadLaboral == "-1"){
		    	jsonData.actividadLaboral = null;
		    }

            /** Informacion de tarjeta adicional */
            var existeDivDatosTarjetaAdicional = $("#datosTarjetaAddicional").attr('class').includes('hide') == false;
            if(existeDivDatosTarjetaAdicional){
                jsonData.apellidoPaternoAdicional = $("#apellidoPaternoAdicional").val();
                jsonData.apellidoMaternoAdicional = $("#apellidoMaternoAdicional").val();
                jsonData.nombreAdicional = $("#nombreAdicional").val();
                jsonData.fechaNacimientoAdicionalFormat = $("#fechaNacimientoAdicional").val();
                jsonData.parentescoAdicional = $("#parentescoAdicional").val();
                jsonData.celularAdicional = $("#celularAdicional").val();
                jsonData.emailAdicional = $("#emailAdicional").val();
                jsonData.nombreTarjetaAdicional = $("#nombreTarjetaAdicional").val();
            }

			return jsonData;
	}
	
	
	
	
	function crearSeccionActivacion(activacion){
		
	var formaActivacion =	"<form class='form-inline' id='formActivacion'>"+
		"<div class='form-group' id='divEnviarToken'>"+
			"<label for='btEnviarToken'>Token de Activaci&oacute;n:</label>"+
			"<button type='button' class='btn btn-default' id='btEnviarToken'	onclick='enviarNumCuenta()'>Enviar C&oacute;digo</button>&nbsp;"+
		"</div>"+
	"</form>";	
	
	
	
	var formaIngreseNip ="<form class='form-inline' id='formIngreseNip'>"+
	 "<br/><div class='col-lg-12'><label class='control-label'><font size='4'>Ingrese el NIP de la tarjeta</font></label></div>"+
	 "<br/>"+
	 "<br/>"+
	 "<div class='row'>"+
	
	 "<div id='divChipAndPin' class='form-group'>"+
	 "<label><font color='#FFFFFF'>--</font></label>"+
	 "<input id='ingreseNip'  type='password' class='form-control' maxlength='4' placeholder='Ingrese Pin'  onkeyup='validateNumber(this);'  />"+
	 "</div>"+
	 "<div class='form-group'>"+
	 "<label><font color='#FFFFFF'>--</font></label>"+
	 "<input id='confirmaNip' type='password' class='form-control' maxlength='4' placeholder='Confirme PIN' onkeyup='validateNumber(this);' />"+
	  "</div>"+
	 "<br/>"+
	 
	 "<div class='form-group col-lg-12'>"+
	 "<br/>"+
	 "<button type='button' class='btn btn-primary' onclick='validarPIN()'>Procesar</button>"+
	 "<label><font color='#FFFFFF'>----------</font></label>"+
	 "</div>"+
	 "</div>"+
	 "<div class='col-md-12'/> " +
	 "</form>"+
		"<div class='alert alert-danger' id='alertasActivacion'>"+
		  "<br>"+
		"</div>";	
	
	

	var seccionActivacion   = crearSeccion("datosActivacion","Activacion de cuenta");
	var seccionNip			= crearSeccion("datosNip","Asignacion de Nip");
	$("#accordion").append(seccionActivacion);
	$("#accordion").append(seccionNip);
	
	$("#accordion").find("div[id='datosActivacion']").find("div.panel-body").append(formaActivacion);
	$("#accordion").find("div[id='datosActivacion']").find("div.panel-body").append("<br/>");
	
	$("#accordion").find("div[id='datosNip']").find("div.panel-body").append(formaIngreseNip);
	$("#accordion").find("div[id='datosNip']").find("div.panel-body").append("<br/>");	
	
	
	$("#datosActivacion").hide();
	$("#datosNip").hide();
	$("alertasActivacion").hide();
	}

	function mostrarMensajeAdvertencia(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function irListado(idSolicitud){
		var baseUrl = getPath() + "/backOffice?seccion=mesaControl";
		redireccionar(baseUrl);
	}
	
	function resetearCheck(campo){
		campo.bootstrapToggle('off');
		campo.val('off');
	}

	function swSwitch(componente){
		var valor = componente.val();	
		if(valor == "on"){
			componente.val("off");
			$("#formBtnTsys").show();
		} else {
			componente.val("on");
			$("#formBtnTsys").hide();
		}
	}