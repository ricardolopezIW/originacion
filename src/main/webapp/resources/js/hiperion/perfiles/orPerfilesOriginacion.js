var tablaPerfiles = "";
var eliminar = false;
var restUrl = $("#urlRestBackOffice").val();
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";



/**
 * Limpia los campos mesa de control, lugar de venta y el perfil seleccionado
 */
function resetForm() {

	$("#idPerfil").val('');

	$('#headPanel').empty();
	$(".active2").removeClass('active2');

	$("#activeMesa option").each(function() {
		$(this).remove();
	});

	$("#activeLv option").each(function() {
		$(this).remove();
	});
	
	$("#activeLvAsig option").each(function() {
		$(this).remove();
	});

}

function asignarMesa() {
	var listLV = [];
	$("#activeLvAsig option").each(function() {
		listLV.push($(this).val());
	});

	var listMesa = [];
	$("#activeMesa option").each(function() {
		listMesa.push($(this).val());
	});

	var idPerfil = $("#idPerfil").val().trim();

	if (idPerfil == '') {
		modalAlert("No seleccionaste ningún perfil");

	}

	var lvEmpty = selectIsEmpty("activeLvAsig");
	if (lvEmpty) {
		modalAlert("Asigna al menos un lugar de venta");
	}
	
	var mesasEmpty = selectIsEmpty("activeMesa");
	if (mesasEmpty) {
		modalAlert("Asigna al menos una mesa de control");
	}

	if (!lvEmpty && !mesasEmpty && idPerfil != '') {
		let json = {
			"idPerfil" : idPerfil,
			"listMesa" : listMesa,
			"listLV" : listLV
		};
		json = JSON.stringify(json);

		$.ajax({
			data : json,
			url : restUrl + '/perfil/asignarMesaLv',
			type : 'post',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			
			success : function(response) {
				
				if (response.errorMsg == null){
					resetForm();
					modalAlert("Asignación correcta");
					
				} else{
					modalAlert(response.errorMsg);
				}
				
				$("#loadingModalAccesos").modal('hide');

			}

		});
	}

}


/**
 * Valida si el elemento select se encuentra vacio
 * @param idSelect ::: ID del select que se desea validar
 * @returns ::: true si esta vacio, false si contiene algun elemento
 */
function selectIsEmpty(idSelect){
	var isEmpty = true;
	var selector = "#"+ idSelect +" option";
	$(selector).each(function() {
		isEmpty = false;
	});
	
	return isEmpty;
}


/**
 * Agrega la mesa seleccionada de la lista de mesas de control, comprobando que no se haya agregado anteriormente
 */
function agregarMesa() {
	var mesaSelec = $("#selectMesa").val().trim();
	var nombreMesa = $("#selectMesa option:selected").text();

	if (nombreMesa != "" && nombreMesa != "Seleccionar") {
		var bandera = false;
		var flag = false

		$("#activeMesa option").each(function() {
			var nombreMesaAgregada = $(this).text().trim();
			if (nombreMesa == nombreMesaAgregada) {
				bandera = true;
				modalAlert("La mesa seleccionada ya se encuentra agregada");
				$("#selectMesa").val(-1);
			}
		});

		if (!bandera) {
			$('#activeMesa').append($('<option>', {
				value : mesaSelec,
				text : nombreMesa
			}));

			$("#selectMesa").val(-1);

			agregarLugaresVenta(mesaSelec);
		}
	} else {
		modalAlert("Debe seleccionar una mesa");
		$("#selectMesa").val(-1);
	}

}


function agregarLugares() {
	var lugarSelec = $("#activeLv").val();
	var nombreLugar = $("#activeLv option:selected").text();
	var arrayNombresLugares = [];
	
	
	$("#activeLv option").each(function() {
		if ($(this).is(':selected')) {
			arrayNombresLugares.push($(this).text());
		}
	});
	
	var arrayNombresValues = [];
	
	 
	 
	$(arrayNombresLugares).each(function(i,n) {
		 var objLugar = {};
		 objLugar.nombre  = arrayNombresLugares[i];
		 objLugar.value  = lugarSelec[i];
		 arrayNombresValues.push(objLugar);
		 
	});


	if (nombreLugar != "" && nombreLugar != "Seleccionar") {
		var bandera = false;
		var flag = false

		$("#activeLvAsig option").each(function() {
			var nombreMesaAgregada = $(this).text().trim();
			if (nombreLugar == nombreMesaAgregada) {
				bandera = true;
				modalAlert("El lugar de venta ya ha sido asignado");
				$("#activeLv").val(-1);
			}
		});

		if (!bandera) {
			
			if(arrayNombresValues.length > 0){
			
				$(arrayNombresValues).each(function(i,n) {
					$('#activeLvAsig').append($('<option>', {
						value : n.value,
						text : n.nombre
					}));
				});
		
			} else {
				
				$('#activeLvAsig').append($('<option>', {
					value : lugarSelec,
					text : nombreLugar
				}));
				
			}
			
			
			

			$("#activeLv").val(-1);

			//agregarLugaresVenta(mesaSelec);
		}
	} else {
		modalAlert("Debe seleccionar un lugar");
		$("#activeLv").val(-1);
	}

}


/**
 * Consulta los lugares de venta de la mesa de control
 * @param idMesa ::: ID de la mesa de la que se obtendran los lugares de venta
 */
function agregarLugaresVenta(idMesa) {
	$.ajax({
		url : restUrl + '/perfil/obtenerLugarVenta/' + idMesa,
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {
			$("#loadingModalAccesos").modal('hide');

			$.each(response, function(i, n) {

				$("#activeLv option").each(function() {
					if ($(this).val() == n.id) {
						$(this).remove();
					}
				});

				$('#activeLv').append($('<option>', {
					value : n.id,
					text : n.nombre
				}));
			});

		}

	});
}


/**
 * Elimina la mesa seleccionada de la lista de mesas agregadas
 */
function quitarMesa() {
	var size = $("#activeMesa option").length;
	
	if (size > 0) {
		var flagMesaSelec = true;
		$("#activeMesa option").each(function() {
			if ($(this).is(':selected')) {
				$(this).remove();
				flagMesaSelec = false;
			}
		});
		if (flagMesaSelec) {
			modalAlert("Debe seleccionar una mesa de control");
		}
	} else {
		modalAlert("La lista de mesas esta vacía");
	}
}

/**
 * Elimina la opcion seleccionada de la lista lugar de venta
 * @returns
 */
function quitarLV() {
	var size = $("#activeLvAsig option").length;
	if (size > 0) {
		var flagLVSelec = true;
		$("#activeLvAsig option").each(function() {
			if ($(this).is(':selected')) {
				$(this).remove();
				flagLVSelec = false;
			}
		});
		if (flagLVSelec) {
			modalAlert("Debe seleccionar un lugar de venta");
		}
	} else {
		modalAlert("La lista de lugares de venta esta vacía");
	}
}


/**
 * Construye la tabla de perfiles
 * @param response ::: Perfiles con los que sera construida la tabla
 * @returns
 */
function fillTablePerfiles(response) {
	if ($.fn.dataTable.isDataTable('#tablaPerfiles')) {
		tablaPerfiles.destroy();
	}

	tablaPerfiles = $('#tablaPerfiles').DataTable({
		"language" : {
			"lengthMenu" : "_MENU_ Perfiles",
			"zeroRecords" : "No hay datos",
			"info" : "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty" : "No se encontraron perfiles",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "Ultimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			},
			"infoFiltered" : "(filtrando del _MAX_ total perfiles)"
		},
		data : response,
		columns : [ {
			title : "Id",
			data : 'idPerfil'
		}, {
			title : "Descripción",
			data : 'descripcion'
		} ]
	});

	$('#tablaPerfiles tbody')
			.unbind()
			.on(
					'click',
					'tr',
					function(event) {

						if (tablaPerfiles.rows().data().length > 0
								&& !eliminar) {

							var td = tablaPerfiles.row(this._DT_RowIndex)
									.data();

							var selected = $(this).hasClass("active2");
							$("#tablaPerfiles tr").removeClass("active2");
							if (!selected)
								$(this).addClass("active2");

							var html = '<h4 class="active2 panel-title"><font size="4">Asignación para el perfil: '
									+ td.descripcion + '</font></h4>';
							$('#headPanel').html(html);

							fillForm(td)


						}
					});
	
	
	$("#btnGuardarN").click(function() {
		console.log("ROWddd");
		asignarMesa();
	});

	$("#btnCancelarN").click(function() {
		resetForm();

	});
	
}

function fillForm(td) {
	$('#activeLv').empty();
	$('#activeMesa').empty();
	$('#activeLvAsig').empty();
	
	$("#idPerfil").val(td.idPerfil)

	$.ajax({
		url : restUrl + '/perfil/obtenerPerfil' + '/' + td.idPerfil,
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModalAccesos").modal('show');
		},
		
		success : function(response) {
			
			if(response.error == null){
				
				$.each(response.lugaresVenta, function(i, n) {
					$('#activeLvAsig').append($('<option>', {
						value : n.id,
						text : n.nombre

					}));
				});

				$.each(response.mesasControl, function(i, n) {
					$('#activeMesa').append($('<option>', {
						value : n.id,
						text : n.nombre
					}));
					
					agregarLugaresVenta(n.id)
				});
			
			} else{
				modalAlert(response.error);
			}
			
			$("#loadingModalAccesos").modal('hide');

		}

	});

}


/**
 * Obtiene y construye la tabla de perfiles
 * @returns
 */
function consultarPerfiles() {
	$.ajax({
		url : restUrl + '/perfil/obtenerTodos',
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {
			fillTablePerfiles(response);
			$("#loadingModalAccesos").modal('hide');

		}

	});

}


/**
 * Obtiene y construye la lista de seleccion de mesas de control
 */
function consultarMesas() {
	$.ajax({
		url : restUrl + '/mesa-control',
		type : 'get',
		contentType : "application/json",
		success : function(response) {


			var html = '<option value="-1">Seleccionar</option>';

			$.each(response.listaMesaControl, function(i, n) {
				html += '<option value="' + n.id + '">' + n.nombre
						+ '</option>';
			});

			$("#selectMesa").html(html);

		}
	});
	

}



/**
 * Muestra un modal
 * @param msj ::: Mensaje que se mostrara
 */
function modalAlert(msj){
	$("#msgModalAlert").html("<h4>"+ msj +"</h4>");
	$("#modalAlert").modal('show');
}

consultarMesas();
consultarPerfiles();