var respuesta = [];
var idEstadoAdd = "";
var idEstadoDel = "";
var idMunicipioAdd = "";
var idMunicipioDel = "";
var tablaMensajeria = "";
var tableEstados = "";
var tableCp = "";
var eliminar = false;
var restUrl = $("#urlRestBackOffice").val();
// var restUrl = "http://localhost:8081/siditOriginacion/backOffice";

$(document).ready(function() {
	consultarWatchList();
	inicia();
});

function resetForm() {
}

function inicia() {
	$("#divEstadosCobertura").hide();
	$("#divMunicipiosCobertura").hide();
	$("#divCpCobertura").hide();
	$("#divSelectEstado").hide;
	$("inputCP").keyup(function() {
		$("inputCP").css("background-color", "pink");
	});

}

function fillTable(response) {

	if ($.fn.dataTable.isDataTable('#divTablaRegistros')) {
		tablaMensajeria.destroy();
	}

	tablaMensajeria = $('#divTablaRegistros')
			.DataTable(
					{
						"language" : {
							"lengthMenu" : "_MENU_ Registros",
							"zeroRecords" : "No hay datos",
							"info" : "Mostrando pagina _PAGE_ de _PAGES_",
							"infoEmpty" : "No se encontraron mensajerias",
							"search" : "Buscar:",
							"paginate" : {
								"first" : "Primero",
								"last" : "Ultimo",
								"next" : "Siguiente",
								"previous" : "Anterior"
							},
							"infoFiltered" : "(filtrando de un total de _MAX_ resgistro(s))",
							
						},
						"ordering" : false,
						data : response,
						columns : [
								{
									title : "Id",
									data : 'id',
								},
								{
									title : "Caso",
									data : 'caso'
								},
								{
									title : "Cliente",
									data : 'cliente'
								},
								{
									title : "Fecha de creacion",
									data : 'fechaCreacion'
								},
								{
									title : "Usuario responsable",
									data : 'usuarioResponsable'
								},
								{
									title : "Edición",
									"render" : function(data, type, full, meta) {

										var h = '<button class ="btn editarWl btn-primary" type="button" onclick="editarWatchList(\''
												+ full.id
												+ '\');"/>Editar</button>'

										return h;
									}
								} ]
					});

	$('#divTablaRegistros thead th')
			.each(
					function() {
						var title = $(this).text();
						if (title == "Id" || title == "Caso"
								|| title == "Cliente") {
							$(this)
									.html(
											''
													+ title
													+ ' <input type="text" placeholder="Buscar '
													+ title + '" />');
						}
					});

	// Apply the search
	tablaMensajeria.columns().every(function() {
		var that = this;

		$('input', this.header()).on('keyup change', function() {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});
	});

	
	$('#divTablaRegistros_filter').html('<button type="button" class="btn btn-primary"   style="margin-right: 30px; font-size:20px;" id="actualizar">Actualizar</button>');
	
	
	
	
	$("#actualizar").click(function() {
		consultarWatchList();
	});
}

function crearModal(obj) {

	$('#origen').val(obj.origen);
	$('#lugarVenta').val(obj.lugarVenta);
	$('#caso').val(obj.caso);
	$('#numero').val(obj.accountNumber);
	$('#nombre').val(obj.nombre);
	$('#id').val(obj.id);
	$('#calle').val(obj.calle);
	$('#paterno').val(obj.apaterno);
	$('#materno').val(obj.amaterno);
	$('#int').val(obj.noInterior);
	$('#ext').val(obj.noExterior);
	$('#curp').val(obj.curp);
	$('#colonia').val(obj.colonia);
	$('#rfc').val(obj.rfc);
	$('#codigoPostal').val(obj.codigoPostal);
	$('#empresa').val(obj.empresa);
	$('#municipio').val(obj.municipio);
	$('#telCasa').val(obj.telefonoCasa);
	$('#estado').val(obj.estado);
	$('#observaciones').val(obj.observaciones);
	$('#telOfi').val(obj.telefonoOficina);

	$("#modalEditarWatchList").modal('show');

	$("#editWatchList").click(function() {
		if ($('#formCargaWatch').parsley().validate()) {
			var obj = {
				id : $("#id").val(),
				origen : $("#origen").val(),
				lugarVenta : $("#lugarVenta").val(),
				caso : $("#caso").val(),
				accountNumber : $("#numero").val(),
				nombre : $("#nombre").val(),
				calle : $("#calle").val(),
				apaterno : $("#paterno").val(),
				noExterior : $("#ext").val(),
				amaterno : $("#materno").val(),
				noInterior : $("#int").val(),
				curp : $("#curp").val(),
				colonia : $("#colonia").val(),
				rfc : $("#rfc").val(),
				codigoPostal : $("#codigoPostal").val(),
				empresa : $("#empresa").val(),
				municipio : $("#municipio").val(),
				estado : $("#estado").val(),
				telefonoCasa : $("#telCasa").val(),
				telefonoOficina : $("#telOfi").val(),
				observaciones : $("#observaciones").val(),
				usuarioResponsable : $("#hiddenUser").val()
			}
			var json = generarObjetoJson(obj);
			actualizarWL(json);
		}
	});
}

function actualizarWL(json) {
	console.log(json)
	$.ajax({
		data : json,
		url : restUrl + '/watch-list/actualizar',
		type : 'post',
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			consultarWatchList();
			$("#modalEditarWatchList").modal('hide');
			mostrarMensajeAdv("Se ha actualizado el registro.");
		}
	});

}

function editarWatchList(idWL) {
	$.ajax({
		url : restUrl + '/watch-list/' + idWL,
		type : 'get',
		contentType : "application/json",
		success : function(response) {
			console.log("Response", response)
			crearModal(response);
		},
		complete : function(xhr, status) {
			eliminar = false;
		}

	});
}

function consultarWatchList() {
	$.ajax({
		url : restUrl + '/watch-list/consultaWatchList',
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			console.log("DATOS", response);
			fillTable(response);
		},
		complete : function(xhr, status) {
			$("#loadingModal").modal('hide');
		}

	});
}

function generarObjetoJson(object) {
	return JSON.stringify(object);
}

function mostrarMensajeAdv(mensaje) {
	$.dialog({
		title : 'Alerta',
		content : mensaje,
	});
}