$(document).ready(function(){
	iniciaMenu();		
});		


function iniciaMenu() {
	
	var listaMenu = JSON.parse(menu);
	
	var codigo = '<ul class="nav" id="sidenav01">';
	for(var i =0; i<listaMenu.length; i++){
							
		if(listaMenu[i].idMenuPadre == null){
			codigo += '<li>';
			codigo += '<a href="#" data-toggle="collapse" data-target="#toggleDemo'+i+'" data-parent="#sidenav01" class="collapsed menuPadre" >';
			codigo += '<i class="' + listaMenu[i].icono + '"></i>';
			codigo += '<span style="padding-left:10px;">'+listaMenu[i].nombre+'</span>';
			codigo += '<span class="caret pull-right"></span>';
			codigo += '</a>';
			codigo += '<div class="collapse" id="toggleDemo'+i+'">';
			codigo += '<ul class="nav nav-list">';
			
			for(var j =0; j<listaMenu.length; j++){
				if(listaMenu[i].idMenu == listaMenu[j].idMenuPadre){
					codigo += '<li><a class="menuHijo" style="padding-left: 50px;" href="'+requestContextPath+'/backOffice?seccion='+listaMenu[j].vista+'">'+listaMenu[j].nombre+'</a></li>';
				}
			}
			codigo +='</li>'
			codigo +='</ul>';
		}
	}
	codigo +='</ul>';
	
	$("#menu").append(codigo);
	
}

