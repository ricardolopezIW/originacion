
var parametros;
var parametrosText;
var camposBase;
var contFila = 0;
var error = false;
var operadores = [];
var arrayReglas = [];
var reglasActuales = [];
var idRegla = null;
 var restUrl = $('#urlRestBackOffice').val();
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
var restConfiguracion = restUrl + "/pld/configuracion";
var restObtenerReglas = restUrl + "/pld/reglas";
var restActualizar = restUrl + "/pld/regla-actualizar";
var restGuardar = restUrl + "/pld/regla";

$(document).ready(function() {
	inicializarObjetos();
	verReglas();
});

function inicializarObjetos() {
	$.ajax({
		data : null,
		url : restConfiguracion,
		type : 'get',
		contentType : "application/json",
		success : function(response) {
			parametros = response.parametros;
			operadores = [ "AND", "OR" ];
			parametrosText = response.parametrosText;
			camposBase = response.camposBase;
			agregarParametro(null);
		}

	});

	$("#addParam").click(function() {
		agregarParametro(null);
	});

	$("#limpiar").click(
			function() {
				arrayReglas = [];
				idRegla = null;
				$("#accionSel").val(-1), $("#mensaje").val(''), $(
						"#nombreRegla").val(''), $("#orden").val(''), $(
						"#idRegla").val('')
				$("#reglasTable tbody").empty();
				idRegla = null;

				$(".classActiva").addClass('hide');
				contFila = 0;
				agregarParametro(null);
			});

	$("#guardar").click(
			function() {
				deshabilitarBoton("guardar", true);
				var url = restGuardar;
				var type = 'post';

				if ($('#forma').parsley().validate()) {
					verificarReglas();
				}

				if (!error) {
					var obj = {
						regla : generarRegla(),
						accion : $("#accionSel").val(),
						mensaje : $("#mensaje").val(),
						nombreRegla : $("#nombreRegla").val(),
						orden : $("#orden").val(),
						reglaVisual : generarReglaVisual()
					}

					var stringIds = '';

					$.each(arrayReglas, function(i, n) {
						stringIds += n.paramVal + ',' + n.operVal + '|';
					});

					if (idRegla != null && idRegla != "") {

						obj.activa = 0;
						obj.id = $("#idRegla").val();
						var activa = $('input[name=checkActiva]:checked',
								'#forma').val();
						if (activa) {
							obj.activa = 1;
						}

						url = restActualizar;
						type = 'put';

					}

					obj.cadenaParametros = stringIds;

					var json = generarObjetoJson(obj);

					$.ajax({
						data : json,
						url : url,
						type : type,
						contentType : "application/json",
						success : function(response) {
							if (response.error == null) {
								$("input").val("");
								$("select").val("-1");
								idRegla = null;
								arrayReglas = [];
								$("#reglasTable tbody").empty();
								$(".classActiva").addClass('hide');
								verReglas();
								agregarParametro(null);
								deshabilitarBoton("guardar", false);
								contFila = 1;
								$.dialog({
									title : 'Resultado',
									content : "Proceso Terminado",
								});
							} else {
								deshabilitarBoton("guardar", false);
								$.dialog({
									title : 'Error',
									content : response.error,
								});
							}
						}
					});

				}
			});

	$("#verRegla").click(function() {
		verificarReglas();
		if (!error) {
			verRegla();
		}
	});

	$("#verReglas").click(function() {
		verReglas();
	});

	$("#btGuardarReglas").click(function() {
		deshabilitarBoton("guardar", true);
		guardarReglas();
	});

}

function deshabilitarBoton(boton, activo) {
	$("#" + boton).prop("disabled", activo);
}

function agregarParametro(parametro) {
	contFila++;
	var combo;
	if (parametro != null) {

		comboText = crearComboBox(parametrosText, parametro.idParametro)
		comboOperador = crearComboBox([ "Y", "O" ], parametro.idOperador)
	} else {

		comboText = crearComboBox(parametrosText, null)
		comboOperador = crearComboBox([ "Y", "O" ], null)

	}

	var newTr = $('<tr>');
	newTr.append($('<td>').append(crearSpanParametro()))
	newTr.append($('<td>').append(comboText));
	newTr.append($('<td>').append(comboOperador));
	$("#reglasTable tbody").append(newTr);

}

function crearSpanParametro() {
	var htmla = $('<a>').attr('href', '#');
	var span = $('<span>').attr('data-toggle', 'tooltip').attr('title',
			'Remover Parámetro').attr('onclick', "removerTr(" + contFila + ")")
			.addClass('glyphicon glyphicon-remove-circle');
	htmla.append(span);
	return htmla;
}

function crearComboBox(texts, id) {

	var selectList = $('<select class="form-control" >');

	selectList.append($('<option>').attr('value', '-1')
			.text("Selecciona Valor"));

	for (var i = 0; i < texts.length; i++) {

		if (id == i) {
			selectList.append($('<option selected>').attr('value', i).text(
					texts[i]));
		} else {
			selectList.append($('<option>').attr('value', i).text(texts[i]));
		}
	}

	return selectList;
}

function removerTr(id) {
	$("#reglasTable tbody tr:nth-child(" + id + ")").remove();

	contFila--;
}

function verificarReglas() {
	// -si es un solo un registro verificar el campo parametro
	// -si es mayor de uno, verificar hasta el n-1 que tengan operadores y
	// todos con el campo parametro
	var paramAll = true;
	var operaAll = true;
	var repetido = false;
	var anterior = [];

	$("#reglasTable tbody tr").each(function() {
		$this = $(this);
		var value = $this.find("select:first").val();
		if (value == "-1")
			paramAll = false;
		if (paramAll) {
			if (anterior.length == 0)
				anterior.push(value);
			else {
				for (var i = 0; i < anterior.length; i++) {
					if (anterior[i] == value)
						repetido = true;
				}
				anterior.push(value);
			}
		}
	});

	if (contFila > 1) {
		var con = 1;
		$("#reglasTable tbody tr").each(function() {
			$this = $(this);
			var value = $this.find("select:last").val();
			if (con <= (contFila - 1)) {
				if (value == "-1") {
					operaAll = false;
				}
			}
			con++;
		});
	}

	if (!paramAll) {
		mostrarMensajeAdv("Se debe seleccionar todos los parametros para la consulta.");
		error = true;
	} else if (!operaAll) {
		mostrarMensajeAdv("Se debe seleccionar el operador para crear la consulta.");
		error = true;
	} else if (repetido) {
		mostrarMensajeAdv("Se tiene un parametro repetido.");
		error = true;
		deshabilitarBoton("guardar", false);
	} else {
		error = false;
	}
}

function mostrarMensajeAdv(mensaje) {
	$.dialog({
		title : 'Verificar',
		content : mensaje,
	});
}

function generarRegla() {
	var regla = "";

	if (contFila > 1) {
		var con = 0;
		$("#reglasTable tbody tr").each(
				function() {
					$this = $(this);
					var param = $this.find("select:first").val();
					var operador = $this.find("select:last").val();
					var str = "@" + parametros[parseInt(param)]
							+ " = LOWER(TRIM(" + camposBase[parseInt(param)]
							+ "))";
					regla += str;

					if (con < (contFila - 1)) {
						regla += " " + operadores[parseInt(operador)] + " ";
					}
					con++;
				});
	} else if (contFila == 1) {
		$("#reglasTable tbody tr").each(
				function() {
					$this = $(this);
					var param = $this.find("select:first").val();
					var operador = $this.find("select:last").val();
					var str = "@" + parametros[parseInt(param)]
							+ " = LOWER(TRIM(" + camposBase[parseInt(param)]
							+ "))";
					regla += str;
				});
	}
	return regla;
}

function generarReglaVisual() {
	var regla = "";
	if (contFila > 1) {
		var con = 0;
		$("#reglasTable tbody tr").each(
				function() {
					var objRegla = {};
					$this = $(this);
					var param = $this.find("select:first option:selected")
							.text();
					var operador = $this.find("select:last option:selected")
							.text();

					objRegla.paramVal = $this.find(
							"select:first option:selected").val();
					objRegla.operVal = $this
							.find("select:last option:selected").val();

					arrayReglas.push(objRegla);

					var str = param + " ";
					regla += str;

					if (con < (contFila - 1)) {
						regla += " " + operador + " ";
					}
					con++;
				});
	} else if (contFila == 1) {

		$("#reglasTable tbody tr").each(
				function() {
					var objRegla = {};
					$this = $(this);
					var param = $this.find("select:first option:selected")
							.text();
					var operador = $this.find("select:last").val();

					objRegla.paramVal = $this.find(
							"select:first option:selected").val();
					objRegla.operVal = $this
							.find("select:last option:selected").val();

					arrayReglas.push(objRegla);

					var str = param + " ";
					regla += str;
				});
	}

	return regla;
}

function verRegla() {
	if (error)
		verificarReglas();
	if (!error)
		$.dialog({
			title : 'Regla',
			content : 'Se valida: ' + generarReglaVisual(),
		});
}

function generarObjetoJson(object) {
	return JSON.stringify(object);
}

function verReglas() {
	var estatus = $('#cmbEstatus').val();
	$("#tReglas").remove();
	$("div.dataTables_wrapper").remove();

	$
			.ajax({
				data : null,
				url : restObtenerReglas + "/" + estatus,
				type : 'get',
				contentType : "application/json",
				success : function(response) {

					reglasActuales = response.reglas;

					var table = $("<table>").addClass(
							"table table-bordered table-condensed").attr("id",
							"tReglas").attr("width", "100%");
					var thead = $("<thead/>");
					var fila = $("<tr/>").append(
							$("<th/>").text("Activa").attr("width", "15%"));
					fila.append($("<th/>").text("Nombre Regla"));
					fila.append($("<th/>").text("Regla"));
					fila.append($("<th/>").text("Accion"));
					fila.append($("<th/>").text("Editar"));
					// fila.append($("<th/>").text("Orden").attr("width","15%"));
					thead.append(fila);
					table.append(thead);
					table.append("<tbody/>");

					$("#divTablaReglas").append(table);

					for (var i = 0; i < reglasActuales.length; i++) {

						var newTr = $('<tr>').attr("id", reglasActuales[i].id)
								.attr("estado", reglasActuales[i].activa);
						var activa = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/></td>';
						if (reglasActuales[i].activa != 1) {

							activa = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/></td>';
						}

						newTr.append($('<td>').append(activa));
						newTr.append($('<td>').text(
								reglasActuales[i].nombreRegla));
						newTr.append($('<td>').text(
								reglasActuales[i].reglaVisual));
						newTr.append($('<td>').text(
								reglasActuales[i].accionDescripcion));

						var id = reglasActuales[i].id;
						var objeto = reglasActuales[i];
						var input = $('<button type="button">Editar</button>')
								.attr('class', 'btn btn-primary').attr("id",
										reglasActuales[i].id).click(function() {

									fillForm($(this).attr("id"));

								});

						newTr.append($('<td>').append(input));
						$("#tReglas tbody").append(newTr);
					}

					$('#modalReglas').modal('show');

					$('#tReglas').DataTable({
						"language" : {
							"search" : "Filtro:",
							"paginate" : {
								"next" : "Siguiente",
								"previous" : "Anterior"
							}
						},
						"iDisplayLength" : 10,
						"bLengthChange" : false,
						"info" : false
					});

					function fillForm(object) {
						var arrayReglas = [];
						contFila = 0;
						$(".classActiva").removeClass('hide');
						var check = '';

						for (var i = 0; i < reglasActuales.length; i++) {
							if (reglasActuales[i].id == object) {
								$('#nombreRegla').val(
										reglasActuales[i].nombreRegla);
								$('#accionSel').val(reglasActuales[i].accion)
										.change();
								$('#mensaje').val(reglasActuales[i].mensaje);
								$('#orden').val(reglasActuales[i].orden);
								$('#idRegla').val(reglasActuales[i].id);

								var activa = reglasActuales[i].activa;
								if (activa == 1) {

									check = '<input type="checkbox" id="activaRegla" name = "checkActiva" checked="checked"/>';

								} else {
									check = '<input type="checkbox" name = "checkActiva"  id="activaRegla"/>';

								}
								$('#idCheck').html(check);
								idRegla = $("#idRegla").val();

								$("#reglasTable tbody").empty();

								$.each(reglasActuales[i].parametrosEditar, function(
										i, n) {
									agregarParametro(n);
								});

							}
						}
					}
				}
			});
}

function guardarReglas() {
	var reglasSave = [];
	var valido = null;

	$("#tReglas tbody tr").each(function() {
		if (valido == null) {
			$this = $(this);
			var id = $this.attr("id")
			var estado = $this.attr("estado")
			var check = $this.find("input[type='checkbox']")[0].checked;
			var orden = $this.find("input:last").val();

			if (orden == "")
				valido = "Verificar registros.";

			if (valido == null && parseInt(estado) != verificarCheck(check))
				reglasSave.push({
					"id" : id,
					activa : verificarCheck(check),
					orden : orden
				});
		}
	});

	if (valido == null) {
		$.ajax({
			data : generarObjetoJson({
				reglas : reglasSave
			}),
			url : restActualizar,
			type : 'put',
			contentType : "application/json",
			success : function(response) {

				if (response.error == null) {
					verReglas();
					deshabilitarBoton("btGuardarReglas", false);
					deshabilitarBoton("guardar", false);
					$.dialog({
						title : 'Resultado',
						content : "Proceso Terminado",
					});
				} else {
					deshabilitarBoton("btGuardarReglas", false);
					deshabilitarBoton("guardar", false);
					$.dialog({
						title : 'Error',
						content : response.error,
					});
				}
			}
		});
	} else {
		$.dialog({
			title : 'Error',
			content : valido,
		});
	}
	deshabilitarBoton("btGuardarReglas", false);
}

function verificarCheck(check) {
	if (check)
		return 1;
	else
		return 0;
}

function crearChechBox(activo) {
	var cb;
	if (activo == 1) {
		cb = $('<input>', {
			type : "checkbox",
			"checked" : "checked"
		});
	} else {
		cb = $('<input>', {
			type : "checkbox"
		});
	}
	return cb;
}