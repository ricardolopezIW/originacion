/**
 * Codigo JavaScript para la carga de solicitudes Aprobas que se encuentran fuera de la Mesa de Control
 */


/***
 * Declaracion de variables 
 */


	var urlestBackOffice 		= $("#urlRestBackOffice").val();
	var usuario 				= $("#usuario").val();
	var pageDetalleSolicitud 	= '/backOffice?seccion=detalleGeneral';
	

	function obtenerSolicitudes(){
		var json = JSON.stringify({
			  "idPerfil"	: $("#hiddenIdPerfil").val().trim(),
			  "folio"		: $("#folio").val().trim(),
			  "numCuenta"	: $("#cuenta").val().trim(),
			  "rfc"			: $("#rfc").val().trim(),
			  "telCelular"	: $("#telCelular").val().trim(),
			  "nombre"		: $("#nombre").val().trim(),
			  "aPaterno"	: $("#aPaterno").val().trim(),
			  "aMaterno"	: $("#aMaterno").val().trim()
			});
		$.ajax({
			url : urlestBackOffice + '/solicitud-venta/listado-solicitudes',
			type : 'post',
			contentType : "application/json",
			data : json,
			success : function(response) {
				 $('#modalLoadingSolicitudGeneral').modal('show');
				var datosSolicitud = [];
				console.log(response);
				datosSolicitud = response.lstSolicitdes;
				crearTablaSolicitudes("Tabla Solicitudes",datosSolicitud)
				 $('#modalLoadingSolicitudGeneral').modal('hide');
				
			},
		    error: function(xhr) {
		        console.log();
		        $('#modalLoadingSolicitudGeneral').modal('hide');
		    }
		});
	}
	
	
	
	function crearTablaSolicitudes(name, response){
		
		if ($.fn.dataTable.isDataTable('#tblSolicitudesAprobadas')) {
			tablaSolicitudes.destroy();
		}
		
		tablaSolicitudes = $('#tblSolicitudesAprobadas')
		.DataTable(
			{
				"language" : {
					"lengthMenu" : "_MENU_ " + name,
					"zeroRecords" : "No hay datos",
					"info" : "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty" : "No se encontraron solicitudes relacionadas a los filtros de b&uacute;squeda ",
					"search" : "Buscar:",
					"paginate" : {
						"first" : "Primero",
						"last"  : "Ultimo",
						"next"  : "Siguiente",
						"previous" : "Anterior"
					},
					"infoFiltered" : "(filtrando del _MAX_ total " + name + ")"
				},
				data : response,
				columns : [ {
								title : "Folio",
								"width" : "10%",
								"render" : function(data,
										type, full, meta) {
									if(full.folioApm == null)
										return '-';
									else return full.folioApm;
								}
							},
							{
								title : "Cliente",
								"width" : "30%",
								"render" : function(data,
										type, full, meta) {
									return '' + 
										  (full.nombre != null ? full.nombre : '' ) + ' ' 
										+ (full.apellidoPaterno != null ? full.apellidoPaterno : ' ') +' '
										+ (full.apellidoMaterno != null ? full.apellidoMaterno : ' ');
								}
							},
							{
								title : "Producto",
								"width" : "15%",
								"render" : function(data,
										type, full, meta) {
									if(full.txProducto == null)
										return '-';
									else return full.txProducto;
								}
							},
							{
								title : "Estado Solicitud",
								"width" : "20%",
								"render" : function(data,
										type, full, meta) {
									if(full.estadoSolicitud == null)
										return '-';
									else return full.estadoSolicitud;
								}
							},
							
							{
								title : "Proceso",
								"width" : "20%",
								"render" : function(data,
										type, full, meta) {
									if(full.procesoApm == null)
										return '-';
									else return full.procesoApm;
								}
							},
							{
								title : "Fecha de Venta",
								"width" : "20%",
								"render" : function(data,
										type, full, meta) {
									if(full.fechaVenta == null)
										return '-';
									else {
										var fechaVenta = new Date(full.fechaVenta);
										return moment(fechaVenta).format('DD/MM/YYYY');
										
									}
								}
							},
							{
								title : "Detalle",
								"width" : "10%",
								"render" : function(data,
										type, full, meta) {
									return '<img id="btnDetalleSolicitud" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_detail.png" align="middle" onclick="detalleSolicitudes(\''
										+ full.idSolicitud + '\');"/>';
									;
								}
							}]
			});
	}
	
	
	
	function detalleSolicitudes(idSolicitud){
		var baseUrl = getPath() + pageDetalleSolicitud + "&idSolicitud=" + idSolicitud;
		redireccionar(baseUrl);
	}