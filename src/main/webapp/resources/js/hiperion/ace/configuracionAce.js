	var productos;
	var nombreConfiguracion;
	var templates = [];
	var parametros;
	var mapParametros = new Map();
	var templatesAgregados = [];
	var parametrosSave = [];
	var camposValidos = true;
	var listaConfiguraciones = [];
	var idConfiguracion;
	var idTemplate;
	var restUrl = $("#urlRestBackOffice").val();
//	var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
	console.log("URL: " + restUrl)
	
	
	$(document).ready(function(){
		inicia();		
	});

	function inicia(){
		
		ocultarCampos();
		
		$.ajax(
			{
				data:  null,
				url:   restUrl + '/configuracion-ace/datos-iniciales',
			    type:  'get',
			    contentType: "application/json",
			    success:  function (response){		
				    productos = response.listaProductos;
				    
				    for(var i =0; i<productos.length;i++){				    	
				    	$('#listaProductos').append($('<option>').attr('value', productos[i].id).text(productos[i].descripcion));
					}
				}
			});
		

		$("#btnBuscar").click(function() {
			var idProducto = $("#listaProductos").val();
			limpiarCampos();
			mostrarCamposBuscar();
			$("#listaTemplates").empty();
			
			if(idProducto == 0){
				alertaGeneral("Error", "Debe seleccionar el producto");	
			}
		
			$.ajax(
				{
					url:   restUrl + '/configuracion-ace/' + idProducto,
					type:  'get',
					contentType: "application/json",
					success:  function (response){
						listaConfiguraciones = response.listaConfiguraciones;
						
						for(i=0; i<listaConfiguraciones.length; i++){
							var idConfiguracion = listaConfiguraciones[i].id;
							var tabla = $('<tr>');
							tabla.append($('<td>').append($('<label>').attr('value', idConfiguracion).text(listaConfiguraciones[i].nombre)));
					 		tabla.append($('<td>').append('<input id="hiddenConfiguracion" type="hidden" value="' + idConfiguracion + '"/>')
					 				.append('<img id="buttonEditar" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_edit.png" align="middle" onclick="buscarTemplatesParamAgregados(\'' + idConfiguracion + '\', \'' + listaConfiguraciones[i].nombre + '\');"/>')
					 				.append('<img id="buttonBorrar" class="btnCheck" src="'+requestContextPath+'/resources/img/borrar.png" align="middle" onclick="eliminarConfiguracion(\'' + idConfiguracion + '\');"/>')
					 				.append('<img id="buttonBorrar" class="btnCheck" src="'+requestContextPath+'/resources/img/copiar.jpg" align="middle" width="25px" onclick="clonarConfiguracion(\'' + idConfiguracion + '\');"/>'));
							tabla.append('</tr>');
							
							$("#configuracionTable tbody").append(tabla);
							
						}
						
					}
				});		
		});
		
		
		$("#btnNuevo").click(function() {
			$('#nombreConfiguracion').val("");
			var idProducto = $("#listaProductos").val();
			
			if(idProducto == 0){
				alertaGeneral("Error", "Debe seleccionar el producto");	
			}else{
				templatesAgregados = [];
				idConfiguracion = null;
				
				$("#templatesTable tbody").empty();
				$("#listaTemplates").empty();
				mostrarCamposNuevo();
					
				$.ajax(
						{
							data:  null,
							url:   restUrl + '/configuracion-ace/template-parametro',
							type:  'get',
							contentType: "application/json",
							beforeSend: function () {
					        	$("#loadingModal").modal('show');
							},
							success:  function (response){
								templates = response.listaTemplates;
								mapParametros = response.mapParametros;			
								
								for(var i =0; i<templates.length;i++){				    	
							    	$('#listaTemplates').append($('<option>').attr('value', templates[i].idTemplate).text(templates[i].nombre));
								}
								
								$("#loadingModal").modal('hide');
							}
						});
			}

		});

		$("#btnAgregarTemplate").click(function(){
			var idTemplate = $("#listaTemplates").val();
			agregarTemplate(idTemplate);
		});
		
		
		$("#btnGuardarTemplate").click(function() {
			if ($('#formParametros').parsley().validate())
				guardarTemplate();
		});


		$("#btnGuardarConfig").click(function() {
			
			nombreConfiguracion = $('#nombreConfiguracion').val();
			console.log(nombreConfiguracion)
			
			validaCamposGuardar(nombreConfiguracion);
			
			if(camposValidos){
				$("#btnGuardarConfig").prop('disabled', true);
				var request = {mapParametros: mapParametros,
						listaTemplatesAgregados: templatesAgregados,
						nombreConfiguracion: $("#nombreConfiguracion").val(),
						idProducto: $("#listaProductos").val(),
						idConfiguracion: idConfiguracion
				}
			
				$.ajax(
						{
							data:  generarObjetoJson(request),
							url:   restUrl + '/configuracion-ace',
							type:  'post',
							contentType: "application/json",
							success:  function (response){
								if(response.error == null){
									limpiarCampos();
									ocultarCampos();
									alertaGeneral("Configuraci&oacute;n", "La configuraci&oacute;n se guardo correctamente");	
								
								}else {
									alertaGeneral("Error", response.error);
								}
								
								$("#btnGuardarConfig").prop('disabled', false);
							}
						});
				}
		});
		
		$("#btnClonar").click(function() {
			validaCamposClonado();
			
			if(camposValidos){
				
				var request = {nombreConfiguracion: $("#nombreConfiguracionClon").val(),
						idProducto: $("#listaProductosClon").val(),
						idConfiguracion: idConfiguracion
				}
			
				$.ajax(
						{
							data:  generarObjetoJson(request),
							url:   restUrl + '/configuracion-ace/clonado',
							type:  'post',
							contentType: "application/json",
							success:  function (response){
								if(response.error == null){
									limpiarCampos();
									ocultarCampos();
									$("#modalConfiguracionClon").modal('hide');
									alertaGeneral("Configuraci&oacute;n", "La configuraci&oacute;n se guardo correctamente");
								
								}else {
									alertaGeneral("Error", response.error);
								}
							}
						});
				}
		});	
		
	}
	
	
	function generarObjetoJson(object){
		return JSON.stringify(object);
	}
	
	// Abre el modal con todos los parametros del template seleccionado
	function agregarTemplate(valTemplate){
		idTemplate = valTemplate;
		console.log("Template: " + idTemplate);
		$('#modalTemplate').modal('show');
		
		var listaParametros = mapParametros[idTemplate];
		var requerido = 0;
		var maxlength = 0;
		var numerico = "";
		
		
		$("#ParametrosTable tbody").empty();
		
		for(var i =0; i<listaParametros.length; i++){
			requerido = (listaParametros[i].requerido == 1 ? true : false);
			asterisco = requerido ? " *" : "";
			maxlength = ((listaParametros[i].fin == null ? listaParametros[i].inicio : listaParametros[i].fin)
							- listaParametros[i].inicio) + 1;
			numerico = (listaParametros[i].numerico == 1 ? "data-parsley-type='number'" : "");
			
			// Las etiquetas parsley hace referencia a la validación que se deberá aplicar al input
			var fila = $('<tr>').attr("id",listaParametros[i].idParametro);
			var input = $('<input style="height:25px" type="text" data-parsley-required=\'' + requerido + 
						'\' data-parsley-maxlength=\'' + maxlength + '\'' + numerico + '"/>')
							.val(listaParametros[i].valorDefault).attr('class','form-control')
							.attr("id",listaParametros[i].id)
							.attr("width","40")
							.attr("orden", listaParametros[i].orden)
							.attr("requerido", listaParametros[i].requerido)
							.attr("numerico", listaParametros[i].numerico)
							.attr("configurable", listaParametros[i].configurable)
							.attr("inicio", listaParametros[i].inicio)
							.attr("fin", listaParametros[i].fin)
			fila.append($('<td>').text(listaParametros[i].nombre + asterisco));
			fila.append($('<td>').text(listaParametros[i].inicio));	
			fila.append($('<td>').text(listaParametros[i].fin));
			fila.append($('<td>').text(maxlength));
			fila.append($('<td>').append(input));

			$("#ParametrosTable tbody").append(fila);
		}
	}
	
	
	function guardarTemplate(){
		var nombreTemplate = $("#listaTemplates option:selected").text();
		var paramSeleccionados = [];
		var existeTemplate = false;
			
		// Valida si ya esta agregado el template
		for(var i =0; i<templatesAgregados.length; i++){
			if(templatesAgregados[i].idTemplate == idTemplate){
				existeTemplate = true;
				break;
			}
		}
		if(!existeTemplate){
			templatesAgregados.push({"idTemplate" : idTemplate});
			
			// Agrega template a la tabla		
					
			var tabla = $('<tr>');
			tabla.append($('<td>').append($('<label>').attr('value', idTemplate).text(nombreTemplate)));					
			tabla.append($('<td>').append('<input id="hiddenTemplate" type="hidden" value="' + idTemplate + '"/>')
					.append('<img id="buttonEditar" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_edit.png" align="middle" onclick="agregarTemplate(\'' + idTemplate + '\');"/>')
					.append('<img id="buttonBorrar" class="btnCheck" src="'+requestContextPath+'/resources/img/borrar.png" onclick="eliminarTemplate(\'' + idTemplate + '\');"/>'));
			tabla.append('</tr>');
					
		}

		$("#ParametrosTable tbody tr").each(function() {	
					
			$this = $(this);
			var id     = $this.attr("id")
			var input = $this.find("input[type='text']");
			var valor  = input.val();
			var orden  = input.attr("orden")
			var nombre = ($this.text().replace("*", ""));
			var numerico = input.attr("numerico")
			var requerido = input.attr("requerido")
			var configurable = input.attr("configurable")
			var inicio = input.attr("inicio")
			var fin = input.attr("fin")
			
			var parametrosAce = {"idParametro":id,
								 "nombre": nombre,
								 "valorDefault":valor,
								 "idTemplate":idTemplate,
								 "orden":orden,
								 "numerico":numerico,
								 "requerido":requerido,
								 "configurable":configurable,
								 "inicio":inicio,
								 "fin":fin
								 }		
								paramSeleccionados.push(parametrosAce);
		});				
		parametrosSave.push({"idTemplate":idTemplate, "parametros":paramSeleccionados});
		mapParametros[idTemplate] = paramSeleccionados;
		
		$("#templatesTable tbody").append(tabla);
		
		$('#modalTemplate').modal('hide');
	
	}	
	
	function buscarTemplatesParamAgregados(configuracion, nombreConfig){
		console.log("La configuración seleccionada es: " + configuracion);
		$('#lblNombreConfig').show();
		$('#nombreConfiguracion').val(nombreConfig);
		$("#templatesTable tbody").empty();
		
		mostrarCamposEditar();
		
		$.ajax(
				{
					url:   restUrl + '/configuracion-ace/templates-param-agregados/' + configuracion,
					type:  'get',
					contentType: "application/json",
					beforeSend: function () {
				        	$("#loadingModal").modal('show');
				    },
					success:  function (response){
						var idTemplate = 0;
						templates = response.listaTemplates;
						templatesAgregados = response.listaTemplatesAgregados
						mapParametros = response.mapParametros;		
						
						for(var i =0; i<templates.length;i++){				    	
					    	$('#listaTemplates').append($('<option>').attr('value', templates[i].idTemplate).text(templates[i].nombre));
						}
						
						for(i=0; i<templatesAgregados.length; i++){
							idTemplate = templatesAgregados[i].idTemplate
							var tabla = $('<tr>');
							tabla.append($('<td>').append($('<label>').attr('value', idTemplate).text(templatesAgregados[i].nombre)));
							tabla.append($('<td>').append('<input id="hiddenTemplate" type="hidden" value="' + idTemplate + '"/>')
									.append('<img id="buttonEditar" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_edit.png" align="middle" onclick="agregarTemplate(\'' + idTemplate + '\');"/>')
									.append('<img id="buttonBorrar" class="btnCheck" src="'+requestContextPath+'/resources/img/borrar.png" align="middle" onclick="eliminarTemplate(\'' + idTemplate + '\');"/>'));
							tabla.append('</tr>');							

							$("#templatesTable tbody").append(tabla);
						}
						
						idConfiguracion = configuracion;
						$("#loadingModal").modal('hide');							
					}
				    
				});	
		
	}
	

	function eliminarConfiguracion(configuracion) {
	var idConfiguracion = configuracion;
	console.log("La configuración a eliminar es: " + idConfiguracion);

	$.confirm({
				title : "Atenci&oacute;n",
				content : "&iquest;Esta seguro de eliminar la configuraci&oacute;n?",

				buttons : {
					confirm : {
						text : 'SI',
						action : function() {

							$
									.ajax({
										url : restUrl + '/configuracion-ace/'
												+ idConfiguracion,
										type : 'delete',
										contentType : "application/json",
										success : function(response) {
											if (response.error == null) {
												alertaGeneral(
														"Configuraci&oacute;n",
														"La configuraci&oacute;n se elimino correctamente");

												$(
														"#configuracionTable tbody tr")
														.each(
																function() {
																	var fila
																	$this = $(this);
																	var input = $this
																			.find("input[type='hidden']");

																	if (input
																			.val() == idConfiguracion) {
																		$this
																				.remove();
																	}

																});

												mostrarCamposBuscar();
											} else {
												alertaGeneral("Error",
														response.error);
											}
										}
									});
						}
					},
					cancel : {
						text : 'CANCELAR'
					}
				}
			});

}
	
	function clonarConfiguracion(configuracion){
		idConfiguracion = configuracion;
		$("#modalConfiguracionClon").modal('show');
		$("#listaProductosClon").html($("#listaProductos").html());
	}
	
	function eliminarTemplate(template){
		var idTemplate = template;	
		

			$.confirm({
		title : "Atenci&oacute;n",
		content : "&iquest;Esta seguro de eliminar el template?",
		buttons : {
			confirm : {
				text: 'SI',
				action: function() {
				
				$("#templatesTable tbody tr").each(function() {
					var fila
					$this = $(this);
					var input = $this.find("input[type='hidden']");

					if (input.val() == idTemplate) {
						$this.remove();
					}

				});

				for (i = 0; i < templatesAgregados.length; i++) {
					if (templatesAgregados[i].idTemplate == idTemplate) {
						templatesAgregados.splice(i, 1);
						break;
					}
				}
			}
			},
			
			
			cancel: {
				text: 'CANCELAR'
			}
		}
	});		
	}
	
	function ocultarCampos(){
		$('#lblNombreConfig').hide();
		$('#nombreConfiguracion').hide();
		$("#configuracionTable").hide();
		$("#templatesTable").hide();
		$('#formTemplate').hide();
		$("#btnGuardarConfig").hide();
	}
	
	function mostrarCamposBuscar(){
		ocultarCampos();
		$("#configuracionTable").show();
	}
	
	function mostrarCamposNuevo(){
		ocultarCampos();
		$('#lblNombreConfig').show();
		$('#nombreConfiguracion').show();
		$('#formTemplate').show();
		$("#templatesTable").show();
		$("#btnGuardarConfig").show();
	}
	
	function mostrarCamposEditar(){
		$('#nombreConfiguracion').show();
		$('#formTemplate').show();
		$("#templatesTable").show();
		$("#btnGuardarConfig").show();
	}
	
	function limpiarCampos(){
		$('#nombreConfiguracion').val("");
		$('#nombreConfiguracionClon').val("");
		$("#ParametrosTable tbody").empty();
		$("#configuracionTable tbody").empty();	
		$("#templatesTable tbody").empty();
		$("#listaTemplates").val(0);
	}
	
	
	function validaCamposGuardar(nombreConfiguracion){
		console.log("Validando campos")
		idProducto = $("#listaProductos").val();
		camposValidos = true;
		
		if(idConfiguracion == null && idProducto == 0){
			alertaGeneral("Error", "Debe seleccionar el producto");			
			camposValidos = false;
		}
		
		else if(idConfiguracion == null && nombreConfiguracion == ""){
			alertaGeneral("Error", "Debe ingresar el nombre de la configuraci&oacute;n");			
			camposValidos = false;
		} 
		
		else if(templatesAgregados.length == 0){
			alertaGeneral("Error", "Debe agregar al menos un template");			
			camposValidos = false;			
		}

	}
	
	function validaCamposClonado(){
		console.log("Validando campos");
		idProducto = $("#listaProductosClon").val();
		configuracionClon = $("#nombreConfiguracionClon").val();
		camposValidos = true;
		
		if(configuracionClon == ""){
			alertaGeneral("Error", "Debe ingresar el nombre de la configuraci&oacute;n");				
			camposValidos = false;	
		}
		
		else if(idProducto == 0){
			alertaGeneral("Error", "Debe seleccionar el producto");				
			camposValidos = false;
		}
		
		

	}
	
