    var error = false;
    var restUrl            = $('#urlRestBackOffice').val();
    var restCargaArchivo   = restUrl + "/watch-list/carga";

    $(document).ready(function(){		
		inicializarObjetos();		
	});

	function inicializarObjetos(obj){				
		$( "#guardar" ).click(function() {
			if ($('#formCargaWatch').parsley().validate()){			
			    if(!error){				    	
			    	var obj =  { origen : $("#origen").val(), lugarVenta : $("#lugarVenta").val(), caso : $("#caso").val(),
			    				numero : $("#numero").val(), nombre : $("#nombre").val(), calle : $("#calle").val(),
			    				paterno : $("#paterno").val(), ext : $("#ext").val(), materno : $("#materno").val(),
			    				inT : $("#int").val(), curp : $("#curp").val(), colonia : $("#colonia").val(),
			    				rfc : $("#rfc").val(), codigoPostal : $("#codigoPostal").val(), empresa : $("#empresa").val(),
			    			    municipio : $("#municipio").val(), estado : $("#estado").val(), telCasa : $("#telCasa").val(),
			    			    telOfi  : $("#telOfi").val(), observaciones: $("#observaciones").val(),
			    			    usuario : $("#hiddenUser").val(), nombreUsuario : $("#hiddenUser").val()
								}			   
					var json = generarObjetoJson(obj);
				    guardarCarga(json);
				}
			}
		});	
		
		$( "#limpiar" ).click(function() {
			limpiarForma();
		});	

	}
		
	function limpiarForma(){
		$("#formaCarga input").val("");
		$("#formaCarga textarea").val("");
	}
	
	function guardarCarga(json){
		console.log(json)
		$.ajax(
				{
					data:  json,
				    url:   restCargaArchivo,
				    type:  'post',
				    contentType: "application/json",
				    success:  function (response)
				    {							    	
				    	console.log(response);
				    	if(response.error == null){
				    		limpiarForma();
				    		$.alert({
				    	        title: "Confirmaci&oacute;n",
				    	        content: "Se han guardado los datos.",
				    	        confirmButton: "Ok"
				    	    });
				    	}
				    	else
				    		$.alert({
				    	        title: "Error",
				    	        content: response.error,
				    	        confirmButton: "Ok"
				    	    });
				    }
				});
		
	}
	
	function mostrarMensajeAdv(mensaje){
		$.dialog({ title: 'Verificar', content: mensaje });	
	}
	
	function quitarCaracteres(campo){		
		campo.value = normalize(campo.value);
		console.log(campo.value);
	}

	function generarObjetoJson(object){
		return JSON.stringify(object);
	}