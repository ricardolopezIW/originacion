/**
 * Codigo js para la carga de Folios Lowes
 * 
 */

var files;
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
var restUrl  = $("#urlRestBackOffice").val();
var restCargaArchivo = restUrl + "/lowes/lstfolios";
var tablaFolios;


$(document).ready(function() {
	
	$("#loadingModal").modal('show');
	$('#checkDisponible').bootstrapToggle('on');

	$("#datetimepicker1").datepicker({
		onSelect : function() {
			var dateObject = $(this).datepicker('getDate');
			console.log("La fechA actual es: " + dateObject);
		}
	});

	$('#buscar').click(function() {
		consultarFolios();

	});

	console.log(tablaFolios);
	$('#btn-export').on('click', function() {
		$('<table>').append(tablaFolios.$('tr').clone()).table2excel({
			exclude : ".excludeThisClass",
			name : "Worksheet Name",
			filename : "FoliosErrones", // do not include extension
		});
	});

	$("#loadingModal").modal('hide');
});

$('#datetimepicker1 input').datepicker({
	clearBtn : true,
	orientation : "top auto"
});

$(function() {

	$('#input').fileupload(
			{
				url : restCargaArchivo,
				// contentType: "application/vnd.ms-excel",
				enctype : 'multipart/form-data',
				dataType : 'json',
				autoUpload : true,
				acceptFileTypes : /(\.|\/)(xls)$/i,
				disableImageResize : /Android(?!.*Chrome)|Opera/
						.test(window.navigator.userAgent),
				previewMaxWidth : 100,
				previewMaxHeight : 100,
				previewCrop : true,
				formAcceptCharset : 'utf-8'
			}).on('fileuploadadd', function(e, data) {
		if (data.files[0].name.split(".")[1] != "xls") {
			console.log("archivo incorrecto");
			data.abort();
			alerta("El archivo debe ser de tipo '.xls'.");
		}

	}).on('fileuploaddone', function(e, data) {
		$("#loadingModal").modal('show');
		alerta(data.result.msgError);
		var lista = [];
		lista = data.result.lstFolios;
		if(data.result.codError != 200){
		console.log("Lista: " + lista);
		createTable(lista);
		}
		$("#loadingModal").modal('hide');

	}).prop('disabled', !$.support.fileInput).parent().addClass(
			$.support.fileInput ? undefined : 'disabled');

});

function alerta(texto) {
	$.alert({
		title : "Alerta",
		content : texto,
		confirmButton : "Ok"
	});
}

function swSwitch(componente) {
	var valor = componente.val();

	if (valor == "on") {
		componente.val("off");
		// $('#fecha').removeAttr("disabled");
		// $("#datetimepicker1").datepicker('enable');
		console.log($('#checkDisponible').val());

	} else if (valor == "off") {
		componente.val("on");
		console.log($('#checkDisponible').val());
		// $('#fecha').attr("disabled", true);
	}
}

function consultarFolios() {

	var json = {};
	json["disponible"] = $('#checkDisponible').val() == 'on' ? 1 : 0;
	json["fechaAsignacion"] = $('#datetimepicker1').datepicker('getDate');
	// data('datepicker').getFormattedDate('yyyy-mm-dd');
	// .viewDate;
	console.log("Disponible " + $('#checkDisponible').val());

	var jsonString = JSON.stringify(json);
	console.log("Json String: " + jsonString);

	var formateado = JSON
			.stringify($('#datetimepicker1').data('datepicker').viewDate);
	console.log("Fecha formateada: " + formateado);

	$.ajax({
		data : jsonString,
		url : restUrl + '/lowes/get-folios',
		type : 'POST',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			mostrarTabla(response);
			$("#loadingModal").modal('hide');
		}
	});

}

function cambiarEstado() {
	var fecha = $('#datetimepicker1').datepicker('getDate');

	if (fecha == null) {
		$('#checkDisponible').bootstrapToggle('on');
		console.log("Buscar disponibles");

	} else {
		$('#checkDisponible').bootstrapToggle('off');
		console.log("No buscar los disponibles");
	}


}

function createTable(response) {

	if ($.fn.dataTable.isDataTable("#tablaFoliosrError")) {
		tablaFolios.destroy();
	}

	tablaFolios = $('#tablaFoliosrError').DataTable({
		"language" : {
			"lengthMenu" : "_MENU_ Folios Erroneos",
			"zeroRecords" : "No hay datos",
			"info" : "Mostrando pagina _PAGE_ de _PAGES_",
			"infoEmpty" : "No hay folios erroneos",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "Ultimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			},
			"infoFiltered" : "(filtrando de _MAX_ Folios Erroneos)"
		},
		data : response,
		columns : [ {
			title : "Tipo Cliente",
			"width" : "25%",
			"render" : function(data, type, full, meta) {
				if (full.tipoCliente == null)
					return '-';
				else
					return full.tipoCliente;
			}
		}, {
			title : "idCliente",
			"width" : "20%",
			"render" : function(data, type, full, meta) {
				if (full.idCliente == null)
					return '-';
				else
					return full.idCliente;
			}
		}, {
			title : "Num Tarjeta",
			"width" : "25%",
			"render" : function(data, type, full, meta) {
				if (full.numTarjeta == null)
					return '-';
				else
					return full.numTarjeta;
			}
		}, {
			title : "Consecutivo",
			"width" : "25%",
			"render" : function(data, type, full, meta) {
				if (full.consecutivo == null)
					return '-';
				else
					return full.consecutivo;
			}
		}

		]

	});
	$('#modalFolios').modal('show');

}

function mostrarTabla(response) {

	if ($.fn.dataTable.isDataTable("#tablaFolios")) {
		tablaConsultaFolios.destroy();
	}

	tablaConsultaFolios = $('#tablaFolios').DataTable({
		"language" : {
			"lengthMenu" : "_MENU_ Folios",
			"zeroRecords" : "No hay datos",
			"info" : "Mostrando pagina _PAGE_ de _PAGES_",
			"infoEmpty" : "No hay folios",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "Ultimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			},
			"infoFiltered" : "(filtrando de _MAX_ Folios)"
		},
		data : response,
		columns : [ {
			title : "idCliente",
			"width" : "20%",
			"render" : function(data, type, full, meta) {
				if (full.idCliente == null)
					return '-';
				else
					return full.idCliente;
			}
		}, {
			title : "Num Tarjeta",
			"width" : "20%",
			"render" : function(data, type, full, meta) {
				if (full.numTarjeta == null)
					return '-';
				else
					return full.numTarjeta;
			}
		}, {
			title : "Archivo",
			"width" : "20%",
			"render" : function(data, type, full, meta) {
				if (full.archivo == null)
					return '-';
				else
					return full.archivo;
			}
		}

		]

	});

}
