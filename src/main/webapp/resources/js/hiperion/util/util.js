function alerta(text){
	$.alert({
        title: "Atenci&oacute;n",
        content: text,
        confirmButton: "Ok"
    });
}

function alertaGeneral(title, text){
	$.alert({
        title: title,
        content: text,
        confirmButton: "Ok"
    });
}

function confirma(text){
	$.dialog({ title: 'Atenci&oacute;n', content: text });	
}

function redireccionar(url){
	window.location.replace(url);
}

// Devuelve un parametro recibido por la URL
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

// Devuelve el path actual de la aplicacion
function getPath(){
	var getUrl = window.location;
	return getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
}

// Arma el request para una peticion
function httpRequest(url, data, method, contentType) {
    method = method == undefined ? "POST" : method;
    contentType = contentType == undefined ? "application/json" : contentType;
    switch (method) {
        case "GET":
            var count = 0;
            var params = '';
            Object.keys(data).forEach(function (key) {
                if (count == 0) {
                    params = params + key + '=' + data[key];
                } else {
                    params = params + "&" + key + '=' + data[key];
                }
                count++;
            });
            var ajaxSettings = {
                "async": true,
                "crossDomain": true,
                "url": url + "?" + params,
                "method": "GET",
                "headers": {
                    "content-type": contentType,
                    "cache-control": "no-cache",
                },
                "processData": false
            }
            break;
        default:
            var ajaxSettings = {
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": method,
                "headers": {
                    "content-type": contentType,
                    "cache-control": "no-cache"
                },
                "processData": false,
                "data": JSON.stringify(data)
            }
            break;
    }
    return ajaxSettings;
}

var normalize = (function() {
	var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
    to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
	      mapping = {};
	 
	  for(var i = 0, j = from.length; i < j; i++ )
	      mapping[ from.charAt( i ) ] = to.charAt( i );
	 
	  return function( str ) {
	      var ret = [];
	      for( var i = 0, j = str.length; i < j; i++ ) {
	          var c = str.charAt( i );
	          if( mapping.hasOwnProperty( str.charAt( i ) ) )
	              ret.push( mapping[ c ] );
	          else
	              ret.push( c );
	      }      
	      return ret.join( '' );
	  }
	 
	})();
