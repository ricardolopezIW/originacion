var idUsuario;
var idUsuarioDefault;
var parametros;
var parametrosText;
var camposBase;
var contFila = 0;
var error = false;
var operadores;
var standby;
var reglasActuales = [];
var urlRestBackOffice = $('#urlRestBackOffice').val();
// var urlRestBackOffice = 'http://localhost:8081/siditOriginacion/backOffice'
$(document)
		.ready(
				function() {
					inicializarObjetos();

					$('#loginForm')
							.bootstrapValidator(
									{
										message : 'Este valor no es valido',
										excluded : ':disabled',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										submitButtons : 'button[ id="btnGuardarUsr"]',
										fields : {
											usuario : {
												validators : {
													notEmpty : {
														message : 'El nombre de usuario es requerido'
													},
													stringLength : {
														min : 10,
														max : 10,
														message : 'El nombre debe tener una longitud de 10 caracteres'

													},
													regexp : {
														regexp : /^[a-zA-Z0-9_]+$/,
														message : 'The username can only consist of alphabetical, number and underscore'
													}
												}
											},
											password : {
												validators : {
													notEmpty : {
														message : 'La contraseña es requerida'
													},
													stringLength : {
														min : 8,
														max : 8,
														message : 'El password debe tener 8 caracteres'

													}
												}
											}
										}
									});

					$("#cargarVenta").click(function() {
						cargaLugarVenta();
					});

					$("#guardarLV").click(function() {
						guardarLV();

					});

					$("#agregarUsuario").click(function() {
						standby = false;
						agregarUsuario();
					});

					$("#editUser").click(function() {
						standby = true;
						editarUsuario();
					});

					$("#verReglas").click(function() {
						verReglas();
					});

					$('#btnGuardarUsr')
							.click(
									function() {
										console.log($('#usr').val());
										console.log($('#pass').val());

										if ($('#usr').val().length < 1
												|| $('#pass').val().length < 1) {
											$
													.alert({
														title : 'Datos Incompletos',
														content : 'Rellene el formulario correctamente',
														confirmButton : "OK",
														confirmButtonClass : 'btn-default'

													});
										} else {
											var userDefault = $("#uDefault")
													.is(':checked');

											if (userDefault) {
												$(function() {
													console
															.log("Abriendo confirm");
													$.confirm({
																title : 'Advertencia',
																content : 'Se seleccionar\u00E1 el nuevo usuario como default',

																buttons: {
																confirm : {
																	text : 'Confirmar',
																	action: function() {
																	console
																			.log("Diste confirmar :)");
																	guardarUsuario();
																}
																},
																cancel :  {
																	text : 'Cancelar'
																}
																}

															});

												});

											} else {
												guardarUsuario();

											}

										}
										return;
									});

				});

function inicializarObjetos() {

	$.ajax({
		data : null,
		url : urlRestBackOffice + '/clave-buro/lista-usuarios',
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			// Acci�n durante la ejecuci�n
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {

			// $.each(response, function(i, item) {
			// console.log(item.nombreUsuario);
			// });
			var usuarios = response;
			cargarUsuarios(usuarios);
		}
	});

	$("#guardarLV").hide();
	$("#editUser").hide();
	$("#duallistboxLugarVenta").empty();
	$('.listBoxLugrVenta').bootstrapDualListbox('refresh');
	$("checkDefault").show();
	$("#loadingModalAccesos").modal('hide');
}

function cargarUsuarios(listaUsuarios) {
	$("#cargaUsuarios").empty();
	$("#checkDefault").empty();

	var selectList = $('<select class="form-control" >');
	selectList.attr('onChange', 'buscarLugarVenta(this)');
	selectList.attr('id', 'lstUsr');
	selectList.append($('<option>').attr('value', '-1')
			.text("Selecciona Valor"));

	$.each(listaUsuarios, function(i, item) {
		console.log("Entrando a iterias lista de usuarios");
		console.log("Key " + i + " Value : " + item);
		selectList.append($('<option>').attr('value', item.idUsuario).text(
				item.nombreUsuario));
		if (item.defaultUser == 1) {
			idUsuarioDefault = item.idUsuario;
			console.log("Este es el defaultUser" + idUsuarioDefault + " : "
					+ item.nombreUsuario);
			$("#checkDefault").append(crearChechBox(item.defaultUser));
		}

	});
	$("#cargaUsuarios").append(selectList);
}

function crearChechBox(activo) {
	var cb;
	cb = $('<input>', {
		type : "checkbox",
		"disabled" : "disabled"
	});

	return cb;
}

function buscarLugarVenta(usuario) {

	idUsuario = usuario.value;
	console.log("Usuario: " + idUsuario);
	console.log("Default: " + idUsuarioDefault);
	(usuario.value == idUsuarioDefault ? $("#checkDefault input[type=checkbox]")
			.prop('checked', true)
			: $("#checkDefault input[type=checkbox]").prop('checked', false))

	$
			.ajax({
				url : urlRestBackOffice + '/clave-buro/usuario-lugarVenta/'
						+ idUsuario,
				type : 'get',
				contentType : "application/json",
				beforeSend : function() {
					// Acci�n durante la ejecuci�n
					$("#loadingModalAccesos").modal('show');
				},
				success : function(response)

				{
					armaDualList(response);

					$.each(response, function(i, item) {

					});
				}
			});

	$("#guardarLV").show();
	$("#editUser").show();
	$("#loadingModalAccesos").modal('hide');
}

function armaDualList(response) {
	$("#duallistboxLugarVenta").empty();
	var dualList = $('<select class="listBoxLugrVenta" >');
	dualList.attr('multiple', 'multiple');
	dualList.attr('data-duallistbox_generated', 'true');
	dualList.attr('id', 'dualListLugarVenta');
	dualList.attr('name', 'name_dualListLugarVenta');
	$.each(response, function(i, item) {
		if (item.asignado == true) {
			dualList.append($('<option>').attr('value', item.idLugarVenta)
					.attr('selected', true).text(item.lugarVenta));
		} else {
			dualList.append($('<option>').attr('value', item.idLugarVenta)
					.text(item.lugarVenta));
		}

	});
	console.log(dualList);
	$("#duallistboxLugarVenta").append(dualList);

	$('.listBoxLugrVenta')
			.bootstrapDualListbox(
					{
						nonSelectedListLabel : "<label class='control-label'>Lugar Venta no Asignado:</label>",
						selectedListLabel : "<label class='control-label'>Asignado:</label>",
						infoText : false,
						filterPlaceHolder : "Buscar",
						moveAllLabel : "Mover Todo",
						moveSelectedLabel : "Mover Seleccionados",
						removeSelectedLabel : "Quitar Seleccionados",
						removeAllLabel : "Quitar Todo",
						moveOnSelect : false,
						preserveSelectionOnMove : false,
						eventMoveAllOverride : false
					});
	$('.listBoxLugrVenta').bootstrapDualListbox('refresh');
}

function guardarLV() {
	console.log("Usuario Actual:" + idUsuario);
	let json = [];
	var item = {};
	$("#bootstrap-duallistbox-selected-list_name_dualListLugarVenta option")
			.each(function() {
				var idLugarVenta = $(this).val();
				var lugarVenta = $(this).text();
				var tamanio = $(this).size();
				if ($(this).find('selected', true)) {
					item = {};
					item["idUsuario"] = idUsuario;
					item["idLugarVenta"] = $(this).val();
					json.push(item);
				}
			});
	item = {};
	item["idUsuario"] = idUsuario;
	json.push(item);
	console.log("json: " + json);
	var jsonString = JSON.stringify(json);
	console.log("json: " + jsonString);

	// Guarda en la BD LA RELACION DE USUARIO LUGAR DE VENGA
	$.ajax({
		data : jsonString,
		url : urlRestBackOffice + '/clave-buro/asigna-lugarVenta',
		type : 'POST',
		contentType : "application/json",
		beforeSend : function() {
			// Acci�n durante la ejecuci�n
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {
			console.log("Usuario Lugar Venta guardado");
		}
	});

	$("#loadingModalAccesos").modal('hide');
	setTimeout(
			'mostrarMensajeAdv("Notificaci\u00F3n", "Se guardo la configuraci\u00F3n correctamente")',
			800);
}

function agregarUsuario() {
	$("#divCheckDefault").empty();
	$("#divCheckDefault").append(crearChechBox());
	$("#divCheckDefault input[type=checkbox]").prop('id', 'uDefault');
	$("#divCheckDefault input[type=checkbox]").prop('disabled', false);
	$('#loginForm').bootstrapValidator('resetForm', true); // Limpia el
															// validator cuando
															// se ejecuta
	$("#loginForm")[0].reset();
	$('#loginModal').modal('show');
}

function editarUsuario() {

	$.ajax({
		url : urlRestBackOffice + '/clave-buro/usuario/' + idUsuario,
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			// Acci�n durante la ejecuci�n
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response)

		{
			usuairo = response;
			console.log();
			$('#usr').val(usuairo.nombreUsuario);
			$('#pass').val(usuairo.passwdUsuario);
			$("#divCheckDefault").empty();
			$("#divCheckDefault").append(crearChechBox(usuairo.defaultUser));
			$("#divCheckDefault input[type=checkbox]").prop('id', 'uDefault');
			$("#divCheckDefault input[type=checkbox]").prop('disabled', false);
			$("#divCheckDefault input[type=checkbox]").prop('checked',
					usuairo.defaultUser);

		}
	});

	$('#loginForm').bootstrapValidator('resetForm', true); // Limpia el
															// validator cuando
															// se ejecuta
	$("#loginForm")[0].reset();
	$("#loadingModalAccesos").modal('hide');

	$('#loginModal').modal('show');

}

function guardarUsuario(flag) {
	var usuario = $("#usr").val();
	var password = $("#pass").val();
	var userDefault = $("#uDefault").is(':checked');

	json = {};

	standby ? json["idUsuario"] = idUsuario : null;
	json["nombreUsuario"] = usuario;
	json["passwdUsuario"] = password;
	json["defaultUser"] = userDefault;

	var jsonString = JSON.stringify(json);
	$
			.ajax({
				data : jsonString,
				url : urlRestBackOffice + '/clave-buro/inserta-usuario',
				type : 'post',
				contentType : "application/json",
				beforeSend : function() {
					// Acci�n durante la ejecuci�n
					$("#loadingModalAccesos").modal('show');
				},
				success : function(response) {
					$('#loginModal').modal('hide');
					if (response.error == null) {
						setTimeout('inicializarObjetos()', 500);
						setTimeout(
								'$.dialog({ title: "Resultado", content: standby ? "Usuario Actualizado" : "Usuario Agregado"})',
								600);

					} else {

						$.dialog({
							title : 'Error',
							content : "Consulte a su administrador"
						});
					}
				},
				error : function(response) {
					setTimeout($("#loadingModalAccesos").modal('hide'), 1100);
					var r = jQuery.parseJSON(response.responseText);
					mostrarMensajeAdv("Error", r.mensaje);
				}
			});

}

function mostrarMensajeAdv(titulo, mensaje) {
	$.alert({
		title : titulo,
		content : mensaje,
		confirmButton : "Cerrar"
	});
}

function generarObjetoJson(object) {
	return JSON.stringify(object);
}

function verificarCheck(check) {
	if (check)
		return 1;
	else
		return 0;
}
