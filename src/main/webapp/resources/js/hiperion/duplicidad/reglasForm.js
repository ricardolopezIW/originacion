function incializarFormaBusqueda(response){
	crearComboBox2(response.productos, "productoSelMod");
	crearComboBox2(response.flujos   , "flujoSelMod");
	crearComboBox2(response.flujos   , "flujoSelClo");
	crearComboBox2(response.productos, "productoSelClo");
}

function incializarFormaRegla(response){
	parametros     = response.parametros;							  
	operadores     = ["AND", "OR"];	
	parametrosText = response.parametrosText;									  
	camposBase     = response.camposBase;						    	
	crearComboBox2(response.productos, "productoSel");
	crearComboBox2(response.flujos   , "flujoSel");
	agregarParametro();
}

function agregarParametro(){
	contFila++;
	var newTr = $('<tr>');
	newTr.append($('<td>').append(crearSpanParametro()))
	newTr.append($('<td>').append(crearComboBox(parametrosText)));
	newTr.append($('<td>').append(crearComboBox(["Y", "O"])));
	$("#reglasTable tbody").append(newTr);					
}

function crearSpanParametro(){		  
	  var htmla = $('<a>').attr('href', '#');
	  var span  = $('<span>').attr('data-toggle', 'tooltip').attr('title', 'Remover Parámetro')
	                        .attr('onclick', "removerTr("+contFila+")").addClass('glyphicon glyphicon-remove-circle');		  		 		  
	  htmla.append(span);		  
	  return htmla;
}

function crearComboBox(texts){
	  var selectList = $('<select class="form-control" >');		  		  	 
	  selectList.append($('<option>').attr('value', '-1').text("Selecciona Valor"));
	  
	  for(var i=0; i<texts.length; i++){
		    selectList.append($('<option>').attr('value', i).text(texts[i]));				
	  }
	  return selectList;
}

function crearComboBox2(catalogo, id){			
	  $('#'+id).append($('<option>').attr('value', '').text('Seleccione'));
	  for(var i=0; i<catalogo.length; i++){
		  $('#'+id).append($('<option>').attr('value', catalogo[i].id).text(catalogo[i].descripcion));				
	  }
}

function crearChechBox(activo){
	var cb;
	if(activo == 1){
	   cb = $('<input>', {
		type:"checkbox",
		"checked":"checked"
	   });
	}
	else{
	   cb = $('<input>', {
			type:"checkbox"			
	   });
	}
	return cb;
}

function crearTablaReglas(response){
	reglasActuales = response.reglas;
	
	var table = $("<table>").addClass("table table-bordered table-condensed")
							.attr("id", "tReglas").attr("width","100%");
	
	var thead = $("<thead/>");
	var fila  = $("<tr/>").append($("<th/>").text("Activa").attr("width","15%"));
	fila.append($("<th/>").text("Nombre Regla"));
	fila.append($("<th/>").text("Regla"));
	fila.append($("<th/>").text("Acci\u00F3n"));
	fila.append($("<th/>").text("Editar"));
	thead.append(fila);
	table.append(thead);
	table.append("<tbody/>");

	$("#divTablaReglas").append(table);
	
	for(var i =0; i<reglasActuales.length;i++){
		
		var newTr = $('<tr>').attr("id", reglasActuales[i].id)
							 .attr("estado", reglasActuales[i].activa);
		
		var activa = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/></td>';
		if (reglasActuales[i].activa != 1) {
			activa = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/></td>';
		}

		newTr.append($('<td>').append(activa));
		newTr.append($('<td>').text(reglasActuales[i].nombreRegla));
		newTr.append($('<td>').text(reglasActuales[i].reglaVisual));
		newTr.append($('<td>').text(obtenerTextSelect("accionSel",reglasActuales[i].accion)));

		var id     = reglasActuales[i].id;
		var objeto = reglasActuales[i];
		var input = $('<button type="button">Editar</button>')
					.attr('class', 'btn btn-primary').attr("id",reglasActuales[i].id).click(function() {
						fillForm($(this).attr("id"));
					});

		newTr.append($('<td>').append(input));
		$("#tReglas tbody").append(newTr);	
	}					

	$('#tReglas').DataTable({
			"language": {
			"search": "Filtro:",
			"paginate": {
			"next"     : "Siguiente",
			"previous" : "Anterior"
			}
			},
			"iDisplayLength": 7,
			"bLengthChange": false,
			"info": false
	});
}

function removerTr(id){		
	$("#reglasTable tbody tr:nth-child(" + id + ")").remove();
	contFila--;
}

function removerFilasTablaRegla(){
	
	$("#reglasTable tbody tr").each(function() {				
		$(this).remove();
		contFila--; 
	});	
}

function deshabilitarBoton(boton, activo){
	$( "#"+boton ).prop("disabled",activo);					
}