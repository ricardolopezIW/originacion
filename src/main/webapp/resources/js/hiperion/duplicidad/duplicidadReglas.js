	var parametros;
	var parametrosText;
	var camposBase;	
	var contFila = 0;
	var error = false;
	var operadores;
	var reglasActuales = [];
	var configuracion;
	var paramJson;
	var restUrl            = $('#urlRestBackOffice').val();
	//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
	var restConfiguracion  = restUrl + "/regla-duplicidad/configuracion";
	var restObtenerReglas  = restUrl + "/regla-duplicidad/listado";
	var restActualizar     = restUrl + "/regla-duplicidad/";
	var restGuardar        = restUrl + "/regla-duplicidad";
	var restClonar         = restUrl + "/regla-duplicidad/clon";
	
	$(document).ready(function(){
		inicializarObjetos();		
	});

	function inicializarObjetos(){		
		callAjax(null, restConfiguracion, 'get', inicializarFormas, mostrarMensajeError);
		$( "#addParam" )      .click(function() { agregarParametro(); });
		$( "#btnClonar" )     .click(function() { $('#modalClonar').modal('show'); });
		$( "#btnBuscar" )     .click(function() { verReglas(); });
		$( "#btClonarReglas" ).click(function() { clonarReglas(); });
		$( "#guardar" )       .click(function() { 
			if( $("#idRegla").val() != ""){
				procesoEditar();				
			}				 
			else{
				procesoGuardar();
			}
		});
	}

	function inicializarFormas(response){
		incializarFormaBusqueda(response);
		incializarFormaRegla(response);
	}

	function procesoGuardar(){
		deshabilitarBoton("guardar", true);
		
		if ($('#formaEditar').parsley().validate()) {
			verificarReglas();
		    if(!error){		    	
				var obj = {
						regla      : generarRegla(),
						accion     : $("#accionSel").val(),    
						mensaje    : $("#mensaje").val(),
						nombreRegla: $("#nombreRegla").val(),
						orden      : $("#orden").val(),						
						reglaVisual: generarReglaVisual(),
						mismoProducto : $("#mismoPro").prop("checked") ? 1 : 0,
						parametros : paramJson,
						idProducto : $("#productoSel").val(),
						idFlujo    : $("#flujoSel").val(),						
						configuracion : generarObjetoJson(configuracion)
				}
				console.log(obj)
				var json = generarObjetoJson(obj);
				callAjax(json, restGuardar, 'post', inicializarDespuesGuardar, mostrarMensajeErrorGuardar);
			}
		    else{
		    	deshabilitarBoton("guardar", false);
		    }
		}	
		else{
	    	deshabilitarBoton("guardar", false);
	    }
	}
	
	function procesoEditar(){
		deshabilitarBoton("guardar", true);
		
		if ($('#formaEditar').parsley().validate()) {
			verificarReglas();
		    if(!error){		    	
				var obj = {
						regla      : generarRegla(),
						accion     : $("#accionSel").val(),    
						mensaje    : $("#mensaje").val(),
						nombreRegla: $("#nombreRegla").val(),
						orden      : $("#orden").val(),						
						reglaVisual: generarReglaVisual(),
						parametros : paramJson,
						idProducto : $("#productoSel").val(),
						idFlujo    : $("#flujoSel").val(),
						confIds    : generarObjetoJson(configuracion),
						id         : $("#idRegla").val(),
						activa     : $("#estado").val()
				}
				$("#idRegla").val("");
				var lista = [];
				lista.push(obj);
				var json = generarObjetoJson({reglas:lista});
				callAjax(json, restActualizar, 'put', inicializarDespuesGuardar, mostrarMensajeErrorGuardar);
			}
		    else{
		    	deshabilitarBoton("guardar", false);
		    }
		}	
		else{
	    	deshabilitarBoton("guardar", false);
	    }
	}
	
	function inicializarDespuesGuardar(){
		$("input").val("");						    		
		$("#reglasTable tbody").empty();
		agregarParametro();
		contFila = 1;
		deshabilitarBoton("guardar", false);
		limpiarSelect();
		$.dialog({
			title: 'Completo',
			content: "La operaci\u00F3n fue completada, actualiza las reglas para ver la actualizaci\u00F3n.",
		});	
	}
	
	function mostrarMensajeErrorGuardar(mensaje){
		$.dialog({
			title: 'Error',
			content: mensaje,
		});	
		deshabilitarBoton("guardar", false);
	}
	
	function limpiarSelect(){
		$("#accionSel").val("");    
		$("#productoSel").val("");
		$("#flujoSel").val("");
	}
	
	function mostrarMensajeError(mensaje){
		$.dialog({
			title: 'Error',
			content: mensaje,
		});	
	}
	
	function verReglas(){
		$("#tReglas").remove();
		$("div.dataTables_wrapper").remove();
		
		if ($('#formBuscar').parsley().validate()) {
			var request = { 'idProducto': parseInt($("#productoSelMod").val()), 
					        'idFlujo'   : parseInt($("#flujoSelMod").val()), 
					        'estatus'	: $("#cmbEstatus").val()}		
			callAjax(generarObjetoJson(request), restObtenerReglas, 'post', crearTablaReglas, mostrarMensajeError);
		 }
	}
	
	function generarObjetoJson(object){
		return JSON.stringify(object);
	}
	
	function clonarReglas(){
		 if ($('#formClonar').parsley().validate()) {
				var request = { 'idProducto'   : parseInt($("#productoSelMod").val()), 
						        'idFlujo'      : parseInt($("#flujoSelMod").val()),
						        'idProductoClo': parseInt($("#productoSelClo").val()),
			                    'idFlujoClo'   : parseInt($("#flujoSelClo").val())}	
				if($("#productoSelMod").val() != "" && $("#flujoSelMod").val() != ""){
					$.ajax({
						data:  generarObjetoJson(request),
					    url:   restClonar,
					    type:  'post',
					    contentType: "application/json",
					    success:  function (response)
					    {	
					    	if(response.error == null){				    						    	
					    		$.dialog({
					    			title: 'Operaci\u00F3n Exitosa',
					    			content: "Se han clonado las reglas.",
					    		});
					    	}
					    	else{
					    		mostrarMensajeErrorGuardar("Error en la operaci\u00F3n, comunicarse con el Administrador.");
					    	}
					    }		  
					});	
				}
				else{
					mostrarMensajeErrorGuardar("Se debe seleccionar Producto y Flujo de la secci\u00F3n buscar para clonar.");
				}												
		}
	}
	
	function verificarReglas(){
		//-si es un solo un registro verificar el campo parametro		
		//-si es mayor de uno, verificar hasta el n-1 que tengan operadores y 
		//todos con el campo parametro
		var paramAll = true;
		var operaAll = true;
		var repetido = false;
		var anterior = [];
		
		$("#reglasTable tbody tr").each(function() {
		    $this = $(this);
		    var value    = $this.find("select:first").val();
		    if(value == "-1")
			   paramAll = false;	
			if(paramAll){
			   if(anterior.length == 0)
			      anterior.push(value);
			   else{
				   for(var i=0; i<anterior.length;i++){
					   if(anterior[i] == value)
						  repetido = true;
				   }
				   anterior.push(value);
			   }
			}
		});				
		
		if(contFila > 1){
			var con = 1;
			$("#reglasTable tbody tr").each(function() {				
				$this = $(this);
				var value = $this.find("select:last").val();
				if(con <= (contFila -1) ){
				   if(value == "-1"){
					  operaAll = false; 
				   }
				}
				con++;
			});
		}
		
		if(!paramAll){
			mostrarMensajeAdv("Se debe seleccionar todos los parametros para la consulta.");			
			error = true;
		}
		else if(!operaAll){
			mostrarMensajeAdv("Se debe seleccionar el operador para crear la consulta.");			
			error = true;
		}
		else if(repetido){
			mostrarMensajeAdv("Se tiene un parametro repetido.");			
			error = true;
		}
		else{
			error = false;
		}		
	}
	
	function mostrarMensajeAdv(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function generarRegla(){
        var regla = "";
        paramJson = "{";
        configuracion = [];
		
		if(contFila > 1){	
			var con = 0;		
			$("#reglasTable tbody tr").each(function() {
				$this = $(this);
				var param    = $this.find("select:first").val();
				var operador = $this.find("select:last").val();			
				configuracion.push([param, operador]);
				var str      = "@"+parametros[parseInt(param)] +" = LOWER(TRIM("+ camposBase[parseInt(param)] +"))";
				paramJson   += '"'+parametros[parseInt($this.find("select:first").val())]+'" :'+ '"'+ parametros[parseInt($this.find("select:first").val())]+'" ';
				
				regla += str;
				
				if(con < (contFila -1) ){
				   regla      += " " + operadores[parseInt(operador)] + " ";
				   paramJson  += ",";
				}								
				con++;				
			});			
		}
		else if(contFila == 1){				
			$("#reglasTable tbody tr").each(function() {
				$this = $(this);
				var param    = $this.find("select:first").val();
				var operador = $this.find("select:last").val();			
				configuracion.push([param, operador]);
				var str      = "@"+parametros[parseInt(param)] +" = LOWER(TRIM("+ camposBase[parseInt(param)] +"))";
				paramJson   += '"'+parametros[parseInt($this.find("select:first").val())]+'" :'+ '"'+ parametros[parseInt($this.find("select:first").val())]+'"';
				regla += str;								
			});			
		}
		paramJson  += "}";		
		return regla;		 
	}
	
	function generarReglaVisual(){
        var regla = "";
      		
		if(contFila > 1){	
			var con = 0;		
			$("#reglasTable tbody tr").each(function() {
				$this = $(this);
				var param    = $this.find("select:first option:selected").text();
				var operador = $this.find("select:last option:selected").text();			
				var str      = param +" ";
				regla += str;
				
				if(con < (contFila - 1) ){
				   regla      += " " + operador + "  ";				  
				}								
				con++;				
			});			
		}
		else if(contFila == 1){		
		
			$("#reglasTable tbody tr").each(function() {				
				$this = $(this);
				var param    = $this.find("select:first option:selected").text();
				var operador = $this.find("select:last").val();			
				var str      = param +" ";
				regla += str;						
			});			
		}
		return regla;		 
	}
	
	function fillForm(id) {
		$(".classActiva").removeClass('hide');
		var check = '';
		removerFilasTablaRegla();
		
		for (var i = 0; i < reglasActuales.length; i++) {
			if (reglasActuales[i].id == id) {
				
				$('#nombreRegla').val(reglasActuales[i].nombreRegla);
				$('#accionSel'  ).val(reglasActuales[i].accion).change();
				
				$('#productoSel'  ).val(obtenerIdSelect("productoSel", reglasActuales[i].producto)).change();
				$('#flujoSel'     ).val(obtenerIdSelect("flujoSel",reglasActuales[i].flujo)).change();
				
				$('#mensaje'    ).val(reglasActuales[i].mensaje);
				$('#orden'      ).val(reglasActuales[i].orden);
				$('#idRegla'    ).val(reglasActuales[i].id);

				var activa = reglasActuales[i].activa;
				
				$("#divActiva").show();
				$("#divLbActiva").show();
				$("#estado").val(activa);
				
				var conf = JSON.parse(reglasActuales[i].idsConf);
				
				for(var j=0; j< conf.length; j++){
					agregarParametro();
					$("#reglasTable tbody tr:last").find("select:first").val(conf[j][0]);
					$("#reglasTable tbody tr:last").find("select:last").val(conf[j][1]);
				}
			}
		}
	}
	
	function obtenerIdSelect(id, text){
		var id;
		$("#"+id +" option").each(function() {				
			if( $(this).text() == text)
				id =  $(this).val(); 
		});	
		return id;
	}
	
	function obtenerTextSelect(id, val){
		var texto = "";
		$("#"+id +" option").each(function() {				
			var valor =  $(this).val(); 
			if(  parseInt(valor) == parseInt(val) )
				texto =  $(this).text(); 
		});	
		return texto;
	}
	
	function callAjax(data, url, metodo, funcSucess, funcError){
		$.ajax({
			data:  data,
		    url:   url,
		    type:  metodo,
		    contentType: "application/json",
		    success:  function (response)
		    {	
		    	if(response.error == null){				    						    	
		    		funcSucess(response);
		    	}
		    	else{
		    		funcError("Error en la operaci\u00F3n, comunicarse con el Administrador.");
		    	}
		    }		  
		});
	}