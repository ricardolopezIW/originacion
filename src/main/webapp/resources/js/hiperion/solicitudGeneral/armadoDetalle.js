	function crearCombos(catalogo, id){
		  var selectList = $('<select class="form-control input-sm" id="'+id+'">');		  		  	 
		  selectList.append($('<option>').attr('value', '-1').text("Selecciona Valor"));
		  for(var i=0; i<catalogo.length; i++){
			    selectList.append($('<option>').attr('value', catalogo[i].id).text(catalogo[i].valor));				
		  }
		  return selectList;
	}

	function crearCombosValor(catalogo, id){
		  var selectList = $('<select class="form-control input-sm" id="'+id+'">');		  		  	 
		  selectList.append($('<option>').attr('value', '-1').text("Selecciona Valor"));
		  for(var i=0; i<catalogo.length; i++){
			    selectList.append($('<option>').attr('value', catalogo[i].valor.trim()).text(catalogo[i].descripcion));				
		  }
		  return selectList;
	}
	
	function crearFormaSolicitud(solicitudVenta){
		if(solicitudVenta.venta != null){
			crearSeccionDatos(solicitudVenta.venta);
			$("#folioApm").attr("disabled","false");
	    	$("#lugarVenta").attr("disabled","false");
	    	$("#producto").attr("disabled","false");
	    	$("#numeroCuenta").val(solicitudVenta.numeroCuenta).attr("disabled","false");
		}
	    if(solicitudVenta.sistema != null){
			crearSeccionDatos(solicitudVenta.sistema);
		}
		if(solicitudVenta.personales != null){			
			crearSeccionDatos(solicitudVenta.personales);
		}	
		if(solicitudVenta.domicilio != null){
			crearSeccionDatos(solicitudVenta.domicilio);
		}
		if(solicitudVenta.adicionales != null){
			crearSeccionDatos(solicitudVenta.adicionales);
		}	
		if(solicitudVenta.laborales != null){
			$("#actividadEmpresarial").val(solicitudVenta.laborales.actividadEmpresarial);
			$("#tipoActividadLaboralEm").val(solicitudVenta.laborales.actividadLaboral);			
			crearSeccionDatos(solicitudVenta.laborales);
		}
		if(solicitudVenta.ingresos != null){
			crearSeccionDatos(solicitudVenta.ingresos);
		}	
		if(solicitudVenta.referencias != null){
			crearSeccionDatos(solicitudVenta.referencias);
		}
	

		if(solicitudVenta.checklist != null){
			crearSeccionCheckList(solicitudVenta.checklist);
		}
		
		deshabilitarForma(true);
		crearSeccionBotones();
	}
	
	function crearSeccionDatos(objeto){	
		for (var key in objeto) {
		  if (objeto.hasOwnProperty(key)) {
		 	var val = objeto[key];
			$("#"+key).val(val);
		  }
		}
	}
	
	function crearSeccion(id, titulo){
		var div =  "<div class='panel panel-default' id='"+id+"'>"+
						"<div class='panel-heading'>"+
							"<h4 class='panel-title'>"+
								"<a data-toggle='collapse' data-parent='#accordion' href='#collapse"+contadorSeccion+"'>"+titulo+"</a>"+
							"</h4>"+
						"</div>"+
						"<div id='collapse"+contadorSeccion+"' class='panel-collapse collapse in'>"+
						  "<div class='panel-body'>"+
						  "</div>"+
						"</div>"+
					"</div>";
		contadorSeccion++;
		return div;
	}
	
	function crearTabla(encabezados, datos, id){
	    var tabla = $("<table id='"+id+"'/>").addClass("table-condensed table-bordered");
		var head  = $("<thead/>");
		var fila  = $("<tr/>").attr("bgcolor","#58ACFA");
		
		for(var i=0; i < encabezados.length;i++){
		    fila.append($("<th/>").text(encabezados[i]));
		}
	    head.append(fila);
		
		var fila  = $("<tr/>");
		var body  = $("<tbody/>");

		for(var i=0; i < datos.length;i++){
			var fila  = $("<tr/>");
			for(var j=0; j < datos[i].length;j++){
				fila.append($("<td/>").text(datos[i][j]));
			}
			body.append(fila);			
		}
		tabla.append(head);
		tabla.append(body);
		return tabla;
	}
	
	function crearTablaCheckList(datos, id){
		
	    var tabla = $("<table id='"+id+"'/>").addClass("table-condensed table-bordered");
		var head  = $("<thead/>");
		var fila  = $("<tr/>").attr("bgcolor","#58ACFA");
		
		fila.append($("<th/>").text(""));
		fila.append($("<th/>").text("SI"));
		fila.append($("<th/>").text("NO"));
		
	    head.append(fila);
		
		var fila  = $("<tr/>");
		var body  = $("<tbody/>");

		for(var i=0; i < datos.length;i++){
			fila.append($("<td/>").text(datos[i].nombre));
			if(datos[i].ejecutar == true){
			   fila.append($("<td/>").append("<input type='checkbox' value='' checked='checked'/>"));
			   fila.append($("<td/>").append("<input type='checkbox' value=''/>"));
			}
			else{
			   fila.append($("<td/>").append("<input type='checkbox' value='' />"));
			   fila.append($("<td/>").append("<input type='checkbox' value='' checked='checked'/>"));
			}
			body.append(fila);			
		}
		tabla.append(head);
		tabla.append(body);
		return tabla;
	}
	

	function crearSeccionCheckList(checklistDatos){
		var seccion   = crearSeccion("datosCheckList","Checklist");
		$("#accordion").append(seccion);
		
		var tabla = crearTablaCheckList(checklistDatos, "datosCheckListTb");
		
		$("#accordion").find("div[id='datosCheckList']").find("div.panel-body").append(tabla);
		$("#accordion").find("div[id='datosCheckList']").find("div.panel-body").append("<br/>");
		
		$('#datosCheckListTb').DataTable( {
			"paging":   false,
			"ordering": false,
			"searching": false,
			"info":     false
		} );
	}
	
	function crearSeccionDecision(){
	    var seccion   = crearSeccion('datosDecision','Decision');
		$('#accordion').append(seccion);
				
		var seccion = "<form><div class='radio'>"+
						  "<label><input type='radio' name='optradio'>Continuar</label>"+
						"</div>"+
						"<div class='radio'>"+
						  "<label><input type='radio' name='optradio'>Rechazar Solicitud</label>"+
						"</div>"+
						"<div class='radio disabled'>"+
						  "<label><input type='radio' name='optradio'>En espera de Documentación</label>"+
					  "</div><form/>";
					  
		$('#accordion').find("div[id='datosDecision']").find('div.panel-body').append(seccion);
		$('#accordion').find("div[id='datosDecision']").find('div.panel-body').append('<br/>');
	}
	
	
	
	
	
	function crearSeccionBotones(){
			var botones = "<br/><div class='row'>"+
							"<div class='container col-md-8 col-md-offset-5'>"+
								"<button type='button' class='btn btn-primary' onclick='irListado()'>Cerrar</button>&nbsp;"+
							"</div>";
							  "</div><br/>";
			$("#accordion").append(botones);			
	}

	function deshabilitarForma(accion){
		$("#nombreTarjeta").attr("disabled",accion);
    	$("#datosSistema").find("input[type='text']").attr("disabled",accion);
		$("#datosPersonales").find("input[type='text']").attr("disabled",accion);
		$("#datosDomicilio").find("input[type='text']").attr("disabled",accion);
		$("#datosLaborales").find("input[type='text']").attr("disabled",accion);
		$("#datosIngresos").find("input[type='text']").attr("disabled",accion);
		$("#datosReferencias").find("input[type='text']").attr("disabled",accion);
		$("select").attr("disabled",accion);
		$("#limiteCredito").attr("disabled","false");
		$("#preguntaActivacion").attr("disabled","false");
		$("#autorizacionMercadeo").attr("disabled","false");
		$("#datosAdicionales").find("input[type='text']").attr("disabled",true);
	}
	

	function mostrarMensajeAdv(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function irListado(idSolicitud){
		var baseUrl = getPath() + "/backOffice?seccion=listadoSolicitudes";
		redireccionar(baseUrl);
	}
