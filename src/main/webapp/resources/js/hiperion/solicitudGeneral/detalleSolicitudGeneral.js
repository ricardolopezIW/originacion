    var contadorSeccion  = 10;
    var idSolicitudVenta = getURLParameter('idSolicitud');
	var restUrl          = $('#urlRestBackOffice').val();
	//var restUrl          = "http://localhost:8081/siditOriginacion/backOffice"; //$('#urlRestBackOffice').val();
	var restCatalogo     = restUrl + "/catalogo/lista"
	
	$(document).ready(function(){	
		 
		obtenerCatalogos();
		 
	});
	
	function obtenerCatalogos(){
		$.ajax({
			url : restCatalogo,
			type : 'get',
			contentType : "application/json",
			success : function(response) {
				$('#modalLoadingSolicitudGeneral').modal('show');
				console.log(response)
				$("#divSexo                   ").append(crearCombos(response.sexo, "sexo"));
				$("#divNacionalidad           ").append(crearCombos(response.nacionalidad, "nacionalidad"));
				$("#divEstadoCivil            ").append(crearCombosValor(response.estadoCivil, "estadoCivil"));
				$("#divEstudios               ").append(crearCombosValor(response.estudios, "maximoEstudios"));
				$("#divTieneTDC               ").append(crearCombos(response.respuestaSiNo, "tieneTDC"));
				$("#divTipoActividadLaboral   ").append(crearCombosValor(response.actividadLaboral, "tipoActividadLaboral"));
				$("#divTipoVivienda           ").append(crearCombosValor(response.tipoVivienda, "tipoVivienda"));
				$("#divAutenticaOtroDomicilio ").append(crearCombos(response.respuestaSiNo, "autenticaOtroDomicilio"));
				$("#divAutorizacionMercadeo   ").append(crearCombos(response.respuestaSiNo, "autorizacionMercadeo"));
				$("#divPreguntaActivacion     ").append(crearCombos(response.preguntaActivacion, "preguntaActivacion"));
				$("#divActividadEmpresarial   ").append(crearCombosValor(response.actividadEmpresarial, "actividadEmpresarial"));
				$("#divActividadLaboralEm     ").append(crearCombosValor(response.actividadLaboral, "tipoActividadLaboralEm"));
				var solicitudVenta = obtenerDatos(idSolicitudVenta);
				$('#modalLoadingSolicitudGeneral').modal('hide');
			},
		    error: function(xhr) {
		        mostrarMensajeAdv(xhr.responseJSON.mensaje);
		        $('#modalLoadingSolicitudGeneral').modal('hide');
		    }
		});
	}
	
	function obtenerDatos(idSolicitudVenta){
		$.ajax({
			url : restUrl + '/solicitud-venta/'+idSolicitudVenta,
			type : 'get',
			contentType : "application/json",
			success : function(response) {
				console.log(response)
				var solicitudVenta = { 
						venta: response.venta, personales:response.personal,
						domicilio:response.domicilio, adicionales:response.adicional,
						laborales:response.laborales, ingresos:response.ingresos    ,
						referencias:response.referencias, 
						editarDatos:response.editarDatos,
						asignacion:response.asignacion, verActualizarTsys:response.verActualizarTsys, verAsignar:response.verAsignar,
						verAsignarActualizar: response.verAsignarActualizar, verBuro : response.verBuro,
						numeroCuenta : response.numeroCuenta, checklist:response.checklist,
                        verBtnRechazar: response.verBtnRechazar , verBtnRevision : response.verBtnRevision
						}
				crearFormaSolicitud(solicitudVenta);
			},
		    error: function(xhr) {
		        mostrarMensajeAdv(xhr.responseJSON.mensaje);			     
		    }
		});
	}
				



		
	function getURLParameter(name) {
	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}