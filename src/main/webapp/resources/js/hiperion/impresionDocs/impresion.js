    //var restUrl            = "http://localhost:8081/siditOriginacion/backOffice/";
	var timer = null; 
    interval = 10000;
    value = 0;
    var idProceso;
	
    var restUrl = $("#urlRestBackOffice").val()+"/";
	var restImprimir       = restUrl + "solicitud-venta/asynch/listado/impresion?asynch=true";
	var restVerificarPdf   = restUrl + "solicitud-venta/pdf-verificacion/usuario/";
	var restDescargarPdf   = restUrl + "solicitud-venta/pdf-docs/usuario/";
	var restSubirFolios    = restUrl + "solicitud-venta/archivo/folios-carga";
	
	$(document).ready(function(){
		
	});
	
	$(function() {
		$('#fileupload').fileupload(
				{
					url : restSubirFolios,
					dataType : 'json',
					autoUpload : true,
					acceptFileTypes : /(\.|\/)(txt)$/i,
					disableImageResize : /Android(?!.*Chrome)|Opera/
							.test(window.navigator.userAgent),
					previewMaxWidth : 100,
					previewMaxHeight : 100,
					previewCrop : true,
					formAcceptCharset : 'utf-8'
				}).on('fileuploadadd', function(e, data) {
			if (data.files[0].name.split(".")[1] != "txt") {
				console.log("archivo incorrecto");
				data.abort();
				alerta("El archivo debe ser de tipo '.txt'.");
			}

		}).on('fileuploadprocessalways', function(e, data) {
			console.log("fileuploadprocessalways");

		}).on('fileuploadprogressall', function(e, data) {
			$("#loadingModal").modal('show');

		}).on('fileuploaddone', function(e, data) {
			if (data.result != null) {
				agregarFolios(data.result);
			} else {
				alerta("Error al cargar los Folios.");
			}
			$("#loadingModal").modal('hide');

		}).on('fileuploadfail', function(e, data) {
			$("#loadingModal").modal('hide');

		}).prop('disabled', !$.support.fileInput).parent().addClass(
				$.support.fileInput ? undefined : 'disabled');
	});
	
	function agregarFolio(){
		agregarOptionFolio($("#folio").val());
		$("#folio").val("");
	}
	
	function agregarFolios(ids){
		for(var i=0; i<ids.length;i++){
			agregarOptionFolio(ids[i]);
		}
	}
	
	function agregarOptionFolio(folio){
		var encontrado = false;
		$('#folios').find('option').each(function() {
			if( $(this).text() == folio){
				encontrado = true;
			} 
		});
	
		if(!encontrado){
		  var option = $('<option>'+ folio + '</option>');
		  $("#folios").append(option);
		}
	}
	
	function eliminarFolio(){
		$('#folios :selected').each(function() {
			$(this).remove();
		});
	}
	
	function limpiar(){
		$('#folios').find('option').each(function() {
		   $(this).remove();
		});
		$("#folio").val("");
	}
	
	function imprimir(){
		
		if(!$('#cbSolicitud').prop('checked') && !$('#cbCaratula').prop('checked') && !$('#cbChecklist').prop('checked')){
			mostrarMensajeAdv("Se debe seleccionar un tipo de documento a imprimir");
		}
		else if($('#folios').find('option').length == 0){
			mostrarMensajeAdv("Se debe agregar un folio para imprimir");
		}
		else{
			var folios = []; 
			$('#folios').find('option').each(function() {
				 folios.push($(this).val());
			});
			
			idProceso = getRandomInt(1, 1000);
			var datos = {
					idPerfil   : $('#hiddenIdPerfil').val(),
					caratula   : $('#cbCaratula').prop('checked'),
					checkList  : $('#cbChecklist').prop('checked'),
					solicitud  : $('#cbSolicitud').prop('checked'),
					idSolicitud:folios,
					usuario    : $('#hiddenUser').val(),
					idProceso  : idProceso
			}
			callImprimir(datos);
		}

	}
	
	function callImprimir(data){
		var data = JSON.stringify(data);
	
		$.ajax({
			data : data,
			url  : restImprimir,
			type : 'post',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(output, status, xhr) {
				verificarDescargaPdf();
			},
		    error: function(xhr) {
		    	$("#loadingModalAccesos").modal('hide');
		    	$("div.modal-backdrop").hide()
		        mostrarMensajeAdv(xhr.responseJSON.mensaje);			     
		    }
		});	
	}
	
	function verificarDescargaPdf(){
		if (timer !== null) return;
		  
			timer = setInterval(function () {
		      
					$.ajax({
						url  : restVerificarPdf + $("#hiddenUser").val() + '/' +idProceso,
						type : 'get',
						contentType : "application/json",
						success : function(response) {
							
							if(response.msgError != null && response.msgError != ""){
								$("#loadingModalAccesos").modal('hide');
						    	$("div.modal-backdrop").hide()			
						    	clearInterval(timer);
						    	timer = null;
						    	var tokens = response.msgError.split('');
						    	
						    	if(tokens[tokens.length-1] != ','){
						    		mostrarMensajeAdv(response.msgError);	
						    	}
						    	else{
						    		mostrarMensajeAdv(response.msgError.substring(0, tokens.length-1));
						    	}
								
							}
							else if(response.path != "" && response.path != null ){
						       clearInterval(timer);
							   timer = null;
							   $("#loadingModalAccesos").modal('hide');
							   window.open(restDescargarPdf + $("#hiddenUser").val());
							}
						},
					    error: function(xhr) {
					    	clearInterval(timer);
					    	timer = null;
					    	$("#loadingModalAccesos").modal('hide');
					    	$("div.modal-backdrop").hide()
					        mostrarMensajeAdv("Error al procesar la petición");			     
					    }
					});	
		}, interval);
	}

	function mostrarMensajeAdv(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function getRandomInt(min, max) {
		  return Math.floor(Math.random() * (max - min + 1) + min);
	}