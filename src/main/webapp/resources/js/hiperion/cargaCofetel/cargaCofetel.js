var restUrl            = $('#urlRestBackOffice').val();	
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
var restCargaArchivo   = restUrl + "/cofetel/carga-archivo";
var restCargaConfiguracion  = restUrl + "/cofetel/carga-configuracion";
	
$(document).ready(function(){
		inicializarObjetos();
});

$(function () {
 
	$('#fileupload').fileupload({
        url: restCargaArchivo,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(csv)$/i,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        formAcceptCharset: 'utf-8'
    }).on('fileuploadadd', function (e, data) {   	
        if(data.files[0].name.split(".")[1] != "csv"){
        	console.log("archivo incorrecto");
        	data.abort();
        	alerta("El archivo debe ser de tipo '.csv'.");
        }
    
    }).on('fileuploadprocessalways', function (e, data) {
    	console.log("fileuploadprocessalways");
    
    }).on('fileuploadprogressall', function (e, data) {
    	$("#loadingModal").modal('show');
    
    }).on('fileuploaddone', function (e, data) {
    	 if(data.result.error == null){
    		 alerta("Se ha iniciado el proceso de actualizado de Cofetel.");
    	 }
	     else {
	    	 alerta(data.result.error);
	     }
    	$("#loadingModal").modal('hide');

    }).on('fileuploadfail', function (e, data) {
    	$("#loadingModal").modal('hide');
    
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function alerta(texto){
	$.alert({
        title: "Alerta",
        content: texto,
        confirmButton: "Ok"
    });
}

function inicializarObjetos(obj){				
	$("#fecha").attr('disabled', true);
	$("#actualizados").attr('disabled', true);
	$("#total").attr('disabled', true);
	verificarDatos();
}

function verificarDatos(){
	console.log(restCargaConfiguracion);
		$.ajax(
			{
				url:   restCargaConfiguracion,
			    type:  'get',
			    contentType: "application/json",
			    success:  function (response)
			    {				    	
			    	console.log(response);
			    	if(response.error == null){
			    		$("#fecha").val(response.fechaActualizacion == null ? "" : response.fechaActualizacion);
			    		$("#actualizados").val(response.nuevosReg == null ? "" : response.nuevosReg);
			    		$("#total").val(response.totalReg == null ? "" : response.totalReg);
			    		
			    		if(response.actualizandP == false){
			    			$("#spload").attr('disabled', false);
			    		}
			    		else{
				    		$("#spload").attr('disabled', true);
				    		alert("Actualmente existe una carga en proceso.");
				    	}
			    	}
			    	else{
			    		alert(response.error);
			    		$("#fecha").val(response.fechaActualizacion == null ? "" : response.fechaActualizacion);
			    		$("#actualizados").val(response.nuevosReg == null ? "" : response.nuevosReg);
			    		$("#total").val(response.totalReg == null ? "" : response.totalReg);
			    	}
			    }
			});
}