var listaEstadosCombo = [];
var idEstado;
var listadoTempEstados = [];
var listaEstadosTodos = [];
var listadoTempMunicipios = [];
var listaMunicipiosTodos = [];
var listadoTempCodigos = [];
var listaCodigosTodos = [];

var urlRest                = $('#urlRestBackOffice').val()+"/";
//var urlRest	= "http://localhost:8081/siditOriginacion/backOffice/"
var urlconsultaEstados     = urlRest + 'cobertura/consultaEstados';
var urlinsertaEstados  	   = urlRest + 'cobertura/insertaEstados';
var urlconsultaMunicipios  = urlRest + 'cobertura/consultaMunicipios';
var urlinsertaMunicipios   = urlRest + 'cobertura/insertaMunicipios';
var urlconsultaCP          = urlRest + 'cobertura/consultaCP';
var urlinsertaCP           = urlRest + 'cobertura/insertaCodigoPostal';

$(document).ready(function(){
	iniciaCobertura();		
});

function iniciaCobertura() {
	$("#divEstadosCobertura").hide();
	$("#divMunicipiosCobertura").hide();
	$("#divCpCobertura").hide();
	$("inputCP").keyup(function() {
		alert("n");
		$("inputCP").css("background-color", "pink");
	});
	
}

function consultarCampoCobertura(campo) {
	var cobertura = campo.value;
	$("#divEstadosCobertura").hide();
	$("#divMunicipiosCobertura").hide();
	$("#divCpCobertura").hide();

	if (cobertura == "ESTADOS") {
		$("#divMunicipiosCobertura").hide();
		$("#divCpCobertura").hide();
		buscarEstado();
		$("#divEstadosCobertura").show();

	} else if (cobertura == "MUNICIPIOS") {

		$("#divEstadosCobertura").hide();
		$("#divCpCobertura").hide();
		$("#divMunicipiosCobertura").show();
		$("#btnGuardarMunicipio").hide();
		llenaComboEstado($('#comboEstados'));
		$("#comboEstados").val(0);
		
	} else if (cobertura == "CP") {
		$("#divEstadosCobertura").hide();
		$("#divMunicipiosCobertura").hide();
		$("#divCpCobertura").show();
		$("#btnGuardarCP").hide();
		llenaComboEstado($('#comboEstadosCP'));
		$("#comboEstadosCP").val(0);
		$("#comboMunicipios").val(0);
	}

}


function llenaComboEstado(combo) {
	combo.empty();
	combo.append($('<option>').attr('value', 0).text('Seleccione'));
	
	if(listaEstadosCombo.length < 1){
		$.ajax({
					url : urlconsultaEstados,
					type : 'get',
					contentType : "application/json",
					beforeSend : function() {
						$("#loadingModalAccesos").modal('show');
					},
					success : function(response) {
						if (response.msgError == null) {
							listaEstadosCombo = response.estados;
							for(var i =0; i<listaEstadosCombo.length;i++){			    	
								combo.append($('<option>').attr('value', listaEstadosCombo[i].estadoId).text(listaEstadosCombo[i].nombre));
							}
						}else{
							alertaGeneral("Error", response.msgError);
						}
						$("#loadingModalAccesos").modal('hide');
					}
	
				});
		}else{
			for(var i =0; i<listaEstadosCombo.length;i++){			    	
				combo.append($('<option>').attr('value', listaEstadosCombo[i].estadoId).text(listaEstadosCombo[i].nombre));
			}
		}
}


function buscarEstado(){
	
	var checkEdoSi = $('input:checkbox[name=checkEdoSi]:checked').val();
	var checkEdoNo = $('input:checkbox[name=checkEdoNo]:checked').val();
	var restriccion = seleccionaRestriccion(checkEdoSi, checkEdoNo);
	
	$("#estadoTable").remove();
	$("div.dataTables_wrapper").remove();
	
	$.ajax({
			data:  null,
			url:   urlRest + 'cobertura/estado/' + restriccion,
			type:  'get',
			contentType: "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success:  function (response){
				
				if(response.msgError == null){
					listaEstadosTodos = response.estadosTodos;
					var listaEstados = response.estados;
					
					var table = $("<table>").addClass("table table-striped")//table table-striped
								.attr("id", "estadoTable");
					var thead = $("<thead/>");
					var fila = $("<tr/>").append($("<th/>").text("Estado"));
					fila.append($("<th/>").text("Restricción"));
					fila.append($("<th/>").text("Estado"));
					fila.append($("<th/>").text("Restricción"));
					thead.append(fila);
					table.append(thead);
					table.append("<tbody/>");	
		
					$("#divTablaEstado").append(table);
	
					for(i=0; i<listaEstados.length; i++){
						var tabla = $('<tr>');
						//Primer columna estado
						tabla.append(armaTablaEstado(listaEstados[i], tabla));
						
				 		//Segunda columna Estado
				 		if(++i < listaEstados.length){
				 			tabla.append(armaTablaEstado(listaEstados[i], tabla));
				 		}else{
				 			tabla.append($('<td/>'));
				 			tabla.append($('<td/>'));
						}
				 		tabla.append('</tr>');

						$("#estadoTable tbody").append(tabla);
					}

					$('#estadoTable').DataTable({
						"language": {
						    "search": "Filtro:",
						    "paginate": {
								"next"     : "Siguiente",
								"previous" : "Anterior"
						    }
						 },
						"iDisplayLength": 16,
						"bLengthChange": false,
				        "info": false
					});

				}else{
					alertaGeneral("Error", response.msgError);
				}
				$("#loadingModalAccesos").modal('hide');
			}
		});

}


function insertaEstado() {
	
	for (var i in listaEstadosTodos) {
		
		for(var j in listadoTempEstados){
			if(listaEstadosTodos[i].idEstadoSepomex == listadoTempEstados[j].idEstadoSepomex){
				listaEstadosTodos[i].restriccion = listadoTempEstados[j].restriccion
			}
		}

	}
	
	let json = {
		"listaEstados" : listaEstadosTodos
	};
	json = JSON.stringify(json);

	$.ajax({
		data : json,
		url : urlinsertaEstados,
		type : 'post',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {
			if (response.error == null) {
				alertaGeneral("Restricción Cobertura Estados", "Se guardaron correctamente los cambios");
				listadoTempEstados = [];

			} else{
				alertaGeneral("Error", response.error);
			}
			$("#loadingModalAccesos").modal('hide');
		}

	});
}


function obtenerMunicipio() {
	$("#btnGuardarMunicipio").show();
	$("#municipiosTable").remove();
	$("div.dataTables_wrapper").remove();
	
	var checkMunSi = $('input:checkbox[name=checkMunSi]:checked').val();
	var checkMunNo = $('input:checkbox[name=checkMunNo]:checked').val();
	var restriccion = seleccionaRestriccion(checkMunSi, checkMunNo);
	idEstado = $("#comboEstados").val()

	let json = {
		idEstado : idEstado,
		restriccion : restriccion
	};
	json = JSON.stringify(json);

	$.ajax({
				data : json,
				url : urlconsultaMunicipios,
				type : 'post',
				contentType : "application/json",
				beforeSend : function() {
					$("#loadingModalAccesos").modal('show');
				},
				success : function(response) {
					console.log("Consultando Municipios");
					
					if (response.msgError == null) {
						listaMunicipiosTodos = response.municipiosTodos;
						var listaMunicipios = response.municipios;
						
						var table = $("<table>").addClass("table table-striped")//table table-striped
									.attr("id", "municipiosTable");
						var thead = $("<thead/>");
						var fila = $("<tr/>").append($("<th/>").text("Municipio"));
						fila.append($("<th/>").text("Restricción").attr("width","90px"));
						fila.append($("<th/>").text("Municipio"));
						fila.append($("<th/>").text("Restricción").attr("width","90px"));
						fila.append($("<th/>").text("Municipio"));
						fila.append($("<th/>").text("Restricción").attr("width","90px"));
						thead.append(fila);
						table.append(thead);
						table.append("<tbody/>");
						
						$("#divTablaMunicipio").append(table);
						
						for(i=0; i<listaMunicipios.length; i++){
							var tabla = $('<tr>');
							//Primer columna municipio
							tabla.append(armaTablaMunicipio(listaMunicipios[i], tabla));
							
							//Segunda columna municipio
							if(++i < listaMunicipios.length){
								tabla.append(armaTablaMunicipio(listaMunicipios[i], tabla));
							}else{
								tabla.append($('<td/>'));
								tabla.append($('<td/>'));
							}
							
							//Tercer columna municipio 
							if(++i < listaMunicipios.length){
								tabla.append(armaTablaMunicipio(listaMunicipios[i], tabla));
							}else{
								tabla.append($('<td/>'));
								tabla.append($('<td/>'));
							}
							tabla.append('</tr>');

							$("#municipiosTable tbody").append(tabla);	
						}
						
						$('#municipiosTable').DataTable({
							"language": {
							    "search": "Filtro:",
							    "paginate": {
									"next"     : "Siguiente",
									"previous" : "Anterior"
							    }
							 },
							"iDisplayLength": 15,
							"bLengthChange": false,
					        "info": false
						});

					}else{
						alertaGeneral("Error", response.msgError);
					}

					$("#loadingModalAccesos").modal('hide');
				}

			});

}


function insertaMunicipio() {
	
	for (var i in listaMunicipiosTodos) {
		
		for(var j in listadoTempMunicipios){
			if(listaMunicipiosTodos[i].idMunicipioSepomex == listadoTempMunicipios[j].idMunicipioSepomex){
				listaMunicipiosTodos[i].restriccion = listadoTempMunicipios[j].restriccion
			}
		}

	}
	
	//Regresamos el tamaño de la tabla
//	limitarTabla($('#municipiosTable'));
	
	let json = {
			"listaMunicipios" : listaMunicipiosTodos,
			"idEstado" : idEstado
	};
	json = JSON.stringify(json);

	$.ajax({
		data : json,
		url : urlinsertaMunicipios,
		type : 'post',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModalAccesos").modal('show');
		},
		success : function(response) {
			if (response.error == null) {
				alertaGeneral("Restricción Cobertura Municipios", "Se guardaron correctamente los cambios");
				listadoTempMunicipios = [];
			} else{
				alertaGeneral("Error", response.error);
			}
			$("#loadingModalAccesos").modal('hide');
		}	
		
	});
}


function obtenerMunicipioCP(){
	$("#comboMunicipios").empty();
	$("#comboMunicipios").append($('<option>').attr('value', 0).text('Seleccione'));
	
	let json = {
		idEstado : $("#comboEstadosCP").val(),
	};
	json = JSON.stringify(json);

	$.ajax({
				data : json,
				url : urlconsultaMunicipios,
				type : 'post',
				contentType : "application/json",
				beforeSend : function() {
					$("#loadingModalAccesos").modal('show');
				},
				success : function(response) {
					console.log("Consultando Municipios");
					
					if (response.msgError == null) {
						var listaMunicipios = response.municipios;
						
						for(var i =0; i<listaMunicipios.length;i++){			    	
							$("#comboMunicipios").append($('<option>').attr('value', listaMunicipios[i].idMunicipioSepomex).text(listaMunicipios[i].nombre));
						}
						
					}else{
						alertaGeneral("Error", response.msgError);
					}

					$("#loadingModalAccesos").modal('hide');
					
					}

				});
	
}


function consultarCP() {
	$("#codigoPostalTable").remove();
	$("div.dataTables_wrapper").remove();

	var checkCPSi = $('input:checkbox[name=checkCPSi]:checked').val();
	var checkCPNo = $('input:checkbox[name=checkCPNo]:checked').val();
	var restriccion = seleccionaRestriccion(checkCPSi, checkCPNo);
	
	if($("#comboEstadosCP").val() == 0){
		alertaGeneral("Alerta", "Debe seleccionar el estado");

	}else if($("#comboMunicipios").val() == 0){
		alertaGeneral("Alerta", "Debe seleccionar el municipio");

	}else{
		$("#btnGuardarCP").show();
		
		let json = {
				idEstado : $("#comboEstadosCP").val(),
				idMunicipio : $("#comboMunicipios").val(),
				restriccion : restriccion
			};
			json = JSON.stringify(json);
			
			$.ajax({
					data : json,
					url : urlconsultaCP,
					type : 'post',
					contentType : "application/json",
					beforeSend : function() {
						// Acci�n durante la ejecuci�n
						$("#loadingModalAccesos").modal('show');
					},
					success : function(response) {
						
						console.log("Consultando Codigo Postal");
										
						if (response.msgError == null) {
							listaCodigosTodos = response.codigosPostalesTodos;
							var listaCodigosPostales = response.codigosPostales;
											
							var table = $("<table>").addClass("table table-striped")//table table-striped
										.attr("id", "codigoPostalTable");
							var thead = $("<thead/>");
							var fila = $("<tr/>").append($("<th/>").text("Código Postal"));
							fila.append($("<th/>").text("Restricción"));
							fila.append($("<th/>").text("Código Postal"));
							fila.append($("<th/>").text("Restricción"));
							fila.append($("<th/>").text("Código Postal"));
							fila.append($("<th/>").text("Restricción"));
							thead.append(fila);
							table.append(thead);
							table.append("<tbody/>");
											
							$("#divTablaCP").append(table);
											
							for(i=0; i<listaCodigosPostales.length; i++){
								var tabla = $('<tr>');
								//Primer columna codigo postal
								armaTablaCp(listaCodigosPostales[i], tabla);
								
								//Segunda columna codigo postal
								if(++i < listaCodigosPostales.length){
									armaTablaCp(listaCodigosPostales[i], tabla);
								}else{
									tabla.append($('<td/>'));
									tabla.append($('<td/>'));
								}
												
								//Tercer columna codigo postal
								if(++i < listaCodigosPostales.length){
									armaTablaCp(listaCodigosPostales[i], tabla);			
								}else{
									tabla.append($('<td/>'));
									tabla.append($('<td/>'));
								}
								tabla.append('</tr>');
								
								$("#codigoPostalTable tbody").append(tabla);	
							}
											
							$('#codigoPostalTable').DataTable({
									"language": {
											"search": "Filtro:",
											"paginate": {
												"next"     : "Siguiente",
												"previous" : "Anterior"
											   }
										},
										"iDisplayLength": 15,
										"bLengthChange": false,
										"info": false
									});
	
								}else{
									alertaGeneral("Error", response.msgError);
								}
	
							$("#loadingModalAccesos").modal('hide');
							$("#divCpCobertura").show();
							$("#loadingModalAccesos").modal('hide');
							$("#divAddBtnCp").empty();
						}
					});
	}
}


function insertaCP() {
	
	for (var i in listaCodigosTodos) {
		for(var j in listadoTempCodigos){
			if(listaCodigosTodos[i].cp == listadoTempCodigos[j].cp){
				listaCodigosTodos[i].restriccion = listadoTempCodigos[j].restriccion
			}
		}
	
	}
		
	let json = {
				"listaCodigoPostal" : listaCodigosTodos,
				"idMunicipio" : $("#comboMunicipios").val(),
				"idEstado" : $("#comboEstadosCP").val()
	};
	json = JSON.stringify(json);

	$.ajax({
			data : json,
			url : urlinsertaCP,
			type : 'post',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},
			success : function(response) {
				if (response.error == null) {
					alertaGeneral("Restricción Cobertura Código Postal", "Se guardaron correctamente los cambios");
					listadoTempCodigos = [];
				} else{
					alertaGeneral("Error", response.error);
				}
				$("#loadingModalAccesos").modal('hide');
			}	
	});
}

function armaTablaEstado(estado, tabla){
	var idEstadoCobertura = estado.estadoId;
	var idEstadoSepomex = estado.idEstadoSepomex;
	var checkearSi = idEstadoCobertura != null ? checked="checked" : "";
	var checkearNo = idEstadoCobertura == null ? checked="checked" : "";
	
	tabla.append($('<td>').append($('<label>').text(estado.nombre)));
	tabla.append($('<td>').append('<input name="'+ estado.nombre +'" type="hidden" value="' + idEstadoSepomex + '" />')
				.append('<label class="radio-inline"><input type="radio" id="'+idEstadoSepomex+'|'+estado.nombre+'" name="radioEstado'+ idEstadoSepomex + '" value="1" ' + checkearSi + ' onclick="obtenerRadioEstado(this)" />Si</label>' +
						'<label class="radio-inline"><input type="radio" id="'+idEstadoSepomex+'|'+estado.nombre+'" name="radioEstado'+ idEstadoSepomex + '" value="0" ' + checkearNo + ' onclick="obtenerRadioEstado(this)" />No</label>'));

	return tabla;
}

function armaTablaMunicipio(municipio, tabla){
	var idMunicipioCobertura = municipio.municipioId;
	var idMunicipioSepomex = municipio.idMunicipioSepomex;
	var checkearSi = idMunicipioCobertura != null ? checked="checked" : "";
	var checkearNo = idMunicipioCobertura == null ? checked="checked" : "";
	
	tabla.append($('<td>').append($('<label>').text(municipio.nombre)));
	tabla.append($('<td>').append('<input name="'+ municipio.nombre +'" id="hiddenMunicipio" type="hidden" value="' + idMunicipioSepomex + '"/>')
			.append('<label class="radio-inline"><input type="radio" id="'+idMunicipioSepomex+'|'+municipio.nombre+'" name="radioMunicipio'+ idMunicipioSepomex + '" value="1" ' + checkearSi + ' onclick="obtenerRadioMunicipio(this)" />Si</label>' +
					'<label class="radio-inline"><input type="radio" id="'+idMunicipioSepomex+'|'+municipio.nombre+'" name="radioMunicipio'+ idMunicipioSepomex + '" value="0" ' + checkearNo + ' onclick="obtenerRadioMunicipio(this)" />No</label>'));

	return tabla;
}

function armaTablaCp(codigoPostal, tabla){
	var idCodigoCobertura = codigoPostal.cpId;
	var idCodigo = codigoPostal.cp;
	var checkearSi = idCodigoCobertura != null ? checked="checked" : "";
	var checkearNo = idCodigoCobertura == null ? checked="checked" : "";
			
	tabla.append($('<td>').append($('<label>').text(idCodigo)));
	tabla.append($('<td>').append('<input id="hiddenCodigo" type="hidden" value="' + idCodigo + '"/>')
			.append('<label class="radio-inline"><input type="radio" id="'+idCodigo+'" name="radioCodigo'+ idCodigo + '" value="1" ' + checkearSi + ' onclick="obtenerRadioCodigo(this)" />Si</label>' +
					'<label class="radio-inline"><input type="radio" id="'+idCodigo+'" name="radioCodigo'+ idCodigo + '" value="0" ' + checkearNo + ' onclick="obtenerRadioCodigo(this)" />No</label>'));
	
	return tabla;
}


function seleccionaRestriccion(checkSi, checkNo){
	var restriccion;
	
	if(checkSi == 'on' && checkNo == 'on'){
		restriccion = null;
	
	}else if(checkSi == 'on' && checkNo == null){
		restriccion = 1;
	
	}else if(checkSi == null && checkNo == 'on' ){
		restriccion = 0;
	
	}else{
		restriccion = null;
	}
	
	return restriccion
}


function obtenerRadioEstado(input){
	var radio = $(input).val();
	var id = $(input).attr("id");
	var temp = id.split("|");
	var encontrado = 0;
	
	var estado = {"idEstadoSepomex" : temp[0],
	  		"nombre" : temp[1],
	  		"restriccion" : radio
		}
	
	if(listadoTempEstados.length > 0){
		for(var i in listadoTempEstados){
			if(listadoTempEstados[i].idEstadoSepomex == temp[0]){
				listadoTempEstados[i].restriccion = radio;
				encontrado = 1;
				break;	
			}
		}
		
		if(encontrado != 1){
			listadoTempEstados.push(estado);
		}
		
	}else{
		listadoTempEstados.push(estado);
	}
	
}


function obtenerRadioMunicipio(input){
	var radio = $(input).val();
	var id = $(input).attr("id");
	var temp = id.split("|");
	var encontrado = 0;
	
	var municipio = {"idMunicipioSepomex" : temp[0],
			"nombre" : temp[1],
			"idEstado" : idEstado,
			"restriccion" : radio
	}
	
	if(listadoTempMunicipios.length > 0){
		for(var i in listadoTempMunicipios){
			if(listadoTempMunicipios[i].idMunicipioSepomex == temp[0]){
				listadoTempMunicipios[i].restriccion = radio;
				encontrado = 1;
				break;	
			}
		}
		
		if(encontrado != 1){
			listadoTempMunicipios.push(municipio);
		}
		
	}else{
		listadoTempMunicipios.push(municipio);
	}
	
}


function obtenerRadioCodigo(input){
	var radio = $(input).val();
	var cp = $(input).attr("id");
	var encontrado = 0;
	
	var codigoPostal = {"cp" : cp,
						"restriccion" : radio
	}
	
	if(listadoTempCodigos.length > 0){
		for(var i in listadoTempCodigos){
			if(listadoTempCodigos[i].cp == cp){
				listadoTempCodigos[i].restriccion = radio;
				encontrado = 1;
				break;	
			}
		}
		
		if(encontrado != 1){
			listadoTempCodigos.push(codigoPostal);
		}
		
	}else{
		listadoTempCodigos.push(codigoPostal);
	}
	
}