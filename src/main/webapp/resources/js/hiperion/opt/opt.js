    var error = false;
    var restUrl            = $('#urlRestBackOffice').val();	
	var restConfiguracion  = restUrl + "/opt/configuracion";
	var restGuardar        = restUrl + "/opt";
	
	$(document).ready(function(){
		inicializar();
		inicializarObjetos();
	});

	function inicializarObjetos(){				
		$( "#guardar" ).click(function() {
			verificarCamposVacios();			
		    if(!error){
				var opt =  {token:$("#token").val(), numIntento:$("#numIntento").val(), horas:$("#horas").val(), 
							tokenCreado:$("#tokenCreado").val(), tokenNoCreado:$("#tokenNoCreado").val(), folioNoExiste:$("#folioNoExiste").val(),
							vigenciaError:$("#vigenciaError").val(), tokenCorrecto:$("#tokenCorrecto").val(), tokenIncorrecto:$("#tokenIncorrecto").val(),
							errorValidacion:$("#errorValidacion").val()
							}
				
				var optJson = {token:$("#token").val(), numIntento:$("#numIntento").val(), horas:$("#horas").val()};
			    var msgJson = {	tokenCreado:$("#tokenCreado").val(), tokenNoCreado:$("#tokenNoCreado").val(), folioNoExiste:$("#folioNoExiste").val(),
							    vigenciaError:$("#vigenciaError").val(), tokenCorrecto:$("#tokenCorrecto").val(), tokenIncorrecto:$("#tokenIncorrecto").val(),
						        errorValidacion:$("#errorValidacion").val(), mensajeSms:$("#mensajeSms").val()
						       };
			    
			    opt = {optJson:generarObjetoJson(optJson), msgJson:generarObjetoJson(msgJson)}
				var json = generarObjetoJson(opt);
				callSaveOpt(json);
			}
		});		
	}
	
	function inicializar(){
		$.ajax(
				{
				    url:   restConfiguracion,
				    type:  'get',
				    contentType: "application/json",
				    success:  function (response)
				    {				    					    	
				    	$("#token")          .val(response.token);
				    	$("#numIntento")     .val(response.numIntento);
				    	$("#horas")          .val(response.horas); 
						$("#tokenCreado")    .val(response.tokenCreado);
						$("#tokenNoCreado")  .val(response.tokenNoCreado);
						$("#folioNoExiste")  .val(response.folioNoExiste);
						$("#vigenciaError")  .val(response.vigenciaError);
						$("#tokenCorrecto")  .val(response.tokenCorrecto);
						$("#tokenIncorrecto").val(response.tokenIncorrecto);
						$("#errorValidacion").val(response.errorValidacion);
						$("#mensajeSms")     .val(response.mensajeSms);
				    }
				});
	}
	
	function callSaveOpt(json){
		$.ajax(
				{
					data:  json,
				    url:   restGuardar,
				    type:  'post',
				    contentType: "application/json",
				    success:  function (response)
				    {				    	
				    	console.log(response);
				    	if(response.error == null)
				    		$.alert({
				    	        title: "Confirmaci&oacute;n",
				    	        content: "Se han guardado los datos.",
				    	        confirmButton: "Ok"
				    	    });
				    	else
				    		$.alert({
				    	        title: "Error",
				    	        content: response.error,
				    	        confirmButton: "Ok"
				    	    });
				    }
				});
	}
	
	function verificarCamposVacios(){		
		var vacio = false;
		var mensaje = "";
		
		if($("#token").val() == ""){
			vacio = true;
			mensaje = "El campo token Master es Obligatorio.";
		}
		
		if($("#numIntento").val() == ""){
			vacio = true;
			mensaje = "El campo numero de intentos es Obligatorio.";
		}
		
		if($("#horas").val() == ""){
			vacio = true;
			mensaje = "El campo horas de vigencia es Obligatorio.";
		}
		
		
		if($("#tokenCreado").val() == ""){
			vacio = true;
			mensaje = "El mensaje Token Creado es Obligatorio.";
		}
		
		if($("#tokenNoCreado").val() == ""){
			vacio = true;
			mensaje = "El mensaje Token No Creado es Obligatorio.";
		}
		
		if($("#folioNoExiste").val() == ""){
			vacio = true;
			mensaje = "El mensaje Folio No Existe es Obligatorio.";
		}
		
		if($("#vigenciaError").val() == ""){
			vacio = true;
			mensaje = "El mensaje Vigencia Error es Obligatorio.";
		}
		
		if($("#tokenCorrecto").val() == ""){
			vacio = true;
			mensaje = "El mensaje Token Correcto es Obligatorio.";
		}
		
		if($("#tokenIncorrecto").val() == ""){
			vacio = true;
			mensaje = "El mensaje Token Incorrecto es Obligatorio.";
		}
		
		if($("#errorValidacion").val() == ""){
			vacio = true;
			mensaje = "El mensaje Error Validacion es Obligatorio.";
		}
		
		if(isNaN($("#numIntento").val())){
			vacio = true;
			mensaje = "El campo Numero de Intentos debe ser un numero entero.";
		}
		
		if(isNaN($("#horas").val())){
			vacio = true;
			mensaje = "El campo Horas de Vigencia debe ser un numero entero.";
		}
		
		if(isNaN($("#token").val())){
			vacio = true;
			mensaje = "El campo Token Master debe ser un numero entero.";
		}
		else{
			if( parseInt($("#token").val()) <= 9999 || parseInt($("#token").val()) >= 100000){
				vacio = true;
				mensaje = "El campo Token Master debe ser un numero entero mayor a 9999 y menor a 100000.";
			}
		}
		
		if(vacio){
			mostrarMensajeAdv(mensaje);			
			error = true;
		}
		else{			
			error = false;
		}				   		
	}
	
	function mostrarMensajeAdv(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function generarObjetoJson(object){
		return JSON.stringify(object);
	}
	
	function getURLParameter(name) {
	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}