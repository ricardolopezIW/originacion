var idLugarVenta;
var tablaLugarVenta = "";
var lugaresApm;

var urlRest = $('#urlRestBackOffice').val();
//var urlRest = 'http://localhost:8081/siditOriginacion/backOffice';

$(document).ready(function() {
	$("#btnLv").prop("disabled", true);
	
	// carga inicial de la vista
	consultarLugaresVenta();
	verificarSincLugar();
	
	// boton que activa las acciones de edicion en la pantalla de detalle
	$('#btnEditar').click(function() {
		readonlyDetalleLugarVenta(false);
		$('#btnActualizar').show();
		$('#btnEditar').hide();
		$("#inCodCanal").prop("disabled", true);
		$('#inAPM').prop("disabled", true);
	});

	// accion al presionar el boton de actualizar en la pantalla de edicion,
	// mensaje de confirmacion
	$('#btnActualizar').click(function() {
		if ($('#formDetalle').parsley().validate())
			$('#modalConfirmacion').modal('show');
	});

	// accion al aceptar el modal de confirmacion en la pantalla de
	// edicion/nuevo lugar de venta
	$('#btnAceptarEdit').click(function() {
		$('#modalConfirmacion').modal('hide');
		guardarLugarVenta();
	});
	
	
	// limpia y muestra el modal de detalle de lugar de venta
	$('#btnAplicar').click(function() {
		$('#modalMatch').modal('hide');
		guardarMatch();
	});

});




// carga de la tabla inicial de los lugares de venta
function consultarLugaresVenta() {

	// Se crea el request de la peticion
	var request = httpRequest(urlRest + '/lugar-venta', // url
	{}, // data {}
	'GET', // method
	'application/json' // contentType
	);

	// Accion antes de la ejecucion
	mostrarLoading();

	$.ajax(request).success(function(response) {
		var source = [];
		if (response.error == 'OK') {
			source = response.lista;
		}
		crearTablaLugarVenta(source);
	}).error(function(response) {
		console.log('error ' + response);
		alerta('Hubo un error al Consultar los Lugares de Venta');
		crearTablaLugarVenta(null);
	});

	// Accion despues de la ejecucion
	ocultarLoading();

}

function mostrarLoading() {
	$("#modalLoadingLugar").modal('show');
}

function ocultarLoading() {
	$("#modalLoadingLugar").modal('hide');
}

function crearTablaLugarVenta(source) {

	if ($.fn.dataTable.isDataTable('#tablaLugarVenta')) {
		tablaLugarVenta.destroy();
	}

	tablaLugarVenta = $('#tablaLugarVenta')
			.DataTable(
					{
						"language" : {
							"lengthMenu" : "_MENU_ Lugares",
							"zeroRecords" : "No hay datos",
							"info" : "Mostrando pagina _PAGE_ de _PAGES_",
							"infoEmpty" : "No se encontraron lugares de venta",
							"search" : "Buscar:",
							"paginate" : {
								"first" : "Primero",
								"last" : "Ultimo",
								"next" : "Siguiente",
								"previous" : "Anterior"
							},
							"infoFiltered" : "(filtrando del _MAX_ total lugares)"
						},
						data : source,
						columns : [
								{
									title : "Descripci&oacute;n",
									data : 'nombre',
									"width" : "20%"
								},
								{
									title : "C&oacute;digo Canal",
									"width" : "15%",
									"render" : function(data, type, full, meta) {
										if (full.codigoCanal == null)
											return '-';
										else
											return full.codigoCanal;
									}
								},
								{
									title : "Canal",
									"width" : "20%",
									"render" : function(data, type, full, meta) {
										if (full.canal == null)
											return '-';
										else
											return full.canal;
									}
								},
								{
									title : "Branch",
									"width" : "10%",
									"render" : function(data, type, full, meta) {
										if (full.branch == null)
											return '-';
										else
											return full.branch;
									}
								},
								{
									title : "Mesa de Control",
									"width" : "15%",
									"render" : function(data, type, full, meta) {
										if (full.mesaControl == null
												|| full.mesaControl.nombre == null)
											return '-';
										else
											return full.mesaControl.nombre;
									}
								},
								{
									title : "Riesgo",
									"width" : "10%",
									"render" : function(data, type, full, meta) {
										if (full.riesgo == 0)
											return 'Sin Riesgo';
										else
											return 'Con Riesgo';
									}
								},
								{
									title : "Estatus",
									"width" : "10%",
									"render" : function(data, type, full, meta) {
										if (full.estatus == 0)
											return 'Inactivo'
										else
											return 'Activo'
									}
								},
								{
									title : "Detalle",
									"width" : "10%",
									"render" : function(data, type, full, meta) {
										return '<img id="btnDetalleLugar" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_detail.png" align="middle" onclick="detalleLugar(\''
												+ full.id + '\');"/>';
										;
									}
								} ]
					});
}



$('#btnCancelar').click(function() {
	$('#modalMatch').modal('hide');
	// guardarMatch();
});

function actualizarLugar() {
	$("#modalMatch").modal('show');
}
function verificarSincLugar() {

	var request = httpRequest(urlRest + '/lugar-venta/getDiferencias', // url

	{}, // data {}
	'GET', // method
	'application/json' // contentType
	);

	$.ajax(request).success(function(response) {
		lugaresApm = response.lugaresApm
		lugaresStatusDif = response.lugaresDiferentStatus
		if (lugaresApm.length > 0 ||  lugaresStatusDif.length > 0) {
			$("#btnLv").prop("disabled", false);
		}
		
		$.each(response.lugaresApm, function(i, n) {

			$('#lugaresApm').append($('<option>', {
				value : n.nombre,
				text : n.nombre
			}));
		});

		
		
		$.each(response.lugaresDiferentStatus, function(i, n) {

			$('#lugaresDifEstatus').append($('<option>', {
				value : n.nombre,
				text : n.nombre
			}));
		});
		
		
		// $("#modalMatch").modal('show');

	}).error(function(response) {
		console.log('error ' + response);
		alerta('Hubo un error al Consultar los Lugares de Venta');
	});

}

function guardarMatch() {

	var request = httpRequest(urlRest + '/lugar-venta/saveLugaresApm', // url

	{}, // data {}
	'PUT', // method
	'application/json' // contentType
	);

	$.ajax(request).success(function(response) {

		$("#btnLv").prop("disabled", true);		
		consultarLugaresVenta();

	}).error(function(response) {
		console.log('error ' + response);
		alerta('Hubo un error al Consultar los Lugares de Venta');
	});

}

function nuevoLugar() {
	idLugarVenta = null;
	obtenerMesasControl(-1);
	obtenerMensajerias(-1);
	$('#inDescripcion').val('');
	$('#inCanal').val('');
	$('#inBranch').val('');
	$('#inCodCanal').val('');
	$('#inAPM').val('');
	$('#btnEditar').hide();
	$('#inCustom10').val('');
	$('#inCustom11').val('');
	$('#inStatusCode').val('');
	$('#inReasonCode').val('');
	$('#txaDescripcion').val('');
	$('#btnActualizar').show();
	$("#inCodCanal").prop("disabled", false);
	$('#inAPM').prop("disabled", false);
	var selectEstatus = document.getElementById("lstEstatus");
	selectEstatus.options[0] = new Option('Inactivo', 0, false, false);
	selectEstatus.options[1] = new Option('Activo', 1, false, true);
	armaListaRiesgo();
	armaListaCustom80();
	armaRadioGeneraNip("", 'checked');
	armaListaTipoVenta();

	readonlyDetalleLugarVenta(false);
	$("#modalDetalleLugar").modal('show');
	$('#formDetalle').parsley().reset();
}

// obtiene el detalle de un lugar de venta
function detalleLugar(id) {
	console.log('detalleLugar ' + id);
	$('#modalDetalleLugar').bootstrapValidator('resetForm', true); // Limpia el
	// validator
	// cuando se
	// ejecuta
	idLugarVenta = id;

	// Se crea el request de la peticion
	var request = httpRequest(urlRest + '/lugar-venta/' + id, // url
	{}, // data {}
	'GET', // method
	'application/json' // contentType
	);
	mostrarLoading();
	$('#modalConfirmacion').modal('hide');

	$.ajax(request).success(function(response) {
		var source = [];
		if (response.error == 'OK') {
			source = response.lugarVenta;
		}
		obtenerMesasControl(source.idMesaControl);
		obtenerMensajerias(source.id);
		$('#inDescripcion').val(source.nombre);
		$('#inCanal').val(source.canal);
		$('#inCodCanal').val(source.codigoCanal);
		$('#inAPM').val(source.idAPM);
		$('#inBranch').val(source.branch);
		$('#inCustom10').val(source.customData10);
		$('#inCustom11').val(source.customData11);
		$('#inStatusCode').val(source.statusCode);
		$('#inReasonCode').val(source.reasonCode);
		$('#txaDescripcion').val(source.descripcion);

		var checkSi = source.generaNip == 'S' ? "checked" : ""
		var checkNo = source.generaNip == 'N' ? "checked" : ""
		armaRadioGeneraNip(checkSi, checkNo);

		var selectEstatus = document.getElementById("lstEstatus");
		if (source.estatus == 0) {
			selectEstatus.options[0] = new Option('Inactivo', 0, false, true);
			selectEstatus.options[1] = new Option('Activo', 1, false, false);
		} else {
			selectEstatus.options[0] = new Option('Inactivo', 0, false, false);
			selectEstatus.options[1] = new Option('Activo', 1, false, true);
		}

		armaListaRiesgo();
		$('#lstRiesgo').val(source.riesgo);
		armaListaCustom80();
		$('#lstCustom80').val(source.customData80);
		armaListaTipoVenta();
		$('#lstTipoVenta').val(source.tipoVenta);

		readonlyDetalleLugarVenta(true);
		$('#btnActualizar').hide();
		$('#btnEditar').show();
		ocultarLoading();
		$("#modalDetalleLugar").modal('show');
	}).error(function(response) {
		console.log('error ' + response);
		$("#modalDetalleLugar").modal('hide');
		alerta('Hubo un error al Cargar el Detalle del Lugar de Venta');
	});

	// Accion despues de la ejecucion
	ocultarLoading();
}

// coloca los campos en editables o no editables
function readonlyDetalleLugarVenta(activo) {
	$('#inDescripcion').attr('readonly', activo);
	$('#inCanal').attr('readonly', activo);
	$('#inCodCanal').attr('readonly', activo);
	$('#inEstatus').attr('readonly', activo);
	$('#inAPM').attr('disabled', activo);
	$('#lstMesaControl').attr('disabled', activo);
	$('#lstEstatus').attr('disabled', activo);
	$('#inBranch').attr('readonly', activo);
	$('#lstRiesgo').attr('disabled', activo);
	$('#inCustom10').attr('readonly', activo);
	$('#inCustom11').attr('readonly', activo);
	$('#inStatusCode').attr('readonly', activo);
	$('#inReasonCode').attr('readonly', activo);
	$('#lstCustom80').attr('disabled', activo);
	$("input[type=radio]").attr('disabled', activo);
	$('#lstTipoVenta').attr('disabled', activo);
	$('#txaDescripcion').attr('readonly', activo);
}

function obtenerMesasControl(idDefault) {
	console.log('obtenerMesasControl idDefault: ' + idDefault);
	document.getElementById('lstMesaControl').options.length = 0;
	var select = document.getElementById("lstMesaControl");

	// Se crea el request de la peticion
	var request = httpRequest(urlRest + '/mesa-control', // url
	{}, // data {}
	'GET', // method
	'application/json' // contentType
	);

	$
			.ajax(request)
			.success(
					function(response) {
						var source = [];
						source = response.listaMesaControl;
						$.each(source, function(i, item) {
							if (item.id == idDefault)
								select.options[i] = new Option(item.nombre,
										item.id, false, true);
							else
								select.options[i] = new Option(item.nombre,
										item.id);
						});
						if (idDefault == -1)
							select.options[document
									.getElementById("lstMesaControl").options.length] = new Option(
									'Sin Asignar', -1, false, true);
					}).error(function(response) {
				console.log('error ' + response);
				select.options[0] = new Option('Seleccione', -1);
			});
}

function obtenerMensajerias(idLugarVenta) {
	console.log('obtenerMensajerias idLugarVenta ' + idLugarVenta);
	var source = [];

	// Se crea el request de la peticion
	var request = httpRequest(urlRest + '/lugar-venta/' + idLugarVenta
			+ '/mensajeria', // url
	{}, // data {}
	'GET', // method
	'application/json' // contentType
	);
	$.ajax(request).success(function(response) {
		source = response.mensajerias;
		armaDualList(source);
	}).error(function(response) {
		console.log('error ' + response);
		armaDualList(source);
	});

}

// arma el select multiple de las mensajerias
function armaDualList(response) {
	$("#duallistboxMensajeria").empty();
	var dualList = $('<select class="listBoxMensajeria" >');
	dualList.attr('multiple', 'multiple');
	dualList.attr('data-duallistbox_generated', 'true');
	dualList.attr('id', 'dualListMensajeria');
	dualList.attr('name', 'name_dualListMensajeria');
	$.each(response, function(i, item) {
		if (item.activa == 1) {
			dualList.append($('<option>').attr('value', item.id).attr(
					'selected', true).text(item.nombre));
		} else {
			dualList.append($('<option>').attr('value', item.id).text(
					item.nombre));
		}
	});
	$("#duallistboxMensajeria").append(dualList);
	$('.listBoxMensajeria')
			.bootstrapDualListbox(
					{
						nonSelectedListLabel : "<label class='control-label'>Mensajer&iacute;a No Asignada:</label>",
						selectedListLabel : "<label class='control-label'>Mensajer&iacute;a Asignada:</label>",
						infoText : false,
						filterPlaceHolder : "Buscar",
						moveAllLabel : "Mover Todo",
						moveSelectedLabel : "Mover Seleccionados",
						removeSelectedLabel : "Quitar Seleccionados",
						removeAllLabel : "Quitar Todo",
						moveOnSelect : false,
						preserveSelectionOnMove : false,
						eventMoveAllOverride : false
					});
	$('.listBoxMensajeria').bootstrapDualListbox('refresh');
}

// guarda o actualiza el lugar de venta
function guardarLugarVenta() {
	console.log("guardarLugarVenta " + idLugarVenta);
	let arrMensajeria = [];
	var lugarVenta = {};
	$("#bootstrap-duallistbox-selected-list_name_dualListMensajeria option")
			.each(function() {
				var idMensajeria = $(this).val();
				var nombreMensajeria = $(this).text();
				var tamanio = $(this).size();
				var mensajeria = {};
				if ($(this).find('selected', true)) {
					mensajeria = {};
					mensajeria["id"] = idMensajeria;
					arrMensajeria.push(mensajeria);
				}
			});

	if (idLugarVenta != null)
		lugarVenta["id"] = idLugarVenta;

	lugarVenta["nombre"] = $('#inDescripcion').val();
	lugarVenta["canal"] = $('#inCanal').val();
	lugarVenta["codigoCanal"] = $('#inCodCanal').val();
	lugarVenta["idAPM"] = $('#inAPM').val();
	lugarVenta["branch"] = $('#inBranch').val();
	lugarVenta["estatus"] = $('#lstEstatus').val();
	lugarVenta["riesgo"] = $('#lstRiesgo').val();
	lugarVenta["idMesaControl"] = $('#lstMesaControl').val() == -1 ? null : $(
			'#lstMesaControl').val();
	lugarVenta["customData10"] = $('#inCustom10').val();
	lugarVenta["customData11"] = $('#inCustom11').val();
	lugarVenta["customData80"] = $('#lstCustom80').val();
	lugarVenta["statusCode"] = $('#inStatusCode').val();
	lugarVenta["reasonCode"] = $('#inReasonCode').val();
	lugarVenta["generaNip"] = $("input[name=radioNip]:checked").val();
	lugarVenta["mensajerias"] = arrMensajeria;
	lugarVenta["tipoVenta"] = $('#lstTipoVenta').val();
	lugarVenta["descripcion"] = $('#txaDescripcion').val();

	var jsonString = JSON.stringify(lugarVenta);
	console.log("jsonStr: " + jsonString);

	// Se crea el request de la peticion
	var request = httpRequest(urlRest + '/lugar-venta', // url
					lugarVenta, // data {}
					'POST', // method
					'application/json' // contentType
			);

	$.ajax(request).success(function(response) {
		source = response.error;
		if (source == 'OK') {
			alerta('El Lugar de Venta se ha guardado exitosamente');
			idLugarVenta = null;
			$("#modalDetalleLugar").modal('hide');
			consultarLugaresVenta();
		} else {
			console.log(response.error);
			alerta(response.estado);
		}
	}).error(function(response) {
		console.log(response);
		console.log('error al guardar/ actualizar el lugar de venta ');
		alerta('No se pudo procesar la peticion');
	});
}

function armaListaRiesgo() {
	$('#lstRiesgo').empty();
	$('#lstRiesgo').append($('<option>').attr('value', "").text('Seleccione'));
	$('#lstRiesgo').append($('<option>').attr('value', 0).text('Sin Riesgo'));
	$('#lstRiesgo').append($('<option>').attr('value', 1).text('Con riesgo'));
}

function armaRadioGeneraNip(checkSi, checkNo) {
	$('#divRadioGeneraNip').empty();
	$('#divRadioGeneraNip')
			.append(
					'<label class="radio-inline"><input type="radio" id="generaNipSi" name="radioNip" value="S" '
							+ checkSi + '/>Si</label>');
	$('#divRadioGeneraNip')
			.append(
					'<label class="radio-inline"><input type="radio" id="generaNipNo" name="radioNip" value="N" '
							+ checkNo + '/>No</label>');
}

function armaListaCustom80() {
	$('#lstCustom80').empty();
	$('#lstCustom80')
			.append($('<option>').attr('value', "").text('Seleccione'));
	$('#lstCustom80').append(
			$('<option>').attr('value', 'Vendida').text('Vendida'));
	$('#lstCustom80').append(
			$('<option>').attr('value', 'Colocada').text('Colocada'));
	$('#lstCustom80').append(
			$('<option>').attr('value', 'Activada').text('Activada'));
}

function armaListaTipoVenta() {
	$('#lstTipoVenta').empty();
	$('#lstTipoVenta').append(
			$('<option>').attr('value', "").text('Seleccione'));
	$('#lstTipoVenta').append(
			$('<option>').attr('value', 'Empleado').text('Empleado'));
	$('#lstTipoVenta').append(
			$('<option>').attr('value', 'Mercado Abierto').text(
					'Mercado Abierto'));
}
