    var contadorSeccion  = 10;
    var idSolicitudVenta = getURLParameter('idSolicitud');
	var restUrl          = $('#urlRestBackOffice').val();
//	var restUrl          = "http://localhost:8081/siditOriginacion/backOffice"; //$('#urlRestBackOffice').val();
	var restManual       = restUrl + "/solicitud-venta/{id}/cuenta/manual";
	var restAutomatico   = restUrl + "/solicitud-venta/{id}/cuenta/automatica";
	var restActualizar   = restUrl + "/solicitud-venta/cuenta-actualizar/{id}";
	var restActualizarForma = restUrl + "/solicitud-venta/solicitud-actualizar/{id}";
	var restCatalogo     = restUrl + "/catalogo/lista"
	var tipoAsignacion;
	var error			 = false
	var flujoCuenta      = false;
	var estadoSolicitud;
	var solicitudVenta = ''; 
	
/**VARIABLES DE ACTIVACION**/
	
	
	$("#cerrar").hide();
	var ipWebServices = null;
	var nip = "";
	var nipAnterior = "";
	var intentosNIP = 0;
	var urlRest = $('#urlRestBackOffice').val();
	//var urlRest = 'http://localhost:8081/siditOriginacion/backOffice';
	var urlConsultaDatos = urlRest + '/activaCta/datosCliente';
	var usuarioId = $('#hiddenIdPerfil').val(); 

	var user = $('#hiddenUser').val(); 
	var pass = null;
	
	
	$(document).ready (function (){
		iniciarActivacion();
		});
	
	function iniciarActivacion(){	
		console.log('Inicio');
		restManual     		= restManual.replace("{id}", idSolicitudVenta);
		restAutomatico 		= restAutomatico.replace("{id}", idSolicitudVenta);
		restActualizar 		= restActualizar.replace("{id}", idSolicitudVenta);
		restActualizarForma = restActualizarForma.replace("{id}", idSolicitudVenta);
		
		usuarioId = sessionStorage.getItem("usuarioId");
		usuario = sessionStorage.getItem("usuario");
		contrasenia = sessionStorage.getItem("contrasenia");
		ipWebServices = sessionStorage.getItem("ipWebServices");
		console.log(producto);
		ingresarCodActivacion('', '');
		getDatosUsuarioActivacion();
		 
		
	}
	

	function obtenerDatosAct(idSolicitudVenta){
		$.ajax({
			url : restUrl + '/solicitud-venta/'+idSolicitudVenta,
			type : 'get',
			contentType : "application/json",
			beforeSend : function() {
				$("#loadingModalAccesos").modal('show');
			},	
			success : function(response) {
				console.log(response)
				var solicitudVenta = { 
						venta: response.venta, sistema: response.sistema, personales:response.personal,
						domicilio:response.domicilio, adicionales:response.adicional,
						laborales:response.laborales, ingresos:response.ingresos    ,
						referencias:response.referencias, 
						buro:response.buro, 
						buroEncabezadosSegmentos:response.buroEncabezadosSegmentos, buroDatosSegmentos:response.buroDatosSegmentos,
						buroEncabezadosDirecciones:response.buroEncabezadosDirecciones, buroDatosDirecciones:response.buroDatosDirecciones,
						editarDatos:response.editarDatos,
						asignacion:response.asignacion, verActualizarTsys:response.verActualizarTsys, verAsignar:response.verAsignar,
						verAsignarActualizar: response.verAsignarActualizar, verBuro : response.verBuro,
						numeroCuenta : response.numeroCuenta, checklist:response.checklist,
                        verBtnRechazar: response.verBtnRechazar , verBtnRevision : response.verBtnRevision
						}
				estadoSolicitud = response.estado;
				tipoAsignacion = response.asignacion;
				console.log(tipoAsignacion)
				$("#loadingModalAccesos").modal('hide');
				
			},
		    error: function(xhr) {
		        mostrarMensajeAdvertencia(xhr.responseJSON.mensaje);			     
		    }
		});
		
	}
				

	
	function mostrarMensajeAdvertencia(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
		$("#loadingModalAccesos").modal('hide');
	}
		
	
	function validateNumber(event){
		event.value = event.value.replace(/[^0-9\.]/g,'');
	}
	
	
	
	/********************************************ACTIVACION DE LA CUENTA********************************************************/
	
	
	function getDatosUsuarioActivacion(){
		console.log('Consultando usuario' +  $("#hiddenUser").val());
		var url = 
		$.ajax({
			url 	: restUrl + '/activaCta/consult-usuario/' + $('#hiddenUser').val(),
			type	: 'get',
			async	: false, 
			headers	: {
				Accept: "application/json; charset=utf-8",
						"Content-type": "application/json; charset=utf-8"
			},
			success: function (response){
				if (response.codError == 0 && response.pass != null) {
					pass = response.pass;
					user = response.usuario;
				console.log('Se encontraron datos de activacion')}
			},
			error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices consulta Datos de usuario de activacion');
	        	alertaPop('No se logro obtener los datos del usuario que intenta activar');
	        }
		});
		
	}
	
	
	function enviarNumCuenta(){
		console.log('Comenzando proceso de activacion');
		var numCuenta = $("#numCuenta").val();
		if(numCuenta.length == 16 && pass != null){
				enviarTokenActivacion(numCuenta, producto, 0);	
		} else {
			
			alerta(pass == null ? numCuenta.length != 16 ? 
					"Cuenta no valida; Usuario activacion invalido; "
				  : "No se logro obtener los datos del usuario para activar "
				  : "El n&uacute;mero de cuenta escrito es incorrecto");
		}
	}
	function enviarTokenActivacion(numCuenta, producto, reenvio){
		console.log('Enviando token de activacion');
		var data = JSON.stringify({
			"numCuenta"		: numCuenta,
	    	"usuario"		: user,
	    	"contrasenia"	: pass
	    });
		
		$.ajax({
			data:  data,
			url:   urlRest +'/activaCta/mandarToken',
	        type:  'post',
	        headers: {          
	        	Accept: "application/json; charset=utf-8",         
	        	"Content-Type": "application/json; charset=utf-8"   
	        }, 
	        beforeSend: function () {
	            //Acción durante la ejecución
	        	$("#loadingModalAccesos").modal('show');
	        },
	        success:  function (response) {
	        	//Accion al terminar la ejecucion
	        	
	        	if(response.code == '200' || response.code == '0' ){
	        		ingresarCodActivacion(numCuenta, producto,reenvio);
	        		$("#loadingModalAccesos").modal('hide');
		        } else {
		        	alertaPop(response.msgCode);
		        	$("#loadingModalAccesos").modal('hide');
		        }
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices ActivaPin');
	        	alertaPop('No se ha enviando el Token.');
	        	$("#loadingModalAccesos").modal('hide');
	        }
	    });
	}

	function ingresarCodActivacion(numCuenta, producto, reenvio){
		if (reenvio != 1) {
			
		
		$("#formActivacion").trigger("reset");
		var html = "<div class='col-lg-3'>";				
		html += "<div class='row'>";
		html += "<div id='divNumCuentaCodAct' class='form-group'>";
		html += "<label class='control-label'><font size='4'>Ingrese C&oacute;digo de Activaci&oacute;n</font></label> "
		html += "<br/>";
		html += "<label class='control-label'>Cuenta:</label>";
		html += "<label><font color='#FFFFFF'>--</font></label>";
		html += "<label style='font-weight:normal;'>" + numCuenta + "</label>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='control-label'>Producto:</label>";
		html += "<label><font color='#FFFFFF'>--</font></label>";
		html += "<label style='font-weight:normal;'>" + producto + "</label>";
		html += "</div>";
		html += "<input id='inputCodAct' class='form-control' maxlength='16' placeholder='C&oacute;digo de Activaci&oacute;n'/>";
		html += "<br/>";
		
		html += "<div class='form-group'>";
		html += "<button type='button' class='btn btn-primary' onclick='validaTokenActivacion(\"" + numCuenta + "\", \"#inputCodAct\");'>Enviar</button>";
		html += "<label><font color='#FFFFFF'>----------</font></label>";
		html += "<button type='button' class='btn btn-primary' onclick='enviarTokenActivacion(\"" + numCuenta + "\", \"" + producto + "\", 1);'>Reenviar SMS</button>";
		html += "</div>";
		html += "</div>";
		$("#formActivacion").hide();
		$("#accordion").find("div[id='datosActivacion']").find("div.panel-body").append(html);
		}
		
		
	}

	function validaTokenActivacion(numCuenta, codActID) {
		console.log('Validarndo token de activacion');
		var codAct = $(codActID).val().trim();
		
		var data = JSON.stringify({
			"numCuenta"			: numCuenta,
	    	"codigoActivacion"	: codAct,
	    	"usuario" 			: user,
	    	"contrasenia"		: pass
	    });
		
		$.ajax({
			data:  data,
			url:   urlRest + '/activaCta/validarToken',
	        type:  'post',
	        headers: {          
	        	Accept: "application/json; charset=utf-8",         
	        	"Content-Type": "application/json; charset=utf-8"   
	        }, 
	        beforeSend: function () {
	            //Accion durante la ejecucion
	        	$("#loadingModalAccesos").modal('show');
	        	
	        },
	        success:  function (response) {
	        	//Accion al terminar la ejecucion
	        	
	        	if(response.code == '200'){
	        		consultarChipAndPin(numCuenta);
		        } else {
		        	alerta(response.msgCode);
		        	$("#loadingModalAccesos").modal('hide');
		        }
	        	
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices validarToken');
	        	alertaPop('No se ha logrado validar el token .');
	        }
	    });
	}

	function consultarChipAndPin(numCuenta){
		console.log('Consulta chipAndPin');
		var data = JSON.stringify({
			"numCuenta"		: numCuenta,
	    	"tipoCMID"		: "2,3",
	    	"usuario" 		: user,
	    	"contrasenia"	: pass
	    });
		
		$.ajax({
			data:  data,
			url:   urlRest + '/activaCta/consultaChipAndPin',
	        type:  'post',
	        headers: {          
	        	Accept: "application/json; charset=utf-8",         
	        	"Content-Type": "application/json; charset=utf-8"   
	        }, 
	        success:  function (response) {
	        	//Accion al terminar la ejecucion
	        	
		        if(response.status == '200'){
		        	if(response.chipAndPin == '1'){
		        		$("#loadingModalAccesos").modal('hide');
		        		ingresarPin();
		        	} else {
		        		console.log('Aqui se activa la cuenta');
		        		activarCuenta(numCuenta);
		        	}
		        	
		        } else {
		        	alertaPop(response.message);
		        }
		    },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices consultaChipAndPin');
	        	alertaPop('No se ha logrado hacer la consulta de Chip & Pin.');
	        }
	    });
	}

	function activarCuenta(numCuenta){
		
		var data = JSON.stringify({
			"numCuenta"		: numCuenta,
	    	"usuario"		: user,
	    	"contrasenia"	: pass
	    });
			
		$.ajax({
			data:  data,
			url:   urlRest + '/activaCta/activarCuenta',
	        type:  'post',
	        headers: {          
	        	Accept: "application/json; charset=utf-8",         
	        	"Content-Type": "application/json; charset=utf-8"   
	        }, 
	        success:  function (response) {
	        	//Accion al terminar la ejecucion
		        if(response.resultado == '200'){
		        	alertaPop("Activaci&oacute;n realizada con &eacute;xito");
		        } else {
		        	alertaPop(response.mensaje);
		        }
		        $("#loadingModalAccesos").modal('hide');
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices ActivaPin');
	        	alertaPop('No se ha activado la cuenta.');
	        	$("#loadingModalAccesos").modal('hide');
	        }
	    });
	}

	function ingresarPin(){
		console.log('Mostrando formulario para el NIP');
		$("#datosActivacion").hide();
		$("#datosNip").show();
		$("#alertasActivacion").hide();
		
	}
	
	
	$("#ingreseNip").keyup(function (){
		if (nip.length < 4) {
			nip +=$(this).val();
		}else {
			alertaPop("El NIP ingresado ya tiene 4 d&iacute;gitos.");
		}
		
		
		
	});
	
	function validarPIN(){
		console.log('Validando NIPs');
		var numCuenta 		= $("#numCuenta").val();
			nip 			= $("#ingreseNip").val().trim();
		var nipConfirmacion	= $("#confirmaNip").val().trim();
		var mensajePin		= '';
		$("#alertasPin").remove();
		
		if(nip.length < 4 || nipConfirmacion.length < 4) {
			mensajePin = 'El NIP se encuentra incompleto';
		}
		
		if(nip == '0000' || nipConfirmacion == '0000' && mensajePin == ''){
			$("#ingreseNip").empty();
			$("#confirmaNip").empty();
			nip= '';
			mensajePin = 'No se permite ingresar \'0000\'';
		 }
		
		if(nip == nipConfirmacion && mensajePin == ''){
				$("#alertasActivacion").hide();
				insertarPIN(numCuenta);
				nip= '';
			}else if (mensajePin == ''){
					$("#ingreseNip").empty();
					$("#confirmaNip").empty();
					nip= '';
					mensajePin = 'Los NIP\'s ingresados son diferentes, verifique su informaci&oacute;n';
			}
		
		
		
		
		if(mensajePin != ''){
			$("#alertasPin").remove();
			var alertaPin = "<div class='alert alert-danger' id='alertasPin'>"+
			"<strong>Error! </strong> "+mensajePin+
			  "<br>"+
			"</div>";
			$("#accordion").find("div[id='datosNip']").find("div.panel-body").append(alertaPin);
		}
	}

	function insertarPIN(numCuenta){
		
		$("#chipAndPinModal").modal('hide');
		
		var data = JSON.stringify({
			"numCuenta"		: numCuenta,
	    	"nip"			: nip,
	    	"usuario"		: user,
	    	"contrasenia" 	: pass
	    });
		
		$.ajax({
			data:  data,
			url:   urlRest + '/activaCta/insertarPIN',
	        type:  'post',
	        headers: {          
	        	Accept: "application/json; charset=utf-8",         
	        	"Content-Type": "application/json; charset=utf-8"   
	        }, 
	        beforeSend: function () {
	            //Accion durante la ejecucion
	        	$("#loadingModalAccesos").modal('show');
	        },
	        success:  function (response) {
	        	//Accion al terminar la ejecucion
		        if(response.code == '500'){
		        	activarCuenta(numCuenta);
		        } else if(response.code == '999'){
		        	activarCuenta(numCuenta);
		        } else {
		        	alertaPop(response.msgCode);
		        	$("#loadingModalAccesos").modal('hide');
		        }
		    },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	console.log('Error al consumir WebServices InsertaPin');
	        	alertaPop(response.msgCode);
	        	$("#loadingModalAccesos").modal('hide');
	        }
	    });
	}


	function validateNumber(event){
		event.value = event.value.replace(/[^0-9\.]/g,'');
	}


	function alertaPop(text){
		$.alert({
	        title: "Atenci&oacute;n",
	        content: text,
	        confirmButton:"OK",
	        confirm:  function (){
	        	
	        }
	    });
	}
	
	