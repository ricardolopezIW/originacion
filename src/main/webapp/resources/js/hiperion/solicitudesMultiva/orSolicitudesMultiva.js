var tablaSolicitudes = "";
var tablaSolicitudesCat = "";
var idPerfil;

var urlRest = $('#urlRestBackOffice').val();
//var urlRest = 'http://localhost:8081/siditOriginacion/backOffice';

var pageDetalleSolicitud = '/backOffice?seccion=asignacionActivacion';

$(document).ready(function () {
	
	idPerfil = $('#hiddenIdPerfil').val();
	
	// Carga inicial de la vista
	consultar();
	
	// Accion al presionar el boton de actualizar 
	$('#btnActualizar').click(function () {
		actualizar();
	});
});

// Realiza la actualizacion de las solicitudes con APM
function actualizar() { 
	// Se crea el request de la peticion
	var request = httpRequest( urlRest+'/apm/carga-solicitud/null', // url
							   {}, 				 			  		// data
							   'GET',  			  			  		// method
							   'application/json' 			  		// contentType
							  );
	$.ajax(request)
		.success(function (response) {
			var source = [];
			if (response.codError == '200') {
				alerta('Se ha realizado la actualizacion de las solicitudes');
			} else if (response.codError == '204') {
				alerta('No se encontraron solicitudes nuevas');
			} else{
				alerta('No se pudo actualizar las solicitudes');
			}
			consultar();
		})
		.error(function (response) {
			console.log(response); 
			alerta('Hubo un error al Consultar las Solicitudes'); 
			crearTablaSolicitudes("Solicitudes", null); 
		});
}

// Realiza la consulta y carga la tabla de solicitudes 
function consultar() { 
	
	// Se crea el request de la peticion
	var request = httpRequest( urlRest+'/solicitud-venta/aprobada/perfil/'+idPerfil, // url
							   {}, 				 			  		// data
							   'GET',  			  			  		// method
							   'application/json' 			  		// contentType
							  );
	
	// Accion antes de la ejecucion
	mostrarLoading();
	
	$.ajax(request)
		.success(function (response) {
			var source = [];
			if (response.error == 'OK') {
				source = response.solicitudes; 
			}
			crearTablaSolicitudes("Solicitudes", source); 
		})
		.error(function (response) {
			console.log(response); 
			alerta('Hubo un error al Consultar las Solicitudes'); 
			crearTablaSolicitudes("Solicitudes", null); 
		});
	
	// Accion despues de la ejecucion 
	ocultarLoading();
	
}


function mostrarLoading(){
	$("#modalLoadingSolicitud").modal('show');
}

function ocultarLoading(){
	$("#modalLoadingSolicitud").modal('hide');
}

function asignarActivarCuenta(idSolicitud){
	var baseUrl = getPath() + pageDetalleSolicitud + "&idSolicitud=" + idSolicitud;
	redireccionar(baseUrl);
}


function crearTablaSolicitudes(name, response){
	
	if ($.fn.dataTable.isDataTable('#tablaSolicitudes')) {
		tablaSolicitudes.destroy();
	}
	
	tablaSolicitudes = $('#tablaSolicitudes')
	.DataTable(
		{
			"language" : {
				"lengthMenu" : "_MENU_ " + name,
				"zeroRecords" : "No hay datos",
				"info" : "Mostrando pagina _PAGE_ de _PAGES_",
				"infoEmpty" : "No se encontraron " + name,
				"search" : "Buscar:",
				"paginate" : {
					"first" : "Primero",
					"last"  : "Ultimo",
					"next"  : "Siguiente",
					"previous" : "Anterior"
				},
				"infoFiltered" : "(filtrando del _MAX_ total " + name + ")"
			},
			data : response,
			columns : [ {
							title : "Folio",
							"width" : "10%",
							"render" : function(data,
									type, full, meta) {
								if(full.folioApm == null)
									return '-';
								else return full.folioApm;
							}
						},
						{
							title : "Cliente",
							"width" : "20%",
							"render" : function(data,
									type, full, meta) {
								return '' + 
									  (full.nombre != null ? full.nombre : '' ) + ' ' 
									+ (full.apellidoMaterno != null ? full.apellidoPaterno : '');
							}
						},
						{
							title : "Producto",
							"width" : "20%",
							"render" : function(data,
									type, full, meta) {
								if(full.txProducto == null)
									return '-';
								else return full.txProducto;
							}
						},
						{
							title : "Lugar de Venta",
							"width" : "20%",
							"render" : function(data,
									type, full, meta) {
								if(full.txLugarVenta == null)
									return '-';
								else return full.txLugarVenta;
							}
						},
						{
							title : "Mensajer&iacute;a",
							"width" : "20%",
							"render" : function(data,
									type, full, meta) {
								if(full.txMensajeria == null)
									return '-';
								else return full.txMensajeria;
							}
						},
						{
							title : "Detalle",
							"width" : "10%",
							"render" : function(data,
									type, full, meta) {
								
								
								return '<img id="btnDetalleSolicitud" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_access.png" align="middle" onclick="asignarActivarCuenta(\''
									+ full.idSolicitud + '\');"/>';
								;
							}
						}]
		}).search( 'MULTIVA').draw();
	
	
}