var restUrl                 = $('#urlRestBackOffice').val();	
var restCargaArchivo        = restUrl + "/sepomex/carga-archivo";
var restCargaConfiguracion  = restUrl + "/sepomex/carga-configuracion";


$(document).ready(function(){
		inicializarObjetos();
});

$(function () {
 
	$('#fileupload').fileupload({
        url: restCargaArchivo,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(txt)$/i,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        formAcceptCharset: 'utf-8'
    }).on('fileuploadadd', function (e, data) {   	
        if(data.files[0].name.split(".")[1] != "txt"){
        	console.log("archivo incorrecto");
        	data.abort();
        	alerta("El archivo debe ser de tipo '.txt'.");
        }
    
    }).on('fileuploadprocessalways', function (e, data) {
    	console.log("fileuploadprocessalways");
    
    }).on('fileuploadprogressall', function (e, data) {
    	$("#loadingModal").modal('show');
    
    }).on('fileuploaddone', function (e, data) {
    	 if(data.result.error == null){
    		 alerta("Se ha iniciado el proceso de actualizado de SEPOMEX.");
    	 }
	     else {
	    	 alerta(data.result.error);
	     }
    	$("#loadingModal").modal('hide');

    }).on('fileuploadfail', function (e, data) {
    	$("#loadingModal").modal('hide');
    
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function alerta(texto){
	$.alert({
        title: "Alerta",
        content: texto,
        confirmButton: "Ok"
    });
}

function inicializarObjetos(obj){				
	$("#fecha").attr('disabled', true);
	$("#actualizados").attr('disabled', true);
	$("#total").attr('disabled', true);
	verificarDatos();
}

function verificarDatos(){
		$.ajax(
			{
				url:   restCargaConfiguracion,
			    type:  'get',
			    contentType: "application/json",
			    success:  function (response)
			    {				    	
			    	console.log(response);
			    	if(response.error == null){
			    		$("#fecha").val(response.fechaActualizacion == null ? "" : response.fechaActualizacion);
			    		$("#actualizados").val(response.nuevosReg == null ? "" : response.nuevosReg);
			    		$("#total").val(response.totalReg == null ? "" : response.totalReg);
			    		
			    		if(response.actualizandP == false){
			    			$("#spload").attr('disabled', false);
			    		}
			    		else{
				    		$("#spload").attr('disabled', true);
				    		alert("Actualemente existe una carga en proceso.");
				    	}
			    	}
			    	else{
			    		alert(response.error);
			    		$("#fecha").val(response.fechaActualizacion == null ? "" : response.fechaActualizacion);
			    		$("#actualizados").val(response.nuevosReg == null ? "" : response.nuevosReg);
			    		$("#total").val(response.totalReg == null ? "" : response.totalReg);
			    	}
			    }
			});
}