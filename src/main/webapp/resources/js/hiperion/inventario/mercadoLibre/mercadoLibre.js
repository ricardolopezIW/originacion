$(document).ready(function(){
	console.log("ready page");
});

var folioAnterior;
var folioNuevo;
var usuario;
var clave;
var restUrl = $("#urlRestBackOffice").val();
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
console.log("URL: " + restUrl)

function validaFolios(){
	console.log("validando datos...");

	if(!validarDatos()){
		return;
	}
	var clav = $('#checkAccept').prop('checked')==true?"S008":"S009";
	console.log("--> "+clav);
	$.ajax({
		url: restUrl+"/mercadoLibre/validacionFolios?folioAnt="+ $('#folioAnt').val()+"&folioNuevo="+$('#folioNuevo').val()+"&usuario="+$("#hiddenIdUsuario").val()+"&clave="+clav,
		type:'GET',
	    success: function(response, status, code){
	    	console.log("response success -- -  "+response+" -->"+status+"---- code"+code.status);	 
	    	if(code.status<320){
		    	switch(code.status) {
			    	case 204:
			    		alerta("No se encontro el folio.");
			        break;
			    	case 200:
			    		alerta("Se valido correctamente el folio."+ $('#folioAnt').val());
			        break;
			    	case 202:
			    		alerta("Se cancelo correctamente el folio."+$('#folioAnt').val());
			        break;
			    	case 302:
			    		alerta("No se pudo actualizar Tsys");
			    	break;
	//		        default:
	//		        	alerta("")
	//		        break;
		       }
	    	}
	    },error:function(error){
	    	alerta("No se pudo actualizar Tsys ["+error.status+"]");
	    }
	})
}


function consultaFolios(){
	
	if(!validarDatos()){
		return;
	}
	
	var folioN = $('#folioNuevo').val()!=null?$('#folioNuevo').val():" ";
	console.log("consultando folios...")
	$.ajax({
		url: restUrl+"/mercadoLibre/consultaFolios?folioAnt="+ $('#folioAnt').val()+"&folioNuevo="+folioN,
		type:'GET',
	    success: function(response){
	    	console.log("..."+response.folioAnt);
	    	 $('#tdFolioAnt').text(response.folioAnt)
	    	 $('#tdNombreAnt').text(response.nombreFolioAnt);
	    	 $('#tdApePatAnt').text(response.apellidoPatAnt);
	    	 $('#tdApeMatAnt').text(response.apellidoMatAnt);
	    	 $('#tdRfcAnt').text(response.rfcFolioAnt);
	    	 
	    	 $('#tdFolioNuevo').text(response.folioNuevo);
	    	 $('#tdNombreNuevo').text(response.nombreFolioNuevo);
	    	 $('#tdApePatNuevo').text(response.apellidoPatNuevo);
	    	 $('#tdApeMatNuevo').text(response.apellidoMatNuevo);
	    	 $('#tdRfcNuevo').text(response.rfcFolioNuevo);
	    	 $('#validar').prop('disabled', false);
	    },error(error){
	    	console.log("-->");
	    	if(error.status==404){
	    		alerta("No se encontro alguno de los datos.");
	    	}
	    }	
   })
}

function validarDatos(){
  var reg = new RegExp('^[0-9]+$');
	if($('#folioAnt').val()==""){
		alerta("El folio anterior debe ser agregado.");
		return false;
	}else{
		return true;
	}
}

function limpiar(){
	console.log("limpiar"+$('#checkAccept').prop('checked'));
	$('#folioAnt').val("");
	$('#folioNuevo').val("");
	$('#validar').prop('disabled', true);
	$('#tdFolioAnt').text("");
    $('#tdNombreAnt').text("")
	$('#tdApePatAnt').text("")
	$('#tdApeMatAnt').text("")
	$('#tdRfcAnt').text("")
	 
	$('#tdFolioNuevo').text("")
	$('#tdNombreNuevo').text("")
	$('#tdApePatNuevo').text("")
	$('#tdApeMatNuevo').text("")
	$('#tdRfcNuevo').text("")
}