var respuesta = [];
var idEstadoAdd = "";
var idEstadoDel = "";
var idMunicipioAdd = "";
var idMunicipioDel = "";
var tablaMensajeria = "";
var tableEstados = "";
var tableCp = "";
var eliminar = false;
var restUrl = $("#urlRestBackOffice").val();
//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";

$(document).ready(function(){
	consultarMensajerias();
	consultaEstados();
	inicia();
});

function inicia() {
	$("#divEstadosCobertura").hide();
	$("#divMunicipiosCobertura").hide();
	$("#divCpCobertura").hide();
	$("#divSelectEstado").hide;
	
	$("#selectStatesMun").change(function() {
		var idState = this.value;
		consultaMunicipio(idState)

	});

	$("#btnGuardar").click(function() {
		insertarMensajeria(false);
	});

	$(".deleteState").click(function() {
		acceptMun();
	});

	$("#btnGuardarCamb").click(function() {
		insertarMensajeria(true);
	});
	$("#btnCancelar").click(function() {
		resetForm();
		$("#sectionTable").removeClass("hide")
		$("#formMensajeria").addClass("hide")

	});

}

function resetForm() {

	$("#initCP").val('');
	$("#endCP").val('');

	$("#nombreMensajeria").val('');

	$("#activeStates option").each(function() {
		$(this).remove();
	});

	$("#activeMun option").each(function() {
		$(this).remove();
	});

	$("#activeCp option").each(function() {
		$(this).remove();
	});

}



function insertarMensajeria(update) {

	var nombreMensajeria = $("#nombreMensajeria").val().trim();

	if (nombreMensajeria == '') {
		modalAlert("El nombre de la mensajeria es obligatorio");
		$("#nombreMensajeria").focus();

	} else {

		var estados = [];
		var municipios = [];
		var codigosPostales = [];

		var estadosEmpty = false;
		var municipiosEmpty = false;
		var CPempty = false;

		$("#activeStates option").each(function() {
			estados.push($(this).val());
			estadosEmpty = true;
		});
		
		

		$("#activeMun option").each(function() {
			var idGral = $(this).val().split("/")
			var idMun = idGral[1]
			var idEstado = idGral[0]
			municipio = {};
			municipio.idMunicipio = idMun;
			municipio.idEstado = idEstado;
			municipios.push(municipio);
			
			municipiosEmpty = true;
		});

		$("#activeCp option").each(function() {
			var cpId = $(this).val().split("/")
			codigosPostales.push(cpId[2]);
			CPempty = true;
		});

		if (estadosEmpty || municipiosEmpty || CPempty) {
			console.log("Estados", estados);
			let data;
			if (!update) {
				data = {
					"estados" : estados,
					"municipios" : municipios,
					"codigosPostales" : codigosPostales,
					"nombreMensajeria" : nombreMensajeria
				};

				var url = restUrl + '/resources/insertarMensajeria'

			} else {

				var idMensajeria = $('#idMensajeria').val();

				data = {
					"idMensajeria" : idMensajeria,
					"estados" : estados,
					"municipios" : municipios,
					"codigosPostales" : codigosPostales,
					"nombreMensajeria" : nombreMensajeria
				};
				var url = restUrl + '/resources/updateMensajeria'
			}

			data = JSON.stringify(data);
			console.log("data", data);

			$.ajax({
				data : data,
				url : url,
				type : 'post',
				contentType : "application/json",
				success : function(response) {

					console.log("Response insertar mensajeria", response);
					consultarMensajerias();
					resetForm();
					$("#sectionTable").removeClass("hide")
					$("#formMensajeria").addClass("hide")

				}

			});
		} else {
			modalAlert("Selecciona al menos un tipo de cobertura");
		}
	}
}



function nuevaMensajeria() {

	$("#sectionTable").addClass("hide")
	$("#formMensajeria").removeClass("hide")
	$("#btnGuardarCamb").addClass("hide");
	$("#btnGuardar").removeClass("hide");

}



function agregarEstado() {
	var folioVal = $("#selectStates").val().trim();
	var folio = $("#selectStates option:selected").text();
	if (folio != "" && folio != "Seleccionar") {
		var bandera = false;
		var flag = false

		$("#activeStates option").each(function() {
			var folioAgregado = $(this).text().trim();
			if (folio == folioAgregado) {
				bandera = true;

				modalAlert("El estado ya se encuentra agregado");
			}
		});

		$("#activeMun option").each(function() {
			var folioChild = $(this).val().split("/")
			if (folioVal == folioChild[0]) {
				modalAlert("El estado no puede ser dado de alta, ya que existe un municipio del mismo");
				flag = true;
			}

		});

		$("#activeCp option").each(function() {
			var folioChild = $(this).val().split("/")

			if (folioVal == folioChild[0]) {
				modalAlert("El estado no puede ser dado de alta, ya que existe un CP del mismo");
				flag = true;
			}

		});

		
		if (!bandera && !flag) {
			$('#activeStates').append($('<option>', {
				value : folioVal,
				text : folio
			}));
			
			$("#selectStates").val(-1);
		}
	} else {
		modalAlert("Seleccione un estado");
	}
}



function quitarEstado() {
	var size = $("#activeStates option").length;
	if (size > 0) {
		var contador = 0;
		$("#activeStates option").each(function() {
			if ($(this).is(':selected')) {
				$(this).remove();
				contador++;
			}
		});
		if (contador == 0) {
			modalAlert("Seleccione un estado.");
		}
	} else {
		modalAlert("No se han agregado estados");
	}
}



function validAgregarMunicipio() {
	console.log("< validAgregarMunicipio");
	var idsEstMun = $("#selectMun").val().trim().split("/");
	var municipio = $("#selectMun option:selected").text();
	var estadoAgregado = validarEstadoAgregado(idsEstMun[0]);
	
	if(estadoAgregado){
		$("#modalMunEdo").modal('show');
	}else{
		agregarMunicipio();
	}

	console.log("> validAgregarMunicipio")
}



function validAgregarCP() {
	var folioVal = $("#selectMun").val().trim();
	var folio = $("#selectMun option:selected").text();
	var foliValid = folioVal.split("/")

	var obj = $("#activeStates option").val()

	if (typeof (obj) === "undefined") {
		consultaRangoCP();
	}

	$("#activeStates option").each(function() {
		if (foliValid[0] == $(this).val()) {
			$("#modalMunEdo").modal('show');
		} else {
			consulCP();
		}
	});

}



function acceptMun() {
	console.log("< acceptMun");
	var folioVal = $("#selectMun").val().trim();
	var foliValid = folioVal.split("/")
	$("#activeStates option").each(function() {
		if (foliValid[0] == $(this).val()) {
			$(this).remove();
			agregarMunicipio();
		}
	});

	console.log("> acceptMun");
}



function agregarMunicipio() {
	console.log("< agregarMunicipio");
	var idsEstMun = $("#selectMun").val().trim();
	var nombreMun = $("#selectMun option:selected").text();
	
	if (nombreMun != "" && nombreMun != "Seleccionar") {
		var bandera = false;
		var flag = false;
		$("#activeMun option").each(function() {
			var munAgregado = $(this).text().trim();
			if (nombreMun == munAgregado) {
				bandera = true;
			}
		});
		
		
		$("#activeCp option").each(function() {
			var foliosEstMunCP = $(this).val().split("/");
			var foliosEstMun = idsEstMun.split("/");
			
			if ((foliosEstMun[0] == foliosEstMunCP[0]) && (foliosEstMun[1] == foliosEstMunCP[1])) {
				modalAlert("El municipio no puede ser dado de alta, ya que existe un codigo postal del mismo");
				flag = true;
			}

		});

		if (!bandera && !flag) {
			$('#activeMun').append($('<option>', {
				value : idsEstMun,
				text : nombreMun

			}));
			$("#selectMun").val(-1);

		}
		
	} else {
		modalAlert("Debe seleccionar un estado y un municipio");
		$("#selectMun").val(-1);
	}

	console.log("> agregar municipio");
}


function quitarMunicipio() {
	var size = $("#activeMun option").length;
	if (size > 0) {
		var contador = 0;
		$("#activeMun option").each(function() {
			if ($(this).is(':selected')) {
				$(this).remove();
				contador++;
			}
		});
		if (contador == 0) {
			modalAlert("Debe seleccionar un municipio");
		}
	} else {
		modalAlert("No se han agregado municipios");
	}
}



function quitarCP() {
	var size = $("#activeCp option").length;
	if (size > 0) {
		var contador = 0;
		$("#activeCp option").each(function() {
			if ($(this).is(':selected')) {
				$(this).remove();
				contador++;
			}
		});
		if (contador == 0) {
			modalAlert("Seleccione un codigo postal.");
		}
	} else {
		modalAlert("No hay codigos postales agregados.");
	}
}


function fillTable(response) {

	if ($.fn.dataTable.isDataTable('#tablaMensajeria')) {
		tablaMensajeria.destroy();
	}
	
	tablaMensajeria = $('#tablaMensajeria')
			.DataTable(
					{
						"language" : {
							"lengthMenu" : "_MENU_ Mensajerias",
							"zeroRecords" : "No hay datos",
							"info" : "Mostrando pagina _PAGE_ de _PAGES_",
							"infoEmpty" : "No se encontraron mensajerias",
							"search" : "Buscar:",
							"paginate" : {
								"first" : "Primero",
								"last" : "Ultimo",
								"next" : "Siguiente",
								"previous" : "Anterior"
							},
							"infoFiltered" : "(filtrando de un total de _MAX_ mensajeria(s))"
						},
						"ordering" : false,
						data : response,
						columns : [
								{
									data : 'estados',
									visible : false
								},
								{
									data : 'idMensajeria',
									visible : false
								},
								{
									title : "Nombre",
									data : 'nombreMensajeria'
								},
								{
									title : "Cobertura Estados",
									"render" : function(data, type, full, meta) {

										var h = '';
										if (full.estados.length > 0) {

											h += '<div stryle="color:#840925;" align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-success center">Si</span></div>';
										} else {

											h += '<div align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-default center">No</span></div>';
										}

										return h;
									}
								},
								{
									title : "Cobertura Municipios",
									"render" : function(data, type, full, meta) {

										var h = '';
										if (full.municipios.length > 0) {

											h += '<div stryle="color:#840925;" align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-success center">Si</span></div>';
										} else {

											h += '<div align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-default center">No</span></div>';
										}

										return h;
									}
								},
								{
									title : "Cobertura CP",
									"render" : function(data, type, full, meta) {

										var h = '';
										if (full.codigosPostales.length > 0) {

											h += '<div stryle="color:#840925;" align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-success center">Si</span></div>';
										} else {

											h += '<div align="center">&nbsp&nbsp&nbsp<span class="badge badge-lg  badge-default center">No</span></div>';
										}

										return h;
									}
								},
								{
									title : "Eliminar",
									"render" : function(data, type, full, meta) {
										return '<img id="buttonBorrar" class="btnCheck"  src="'+requestContextPath+'/resources/img/borrar.png" align="middle"onclick="eliminarMensajeria(\''
												+ full.idMensajeria + '\');"/>';
										;
									}
								} ]
					});

	$('#tablaMensajeria tbody').unbind().on('click', 'tr', function(event) {

		if (tablaMensajeria.rows().data().length > 0 && !eliminar) {
			$("#sectionTable").addClass("hide");
			$("#formMensajeria").removeClass("hide");

			var mensajeria = tablaMensajeria.row(this._DT_RowIndex).data();
			$(this).addClass('active');
			console.log("ROW", mensajeria);
			fillForm(mensajeria);

		}
	});
}



function fillForm(mensajeria) {
	
	$("#btnGuardar").addClass('hide');

	$("#btnGuardarCamb").removeClass('hide');

	$("#nombreMensajeria").val(mensajeria.nombreMensajeria)

	var estados = mensajeria.estados;
	var municipios = mensajeria.municipios;
	var codigos = mensajeria.codigosPostales;

	for ( var j in estados) {
		$("#selectStates option").each(function() {
			if ($(this).val() == estados[j]) {
				var valEstado = $(this).val();
				var text = $(this).text();

				$('#activeStates').append($('<option>', {
					value : valEstado,
					text : text

				}));
			}
		});
	}

	for ( var j in municipios) {
		
		var idEstado = municipios[j].idEstado;
		var idMunicipio = municipios[j].idMunicipio;
		var idGral = idEstado +"/"+idMunicipio;
		var text = municipios[j].nombre;
		
		
		$('#activeMun').append($('<option>', {
			value : idGral,
			text : text

		}));

	}

	if (codigos.length > 0) {
		consultaCPs(codigos);
	}

	$("#idMensajeria").val(mensajeria.idMensajeria);

}



function consultarMensajerias() {
	$.ajax({
		url : restUrl + '/resources/consultarMensajerias',
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			fillTable(response);
		},
		complete : function(xhr, status){
			$("#loadingModal").modal('hide');
		}

	});

}



function consultaEstados() {

	$.ajax({
		url : restUrl + '/resources/consultaEstadosMensajeria',
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
				var html = '<option value="-1">Seleccionar</option>';

				for ( var j in response) {
					var idEstado = response[j].idEstado;
					var nombre = response[j].nombre;

					html += '<option value="' + idEstado + '">' + nombre
							+ '</option>';
				}

				$("#selectStates").html(html);
				$("#selectStatesMun").html(html);

		},
		complete : function(xhr, status){
			$("#loadingModal").modal('hide');
		}

	});

}



function consultaMunicipio(campo) {

	$.ajax({
		url : restUrl + '/resources/consultaMunicipiosMensajeria' + '/' + campo,
		type : 'get',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			console.log("ResponseMunicipios:", response);
				var html = '<option value="-1">Seleccionar</option>';
				for ( var j in response) {
					var idMunicipio = response[j].idMunicipio;
					var nombre = response[j].nombre;
					var idEstado = response[j].idEstado;

					html += '<option value="' + idEstado + '/' + idMunicipio
							+ '">' + nombre + '</option>';
				}

				$("#selectMun").html(html);

		},
		complete : function(xhr, status){
			$("#loadingModal").modal('hide');
		}

	});
}



function eliminarMensajeria(idMensajeria) {
	eliminar = true;

	$.ajax({
		url : restUrl + '/resources/eliminarMensajeria' + '/' + idMensajeria,
		type : 'delete',
		contentType : "application/json",
		success : function(response) {
			consultarMensajerias();
		},
		complete : function(xhr, status){
			eliminar = false;
		}

	});
}



/**
 * Agrega un CP a la lista de "Cobertura de codigos postales"; remueve el estado o municipio al que pertenece el
 * CP si se agrego anteriormente
 * @param infoCP ::: Detalle sobre la informacion del CP
 */
function addCP(infoCP) {
	$("#activeStates option").each(function() {
		if (infoCP.idEstado == $(this).val()) {
			$(this).remove();
		}
	});

	$("#activeMun option").each(function() {
		var idsEstMun = $(this).val().split("/");
		if ((infoCP.idEstado == idsEstMun[0]) && (infoCP.idMunicipio == idsEstMun[1])) {
			$(this).remove();
		}
	});

	$('#activeCp').append($('<option>', {
		value : infoCP.idEstado + '/' + infoCP.idMunicipio + '/' + infoCP.codigoPostal,
		text : infoCP.codigoPostal + ' ' + infoCP.estado + ', ' + infoCP.municipio

	}));

}


/**
 * Busca los codigos postales dentro de un rango y los agrega a la lista
 * "Cobertura de codigos postales"
 * @returns
 */
function consultaRangoCP(initCP, endCP) {
	let json = {
		"initCP" : initCP,
		"endCP" : endCP
	};
	json = JSON.stringify(json);

	$.ajax({
		data : json,
		url : restUrl + '/resources/consultaRangeCP',
		type : 'post',
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			
			var codPostales = new Array();
			for(var i in response){
				codPostales.push(response[i].codigoPostal);
			}
			
			var cpsNoAgregados = validarRangoCpsAgregados(codPostales);
			
			if(codPostales.length > 0 && cpsNoAgregados.length == 0){
				modalAlert("Los codigos postales ya se encuentran agregados");
			}else{
				var cpsPorAgregar = new Array();
				
				for(var i = 0; i<cpsNoAgregados.length; i++){
					for(var j = 0; j<response.length; j++){
						if(cpsNoAgregados[i] == response[j].codigoPostal){
							cpsPorAgregar.push(response[j]);
							break;
						}
					}
				}
				
				compExclusionCobertura(cpsPorAgregar);
			}
			
		},
		complete : function(xhr, status){
			$("#loadingModal").modal('hide');
		}

	});
	
}


/**
 * Realiza una peticion para buscar el estado y municipio al que pertenecen los codigos postales
 * @param cps ::: Codigos postales que seran consultados
 * @returns
 */
function consultaCPs(cps){
	
	let jsonCPs = {
			"codPostales" : cps
	};
	
	jsonCPs = JSON.stringify(jsonCPs);
	
	$.ajax({
		data : jsonCPs,
		url : restUrl + '/resources/consultaCPs',
		type : "post",
		contentType : "application/json",
		beforeSend : function() {
			$("#loadingModal").modal('show');
		},
		success : function(response) {
			compExclusionCobertura(response);
		},
		complete : function(xhr, status){
			$("#loadingModal").modal('hide');
		}
	});
	
}

/**
 * Comprueba la exclusion entre estado, municipio y CP, y los agrega a la lista de "Cobertura de codigos postales"
 * @returns
 */
function compExclusionCobertura(response){
	$("#initCP").val('');
	$("#endCP").val('');

	if (response.length == 0) {
		modalAlert("No se encontraron Codigos Postales en ese rango");
	} else {

		$.each(response, function(i, infoCP) {
			var estado = false;
			var mun = false;

			$("#activeStates option").each(function() {
				if (infoCP.idEstado == $(this).val()) {
					$('#modalCpEdo').modal('show');
					estado = true;
				}
			});

			$("#activeMun option").each(function() {
				var idsEstMun = $(this).val().split("/");
				
				if ((infoCP.idEstado == idsEstMun[0]) && (infoCP.idMunicipio == idsEstMun[1])) {
					$('#modalCpMun').modal('show');
					mun = true;
				}
			});

			if (!(estado || mun)) {
				addCP(infoCP);
			}

			$(".deleteStateCp").unbind().click(function() {
				addCP(infoCP)
			});

		});

	}

	return;
}


/**
 * Busca los codigos postales capturados en los cuadros de texto
 * si se ingresa CP en ambos cuadros de texto se realiza la busqueda por rango
 * si se agrega solo en uno de ellos se busca especificamente informacion de ese CP
 * @returns
 */
function buscarCP(){
	var initCP = $("#initCP").val().trim();
	var endCP = $("#endCP").val().trim();
	var isValidInitCP = validarFormatoCP(initCP);
	var isValidEndCP = validarFormatoCP(endCP);

	if((initCP !== '' && endCP !== '')){
		if(isValidInitCP && isValidEndCP){
			if(initCP > endCP){
				var auxCP = initCP;
				initCP = endCP;
				endCP = auxCP;
			}
			
			consultaRangoCP(initCP, endCP);
		}else{
			modalAlert("El rango de codigos postales tiene un formato invalido");
		}
	}
	
	var cp = "";
		
	if(initCP !== '' && endCP === ''){
		cp = initCP;
	}
	if(initCP === '' && endCP !== ''){
		cp = endCP;
	}
	
	if(cp != ''){
		if(validarFormatoCP(cp)){
			if(validarCPAgregado(cp)){
				modalAlert("El codigo postal ya se encuentra agregado");
			}else{
				consultaCPs([cp]);
			}
		}else{
			modalAlert("El codigo Postal tiene un fomato invalido");
		}
	}
		
}


/**
 * Valida el formato para un codigo postal
 * @param cp ::: codigo postal que sera validado
 * @returns ::: true si cumple con el formato, false si no cumple con el formato
 */
function validarFormatoCP(cp){
	var regxCP = /^\d{5}$/;
	var isValid = true;
	
	isValid = regxCP.test(cp);
	
	return isValid;
}


/**
 * Verifica si un codigo postal ya se encuentra agregado en la lista de cobertura
 * @returns
 */
function validarCPAgregado(cpValidar){
	var cpsAgregados = obtenerCPsAgregados();
	
	for(var i in cpsAgregados){
		if(cpsAgregados[i] == cpValidar){
			return true;
		}
	}
	
	return false;
}

/**
 * Verifica que los codigos postales no se encuentren repetidos
 * @param codPostales lista de codigos postales que sera verificada contra los que ya se encuentran agregados
 * @returns lista de codigos postales que no se encuentran agregados
 */
function validarRangoCpsAgregados(codPostales){
	var cpAgregados = obtenerCPsAgregados();
	
	if(cpAgregados.length == 0){
		return codPostales;
	}
	
	var cpValidos = new Array();
	var cp;
	
	for(var i = 0; i<codPostales.length; i++){
		var flag = false;
		
		for(var j = 0; j<cpAgregados.length; j++){
			if(codPostales[i] == cpAgregados[j]){
				flag = true;
				break;
			}else{
				cp = codPostales[i];
			}
		}
		
		if(!flag){
			cpValidos.push(cp);
		}
		
	}
	
	return cpValidos;
}


/**
 * Obtiene los codigos postales agregados en la lista de cobertura
 * @returns true si esta agregado, en caso contrario regresa false
 */
function obtenerCPsAgregados(){
	var cpsAgregados = new Array();

	$("#activeCp option").each(function() {
		var cpInfo = $(this).val().split("/");
		if(cpInfo.length == 3){
			var cp = cpInfo[2].trim();
			if(cp != null && cp != ''){
				cpsAgregados.push(cp.trim());
			}
		}
	});
	
	return cpsAgregados;
}


/**
 * Valida si un estado ya se encuentra agregado en la lista de "Cobertura de estados"
 * @param idEstado ::: id del estado que se desea validar
 * @returns ::: true si el estado ya se encuentra agregado, false en caso contrario
 */
function validarEstadoAgregado(idEstado){
	console.log("< validarEstadoAgregado");
	var idsEstadosAgregados = getIdsEstadosAgregados();
	
	if (idsEstadosAgregados.length > 0){
		for(var i in idsEstadosAgregados){
			if(idsEstadosAgregados[i] === idEstado){
				return true;
			}
		}
	}
	
	console.log("> validarEstadoAgregado");
	return false;
}

/**
 * Obtiene los ids de los estados que se encuentran agregado en la lista de "Cobertura de estados"
 * @returns ::: array con los ids de los estados
 */
function getIdsEstadosAgregados(){
	var idsEstAgregados = new Array();
	var actStates = $("#activeStates option").val();
	
	if (typeof (actStates) != "undefined"){
		$("#activeStates option").each(function(){
			idsEstAgregados.push($(this).val().trim());
		});
	}
	
	return idsEstAgregados;
}

/**
 * Muestra un modal
 * @param msg ::: Mensaje que sera mostrado
 */
function modalAlert(msg){
	$("#msgModalAlert").html("<h4>"+ msg +"</h4>");
	$("#modalAlert").modal('show');
}



