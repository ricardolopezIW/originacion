    var error = false;
    var productos;							  
	var lugaresVenta;							  
	var configuraciones;
	var restUrl = $("#urlRestBackOffice").val();
	
    $(document).ready(function(){
		inicializarObjetos();	
	});
	
	function inicializarObjetos(){	 
		$('#fecha').datepicker({ dateFormat: 'dd-mm-yy' }).bind("change",function(){
            var tokens  = $("#fecha").val().split("/");
			var fecha   = tokens[0] + "/"+ tokens[1] + "/"+ tokens[2]; 
            $('#fecha').val(fecha);
        })

		$("#guardar").prop('disabled', false);
		
		$( "#guardar" ).click(function() {
			$("#guardar").prop('disabled', true);
			verificarCamposVacios();
			
		    if(!error){
				var json = generarRequestJson();
				generarAce(json);
			}
		    else{
				$("#guardar").prop('disabled', false);
		    }
		});
		
		$( "#limpiar" ).click(function() {
			$("#fecha").val("");
			$("#transmission").val("");			
			$("#numTarjeta").val("");
			$("#adicionales").prop('checked', false);
		});
		
		obtenerDatosIniciales();
	}
		
	function obtenerDatosIniciales(){	
		$.ajax(
				{
					data:  null,
				    url:   restUrl + '/ace/configuracion',
				    type:  'get',
				    contentType: "application/json",
				    success:  function (response)
				    {				    	
				    	if(response != null){				    						    	
					    	productos       = response[1];							  
					    	lugaresVenta    = response[0];							  
					    	configuraciones = response[2];									  
					    	
					    	crearComboBox2(lugaresVenta    , "lugarSel");
					    	crearComboBox2(productos       , "productoSel");
					    	crearComboBox2(configuraciones , "configSel");
					    	asignarListenerProductosSel();
				    	}
				    	else{
				    		$.dialog({
								title: 'Error',
								content: 'Al obtener datos de la Base de Datos.',
							});	
				    	}
				    }
	    });
	}
	
	function crearComboBox2(catalogo, id){			
		  for(var i=0; i<catalogo.length; i++){
			  $('#'+id).append($('<option>').attr('value', catalogo[i].id).text(catalogo[i].descripcion));				
		  }
	}
	
	function asignarListenerProductosSel(){			
		$( "#productoSel" ).change(function() {
			 
			 $("#configSel").html();
			 $("#configSel option").remove();
			 
			 var confProducto = [];
			 var idProducto   = $("#productoSel").val();
			 
			 for(var i=0; i<configuraciones.length;i++){
				 if(parseInt(configuraciones[i].valor) == idProducto){
					 confProducto.push(configuraciones[i]);
				 }
			 }
			 crearComboBox2(confProducto , "configSel");
		});	  
	}
		
	function generarRequestJson(){
		var tokens  = $("#fecha").val().split("/");
		var anio    = tokens[2].split('');
		var fecha   = tokens[0] + tokens[1] + anio[2] + anio[3]; 
		var request = { numeroTarjetas    : $("#numTarjeta").val() , 
                        idProducto        : $("#productoSel").val() , 
                        idLugarVenta      : $("#lugarSel").val(), 
			            idFechatransmision: $("#fecha").val(), 
			            transmissionId    : $("#transmission").val() , 
			            adicionales       : $("#adicionales").prop('checked'),
			            idConfiguracion   : $("#configSel").val(),
			            lugarVenta        : $("#lugarSel").text(),
			            solicitante       : $("#hiddenUser").val()
			          }
		return JSON.stringify(request);
	}
	
	function obtenerClienteProducto(id){
		for(var i=0;i<productos.length;i++){
			if(parseInt(productos[i].id) == 1){
				return productos[i].valor
			}
		}
	}
	
	function verificarCamposVacios(){		
		var vacio = false;
		var mensaje = "";
		
		if($("#numTarjeta").val() == ""){
			vacio = true;
			mensaje = "El campo Numero de tarjetas es Obligatorio.";
		}
		
		else if($("#fecha").val() == ""){
			vacio = true;
			mensaje = "El campo Fecha es Obligatorio.";
		}
		
		else if(isNaN($("#numTarjeta").val())){
			vacio = true;
			mensaje = "El campo Numero de tarjetas debe ser un numero entero.";
		}
		
		else if(parseInt($("#numTarjeta").val()) <= 0 || parseInt($("#numTarjeta").val()) >= 5001){
				vacio = true;
				mensaje = "El campo Numero de tarjetas debe ser un numero entero mayor a 1 y menor a 5001.";
		}
		
		else if( $("#transmission").val().length != 8 ){
			vacio = true;
			mensaje = "El campo Transmission Id debe ser de longitud de 8 caracteres.";
		}
		else if(isNaN($("#transmission").val())){
			vacio = true;
			mensaje = "El campo Transmission Id debe ser un numero entero.";
		}
		
		if(vacio){
			mostrarMensajeAdv(mensaje);			
			error = true;
		}
		else{			
			error = false;
		}				   		
	}
	
	function mostrarMensajeAdv(mensaje){
		$.dialog({
			title: 'Verificar',
			content: mensaje,
		});	
	}
	
	function generarAce(json){
		$.ajax(
				{
					data:  json,
				    url:   restUrl + '/ace/proceso',
				    type:  'post',
				    contentType: "application/json",
				    success:  function (response)
				    {				
				    	console.log(response);
				    	if(response.error == null)
				    		$.alert({
				    	        title: "",
				    	        content: "Se ha generado el Archivo ACE.",
				    	        confirmButton: "Ok"
				    	    });
				    	else
				    		$.alert({
				    	        title: "Error",
				    	        content: response.error,
				    	        confirmButton: "Ok"
				    	    });
				    	$("#guardar").prop('disabled', false);
				    },
				always: (function() {
			    	$("#guardar").prop('disabled', false);
				  })
				});
	}