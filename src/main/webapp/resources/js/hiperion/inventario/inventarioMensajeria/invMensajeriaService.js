var restUrl = $("#urlRestBackOffice").val();
// var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
var tablaGenerada = false;

$(document).ready(function() {
	inicializarObjetos();
	getCuentasInventario();
	var tablaCuentas = '';

});

/**
 * Obtiene informacion de las mensajerias y de los productos que contiene cada
 * una,
 * 
 */
function getCuentasInventario() {
	console.log("getCuentasInventario");
	$.ajax({
		url : restUrl + '/mensajeria/numeroCuentas',
		type : 'get',
		contentType : "application/json",
		success : function(response) {
			initTableMensajeriaInventario(response);
		}
	});
}

/**
 * Obtiene las cuentas titulares y adicionales por producto para cada mensajeria
 * e inicializa las tablas de numero de cuentas para cada mensajeria y producto
 * 
 * @param ids ::
 *            contiene el ID de la mensajeria y el ID del producto en formato
 *            idMensajeria/idProducto, ejem: 51/1
 * @param isAdicionales ::
 *            indica el tipo de cuenta que se buscaran en la BD, true para
 *            adicionales y false para principales
 */

function getCuentasPorProducto(ids, isAdicionales) {
	console.log("getCuentasPorProducto");

	if (isAdicionales) {
		url = restUrl + '/mensajeria/cuentasAdicionales/' + ids;
	} else {
		url = restUrl + '/mensajeria/cuentasTitulares/' + ids;
	}

	$.ajax({
		url : url,
		type : 'get',
		contentType : "application/json",
		success : function(response) {
			initTableModalCuentas(response);
		}

	});
}

/**
 * Crea el modal para mostrar las cuentas asignadas por producto
 * 
 * @param response
 *            ::: lista de cuentas a partir de la que se construira la tabla
 */
function initTableModalCuentas(response) {

	var body = '';
	$.each(response, function(i, n) {
		body += '<tr><td aling ="center">' + n + '</td></tr>';
	});

	$('#tbodyR').html(body);

	if ($.fn.dataTable.isDataTable('#tablaCuentas')) {
		tablaCuentas.destroy();
	}

	tablaCuentas = $('#tablaCuentas').DataTable({
		"language" : {
			"lengthMenu" : "_MENU_ Cuentas",
			"zeroRecords" : "No hay datos",
			"info" : "Mostrando pagina _PAGE_ de _PAGES_",
			"infoEmpty" : "No se encontraron cuentas",
			"search" : "Buscar:",
			"scrollY" : true,
			"paginate" : {
				"first" : "Primero",
				"last" : "Ultimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			},

			"infoFiltered" : "(filtrando del _MAX_ total cuentas)"
		},
		"searching" : false,
		"paging" : false,
		"ordering" : false,
		"info" : false
	});
	$("#modalAccounts").modal('show');

	$(".tableModal").unbind().click(function() {
		tablaCuentas.destroy();
	});

}

/**
 * Construye el dataTable que contiene el inventario de mensajerias con sus
 * productos y cuentas correspondientes
 * 
 * @param response
 *            ::: mensajerias, productos y numero de cuentas
 */
function initTableMensajeriaInventario(response) {

	console.log("initTableMensajeriaInventario");

	var productos = response[0].productos;
	var h = '<thead><tr><th rowspan="2">Mensajeria</th>';
	$.each(productos, function(i, n) {
		h += '<th colspan="2">&nbsp&nbsp&nbsp' + n.nombreProducto + '</th>';
	});
	h += '</tr><tr>';
	$
			.each(
					productos,
					function(i, n) {
						h += '<th>&nbsp&nbsp&nbspTitulares&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th>&nbsp&nbsp&nbspAdicionales&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
					});
	h += '</tr></thead>';
	$('#tablaMensajeriaInventario').append(h)
	var body = '<tbody>'
	$
			.each(
					response,
					function(i, n) {
						body += '<tr><td >' + n.nombreMensajeria + '</td>';
						$
								.each(
										n.productos,
										function(i2, n2) {
											body += '<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="button" class="btn btn-primary numAccounts" id="'
													+ n.idMensajeria
													+ '/'
													+ n2.idProducto
													+ '">'
													+ n2.numCuentasTitulares
													+ '</button></td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="button" class="btn btn-primary adicionales numAccounts" id="'
													+ n.idMensajeria
													+ '/'
													+ n2.idProducto
													+ '">'
													+ n2.numCuentasAdicionales
													+ '</button></td>'
										});
						body += '</tr>';
					});
	body += '</tbody>';
	$('#tablaMensajeriaInventario').append(body);

	$(".btn.numAccounts").unbind().click(function() {
		var ids = $(this).attr('id');
		var isAdicionales = $(this).hasClass("adicionales");
		getCuentasPorProducto(ids, isAdicionales);
	});

	if ($.fn.dataTable.isDataTable('#tablaMensajeriaInventario')) {
		tablaMensajeria.destroy();
	}

	tablaMensajeria = $('#tablaMensajeriaInventario').DataTable({
		"language" : {
			"lengthMenu" : "_MENU_ Mensajer\u00EDas",
			"zeroRecords" : "No hay datos",
			"info" : "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty" : "No se encontraron mensajer\u00EDas",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "\u00DAltimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			},
			"infoFiltered" : "(filtrando del _MAX_ total mensajer\u00EDas)"
		},
		"ordering" : false,
		"scrollX" : true

	});

	$('#tablaMensajeriaInventario tbody').unbind().on(
			'click',
			'tr',
			function(event) {

				if (tablaMensajeria.rows().data().length > 0) {

					var mensajeria = tablaMensajeria.row(this._DT_RowIndex)
							.data();
					$(this).addClass('active');

				}
			});
}

function inicializarObjetos(obj) {
	$("#modalAsignar").hide();
	obtenerDatosInicio();

	$("#limpiar").click(function() {
		$("#inventarioTb tbody").empty();
		$("#modalAsignar").hide();
		actualizarTablaCtas();
	});

	$("#guardar").click(
			function() {
				guardarAsignacion($("#productos").val(), $("#mensajeriaSel")
						.val(), $("#agente").val());
			});

	$("#fileuploadTs").unbind().click(function() {
		initPluguin();
		cleanResult();
		cleanMensajeError();
	});

	$("#productoSel").change(function() {
		cleanResult();
	});

}

/**
 * Actualiza el inventario de mensajerias para cada producto y numeros de cuenta
 * que tiene asignados
 *
 * @returns
 */
function actualizarTablaCtas() {
	console.log("actualizarTablaCtas");
	var table = '<table id="tablaMensajeriaInventario" class="table table-striped" cellspacing="0" width="90%"></table>';
	$("#contenedorTablaMensajeriaInventario").html(table);
	getCuentasInventario();
}

$(function() {

	$('#fileupload').fileupload(
			{
				url : restUrl + "/mensajeria/archivo",
				dataType : 'json',
				autoUpload : true,
				acceptFileTypes : /(\.|\/)(txt)$/i,
				disableImageResize : /Android(?!.*Chrome)|Opera/
						.test(window.navigator.userAgent),
				previewMaxWidth : 100,
				previewMaxHeight : 100,
				previewCrop : true,
				formAcceptCharset : 'utf-8'
			}).on('fileuploadadd', function(e, data) {
		if (data.files[0].name.split(".")[1] != "txt") {
			console.log("archivo incorrecto");
			data.abort();
			alerta("El archivo debe ser de tipo '.txt'.");
		}

	}).on('fileuploadprocessalways', function(e, data) {
		console.log("fileuploadprocessalways");

	}).on('fileuploadprogressall', function(e, data) {
		$("#loadingModal").modal('show');

	}).on('fileuploaddone', function(e, data) {
		if (data.result.error == null) {
			console.log("carga sin errores");
			existCtasErroneas(data.result);
		} else {
			console.log("Ocurrio un error al cargar el archivo");
			alerta(data.result.error);
		}
		$("#loadingModal").modal('hide');

	}).on('fileuploadfail', function(e, data) {
		console.log("fileuploadfail");
		$("#loadingModal").modal('hide');

	}).prop('disabled', !$.support.fileInput).parent().addClass(
			$.support.fileInput ? undefined : 'disabled');
});

function initPluguin() {

	var idProducto = $('#productoSel').val();
	var adicionales = $('input[name=adicional]:checked', '#myForm').val();

	$('#fileuploadTs').fileupload(
			{
				url : restUrl + "/mensajeria/archivoTs/" + adicionales + "/"
						+ idProducto,
				dataType : 'json',
				autoUpload : true,
				acceptFileTypes : /(\.|\/)(txt)$/i,
				disableImageResize : /Android(?!.*Chrome)|Opera/
						.test(window.navigator.userAgent),
				previewMaxWidth : 100,
				previewMaxHeight : 100,
				previewCrop : true,
				formAcceptCharset : 'utf-8'
			}).on('fileuploadadd', function(e, data) {
		if (data.files[0].name.split(".")[1] != "txt") {
			console.log("archivo incorrecto");
			data.abort();
			mostrarMensajeError("El archivo debe ser de tipo '.txt'.");
		}

	}).on('fileuploadprocessalways', function(e, data) {
		console.log("fileuploadprocessalways");

	}).on('fileuploadprogressall', function(e, data) {
		$("#loadingModal").modal('show');

	}).on('fileuploaddone', function(e, data) {
	    if (data.result.error == null) {
            console.log("carga sin errores");
            console.log("DATA", data.result)
            existCtasTs(data.result);
        } else {
            console.log("Ocurrio un error al cargar el archivo");
            mostrarMensajeError(data.result.error);
        }
        $("#loadingModal").modal('hide');

	}).on('fileuploadfail', function(e, data) {
		console.log("fileuploadfail");
		$("#loadingModal").modal('hide');

	}).prop('disabled', !$.support.fileInput).parent().addClass(
			$.support.fileInput ? undefined : 'disabled');
}

/**
 * Verifica si existen cuentas erroreas, en caso de existir muestra un mensaje
 * de error mostrando la primera cuenta en la que se encontro error en el
 * formato; Si no existen cuentas con error construye la tabla y se muestran las
 * cuentas que se cargaron
 *
 * @param resultCtas
 *            ::: json que contiene las cuentas
 * @returns
 */
function existCtasErroneas(resultCtas) {
	$("#inventarioTb tbody").empty();
	var ctasError = resultCtas.cuentasFormatoInvalido;
	if (ctasError != null && ctasError.length > 0) {
		alerta('Existe un error en el formato de la cuenta: "' + ctasError[0]
				+ '"');
		$("#modalAsignar").hide();
	} else {
		mostrarCuentas(resultCtas.cuentas);
		$("#modalAsignar").show();
	}

}

function existCtasTs(resultCtas) {
	// $("#inventarioTb tbody").empty();

	$('#resultado').removeClass('hide')
	$("#total").text(resultCtas.total)
	$("#cuentasDuplicadas").text(resultCtas.cuentasDuplicadas.length)
	$("#cuentasDuplicadasSistema").text(
			resultCtas.cuentasDuplicadasSistema.length);
	$("#cuentasValidas").text(resultCtas.cuentas.length);
	$("#cuentasProductoInvalido").text(
			resultCtas.cuentasProductoInvalido.length);
	$("#cuentasInvalidas").text(resultCtas.cuentasFormatoInvalido.length);

	console.log(resultCtas);

}

function cleanResult() {
	$('#resultado').addClass('hide')
	// $("#inventarioTb tbody").empty();
	$("#cuentasDuplicadasSistema").text(0);

	$("#total").text(0)
	$("#cuentasDuplicadas").text(0)
	$("#cuentasValidas").text(0);
	$("#cuentasProductoInvalido").text(0);
	$("#cuentasInvalidas").text(0);

}

function alerta(texto) {

	$.alert({
		title : "Alerta",
		content : texto,
		confirmButton : "Ok"
	});
}

function mostrarMensajeError(mensaje){
    $('#resultadoErrorCargarArchivo').removeClass('hide');
    $('#errorArchivo').text(mensaje);
}

function cleanMensajeError(){
    $('#resultadoErrorCargarArchivo').addClass('hide');
    $('#errorArchivo').text('');
}

function mostrarCuentas(datos) {
	$("#inventarioTb").remove();
	$("div.dataTables_wrapper").remove();

	var numCol = 5;
	var fila = $("<tr/>");
	var table = $("<table>").addClass("table table-condensed table-bordered")
			.attr("id", "inventarioTb");
	var thead = $("<thead/>");
	var filath = $("<tr/>").append($("<th/>").text("Cuenta"));

	filath.append($("<th/>").text("Cuenta"));
	filath.append($("<th/>").text("Cuenta"));
	filath.append($("<th/>").text("Cuenta"));
	thead.append(filath.append($("<th/>").text("Cuenta")));
	table.append(thead);
	table.append("<tbody/>");

	$("#containerTable").append(table);

	var indice = 1;
	for (var i = 0; i < datos.length; i++) {
		if (indice == 5) {
			fila.append(createTdCuenta(datos[i]));
			$("#inventarioTb tbody").append(fila);
			fila = $("<tr/>");
			indice = 1;
		} else {
			fila.append(createTdCuenta(datos[i]));
			indice++;
		}
	}

	if (indice > 1 && indice <= 5) {
		for (var i = indice; i <= 5; i++)
			fila.append($("<td/>"));
		$("#inventarioTb tbody").append(fila);
	}

	$('#inventarioTb').DataTable({
		"paging" : true,
		"ordering" : false,
		"info" : false,
		"language" : {
			"search" : "Filtro:",
			"lengthMenu" : "Muestra  _MENU_ registros por p\u00E1gina",
			"paginate" : {
				"first" : "Primero",
				"last" : "\u00DAltimo",
				"next" : "Siguiente",
				"previous" : "Anterior"
			}
		}
	});
	tablaGenerada = true;
}

function createTdCuenta(cuenta) {
	var clase = defineClassIcon(cuenta);
	var cuentaTd = $("<td/>");
	cuentaTd.append($("<span/>").text(cuenta.numero));
	cuentaTd.append($("<span/>").addClass(clase));
	return cuentaTd;
}

function defineClassIcon(cuenta) {
	var clase = "";
	if (cuenta.existe) {
		if (cuenta.venta) {
			clase = "glyphicon glyphicon-ok";
		} else {
			if (cuenta.mensajeria) {
				clase = "glyphicon glyphicon-envelope";
			}
		}
	} else {
		clase = "glyphicon glyphicon-remove";
	}
	return clase;
}

/**
 * Obtiene y construye las los combos para asignar a una mensajeria y un
 * producto las cuentas que se cargaron desde el archivo
 */
function obtenerDatosInicio() {
	console.log("obtenerDatosInicio: ");
	$.ajax({
		url : restUrl + '/mensajeria/configuracion',
		type : 'get',
		contentType : "application/json",
		success : function(response) {

			if (response.error == null) {
				crearComboMensajeria("mensajeriaSel", response.mensajerias);
				debugger
				crearComboBox("productoSel", response.tarjetas);
				crearComboBox("productos", response.tarjetas);

				$("#agente").val(response.agente);
			} else {
				alerta("Error al obtener datos de inicio.");
			}
		}
	});
}

/**
 * Guarda en BD las mensajerias con la cuentas y agente asignados
 * 
 * @param producto
 *            ::: producto al que seran asignadas las cuentas
 * @param mensajeria
 *            ::: mensajeria a la que seran asignadas las cuentas
 * @param agente
 *            ::: id del agente
 */
function guardarAsignacion(producto, mensajeria, agente) {
	$("#loadingModal").modal('show');
	var json = {
		idProducto : producto,
		idMensajeria : mensajeria,
		agente : agente
	};

	$.ajax({
		data : JSON.stringify(json),
		url : restUrl + '/mensajeria/inventario',
		type : 'post',
		contentType : "application/json",
		success : function(response) {
			if (response.error == null) {
				alerta("Proceso completado.");
				$("#modalAsignar").hide();
				$("#inventarioTb tbody").empty();
				actualizarTablaCtas();
			} else {
				alerta("Error al obtener datos de inicio.");
			}
			$("#loadingModal").modal('hide');
		}
	});
}

function crearComboMensajeria(id, datos) {
	for (var i = 0; i < datos.length; i++) {
		$('#' + id).append(
				$('<option>').attr('value', datos[i].idMensajeria).text(
						datos[i].nombreMensajeria));
	}
}

function crearComboBox(id, datos) {
	for (var i = 0; i < datos.length; i++) {
		$('#' + id).append(
				$('<option>').attr('value', datos[i].id).text(datos[i].nombre));
		console.log(id)
	}
}