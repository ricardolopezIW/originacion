	var checklist = [];
	var formaAsignacion = [];
	var idMesaControl;
	var restUrl = $("#urlRestBackOffice").val();
	//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
	console.log("URL: " + restUrl)
	
		function cargaTablaMesaCtrl(mesaControl){
		
		if ($.fn.dataTable.isDataTable('#tablaMesaControl')) {
			tablaMesaControl.destroy();
		}
	
		tablaMesaControl = $("#tableMesaCtrl").dataTable({
			destroy: true,
	        "pagingType": "full_numbers",
	        "language": {
		        "paginate": {
		        	"first": "Primera p&aacute;gina",
		        	"previous": "Anterior",
		        	"next": "Siguiente",
		        	"last": "&Uacute;ltima p&aacute;gina"
		        },
		        "search": "Buscar:",
		        "zeroRecords": "No se encontraron registros."
	        },
			data : mesaControl,
			columns : [
					{
						data : 'id',
						visible : false
					},
					{
						title : "Nombre",
						data : 'nombre'
					},
					{
						title : "Asignar",
						className : "text-center",
						"render" : function(data, type, full, meta) {
							var input = '';
							if(full.verBtnAsignaInven == 1){
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/>';
							} else{
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/>';
							}
							return input;
						}
					},
					{
						title : "Actualizar",
						className : "text-center",
						"render" : function(data, type, full, meta) {
							var input = '';
							if(full.verBtnActTsys == 1){
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/>';
							} else{
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/>';
							}
							return input;
						}
					},
					{
						title : "Asignar/Actualizar",
						className : "text-center",
						"render" : function(data, type, full, meta) {
							var input = '';
							if(full.verBtnAsigAct == 1){
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/>';
							} else{
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/>';
							}
							return input;
						}	
							
					},
					{
						title : "Activo",
						className : "text-center",
						"render" : function(data, type, full, meta) {
							var input = '';
							if(full.activo == 1){
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_aceptar.png" width="25px"/>';
							} else{
								input = '<img id="btnActivo" class="btnCheck" src="'+requestContextPath+'/resources/img/icon_rechazar.png" width="25px"/>';
							}
							return input;
						}
					},
					{
						title : "Acciones",
						className : "text-center",
						"render" : function(data, type, full, meta) {
							
							var img = '<img id="buttonEditar" class="btnCheck" title="Editar" src="'+requestContextPath+'/resources/img/icon_edit.png" align="middle" onclick="buscarMesaControl(\'' 
								+ full.id + '\');"/>';
							
							if(full.activo == 1){
								img = img + '<img id="buttonBorrar" class="btnCheck" title="Desactivar" src="'+requestContextPath+'/resources/img/icon_remove.png" align="middle" onclick="eliminaMesaControl(\''
												+ full.id + '\');"/>';
							}
							return img;
						}
					} ],
	        "iDisplayLength": 5,
	        "bLengthChange": false,
	        "info": false
	    });
	}
	
	function cargaTablaLugarVenta(lugarVenta){
		$("#tableLugarVenta").dataTable({
			destroy: true,
	        "pagingType": "full_numbers",
	        "language": {
		        "paginate": {
		        	"first": "Primera p&aacute;gina",
		        	"previous": "Anterior",
		        	"next": "Siguiente",
		        	"last": "&Uacute;ltima p&aacute;gina"
		        },
		        "search": "Buscar:",
		        "zeroRecords": "No se encontraron registros."
	        },
			data : lugarVenta,
			columns : [
					{
						title : "Nombre",
						data : 'nombre'
					}],
	        "iDisplayLength": 10,
	        "bLengthChange": false,
	        "info": false
	    });
	}
	
	$(document).ready(function(){
		inicia();		
	});

	function inicia(){
		
		$('#divDetalleMesaCtrl').hide();
		$('#divBotones').hide();
		$("#no-more-tables").show();
//		console.log("URL: " + restUrl);
		
		$.ajax(
				{
					data:  null,
					url:   restUrl + '/mesa-control/inicio',
				    type:  'get',
				    contentType: "application/json",
				    success:  function (response){
					    checklist = response.listaChecklist;
					    formaAsignacion = response.listaFormaAsignacion;
					    
					    for(var i=0; i<checklist.length; i++){
					    	$('#listaChecklist').append($('<option>').attr('value', checklist[i].id).text(checklist[i].nombre));
					    }
					    
					    for(var i=0; i<formaAsignacion.length; i++){
					    	$('#listaFormaAsignacion').append($('<option>').attr('value', formaAsignacion[i].id).text(formaAsignacion[i].nombre));
					    }
					}
				});

		//Obtiene lista mesas de control
    	obtieneMesasControl();
    	
    	$('#btnGuardar').click(function() {
    		
    		if(validaCamposGuardar()){
        		var mesaControl = {id: idMesaControl,
        				nombre: $('#nombreMesaCtrl').val(),
    					idChecklist: $('#listaChecklist').val(),
    					idFormaAsignacion: $('#listaFormaAsignacion').val(),
    					editarInfo: $('#checkEditSol').val() == "on" ? 1 : 0,
    					verBtnAsignaInven: $('#checkAsigCuenta').val() == "on" ? 1 : 0,
    					verBtnActTsys: $('#checkActCuentaTsys').val() == "on" ? 1 : 0,
    					verBtnAsigAct: $('#checkAsignaAct').val() == "on" ? 1 : 0,
    					verNotas: $('#checkVerNotas').val() == "on" ? 1 : 0,
    					verBuro: $('#checkVerBuro').val() == "on" ? 1 : 0,
    					verRiesgo: $('#checkVerRiesgo').val() == "on" ? 1 : 0,
    					verLimiteCredito: $('#checkLimCredito').val() == "on" ? 1 : 0,
    					enviaMail: $('#checkEnviaMail').val() == "on" ? 1 : 0,
    					enviaSms: $('#checkEnviaAlerta').val() == "on" ? 1 : 0,
    					activo: $('#checkActivo').val() == "on" ? 1 : 0,
    					verBtnRechazar: $('#checkRechazar').val() == "on" ? 1 : 0,
    					verBtnRevision : $('#checkRevision').val() == "on" ? 1 : 0,
    					verActivacion : $('#checkActivacion').val() == "on" ? 1 : 0
    			}
        		var request = {mesaControl : mesaControl}
        		
    			$.ajax(
    					{
    						data:  generarObjetoJson(request),
    						url:   restUrl + '/mesa-control/',
    						type:  'post',
    						contentType: "application/json",
    						success:  function (response){
    							if(response.error == null){
    								alertaGeneral("Confirmaci&oacute;n", "La mesa de control se guardo correctamente");    								
    								obtieneMesasControl();
    								$('#divDetalleMesaCtrl').hide();
    					    		$('#divBotones').hide();
    					    		$("#no-more-tables").show();
    							
    							}else {
    								alertaGeneral("Error", response.error);
    							}
    						}
    					});
    			}

		});
    	
    	$('#btnLugarVenta').click(function() {
    		
			$.ajax(
					{
						url:   restUrl + '/mesa-control/' + idMesaControl + '/lugar-venta',
						type:  'get',
						contentType: "application/json",
						success:  function (response){
							cargaTablaLugarVenta(response.listaLugaresVenta);
						}
					});
			
			$('#modalLugarVenta').modal('show');
			
		});
    	
    	$('#btnCancelar').click(function() {
    		$('#divDetalleMesaCtrl').hide();
    		$('#divBotones').hide();
    		$("#no-more-tables").show();
		});

    	$('#btnNuevo').click(function() {
    		$("#no-more-tables").hide();
    		$('#divDetalleMesaCtrl').show();
    		$('#divBotones').show();
    		$("#lblActivo").hide();
    		$("#divCheckActivo").hide();
    		$("#btnLugarVenta").hide();
    		limpiarCampos();
    		idMesaControl = null;
		});
    	
	}
	
	function generarObjetoJson(object){
		return JSON.stringify(object);
	}
	

	function eliminaMesaControl(campo) {
	$.confirm({
				title : "Atenci&oacute;n",
				content : "&iquest;Desea desactivar la mesa de control?",
				buttons : {
					confirm : {
						text : 'SI',
						action : function() {

							$.ajax({
										url : restUrl + '/mesa-control/'
												+ campo,
										type : 'delete',
										contentType : "application/json",
										success : function(response) {

											if (response.error == null) {
												alertaGeneral(
														"Confirmaci&oacute;n",
														"La mesa de control se desactivo correctamente");
												obtieneMesasControl();
											} else {
												alertaGeneral("Error",
														response.error);
											}

										}

									});

						}
					},
					cancel : {
						text : 'CANCELAR'
					}

				}

			});
}
	
	
	function obtieneMesasControl(){
		
		$.ajax({
				url:   restUrl + '/mesa-control/',
				type:  'get',
				contentType: "application/json",
				success:  function (response){
					cargaTablaMesaCtrl(response.listaMesaControl);
				}
		});	
		
	}
	
	function buscarMesaControl(campo) {
		idMesaControl = campo
		$.ajax({
				url : restUrl + '/mesa-control/' + idMesaControl,
				type : 'get',
				contentType : "application/json",
				beforeSend : function() {
					$("#loadingModal").modal('show');
				},
				success : function(response) {
					var mesaControl = response.mesaControl;
					setearValores(mesaControl);
					$("#no-more-tables").hide();
					$('#divDetalleMesaCtrl').show();
		    		$("#divBotones").show();
		    		$("#lblActivo").show();
		    		$("#divCheckActivo").show();
		    		$("#btnLugarVenta").show();
		    		
				},
				complete : function(xhr, status){
					$("#loadingModal").modal('hide');
				}

			});
	}
	
	function setearValores(mesaControl){
		
		$('#checkEditSol').bootstrapToggle(mesaControl.editarInfo == 0 ? 'off' : 'on' );
		$('#checkAsigCuenta').bootstrapToggle(mesaControl.verBtnAsignaInven == 0 ? 'off' : 'on' );
		$('#checkActCuentaTsys').bootstrapToggle(mesaControl.verBtnActTsys == 0 ? 'off' : 'on' );
		$('#checkAsignaAct').bootstrapToggle(mesaControl.verBtnAsigAct == 0 ? 'off' : 'on' );
		$('#checkRechazar').bootstrapToggle(mesaControl.verBtnRechazar == 0 ? 'off' : 'on' );
		$('#checkRevision').bootstrapToggle(mesaControl.verBtnRevision == 0 ? 'off' : 'on' );
		$('#checkVerNotas').bootstrapToggle(mesaControl.verNotas == 0 ? 'off' : 'on' );
		$('#checkVerBuro').bootstrapToggle(mesaControl.verBuro == 0 ? 'off' : 'on' );
		$('#checkVerRiesgo').bootstrapToggle(mesaControl.verRiesgo == 0 ? 'off' : 'on' );
		$('#checkLimCredito').bootstrapToggle(mesaControl.verLimiteCredito == 0 ? 'off' : 'on' );
		$('#checkEnviaMail').bootstrapToggle(mesaControl.enviaMail == 0 ? 'off' : 'on' );
		$('#checkEnviaAlerta').bootstrapToggle(mesaControl.enviaSms == 0 ? 'off' : 'on' );
		$('#checkActivo').bootstrapToggle(mesaControl.activo == 0 ? 'off' : 'on' );
		$('#checkActivacion').bootstrapToggle(mesaControl.verActivacion == 0 ? 'off' : 'on');
		
		$('#nombreMesaCtrl').val(mesaControl.nombre);
		$('#listaChecklist').val(mesaControl.idChecklist);
		$('#listaFormaAsignacion').val(mesaControl.idFormaAsignacion);
		$('#checkEditSol').val(mesaControl.editarInfo == 0 ? 'off' : 'on');
		$('#checkAsigCuenta').val(mesaControl.verBtnAsignaInven == 0 ? 'off' : 'on');
		$('#checkActCuentaTsys').val(mesaControl.verBtnActTsys == 0 ? 'off' : 'on');
		$('#checkAsignaAct').val(mesaControl.verBtnAsigAct == 0 ? 'off' : 'on');
		$('#checkVerNotas').val(mesaControl.verNotas == 0 ? 'off' : 'on');
		$('#checkVerBuro').val(mesaControl.verBuro == 0 ? 'off' : 'on');
		$('#checkVerRiesgo').val(mesaControl.verRiesgo == 0 ? 'off' : 'on');
		$('#checkLimCredito').val(mesaControl.verLimiteCredito == 0 ? 'off' : 'on');
		$('#checkEnviaMail').val(mesaControl.enviaMail == 0 ? 'off' : 'on');
		$('#checkEnviaAlerta').val(mesaControl.enviaSms == 0 ? 'off' : 'on');
		$('#checkActivo').val(mesaControl.activo == 0 ? 'off' : 'on');
		$('#checkRechazar').val(mesaControl.verBtnRechazar == 0 ? 'off' : 'on');
		$('#checkRevision').val(mesaControl.verBtnRevision == 0 ? 'off' : 'on');
		$('#checkActivacion').val(mesaControl.verActivacion == 0 ? 'off' : 'on');
	
	}
	
	function swSwitch(componente){
		var valor = componente.val();
		
		if(valor == "on"){
			componente.val("off");
		} else {
			componente.val("on");
		}
	}
	
	function validaCamposGuardar(){
		console.log("Validando campos");
		
		var camposValidos = true;
		var nombreMesaCtrl = $("#nombreMesaCtrl").val();
		var checklist = $('#listaChecklist').val();
		var formaAsignacion = $('#listaFormaAsignacion').val();
		if(nombreMesaCtrl == ""){
			alertaGeneral("Error", "Debe ingresar el nombre de la mesa de control");
			camposValidos = false;
		} else if(existeNombreMesa(nombreMesaCtrl) && idMesaControl == null){
			alertaGeneral("Error", "El nombre de la mesa de control ya existe");
			camposValidos = false;
		} else if(checklist == 0){
			alertaGeneral("Error", "Debe seleccionar el Checklist");
			camposValidos = false;
			
		} else if(formaAsignacion == 0){
			alertaGeneral("Error", "Debe seleccionar la Forma de Asignaci&oacute;n");
			camposValidos = false;
		} 
		
		return camposValidos;
	}

	
	/**
	 * Comprueba si el nombre que se sera asignado a la nueva mesa de control ya existe en la BD
	 * @param nombreMesaCtrl ::: Nombre de la mesa de control que sera asignado
	 * @returns ::: true si el nombre ya existe, false si no existe en la BD
	 */
	function existeNombreMesa(nombreMesaCtrl){
		var existMesa = false;
		
		$.ajax({
			url:   restUrl + '/mesa-control/',
			type:  'get',
			contentType: "application/json",
			async : false,
			beforeSend : function(){
				$("#loadingModal").modal('show');
			},
			success:  function (response){
				var mesasControl = response.listaMesaControl;
				for(var i in mesasControl){
					if(mesasControl[i].nombre == nombreMesaCtrl){
						existMesa = true;
					}
				}
				
			},
			complete : function(xhr, status){
				$("#loadingModal").modal('hide');
			}
		});

		return existMesa;
	}
	

	function limpiarCampos(){
		$('#nombreMesaCtrl').val("");
		$('#listaChecklist').val(0);
		$('#listaFormaAsignacion').val(0);
		
		resetearCheck($('#checkEditSol'));
		resetearCheck($('#checkAsigCuenta'));
		resetearCheck($('#checkActCuentaTsys'));
		resetearCheck($('#checkAsignaAct'));
		resetearCheck($('#checkVerNotas'));
		resetearCheck($('#checkVerBuro'));
		resetearCheck($('#checkVerRiesgo'));
		resetearCheck($('#checkLimCredito'));
		resetearCheck($('#checkEnviaMail'));
		resetearCheck($('#checkEnviaAlerta'));
		resetearCheck($('#checkActivo'));
		resetearCheck($('#checkRechazar'));
		resetearCheck($('#checkRevision'));
		resetearCheck($('#checkActivacion'));

	}
	
	function resetearCheck(campo){
		campo.bootstrapToggle('off');
		campo.val('off');
	}