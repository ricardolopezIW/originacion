var urlRest                = $('#urlRestBackOffice').val();//"http://localhost:8081/siditOriginacion/backOffice/"
var urlObtenerReglas       = urlRest + '/cofetel/reglas';
var urlGuardarReglas       = urlRest + '/cofetel/reglas';

function inicializar()
{
	$.ajax(
	{  
	    url:   urlObtenerReglas,
	    type:  'get',
	    contentType: "application/json",
	    success:  function (response) 
	    {
	    	//Acci�n al terminar la ejecuci�n
	    	if(response.codigoError == '0')
	    	{
	    		
	        	for (var i in response.reglas) 
	            {
		        	let tipoTel 			= response.reglas[i].tipoTel;
		        	let validacionTel 		= response.reglas[i].validacionTel;
		        	let detenerCapturas 	= response.reglas[i].detenerCapturas;
		        	let muestraAlerta 		= response.reglas[i].muestraAlerta;
		        	let validacionEstado 	= response.reglas[i].validacionEstado;
		        	let validacionMunicipio = response.reglas[i].validacionMunicipio;
		        	
		        	$("#dataTableReglas tbody tr").each(function (index)
		        	{
		        		let posicion = $(this).attr("id");
		        		
		        		$(this).children("td").each(function (index2)
		        		{
		        			switch (index2) 
		                    {
		                        case 0: 
		                        	if(posicion == tipoTel)
		                        	{
		                        		$(this).parent().find("#inputEstatusValidaTelefono").bootstrapToggle(validacionTel			== 0 ? 'off' : 'on' );
		                        		$(this).parent().find("#inputEstatusDetieneCaptura").bootstrapToggle(detenerCapturas		== 0 ? 'off' : 'on' );
		                        		$(this).parent().find("#inputEstatusMuestraAlerta").bootstrapToggle(muestraAlerta			== 0 ? 'off' : 'on' );
		                        		$(this).parent().find("#inputEstatusValidaEstado").bootstrapToggle(validacionEstado			== 0 ? 'off' : 'on' );
		                        		$(this).parent().find("#inputEstatusValidaMunicipio").bootstrapToggle(validacionMunicipio	== 0 ? 'off' : 'on' );
		                        		
		                        		$(this).parent().find("#inputEstatusValidaTelefono").attr("valor",validacionTel);
		                        		$(this).parent().find("#inputEstatusDetieneCaptura").attr("valor",detenerCapturas);
		                        		$(this).parent().find("#inputEstatusMuestraAlerta").attr("valor",muestraAlerta);
		                        		$(this).parent().find("#inputEstatusValidaEstado").attr("valor",validacionEstado);
		                        		$(this).parent().find("#inputEstatusValidaMunicipio").attr("valor",validacionMunicipio);
		                        	}
		                        break;
		                    }
		        		});
		        	});
		        }
	        }
	    }
	});
}
inicializar();

function swSwitch(componente){
	let estatus = componente.attr("valor");
	
	if(estatus == "0"){
		componente.attr("valor","1");
	} else {
		componente.attr("valor","0");
	}
}

function guardarValidaCofetel(componente, tipo){
	let validacionTel 		= componente.parent().parent().find("#inputEstatusValidaTelefono").attr("valor");
	let detenerCapturas 	= componente.parent().parent().find("#inputEstatusDetieneCaptura").attr("valor");
	let muestraAlerta 		= componente.parent().parent().find("#inputEstatusMuestraAlerta").attr("valor");
	let validacionEstado 	= componente.parent().parent().find("#inputEstatusValidaEstado").attr("valor");
	let validacionMunicipio = componente.parent().parent().find("#inputEstatusValidaMunicipio").attr("valor");

	let json = {
			tipoTel            : tipo ,
			validacionTel      : validacionTel ,
			detenerCapturas    : detenerCapturas ,
			muestraAlerta      : muestraAlerta ,
			validacionEstado   : validacionEstado ,
			validacionMunicipio: validacionMunicipio 
	};
	json = JSON.stringify(json);
	
	let respuesta = [];
	
	$.ajax(
			{
				data:  json,
			    url:   urlGuardarReglas,
			    type:  'post',
			    contentType: "application/json",
			    success:  function (response)
			    {
			    	
			    	//Acci�n al terminar la ejecuci�n
			    	if(response.codigoError == '0')
			    	{
			    		respuesta=
			    		{
			    				"codigoError":response.codigoerror,
			    				"mensajeError":response.mensajeError
			    		};
			    		inicializar();
			    		$.alert({
			    	        title: "Mensaje",
			    	        content: "Se han guardado los datos.",
			    	        confirmButton: "Ok"
			    	    });
			    	}
			    	else
			    	{
			    		respuesta=
			    		{
			    				"codigoError":response.codigoerror,
			    				"mensajeError":response.mensajeError
			    		};
			    		$.alert({
			    	        title: "Mensaje",
			    	        content: "No se han guardado los datos, comunicarse con el administrador.",
			    	        confirmButton: "Ok"
			    	    });
			    	}
			    }
			});	
}