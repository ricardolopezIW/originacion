	var restUrl            = $('#urlRestBackOffice').val();
	                             
	//var restUrl = "http://localhost:8081/siditOriginacion/backOffice";
	var restObtenerProductos  = restUrl + "/producto/lista";
	var restObtenerProducto   = restUrl + "/producto/";
	var restActualizarProducto= restUrl + "/producto/";
	var resCargarApmProductos = restUrl + "/producto/carga-apm";

	$(document).ready(function(){
		inicializarObjetos(null);		
	});

	function inicializarObjetos(mostrarCarga){		
		$.ajax({
				data:  null,
				url:   restObtenerProductos,
				type:  'get',
				contentType: "application/json",
				success:  function (response)
				{				    	
				   	crearComboBox(response);
				   	if(mostrarCarga != null){
				   		mostrarCarga();
				   	}
				},
			    error: function (xhr, ajaxOptions, thrownError) {
			    	$.dialog({
						title: 'Error',
						content: 'Comunicar con el administrador.',
					});	
			    }
			});
		
		
		$.ajax({
			data:  null,
			url:   restUrl + "/producto/emisor",
			type:  'get',
			contentType: "application/json",
			success:  function (response)
			{				    	
			   	crearComboBoxEmisor(response);
			   	if(mostrarCarga != null){
			   		mostrarCarga();
			   	}
			},
		    error: function (xhr, ajaxOptions, thrownError) {
		    	$.dialog({
					title: 'Error',
					content: 'Comunicar con el administrador.',
				});	
		    }
		});
		
   }
	
   function crearComboBox(productos){		  
	      $("#productos").find('option').remove();
	   	  for(var i=0; i<productos.length; i++){
			  $("#productos").append($('<option>').attr('value', productos[i].id).text(productos[i].nombre));				
		  }
   }
   
   function crearComboBoxEmisor(emisor){		  
	      $("#emisor").find('option').remove();
		  for(var i=0; i<emisor.length; i++){
			  
			  $("#emisor").append($('<option>').attr('value', emisor[i].idEmisor).text(emisor[i].descripcion));				
		  }
  }
   
   function cargarSeleccion(){
	   if( $("#productos").val() == ""){
			$.dialog({
				title: 'Advertencia',
				content: 'No se ha seleccionado un Producto.',
			});							
	   }
	   else{
		   $.ajax({
				data:  null,
				url:   restObtenerProducto + $("#productos").val(),
				type:  'get',
				contentType: "application/json",
				success:  function (response)
				{				    	
					mostrarInfoProducto(response);
				},
			    error: function (xhr, ajaxOptions, thrownError) {
			    	$.dialog({
						title: 'Error',
						content: 'Comunicar con el administrador.',
					});	
			    }
			});
	   }
   }
   
   function mostrarInfoProducto(producto){
	   limpiarFormulario();
	   $("#nombre").val(producto.nombre);
	   $("#idProducto").val(producto.id);
	  
	   if (producto.idEmisor != 0)
	   	   $("#emisor").val(producto.idEmisor);
	   
	   if (typeof producto.tpc != 'undefined')
		   $("#tpc").val(producto.tpc);
	   
	   if (typeof producto.cpc != 'undefined')
	   $("#cpc").val(producto.cpc);
	   
	   if (typeof producto.nombreComercial != 'undefined')
		   $("#nombreComercial").val(producto.nombreComercial);
	   
	   if (typeof producto.leyendaSuperior != 'undefined')
		   $("#leyCaratula").val(producto.leyendaSuperior.trim() + ". " + producto.leyendaInferior.trim());
	   
	   if (typeof producto.tasaAnual != 'undefined')
		   $("#tasaAnual").val(producto.tasaAnual).defaultValue= 0.00;
   }
   
   function limpiarFormulario(){
	   $("#nombre").val("");
	   $("#idProducto").val("");
	   $("#emisor").val("");
	   $("#tpc").val("");
	   $("#cpc").val("");
	   $("#nombreComercial").val("");
	   $("#leyCaratula").val("");
	   $("#tasaAnual").val("");
   }
   
   function validarGuardar(){
	   if($('#formaProducto').parsley().validate()){
		   if($("#leyCaratula").val().includes(".")){
			   var producto = {
					      tpc             : $("#tpc").val(),
					      cpc             : $("#cpc").val(),
					      nombreComercial : $("#nombreComercial").val(),
					      id              : $("#idProducto").val(),
					      idEmisor		  : $("#emisor").val(),
					      leyendaSuperior : $("#leyCaratula").val().split(".")[0],
					      leyendaInferior : $("#leyCaratula").val().split(".")[1],
					      tasaAnual		  : $("#tasaAnual").val()
			   }
			   var id = producto.id;
			   console.log(producto.tasaAnual);
			   
			   if ($("#emisor").val() == "") {
				   $.dialog({
						title: 'Advertencia',
						content: 'No se ha seleccionado un Emisor.',
					});
			}
			   
			   else {
				   $.ajax({
						data:  JSON.stringify(producto),
						url :   restActualizarProducto + id,
						type:  'put',
						contentType: "application/json",
						success:  function (response)
						{				    	
							limpiarFormulario();
							$.dialog({
									title: 'Operaci\u00F3n Completa',
									content: 'Producto Actualizado.',
							});	
						},
					    error: function (xhr, ajaxOptions, thrownError) {
					    	$.dialog({
								title: 'Error',
								content: 'Comunicar con el administrador.',
							});	
					    }
					});
			   }
		   }
		   else{
			   $.dialog({
					title: 'Error',
					content: 'Favor de agregar el s\u00EDmbolo "." a la leyenda de Car\u00E1tula.',
				});	 
		   }
	   }
   }
   
   function cargarApmProductos(){		
		$.ajax({
				data:  null,
				url:   resCargarApmProductos,
				type:  'post',
				contentType: "application/json",
				success:  function (response)
				{				    	
					inicializarObjetos(mostrarCarga);			   	
				},
			    error: function (xhr, ajaxOptions, thrownError) {
			    	$.dialog({
						title: 'Error',
						content: 'Comunicar con el administrador.',
					});	
			    }
			});
  }
   
   var mostrarCarga = function () {
	   $.dialog({
			title: 'Operaci\u00F3n Completa',
			content: 'Productos Cargados.',
		});	
   };