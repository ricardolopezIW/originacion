package mx.com.invex.originacion.api.seguridad;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.security.core.userdetails.User;

public class UserDetailsContextMapperImpl implements UserDetailsContextMapper {

    private static Logger logger = Logger.getLogger(UserDetailsContextMapperImpl.class);

	public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
		String sAMAccountName = ctx.getStringAttribute("sAMAccountName");
		
		logger.info("Usuario:"+sAMAccountName);
		User us = new User(sAMAccountName,"", true, true, true, true, authorities);
			
		Authentication auth = new UsernamePasswordAuthenticationToken(us, null, us.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		return us;
	}

	public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
		// TODO Auto-generated method stub
		
	}
}