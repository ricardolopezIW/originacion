package mx.com.invex.originacion.api.repository;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import mx.com.invex.originacion.api.model.pojo.DatosSidit;
import mx.com.invex.originacion.api.model.pojo.Menu;

@Repository
public interface DatosSiditRepository extends Serializable{
	
	public DatosSidit consultaIdUsuario(@Param("usuario") String usuario);
	
	public List<Menu> consultaMenuPerfiles(@Param("idPerfil") Integer idPerfil);

}



