package mx.com.invex.originacion.api.model.dto;

import java.util.List;

import mx.com.invex.originacion.api.model.pojo.Menu;

public class DatosSiditDTO {
	
	private Integer idUsuario;
	private Integer idPerfil;
	private List<Menu> listaMenu;
	private String error;
	
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public List<Menu> getListaMenu() {
		return listaMenu;
	}
	public void setListaMenu(List<Menu> listaMenu) {
		this.listaMenu = listaMenu;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

}
