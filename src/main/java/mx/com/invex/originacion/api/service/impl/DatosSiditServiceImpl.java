package mx.com.invex.originacion.api.service.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.invex.originacion.api.model.dto.DatosSiditDTO;
import mx.com.invex.originacion.api.model.pojo.DatosSidit;
import mx.com.invex.originacion.api.model.pojo.Menu;
import mx.com.invex.originacion.api.repository.DatosSiditRepository;
import mx.com.invex.originacion.api.service.DatosSiditService;

@Service
public class DatosSiditServiceImpl implements DatosSiditService, Serializable{

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(DatosSiditServiceImpl.class);
	
	@Autowired
	private DatosSiditRepository datosSiditRepository;
	
	@Override
	public DatosSiditDTO obtenerDatosUsuario(String usuario){
		DatosSiditDTO datosSiditDTO = new DatosSiditDTO();
	
		try {
			// Obtiene datos del usuario
			DatosSidit datosSidit = datosSiditRepository.consultaIdUsuario(usuario);
			datosSiditDTO.setIdUsuario(datosSidit.getIdUsuario());
			datosSiditDTO.setIdPerfil(datosSidit.getIdPerfil());
			
			// Obtiene el menu
			List<Menu> listaMenu = datosSiditRepository.consultaMenuPerfiles(datosSidit.getIdPerfil());
			datosSiditDTO.setListaMenu(listaMenu);
			
		} catch (Exception e) {
			logger.error("Error al obtener el id del usuario: " + e.getMessage());
		}
		
		return datosSiditDTO;
	}

}
