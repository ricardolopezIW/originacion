package mx.com.invex.originacion.api.model.dto;


public class ReponseDTO {

		private String estado;
		private String error;

		
		public String getEstado() {
			return estado;
		}

		public void setEstado(String estado) {
			this.estado = estado;
		}

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}

}
