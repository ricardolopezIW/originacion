package mx.com.invex.originacion.api.service;

import mx.com.invex.originacion.api.model.dto.DatosSiditDTO;

public interface DatosSiditService {
	
	DatosSiditDTO obtenerDatosUsuario(String usuario);

}
