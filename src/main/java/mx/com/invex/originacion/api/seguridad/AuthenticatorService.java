package mx.com.invex.originacion.api.seguridad;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.authentication.AbstractLdapAuthenticationProvider;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

public class AuthenticatorService extends AbstractLdapAuthenticationProvider {
	
    private LdapAuthenticator        authenticator;
    private LdapAuthoritiesPopulator authoritiesPopulator;    
    private String                   password;
  
    private LdapAuthenticator getAuthenticator() {
        return authenticator;
    }
    
    public AuthenticatorService(LdapAuthenticator authenticator, LdapAuthoritiesPopulator authoritiesPopulator) {
    	this.authenticator = authenticator;
    	this.authoritiesPopulator = authoritiesPopulator;
    }
    
	@Override
	protected DirContextOperations doAuthentication(UsernamePasswordAuthenticationToken auth) {
		DirContextOperations dco = null;
		this.password            = auth.getCredentials().toString();
		dco = getAuthenticator().authenticate(auth);	
        return dco;	
	}

	@Override
	protected Collection<? extends GrantedAuthority> loadUserAuthorities(DirContextOperations arg0, String arg1,String arg2) {
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
	    result.add(new SimpleGrantedAuthority("ROLE_USERSSSSS"));
	    result.add(new SimpleGrantedAuthority("pass_"+this.password));
	    return result;
	}
	
}