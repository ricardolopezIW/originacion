package mx.com.invex.originacion.api.model.pojo;

import java.util.List;

public class DatosSidit {
	
	private String usuario;
	private Integer idUsuario;
	private Integer idPerfil;
	private List<Menu> listaMenu;
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public List<Menu> getListaMenu() {
		return listaMenu;
	}
	public void setListaMenu(List<Menu> listaMenu) {
		this.listaMenu = listaMenu;
	}

}
