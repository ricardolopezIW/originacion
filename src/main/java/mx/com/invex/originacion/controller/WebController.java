package mx.com.invex.originacion.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.invex.originacion.api.model.dto.DatosSiditDTO;
import mx.com.invex.originacion.api.service.DatosSiditService;

@Controller
public class WebController {

    private static Logger logger = Logger.getLogger(WebController.class);

	@Value("${back.office.urlRest}")
	private String urlRest;
	
	@Value("${back.office.version}")
	private String version;
	
	@Autowired
	private DatosSiditService datosSiditService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
    public String start(ModelMap modelObj) {
		logger.info("version"+version);
		modelObj.addAttribute("versionLogin", version);
		return "login";
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(ModelMap modelObj, HttpSession session) {
		return "login";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	    public String loginWithError(Model model) {
			model.addAttribute("versionLogin", version);
			model.addAttribute("error", true);
	        return "login";
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response, Model model) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){
	    	request.getSession().invalidate();
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }   
	    model.addAttribute("versionLogin", version);
	    model.addAttribute("logout", true);
	    return "redirect:/?logout=true";
	}
	
	@RequestMapping(value="/backOffice/home", method = RequestMethod.GET)
    public String home(ModelMap modelObj, HttpSession session) throws JsonProcessingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName   = authentication.getName();
		logger.info("Usuario : "+currentPrincipalName);
		
		//Obtiene datos iniciales
		DatosSiditDTO datosSiditDTO = datosSiditService.obtenerDatosUsuario(currentPrincipalName);

		ObjectMapper mapper = new ObjectMapper();
		String empleados_json = mapper.writeValueAsString(datosSiditDTO.getListaMenu());
		
		modelObj.addAttribute("urlRest", this.urlRest);
		session.setAttribute("idUsuario", datosSiditDTO.getIdUsuario());
		session.setAttribute("usuario", currentPrincipalName);
		session.setAttribute("idPerfil", datosSiditDTO.getIdPerfil());
		session.setAttribute("menu", empleados_json);

		return "home";
    }
	
    @RequestMapping(value="/backOffice", method = RequestMethod.GET)
	public String router(@RequestParam("seccion") String seccion, ModelMap modelObj) {    
	    modelObj.addAttribute("urlRest", this.urlRest);
	    return seccion;
	}
    
}