Proyecto: Originacion (BackOffice)

Descripcion:
Este proyecto tiene  el código front-end del sistema originacion backOffice.

Tipo Proyecto: Web
Path: Originacion/

Comentarios:

1) Este proyecto solo administra la parte visual de la aplicación.
2) Las tecnologías usadas son spring mvc 4, jquery, boostrap.
3) La comunicación con el back-end es a traves de servicios Rest.
4) El único back que existe está relacionado con la obtención de información
del usuario para configurar el sitio.
5) No se debe agregar back-end en este proyecto.

 